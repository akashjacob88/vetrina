//
//  chat_New.h
//  Vetrina
//
//  Created by Amit Garg on 2/19/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import <QMChatViewController/QMChatViewController.h>
#import "Singleton.h"
@interface chat_New : QMChatViewController<QMChatCollectionViewDataSource,QBChatDelegate>
{
    Singleton *singlogin;
}


@property(nonatomic)NSMutableArray *All_messages;


- (id)initWith:(NSString *)groupId_;

@end
