//
//  UploadPic.m
//  Vetrina
//
//  Created by Amit Garg on 9/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "UploadPic.h"
#import "JSON.h"
#import "User_Login.h"
#import "All_SignIn.h"
#import "ViewController.h"
#import "WalkthroughAR.h"
#import "ABCIntroView.h"
#import "MBProgressHUD.h"
#import "ProgressHUD.h"
#import "Vendor_Product.h"

@interface UploadPic ()


@end

@implementation UploadPic


- (void)viewDidLoad
{
    [super viewDidLoad];
    signLogin = [Singleton instance];
    
    startShopingBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    startShopingBtn.layer.borderWidth=2.0f;
    startShopingBtn.clipsToBounds=YES;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)startShoping:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:0.2f];
}


-(void)home
{
//        [self searchItems];
//        [self searchShops];
//        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
//        {
//    
//            [self autoLogingEmail];
//    
//        }
//        else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
//        {
//            //[self autoLoginInstagram];
//        }
//        else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
//        {
//            [self LoginIG];
//        }
//        else
//        {
            ViewController *home = [[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
            [self.navigationController pushViewController:home animated:NO];
            [ProgressHUD showSuccess:@""];
       // }
    
}



-(void)autoLogingEmail
{
    
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"email"];
    NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",savedUsername,savedPassword];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique User ID %@",uniqueid);
    NSString *uName=[eventarray  valueForKey:@"username"];
    NSString *uaddress=[eventarray  valueForKey:@"address"];
    signLogin.rate_status =[eventarray valueForKey:@"rate_status"];
    signLogin.check = [eventarray valueForKey:@"check"];
    signLogin.usrObjctId = [eventarray valueForKey:@"userobjectid"];
    NSString *venPic=[eventarray valueForKey:@"photo"];
    signLogin.userproilePic = venPic;
    signLogin.homAddress=uaddress;
    signLogin.usrName=uName;
    signLogin.userid=uniqueid;
    
    [self prodListing];
    
    if([neww isEqualToString:@"False"])
    {
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
    }
    else
    {
        signLogin.loginStatus = @"User Login";
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
    }
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",signLogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if (cartListArray.count==0)
    {
        
    }
    else
    {
        NSInteger value=0;
        for(NSInteger i =0;i<cartListArray.count;i++)
        {
            NSString *status=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
            if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] )
            {
                value=value+1;
            }
        }
        signLogin.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    }
}



-(void)LoginIG

{
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"VinstagramID"];
    
    NSString *post = [NSString stringWithFormat:@"instagramid=%@&logintype=%@",savedUsername,@"instagram"];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlogin2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    // NSArray *val = [eventarray valueForKey:@"0"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[eventarray  valueForKey:@"phoneno"];
    NSString *uName=[eventarray  valueForKey:@"username_instagramacnt"];
    NSString *shopName=[eventarray  valueForKey:@"fullname_shopname"];
    NSString *image=[eventarray  valueForKey:@"imagee"];
    signLogin.mainCat=[eventarray  valueForKey:@"main_catname"];
    signLogin.mainSubCat=[eventarray  valueForKey:@"main_subcatname"];
    signLogin.shopDec=[eventarray  valueForKey:@"descr"];
    signLogin.subId=[eventarray  valueForKey:@"subid"];
    signLogin.pid = [eventarray  valueForKey:@"staffpick"];
    signLogin.catid = [eventarray  valueForKey:@"main_catid"];
    signLogin.subCatid = [eventarray valueForKey:@"main_subcatid"];
    signLogin.objectIdStr = [eventarray valueForKey:@"objectid"];
    signLogin.vIGUserId =[eventarray valueForKey:@"instagramid"];
 
    signLogin.venPic = image;
    signLogin.vendorName=uName;
    signLogin.vUniqueId=uniqueid;
    signLogin.userid=uniqueid;
    signLogin.vPhone = phoneno;
    signLogin.shopNameVendr = shopName;
    //signLogin.loginStatus = @"IG Login";
    if([neww isEqualToString:@"False"])
    {
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showError:@""];
    }
    else
    {
            signLogin.loginStatus = @"IG Login";
            ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
            [self.navigationController pushViewController:home animated:YES];
            [ProgressHUD showSuccess:@""];
       
    }
}



-(void)searchItems

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlist.php"];
   // NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlist2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        signLogin.searchItemArray=[[NSMutableArray alloc]init];
        signLogin.searchItemArray=searchArray;
    }
}


-(void)searchShops

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
   // NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlist.php"];
     NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/new_premium_venlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        signLogin.searchShopArray=[[NSMutableArray alloc]init];
        signLogin.searchShopArray=searchArray;
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
