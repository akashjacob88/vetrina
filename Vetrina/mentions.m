//
//  mentions.m
//  Vetrina
//
//  Created by Amit Garg on 1/30/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import "mentions.h"
#import "Singleton.h"
#import "JSON.h"
#import "UIImageView+WebCache.h"
#import "mention_details.h"



@interface mentions ()
{
    NSMutableArray *recomendList;
    
}
@end

@implementation mentions


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)list_recomendation{
   
    
    
    NSString *post = [NSString stringWithFormat:@"fid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/recommend_list.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:eventarray forKey:@"mentions"];
    
    if (eventarray.count<1)
    {
        recomendList = [[NSMutableArray alloc]init];
        
    }
    else
    {
        recomendList = [[NSMutableArray alloc]initWithArray:eventarray];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSUInteger i = 0;
        
        while (i < [recomendList count]) {
            id obj = [[recomendList objectAtIndex:i] valueForKey:@"uid"];
            
            if ([seen containsObject:obj]) {
                [recomendList removeObjectAtIndex:i];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                [seen addObject:obj];
                i++;
            }
        }
        
        
        
        
//        [recomendList addObject:[eventarray objectAtIndex:0]];
//        NSLog(@"%@",[[eventarray objectAtIndex:0] valueForKey:@"uid"]);
        
//        for (int i =1; i<eventarray.count; i++) {
//            
//            int k=(int)recomendList.count;
//            for (int j=0; j<k; j++) {
//                
//                
//                if ([[[eventarray objectAtIndex:i] valueForKey:@"uid"] isEqualToString:[[recomendList objectAtIndex:j] valueForKey:@"uid"]]) {
//                    
//                }else{
//                    
//                    
//                    [recomendList addObject:[eventarray objectAtIndex:i]];
//
//                    
//                }
//            }
//            
//        }
        
        
           
        
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    singlogin = [Singleton instance];
    
    [self list_recomendation];


    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        _arebic_Back.hidden=YES;
        _title_lbl.text=@"Mentions";
        _arebicTittle_lbl.hidden=YES;
        
    }else{
        _back_english.hidden=YES;
        _arebicTittle_lbl.text=@"توصيات من الأصدقاء";
        _title_lbl.hidden=YES;

    }




}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 70;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    

    
    return recomendList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)

    {
        
        
        static NSString *CellIdentifier = @"Cell_identifier";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            
            
        }
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
        NSString *strImageUrl1 = [[recomendList objectAtIndex:indexPath.row]objectForKey:@"uimage"];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"profile_icon.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 25.0;;
        [cell addSubview:img];
        img.backgroundColor=[UIColor grayColor];
        
        UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(60 ,  10 , 200, 40)];
        name1.tag=1;
        
        //   name1.backgroundColor=[UIColor lightGrayColor];
        name1.text=[NSString stringWithFormat:@"%@",[[recomendList objectAtIndex:indexPath.row]objectForKey:@"uname"]];
        name1.textAlignment = NSTextAlignmentLeft;

        if (([cell.contentView viewWithTag:1]))
        {
            [[cell.contentView viewWithTag:1]removeFromSuperview];
        }
        
        [cell.contentView addSubview:name1];
        
        
        [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
        [cell.layer setBorderWidth:1.0f];
       // [cell.layer setCornerRadius:7.5f];
        [cell.layer setShadowOffset:CGSizeMake(0, 1)];
        [cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [cell.layer setShadowRadius:8.0];
        [cell.layer setShadowOpacity:0.8];
        
        [cell.layer setMasksToBounds:NO];
 cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;

        
        
    }else{
        
        
        
        
        static NSString *CellIdentifier = @"Cell_identifier";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            
            
        }
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-55 , 5, 50, 50)];
        NSString *strImageUrl1 = [[recomendList objectAtIndex:indexPath.row]objectForKey:@"uimage"];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"profile_icon.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 25.0;;
        [cell addSubview:img];
        img.backgroundColor=[UIColor grayColor];
        
        UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-265,  10 , 200, 40)];
        name1.tag=1;
        name1.textAlignment = NSTextAlignmentRight;

        //   name1.backgroundColor=[UIColor lightGrayColor];
        name1.text=[NSString stringWithFormat:@"%@",[[recomendList objectAtIndex:indexPath.row]objectForKey:@"uname"]];
        
        if (([cell.contentView viewWithTag:1]))
        {
            [[cell.contentView viewWithTag:1]removeFromSuperview];
        }
        
        [cell.contentView addSubview:name1];
        
        
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
        [cell.layer setBorderWidth:1.0f];
       // [cell.layer setCornerRadius:7.5f];
        [cell.layer setShadowOffset:CGSizeMake(0, 1)];
        [cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [cell.layer setShadowRadius:8.0];
        [cell.layer setShadowOpacity:0.8];

        [cell.layer setMasksToBounds:NO];
        

        
        return cell;

        
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    mention_details *obj=[[mention_details alloc]initWithNibName:@"mention_details" bundle:nil];
  
    
    obj.idstr=[[recomendList objectAtIndex:indexPath.row] valueForKey:@"uid"];

    [self.navigationController pushViewController:obj animated:YES];
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAR_action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

    
}

- (IBAction)backE_action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
