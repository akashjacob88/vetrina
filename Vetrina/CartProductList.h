//
//  CartProductList.h
//  Vetrina
//
//  Created by Amit Garg on 5/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
@interface CartProductList : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>
{
    BSKeyboardControls *keyboardControls;
    Singleton *singletonn;
    IBOutlet UITableView *productListTable;
    IBOutlet UILabel *vendorNameLbl,*showOrderLbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITextField *sendMessageTxt;
    IBOutlet UIButton *editBtn;
    NSString *currentValue,*quantStr,*cartIdStr,*statusStr,*orderStr,*addressStr,*untiteld,*name;
    NSMutableArray *finalArray,*dataArray,*addressArray,*valu,*countyArra,*cityarra;
    NSInteger index;
    IBOutlet UIView *replyView,*confirmView,*addressView,*newAddressView;
    IBOutlet UITableView *addressTable;
    IBOutlet UITextField *addressTxt,*countryTxt,*cityTxt;
    
    IBOutlet UITableView *countyTbl;
    IBOutlet UITableView *cityTbl;
    NSString *btnStr;
    NSString *addAdress;
    /*-----Header-----*/
    UIImageView *vendorImage,*arrowImg;
    UIImageView *statusImage;
    UILabel *vendorName;
    UILabel *msgTxt;
    UILabel *timeLbl;
    UILabel *totalLbl;
    IBOutlet UIButton *chatBtn;

    IBOutlet UILabel *count_lbl;
    NSString *statusStr0;
    IBOutlet UIView *noItmeView;
    NSMutableArray *newArray,*array,*vendorNameArray,*imageArr,*arra_1,*countryArry;
    NSInteger i,j;
    NSString *stringOne,*newString,*stringTwo,*statusStrr;
    NSMutableDictionary *dynamicDict;
    
    NSMutableArray *arrayAsk;
    
    IBOutlet UIButton *backBtnOut;
    IBOutlet UIView *topBar;
    
    
    UIRefreshControl *refreshControl;

    
    /* -------- Arrays ------- */
    
    NSMutableArray *newArray12;
    NSMutableArray *array12,*finalArray12;
    NSMutableArray *vendorNameArray12,*dataArray12;
    NSArray *sectionAnimals;
    
    NSMutableArray *imageArr12,*img212;
    
    
    /* -------- Strings ------- */
    
    NSString *newString12;
    NSString *stringOne12,*stringTwo12,*stringTwo112,*stringTwo212;
    
    NSString *itme;
    
    /* -------- Integer ------- */
    
    NSInteger z;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict12;
    

    
    
    
    
    
    
    
    
    
}
-(IBAction)Back:(id)sender;
-(IBAction)Edit:(id)sender;
-(IBAction)OK:(id)sender;
-(IBAction)Confirm:(id)sender;
- (IBAction)countyBtn:(id)sender;
- (IBAction)cityBtn:(id)sender;


@end
