//
//  SearchAR.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface SearchAR : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate,UIGestureRecognizerDelegate>

{
    
    BSKeyboardControls *keyboardControls;
    
    Singleton *singletonn;
    IBOutlet UIButton *shopesBtn;
    IBOutlet UIButton *itemsBtm;
    NSMutableArray *array1, *array5, *array2;
    NSArray *sortedarray1,*sortedarray2;
    NSString *accesToken,*staffPic;
    NSMutableArray *imagearrayLIst, *newdatalist,*imageArray;
    UIImageView *image2, *arrowIcon;
    UILabel *costText1, *lineLbl;
    IBOutlet UITableView *searchTable;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UITextField *searchText;
    IBOutlet UIActivityIndicatorView *indicater;
    IBOutlet UILabel *msg_countLbl;
    
    IBOutlet UILabel *catlbl;
    IBOutlet UILabel *msg_countVen;
    IBOutlet UIView *noitmeView;
}


@property (strong, nonatomic) NSString *tabBtnValue,*selectedName;

@end
