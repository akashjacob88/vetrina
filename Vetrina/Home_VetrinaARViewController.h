//
//  Home_VetrinaARViewController.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Home_VetrinaARViewController : UIViewController


{
    Singleton *singlogin;
    
    /* -------- Arrays ------- */
    
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    NSMutableArray *vendorNameArray,*dataArray,*subCatArry;
    NSArray *sectionAnimals;
    
    NSMutableArray *imageArr,*img2;
    
    
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2;
    
    NSString *itme;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    
    /* -------- OutLets ------- */
    
    UIButton *myFav, *premiumBtn;
    
    UIImageView *proImage,*changepasImg;
    
    IBOutlet UIButton *signIn;
    
    IBOutlet UIImageView *settingIcon;
    
    IBOutlet UILabel *chrtLbl;
    
    IBOutlet UIImageView *img33;
    
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIView *sorryView;
    UIImageView  *Image2;
   
    IBOutlet UIImageView *leftSearch,*rightSearch,*cart,*profilePic;
    
    IBOutlet UILabel *vendorChatLblk;

}

@property (strong, nonatomic) IBOutlet UILabel *count_lbl;
@property (strong, nonatomic) IBOutlet UILabel *count_lbl_user;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)search:(id)sender;

- (IBAction)cart1:(id)sender;


@end
