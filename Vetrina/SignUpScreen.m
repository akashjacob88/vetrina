//
//  SignUpScreen.m
//  Vetrina
//
//  Created by Amit Garg on 7/21/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "SignUpScreen.h"
#import "All_SignIn.h"
#import "SignUp_Shop.h"
#import "User_Login.h"
#import "Vendor_Login.h"

@interface SignUpScreen ()

@end

@implementation SignUpScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)userLogIn:(id)sender
{
    User_Login *login   = [[User_Login alloc]initWithNibName:@"User_Login" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}


- (IBAction)userSignUp:(id)sender
{
    All_SignIn *view = [[All_SignIn alloc]initWithNibName:@"All_SignIn" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
}


- (IBAction)vendorLogin:(id)sender
{
    Vendor_Login *vLogin = [[Vendor_Login alloc]initWithNibName:@"Vendor_Login" bundle:nil];
     [self.navigationController pushViewController:vLogin animated:YES];
}


- (IBAction)vendorSignUp:(id)sender
{
    SignUp_Shop *view1 = [[SignUp_Shop alloc]initWithNibName:@"SignUp_Shop" bundle:nil];
    [self.navigationController pushViewController:view1 animated:YES];
}












- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
