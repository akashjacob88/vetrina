//
//  SearxhItmesAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/8/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"


@interface SearxhItmesAR : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate,UIGestureRecognizerDelegate>

{
    Singleton *singlogin;
    IBOutlet UILabel *msg_countLbl;
    IBOutlet UITableView *itemsTbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    UIImageView *Image2, *img3;
    NSMutableArray *aray_2;
    NSArray *sortedarray1,*sortedarray2;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr,*askforPrice ,*soldout;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    NSString *cartIdStr,*quantStr,*staffPic,*name,*objectIdStr;
    IBOutlet UILabel *catLbl;

    IBOutlet UILabel *msg_countVen;
    NSMutableArray *cartListArray;
    
    IBOutlet UILabel *chrtLbl;
    IBOutlet UITextField *searchText;
    NSMutableArray *vimagearray1,*pimagearray2,*varray1,*parray2;
    UIImageView *vimage,*pimage;
    BSKeyboardControls *keyboardControls;

    IBOutlet UIView *noitemsView;
}
- (IBAction)search_shops:(id)sender;

@end
