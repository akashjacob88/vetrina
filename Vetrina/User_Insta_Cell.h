//
//  User_Insta_Cell.h
//  Vetrina
//
//  Created by Amit Garg on 5/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface User_Insta_Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *vendorImg;
@property (strong, nonatomic) IBOutlet UILabel *vendorName;
@property (strong, nonatomic) IBOutlet UIButton *like;


@end
