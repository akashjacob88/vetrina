//
//  ConfirmProductVenderAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface ConfirmProductVenderAR : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>


{
    BSKeyboardControls *keyboardControls;
    Singleton *singletonn;
    IBOutlet UITableView *confirmTable;
    NSString *deliPriceStr,*totalPriceStr,*orderStr,*addressStr,*name;
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableArray *dataArray,*finalArray;
    
}

@end
