//
//  DeclarePrice.h
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
@interface DeclarePrice : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>
{
    BSKeyboardControls *keyboardControls;
    Singleton *singletonn;
    IBOutlet UITableView *declarePriceTable;
    NSString *currentValue,*unitPirceStr,*cartIdStr,*unitPirceStr2,*name;
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableArray *dataArray,*finalArray;
    NSMutableArray *declareArray;
    NSString *update;
}
@end
