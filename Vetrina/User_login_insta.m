//
//  User_login_insta.m
//  Vetrina
//
//  Created by Amit Garg on 5/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "User_login_insta.h"
#import "JSON.h"
#define kAccessToken         @"access_token="
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#define kBaseURL @"https://instagram.com/"
#define kAuthenticationURL @"oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
//#define kClientID @"4af4e24014f943aaaf16781b45ebf803"
#define kClientID @"c0fa0e5317f543098de48be60c4b7c69"

#define kRedirectURI @"http://VetrinaApp.com"
#import "User_Insta_Cell.h"
#import "AsyncImageView.h"
#import "ProgressHUD.h"
@interface User_login_insta ()

@end

@implementation User_login_insta

- (void)viewDidLoad

{
    [super viewDidLoad];
    signlogin = [Singleton instance];
    [indicator stopAnimating];
    webView2.hidden= YES;
    
    [webView2 sizeToFit];
    webView2.scrollView.scrollEnabled = YES;
    
    vendorTbl.sectionFooterHeight = 0;
    vendorTbl.sectionHeaderHeight = 0;
    
    view1.hidden = YES;

    [self logout];

}


- (void) viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
}


- (IBAction)loginInsta:(id)sender

{
    [webView2 setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    NSString* urlString = [kBaseURL stringByAppendingFormat:kAuthenticationURL,kClientID,kRedirectURI];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [webView2 loadRequest:request];
}


#pragma mark -- WebView Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString* urlString = [[request URL] absoluteString];
    NSURL *Url = [request URL];
    NSArray *UrlParts = [Url pathComponents];
    
    // runs a loop till the user logs in with Instagram and after login yields a token for that Instagram user
    // do any of the following here
    if ([UrlParts count] == 1)
    {
        
        NSRange tokenParam = [urlString rangeOfString: kAccessToken];
        if (tokenParam.location != NSNotFound)
        {
            NSString* token = [urlString substringFromIndex: NSMaxRange(tokenParam)];
            // NSString *followsID = @"395834289";
            // If there are more args, don't include them in the token:
            NSLog(@"ACCESS TOKEN %@",token);
            signlogin.accesstokenNew=token;
            NSRange endRange = [token rangeOfString: @"&"];
            
            if (endRange.location != NSNotFound)
                token = [token substringToIndex: endRange.location];
            
            if ([token length] > 0 )
            {
                
                NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/self/media/recent/?access_token=%@", kInstagramAPIBaseURL,token];
                NSURL * url=[NSURL URLWithString:userInfoUrl];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLResponse *response = NULL;
                NSError *requestError = NULL;
                NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
                
                NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
                //  NSLog(@"GetData--%@",data);
                
                NSDictionary *value=[data JSONValue];
                NSMutableArray *userDict = [value objectForKey:@"data"];
                NSLog(@"User Dict %@",userDict);
                //
                NSMutableArray *user = [[userDict  objectAtIndex:0]valueForKey:@"user"];
                NSLog(@"user bio %@",user);
                NSString* userId=[user valueForKey:@"id"];
                NSString* userFullName=[user valueForKey:@"full_name"];
                NSString* userProfilePic=[user valueForKey:@"profile_picture"];
                NSString *userName = [user valueForKey:@"username"];
                NSLog(@"USER NAME= %@",userName);
                signlogin  = [Singleton instance];
                signlogin.vIGUsername = userName;
                signlogin.vIGUserId = userId;
                signlogin.vIGFullname = userFullName;
                signlogin.vIGProfilePic = userProfilePic;
                NSLog(@"Singleton --- %@",userFullName);
                img4 = [NSString stringWithFormat:@"%@",signlogin.vIGProfilePic];
                NSURL *url2 = [NSURL URLWithString:img4];
                NSData *data2 = [NSData dataWithContentsOfURL:url2];
                pro = [[UIImage alloc] initWithData:data2];
                
                NSMutableArray *img = [userDict valueForKeyPath:@"images"];
                
                NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
                
                NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
                NSLog(@"Pic Links %@",righturl);
                signlogin  = [Singleton instance];
                signlogin.vImgUrl = righturl;
                NSLog(@"%lu",(unsigned long)[signlogin.vImgUrl count]);
                
                NSString* userInfoUrl1 = [NSString stringWithFormat:@"%@/v1/users/self/follows/?access_token=%@", kInstagramAPIBaseURL,token];
                NSURL * url1=[NSURL URLWithString:userInfoUrl1];
                NSURLRequest *request1= [NSURLRequest requestWithURL:url1];
                NSURLResponse *response1 = NULL;
                NSError *requestError1 = NULL;
                NSData *responseData1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&requestError1];
                
                NSString *data1=[[NSString alloc]initWithData:responseData1 encoding:NSUTF8StringEncoding];
                NSDictionary *value1=[data1 JSONValue];
                NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
                
                NSLog(@"dadadad %@",userDict1);
                NSMutableArray *userName1 = [userDict1  valueForKey:@"username"];
                NSLog(@"user bio %@",userName1);
                NSMutableArray *fullName1 = [userDict1  valueForKey:@"full_name"];
                NSMutableArray *profilePic = [userDict1  valueForKey:@"profile_picture"];
                NSMutableArray *folowUsrID=[userDict1 valueForKey:@"id"];
                signlogin.vFolowUserName = userName1;
                signlogin.vFolowFullName = fullName1;
                signlogin.vFolowUserId = folowUsrID;
                signlogin.vFolowProfilePic = profilePic;
                
                [self matchingVendr];
                
            }
        }
        else
        {
            [indicator stopAnimating];
            [ProgressHUD showSuccess:@"Success."];
        }
        return NO;
    }
    return YES;
}


- (void) webViewDidStartLoad:(UIWebView *)webView

{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

}


- (void)webViewDidFinishLoad:(UIWebView *)webView

{
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error

{
    if (error.code == 102)
        return;
    if (error.code == -1009 || error.code == -1005)
    {
        //        _completion(kNetworkFail,kPleaseCheckYourInternetConnection);
    }
    else
    {
        //        _completion(kError,error.description);
    }
    //back to main page
}


-(void)matchingVendr
{
    
    [ProgressHUD showSuccess:@"" Interaction:YES];
    if([signlogin.searchShopArray count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"No Shops available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        //[searchTable setHidden:YES];
    }
    else
    {
        array1=[[NSMutableArray alloc]init];
        array1=signlogin.searchShopArray;
        newdatalist = [[NSMutableArray alloc]init];
        
        for (int i = 0 ; i < signlogin.vFolowUserId.count; i++)
        {
            NSString *folowId = [NSString stringWithFormat:@"%@",[signlogin.vFolowUserId objectAtIndex:i]];
            
            NSPredicate *predicate =[NSPredicate predicateWithFormat:@"instagramid BEGINSWITH[c] %@", folowId];
            NSLog(@"%@", predicate);
            
            NSArray *searchArray1;
            searchArray1=[array1 mutableCopy];
            listFiles1 = [[NSArray alloc]init];
            listFiles1 = [NSArray arrayWithArray:[searchArray1 filteredArrayUsingPredicate:predicate]];
            
            if (listFiles1.count > 0)
            {
                NSString *vendorId=[[listFiles1  valueForKey:@"id"]objectAtIndex:0];
                NSString *vendorPic=[[listFiles1  valueForKey:@"imagee"]objectAtIndex:0];
                NSString *shopName=[[listFiles1  valueForKey:@"fullname_shopname"]objectAtIndex:0];
                NSString *instaId=[[listFiles1  valueForKey:@"instagramid"]objectAtIndex:0];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:vendorId forKey:@"id"];
                [dic setValue:vendorPic forKey:@"imagee"];
                [dic setValue:shopName forKey:@"fullname_shopname"];
                [dic setValue:instaId forKey:@"instagramid"];
                
                [newdatalist addObject:dic];
                
                if (newdatalist.count==1)
                {
                signlogin.SlctVenId =[[newdatalist valueForKey:@"id"]objectAtIndex:0];
                signlogin.venName =[[newdatalist valueForKey:@"fullname_shopname"]objectAtIndex:0];
                signlogin.venPic =[[newdatalist valueForKey:@"imagee"]objectAtIndex:0];
                signlogin.fUserId = [[newdatalist valueForKey:@"instagramid"]objectAtIndex:0];
                }
                else
                {
                 NSInteger a =newdatalist.count-1;
                 signlogin.SlctVenId =[[newdatalist valueForKey:@"id"]objectAtIndex:a];
                 signlogin.venName =[[newdatalist valueForKey:@"fullname_shopname"]objectAtIndex:a];
                 signlogin.venPic =[[newdatalist valueForKey:@"imagee"]objectAtIndex:a];
                 signlogin.fUserId = [[newdatalist valueForKey:@"instagramid"]objectAtIndex:a];
                }
                [self likevendr];
            }
         }
        [self.navigationController popViewControllerAnimated:YES];
        [ProgressHUD showSuccess:@"Success."];
        [indicator stopAnimating];
     }
}


-(void)likevendr

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addfavvendor.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",signlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usrnameStr=[[NSString alloc]initWithFormat:@"%@",signlogin.usrName];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *status=@"No";
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodstatus\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",status] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    
    
    NSString *vidStr=[[NSString alloc]initWithFormat:@"%@",signlogin.SlctVenId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vnameStr=[[NSString alloc]initWithFormat:@"%@",signlogin.venName];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *instaidStr=[[NSString alloc]initWithFormat:@"%@",signlogin.fUserId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"instagramid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",instaidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *picStr=[[NSString alloc]initWithFormat:@"%@",signlogin.venPic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",picStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);


}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)logout

{
    NSURL *url = [NSURL URLWithString:@"https://instagram.com/"];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSEnumerator *enumerator = [[cookieStorage cookiesForURL:url] objectEnumerator];
    NSHTTPCookie *cookie = nil;
    while ((cookie = [enumerator nextObject]))
    {
        [cookieStorage deleteCookie:cookie];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
