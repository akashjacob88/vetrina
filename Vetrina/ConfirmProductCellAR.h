//
//  ConfirmProductCellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmProductCellAR : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *prodImg;
@property (strong, nonatomic) IBOutlet UILabel *prodName;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;


@end
