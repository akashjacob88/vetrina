//
//  Premium_Club.m
//  Vetrina
//
//  Created by Amit Garg on 5/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Premium_Club.h"
#import "ShopSearch.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "Vendor_Detaillist.h"
#import "FavCustomCell.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "CartVendorList.h"
#import "All_SignIn.h"
#import "User_login_insta.h"
#import "CartProductList.h"
#import "Premium_cell.h"
#import "ProgressHUD.h"
#import "PremiumItems.h"
#import "CartProductAR.h"
#import "SearchAR.h"
#import "Vendor_DetaillistAR.h"

@interface Premium_Club ()

@end

@implementation Premium_Club


- (void)viewDidLoad

{
    [super viewDidLoad];
    
    premiumTble.sectionFooterHeight = 0;
    premiumTble.sectionHeaderHeight = 0;
    [indicator stopAnimating];
    shop.layer.cornerRadius = 5.0;
    itemsOutlet.layer.masksToBounds = YES;
    itemsOutlet.layer.cornerRadius = 5.0;
    itemsOutlet.layer.borderWidth = 2.0;
    itemsOutlet.layer.borderColor = [[UIColor whiteColor]CGColor];
    itemsOutlet.clipsToBounds = YES;
    itemsOutlet.layer.opaque = NO;

    
    signlogin=[Singleton instance];
    [self vendornamelisting];
    [self accessTokenList];
    premiumTble.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    view1.hidden=YES;
    cartLbl.hidden=YES;
    [self cartTotalView];
    
}


-(void)cartTotalView
{
    if (signlogin.userid == nil)
    {
        cartLbl.hidden=YES;
    }
    else
    {
        if ([signlogin.totalCart isEqualToString:@"0"])
        {
            cartLbl.hidden=YES;
        }
        else if (signlogin.totalCart ==nil)
        {
            cartLbl.hidden=YES;
        }
        else
        {
            cartLbl.hidden=NO;
            cartLbl.layer.cornerRadius=cartLbl.frame.size.height/2;
            cartLbl.clipsToBounds=YES;
            cartLbl.text=signlogin.totalCart;
        }
    }
}


-(void)vendornamelisting

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/premiumvenlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No vendor available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    if([arra_1 count]==0)
    {
        
        view2.hidden = NO;
        [premiumTble setHidden:YES];
    }
    else
    {
        [premiumTble setHidden:NO];
        view2.hidden = YES;
        [premiumTble reloadData];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arra_1 count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    Premium_cell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Premium_cell" bundle:nil] forCellReuseIdentifier:@"cell_1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(Premium_cell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSString *sectional = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    cell.vendorName.text =[NSString stringWithFormat:@"%@",sectional];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
    cell.vendorPic.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arra_1 objectAtIndex:indexPath.row]objectForKey:@"imagee"]]];
    cell.vendorPic.layer.masksToBounds = YES;
    cell.vendorPic.layer.cornerRadius = 20.0;
    cell.vendorPic.layer.opaque = NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    signlogin.fUserId = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramid"];
    signlogin.venInstaName = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"username_instagramacnt"];
    signlogin.SlctVenId=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"id"];
    signlogin.venName=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    signlogin.objectIdStr = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];

    
    [self insta];
    
    [self performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [indicator startAnimating];
}


-(void)vendorlist

{
    if ([signlogin.primium isEqualToString:@"Yes"])
    {
        
        Vendor_DetaillistAR *vInstPics = [[Vendor_DetaillistAR alloc]initWithNibName:@"Vendor_DetaillistAR" bundle:NULL];
        [self.navigationController pushViewController:vInstPics animated:YES];
        [ProgressHUD showSuccess:@""];
    }
    else
    {
    Vendor_Detaillist *vInstPics = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:NULL];
    [self.navigationController pushViewController:vInstPics animated:YES];
    [ProgressHUD showSuccess:@""];
    }
    [indicator stopAnimating];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 50;
}


-(void)insta
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",signlogin.fUserId);
        
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,signlogin.fUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        signlogin.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            dynamicDict=[[NSMutableDictionary alloc]init];
            
            dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
        }
    }
    //NSLog(@"Dynamic ---- %@",dynamicDict);
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    signlogin.newarayList1 = newdatalist;
}


-(void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"View Disapear");
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
}


-(void)accessTokenList
{
    
    NSURL * url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/tokenlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         //If data were received
         if (data) {
             //Convert to string
             NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             NSLog(@"GetData--%@",result);
             NSDictionary *eventarray=[result JSONValue];
             NSLog(@"GetDatadictt--%@",eventarray);
             
             if(![[eventarray objectForKey:@"list"] isEqual:@"No subcategory available"])
             {
                 array_5=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"list"]];
             }
             else
             {
                 array_5=[[NSMutableArray alloc]init];
                 
             }
             if([array_5 count]==0)
             {
                 
             }
             else
             {
                 accesToken = [[array_5 objectAtIndex:0]valueForKey:@"accesstoken"];
             }
             
         }
         //No data received
         else {
             NSString *errorText;
             //Specific error
             if (error)
                 errorText = [error localizedDescription];
             //Generic error
             else
                 errorText = @"An error occurred when downloading the list of issues. Please check that you are connected to the Internet.";
             
             //Show error
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
             
             //Hide activity indicator
             //[self clearIssuesAccessoryView];
         }
     }];
    
    
    
    NSLog(@"Done");
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)cart:(id)sender

{
    if (signlogin.userid == nil)
  {
    [indicator stopAnimating];
    NSLog(@"Please Login to add cart");
    [view1 setHidden:NO];
  }
  else
  {
    [indicator setHidden:NO];
    [indicator startAnimating];
    [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
      [ProgressHUD show:@"Loading..." Interaction:NO];
      
   }
}


-(void)nextpage
{
    if ([signlogin.primium isEqualToString:@"Yes"])
    {
        CartProductAR *home = [[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
    }
    else
    {
        CartProductList *home = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
        
    }
}


- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    signlogin.loginfrom = @"cart";
    All_SignIn *signIn = [[All_SignIn alloc]initWithNibName:@"All_SignIn" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}


- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}


- (IBAction)searchBtn:(id)sender
{
    if ([signlogin.primium isEqualToString:@"Yes"])
    {
        SearchAR *shop2 = [[SearchAR alloc]initWithNibName:@"SearchAR" bundle:nil];
        [self.navigationController pushViewController:shop2 animated:YES];
    }
    else
    {
        ShopSearch *shop1 = [[ShopSearch alloc]initWithNibName:@"ShopSearch" bundle:nil];
        [self.navigationController pushViewController:shop1 animated:YES];
    }


}

- (IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)items:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:2.0f];
}


-(void)home
{
    PremiumItems *items =[[PremiumItems alloc]initWithNibName:@"PremiumItems" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];
}

-(void)viewWillAppear:(BOOL)animated

{
    [self.navigationController setNavigationBarHidden:YES];
    [self cartTotalView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
