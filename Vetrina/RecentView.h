//
// Copyright (c) 2015 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "Singleton.h"

//#import "SelectSingleView.h"
//#import "SelectMultipleView.h"
//#import "AddressBookView.h"
//#import "FacebookFriendsView.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface RecentView : UIViewController
{
    Singleton *singlogin;


    IBOutlet UILabel *vendorChatLblk;


}
//<UIActionSheetDelegate, SelectSingleDelegate, SelectMultipleDelegate, AddressBookDelegate, FacebookFriendsDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)loadRecents;
@property (strong, nonatomic) IBOutlet UIButton *setting_btn_outlet;
@property (strong, nonatomic) IBOutlet UIImageView *AREABIC_PIC;

@end
