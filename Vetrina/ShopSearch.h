//
//  ShopSearch.h
//  Vetrina
//
//  Created by Amit Garg on 5/7/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface ShopSearch : UIViewController<BSKeyboardControlsDelegate,UIGestureRecognizerDelegate>
{
    BSKeyboardControls *keyboardControls;

    
    
    
    Singleton *singletonn;
    IBOutlet UIButton *shopesBtn;
    IBOutlet UIButton *itemsBtm;
    NSMutableArray *array1, *array5, *array2;
    NSArray *sortedarray1,*sortedarray2;
    NSString *accesToken,*staffPic;
    NSMutableArray *imagearrayLIst, *newdatalist,*imageArray;
    UIImageView *image2, *arrowIcon , *image3;
    UILabel *costText1, *lineLbl;
    IBOutlet UITableView *searchTable;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UITextField *searchText;
    IBOutlet UIView *noPrductView;
    
    IBOutlet UILabel *msg_countVen;
    IBOutlet UILabel *catl_lbl;
    IBOutlet UILabel *msg_countLbl;
    IBOutlet UIView *topBar;
    
}
- (IBAction)itemBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)chart:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;
- (IBAction)textDidChange:(id)textField;


@end
