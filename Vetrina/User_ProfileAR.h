//
//  User_ProfileAR.h
//  Vetrina
//
//  Created by Amit Garg on 11/2/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface User_ProfileAR : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

{
    Singleton *singlogin;
    IBOutlet UIImageView *profilePic;
    IBOutlet UIView *view1,*view2;
    IBOutlet UIView *topBar;
    
    
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    UIImageView  *Image2;
    UIImage *cimage,*cimage2;
    UIImage *picture ;
    UIImage *thumbnail;

}
- (IBAction)OrderHistory_Action:(id)sender;


@property(strong,nonatomic) NSString *test;

@end
