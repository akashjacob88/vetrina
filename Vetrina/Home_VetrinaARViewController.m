//
//  Home_VetrinaARViewController.m
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Home_VetrinaARViewController.h"
#import "HomeAR_Cell1.h"
#import "HomeAR_Cell2.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "SubcatgoryAR.h"
#import "User_ProfileAR.h"
#import "All_SignInAR.h"
#import "SearchAR.h"
#import "Sorry_cartAR.h"
#import "FavoritesAR.h"
#import "CartProductAR.h"
#import "ProgressHUD.h"
#import "Premium_Club.h"
#import "SignUpScreenAR.h"
#import "Vendor_profileAR.h"
#import "RecentView.h"
#import "Vendor_ProductAR.h"
#import "Vendor_itemsAR.h"

#import <Parse/Parse.h>
#import "AppConstant.h"

@interface Home_VetrinaARViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *navView;
    IBOutlet UIView *topBar;
     IBOutlet UIView *vendorViewBar;
    NSMutableArray *recents;
    NSArray *dialog_Objects;

}

@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex1;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;

@end

@implementation Home_VetrinaARViewController

@synthesize isOpen,selectIndex1,indicator;


- (void)dealloc
{
    // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex1 = nil;
    settingIcon =nil;
    img33 =nil;
    indicator = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        
    }
    return self;
}
- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }

    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    _count_lbl.text=[NSString stringWithFormat:@"%d", total];
    
    _count_lbl_user.text=[NSString stringWithFormat:@"%d", total];
	    singlogin.count_msg=total;
    if (singlogin.userid == nil)
    {
        _count_lbl.hidden=YES;
        _count_lbl_user.hidden = YES;
    }
    else
    {
        if ([_count_lbl.text isEqualToString:@"0"])
        {
            _count_lbl.hidden=YES;
            _count_lbl_user.hidden = YES;
        }
        else if ([_count_lbl.text isEqualToString:@""])
        {
            _count_lbl.hidden=YES;
            _count_lbl_user.hidden = YES;
        }
        else
        {
            _count_lbl.hidden=NO;
            _count_lbl_user.hidden = NO;
            _count_lbl.layer.cornerRadius=_count_lbl.frame.size.height/2;
            _count_lbl.clipsToBounds=YES;
            _count_lbl_user.layer.cornerRadius=_count_lbl_user.frame.size.height/2;
            _count_lbl_user.clipsToBounds=YES;
            
//            _count_lbl_user.hidden=NO;
//            _count_lbl_user.hidden = NO;
//            _count_lbl_user.layer.cornerRadius=_count_lbl_user.frame.size.height/2;
//            _count_lbl_user.clipsToBounds=YES;
//            _count_lbl_user.layer.cornerRadius=_count_lbl_user.frame.size.height/2;
//            _count_lbl_user.clipsToBounds=YES;
            
        }
    }
    
}
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singlogin.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}

-(void)prodListing123
{
    //     NSString *post1;
    //    if ([singloging.loginStatus isEqual:@"User Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
    //    }
    //    else if ([singloging.loginStatus isEqual:@"IG Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    //    }
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singlogin.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSMutableArray *data_arr;
    
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        data_arr=[[NSMutableArray alloc]init];
    }
    else
    {
        data_arr = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    
    
    if (data_arr.count==0) {
        vendorChatLblk.hidden=YES;
         singlogin.count_cart=0;
        
    }else{
        int count1=0;
        
        for (int a=0; a<data_arr.count; a++) {
            
            NSString *aarra =[[data_arr objectAtIndex:a] valueForKey:@"statusid"];
            
            if ([aarra isEqualToString:@"2"]) {
                count1++;
            }
            
        }
        
        if (count1 == 0) {
            vendorChatLblk.hidden=YES;
             singlogin.count_cart=0;
        }else{
            
             singlogin.count_cart=count1;
            vendorChatLblk.hidden=NO;
            vendorChatLblk.text=[NSString stringWithFormat:@"%d",count1];
            vendorChatLblk.layer.cornerRadius=8.f;
            vendorChatLblk.layer.masksToBounds=YES;
            
        }
    }
    
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singlogin = [Singleton instance];

    
    [[NSUserDefaults standardUserDefaults] setObject:@"arebic" forKey:@"language"];
    

    
    signIn.layer.masksToBounds = YES;
    signIn.layer.cornerRadius = 5.0;
    signIn.layer.borderWidth = 2.0;
    signIn.layer.borderColor = [[UIColor whiteColor]CGColor];
    signIn.clipsToBounds = YES;
    signIn.layer.opaque = NO;
   
    recents=[[NSMutableArray alloc]init];
    
    
    _count_lbl.hidden = YES; ///may be crash //////////
    
     _count_lbl_user.hidden = YES;
    chrtLbl.hidden = YES;

    
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    
    [self signIn];
    // [self cartTotalView];
    [self catlist];
    sorryView.hidden=YES;
    // chrtLbl.hidden=YES;
    
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        navView.hidden = YES;
        vendorViewBar.hidden = NO;
        [self prodListing123];
        [self loadRecents];

    }
    else if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [self searchShops];
        
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        [self loadRecents];

        [self cartTotalView];
    }
    else
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;

        [self cartTotalView];
    }
    
    
    
    self.expansionTableView.layer.shadowRadius = 8.0f;
    self.expansionTableView.layer.shadowOffset = CGSizeMake(0, 3);
    self.expansionTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.expansionTableView.layer.shadowOpacity = 0.8f;

    
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    

}
-(void)searchShops

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlist.php"];
    // NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/new_premium_venlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        singlogin.searchShopArray=[[NSMutableArray alloc]init];
        singlogin.searchShopArray=searchArray;
    }
}


-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
            
        }
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Servise



-(void)catlist

{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/subcatlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    if (dataArray.count > 0)
    {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"a_subcatname"  ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [sortedArray1 mutableCopy];
    
    
    NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"a_catname" ascending:NO];
    NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
    sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
    
    NSLog(@"sorted array is %@",sortedArray);
    
    newArray=[[NSMutableArray alloc]init];
    array=[[NSMutableArray alloc]init];
    finalArray=[[NSMutableArray alloc]init];
    
    vendorNameArray=[[NSMutableArray alloc]init];
    subCatArry=[[NSMutableArray alloc]init];

    imageArr=[[NSMutableArray alloc]init];
    
    for( i =0;i<sortedArray.count;i++)
    {
        stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
        
        NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"a_catname"];
        NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
        NSString *catidStr=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];

        
        
        
        if (![stringOne isEqualToString:stringTwo ])
        {
            
            [vendorNameArray addObject:vendorName1];
            [subCatArry addObject:catidStr];

            
            
            [imageArr addObject:imagestr];
            
            stringTwo = stringOne;
            
            if ([newString isEqualToString:@"newString"])
            {
                NSInteger a=i-1;
                NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];

                
                
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:catidStr];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            if ([newString isEqualToString:@"oneString"])
            {
                
                NSInteger a=i-1;
                NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];

                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:catidStr];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
                
            }
            newString=@"oneString";
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
        }
        else
        {
            
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
            newString=@"newString";
        }
    }
    
    NSInteger a=i-1;
    NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];

    dynamicDict=[[NSMutableDictionary alloc]init];
    [dynamicDict setValue:newArray forKey:catidStr];
    [finalArray addObject:dynamicDict];
    
    
    dynamicDict=[[NSMutableDictionary alloc]init];
//    newArray=[[NSMutableArray alloc]init];
//    [subCatArry insertObject:@"Premium Club" atIndex:0];
//    [subCatArry insertObject:@"محلاتي المفضلة" atIndex:0];
//    [vendorNameArray insertObject:@"Premium Club" atIndex:0];
//    [vendorNameArray insertObject:@"محلاتي المفضلة" atIndex:0];
//
//    [imageArr insertObject:@"PremiumClub.png" atIndex:0];
//    [imageArr insertObject:@"MyFavorites.png" atIndex:0];
//    
//    
//    [dynamicDict setValue:newArray forKey:@"Premium Club"];
//    [finalArray insertObject:dynamicDict atIndex:0];
//    
//    dynamicDict=[[NSMutableDictionary alloc]init];
//    newArray=[[NSMutableArray alloc]init];
//    [dynamicDict setValue:newArray forKey:@"My Favorites"];
//    [finalArray insertObject:dynamicDict atIndex:0];
    }
    else
    {
        finalArray = [[NSMutableArray alloc]init];
        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];

    }
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
    
    [_expansionTableView reloadData];
}


#pragma mark - Table view data source and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex1.section == section)
        {
            //   return [[[_dataList objectAtIndex:section] objectForKey:@"list"] count]+1;;
            
            NSString *sectionTitle = [subCatArry objectAtIndex:section];
            sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
            return [sectionAnimals count]+1;
            
            
        }
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex1.section == indexPath.section&&indexPath.row!=0)
    {
        HomeAR_Cell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell2" bundle:nil] forCellReuseIdentifier:@"Cell_Ar2"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        }
        
        NSString *sectionTitle = [subCatArry objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        //     NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"list"];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"a_subcatname"];
        return cell;
    }
    else
    {
        HomeAR_Cell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell1" bundle:nil] forCellReuseIdentifier:@"Cell_Ar1"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        }
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        cell.titleLabel.text = sectionTitle;
//        
//        if ([sectionTitle isEqualToString:@"محلاتي المفضلة"])
//        {
//            cell.titleLabel.textColor=[UIColor colorWithRed:237.0f/255 green:92.0f/255 blue:118.0f/255 alpha:1];
//            cell.backgroundColor=[UIColor whiteColor];
//            NSString *image=[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]];
//            cell.iconImg.image=[UIImage imageNamed:image];
//            cell.arrowImageView.image=[UIImage imageNamed:@"ArrowLeft.png"];
//            cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
//            
//            //   cell.lineLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 48, 414, 1)];
//            cell.lineLbl.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
//            cell.backgroundColor=[UIColor whiteColor];
//        }
//        else if ([sectionTitle isEqualToString:@"Premium Club"])
//        {
//            cell.titleLabel.textColor=[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1];
//            cell.backgroundColor=[UIColor whiteColor];
//            NSString *image=[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]];
//            cell.iconImg.image=[UIImage imageNamed:image];
//            cell.arrowImageView.image=[UIImage imageNamed:@"ArrowLeft.png"];
//            cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
//            
//        }
//        else
//        {
//            
//            cell.backgroundColor=[UIColor colorWithRed:237.0f/255 green:92.0f/255 blue:118.0f/255 alpha:1.0f];
//            
//            cell.titleLabel.textColor=[UIColor whiteColor];
            [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
            cell.iconImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
            [cell changeArrowWithUp:([self.selectIndex1 isEqual:indexPath]?YES:NO)];
        //    cell.lineLbl.backgroundColor=[UIColor whiteColor];
      //  }
        return cell;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex1])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex1 = nil;
            
        }
        else
        {
            if (!self.selectIndex1)
            {
                self.selectIndex1 = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
       [ProgressHUD show:@"Loading..." Interaction:NO];
//        if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
//        {
//            NSString *sectionTitle = [subCatArry objectAtIndex:indexPath.section];
//            sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
//            
//            itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
//            singlogin.subName = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_subcatname"];
//            singlogin.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
//            [self performSelector:@selector(subcatgory)withObject:Nil afterDelay:0.5f];
//            
//        }
//        
//        else
//        {
            NSString *sectionTitle = [subCatArry objectAtIndex:indexPath.section];
            sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            
            itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
            singlogin.subName = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_subcatname"];
            singlogin.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
            [self performSelector:@selector(subcatgory)withObject:Nil afterDelay:0.5f];
        //}
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)subcatgory

{
    SubcatgoryAR *sub  = [[SubcatgoryAR alloc]initWithNibName:@"SubcatgoryAR" bundle:nil];
    [self.navigationController pushViewController:sub animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    HomeAR_Cell1 *cell = (HomeAR_Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex1];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex1.section;
    
    NSString *sectionTitle = [subCatArry objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex1 = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
//    if ([sectionTitle isEqualToString:@"محلاتي المفضلة"])
//    {
//        
//        cell.arrowImageView.image=[UIImage imageNamed:@"ArrowLeft.png"];
//        cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
//        
//       
//        if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
//        {
//            [ProgressHUD show:@"Loading..." Interaction:NO];
//            [self performSelector:@selector(favrt)withObject:Nil afterDelay:0.5f];
//        }
//        else
//        {
//            sorryView.hidden = NO;
//            [indicator stopAnimating];
//        }
//    }
//    else if ([sectionTitle isEqualToString:@"Premium Club"])
//    {
//        cell.arrowImageView.image=[UIImage imageNamed:@"ArrowLeft.png"];
//        cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
//       
//        [ProgressHUD show:@"Loading..." Interaction:NO];
//        [self performSelector:@selector(premium)withObject:Nil afterDelay:0.5f];
//    }
   else
   {
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
   }
}


-(void)premium
{
    [ProgressHUD showSuccess:@""];
    
    Premium_Club *club = [[Premium_Club alloc]initWithNibName:@"Premium_Club" bundle:nil];
    singlogin.primium = @"Yes";
    [self.navigationController pushViewController:club animated:NO];
}



-(void)signIn

{
    if ([singlogin.loginStatus isEqual:@"User Login"])
    {
        signIn.hidden = YES;
        settingIcon.hidden = NO;
        NSURL *url1=[NSURL URLWithString:singlogin.userproilePic];
        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        UIImage *emppppic=[UIImage imageWithData:datapic];
      
        
        settingIcon.image=emppppic;

        if (emppppic==nil) {
            
            settingIcon.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        
        
        }
        
        
        settingIcon.layer.masksToBounds = YES;
        settingIcon.layer.cornerRadius = settingIcon.frame.size.width/2;
        settingIcon.layer.opaque = NO;
        
        //        NSURL *url1=[NSURL URLWithString:singling.venPic];
        //        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        //        UIImage *emppppic=[UIImage imageWithData:datapic];
        //        vendorImg.image=emppppic;
    }
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        signIn.hidden = YES;
        settingIcon.hidden = NO;
        NSURL *url1=[NSURL URLWithString:singlogin.venPic];
        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        UIImage *emppppic=[UIImage imageWithData:datapic];
        settingIcon.image=emppppic;
        settingIcon.layer.masksToBounds = YES;
        settingIcon.layer.borderWidth = 1.0f;
        settingIcon.layer.cornerRadius = settingIcon.frame.size.width/2;;
        settingIcon.layer.borderColor = [[UIColor clearColor]CGColor];
        settingIcon.clipsToBounds = YES;
        settingIcon.layer.opaque = NO;
        
        
    }
    else
    {
        settingIcon.hidden = YES;
        cart.hidden = YES;
        signIn.hidden = NO;
    }
    
}


-(void) signUp

{
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [self performSelector:@selector(setting)withObject:Nil afterDelay:0.5f];
        [ProgressHUD show:@"Loading..." Interaction:NO];
          }
    else if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [self performSelector:@selector(VendorSetting)withObject:Nil afterDelay:0.5f];
        [ProgressHUD show:@"Loading..." Interaction:NO];
     
    }
    else
    {
        SignUpScreenAR *back = [[SignUpScreenAR alloc]initWithNibName:@"SignUpScreenAR" bundle:NULL];
        
        [self.navigationController pushViewController:back animated:NO];
    }
}


-(void)setting
{
    
    User_ProfileAR *usr=[[User_ProfileAR alloc]initWithNibName:@"User_ProfileAR" bundle:NULL];
    usr.test=@"no";
    [self.navigationController pushViewController:usr animated:NO];
    [ProgressHUD showSuccess:@""];

}

-(void)VendorSetting
{
    Vendor_profileAR *usr=[[Vendor_profileAR alloc]initWithNibName:@"Vendor_profileAR" bundle:NULL];
    [self.navigationController pushViewController:usr animated:NO];
       [ProgressHUD showSuccess:@""];
}



- (IBAction)signUp:(id)sender

{
    singlogin.loginfrom = @"loging";
    [self signUp];
}



- (IBAction)premiumClb:(id)sender

{
    
}




- (IBAction)search:(id)sender

{
    [indicator setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeMethod) withObject:nil afterDelay:0.3f];

}


-(void)homeMethod

{
    SearchAR *shopVen = [[SearchAR alloc]initWithNibName:@"SearchAR" bundle:NULL];
    shopVen.tabBtnValue = @"shop";
    [self.navigationController pushViewController:shopVen animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)cart1:(id)sender

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [sorryView setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    }

    
}


-(void)nextpage
{
    CartProductAR *cart1=[[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:nil];
    [self.navigationController pushViewController:cart1 animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator setHidden:YES];
    [indicator stopAnimating];
}




- (IBAction)signUpBtn:(id)sender
{
    [sorryView setHidden:YES];
    singlogin.loginfrom = @"cart";
    SignUpScreenAR *signUp = [[SignUpScreenAR alloc]initWithNibName:@"SignUpScreenAR" bundle:nil];
    [self.navigationController pushViewController:signUp animated:NO];
}


- (IBAction)noThanks:(id)sender

{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    sorryView.hidden = YES;
}





- (IBAction)myFavrtBtn:(id)sender

{
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(favrt)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        sorryView.hidden = NO;
    }
    
}
-(void)favrt

{
    FavoritesAR *fav = [[FavoritesAR alloc]initWithNibName:@"FavoritesAR" bundle:nil];
    [self.navigationController pushViewController:fav animated:NO];
    [ProgressHUD showSuccess:@""];
    
}



- (IBAction)chat:(id)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(chatBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        sorryView.hidden = NO;
    }
    
}


-(void)chatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    
}



- (IBAction)permotionalBtn:(id)sender
{
}

-(void)viewWillAppear:(BOOL)animated

{
    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
        
    }];
    
    
    [self cartTotalView];
    [self signIn];
    [self.navigationController setNavigationBarHidden:YES];
    
}


- (IBAction)vendorProductbtn:(id)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorProductBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        sorryView.hidden = NO;
    }
    
}


-(void)vendorProductBtn
{
   Vendor_ProductAR  *view = [[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)vendorChat:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorchatBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        sorryView.hidden = NO;
    }
    
}


-(void)vendorchatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorItems:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorItemsBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        sorryView.hidden = NO;
    }
    
}


-(void)vendorItemsBtn
{
    Vendor_itemsAR *view = [[Vendor_itemsAR alloc]initWithNibName:@"Vendor_itemsAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
