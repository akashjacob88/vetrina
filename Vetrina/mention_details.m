//
//  mention_details.m
//  Vetrina
//
//  Created by Amit Garg on 1/30/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import "mention_details.h"
#import "mentionDetails_Cell.h"
#import "mentions.h"
#import "Singleton.h"
#import "JSON.h"
#import "UIImageView+WebCache.h"
#import "ProgressHUD.h"

@interface mention_details ()
{
    NSMutableArray *item_array;
    UIImageView *Image3;
    UIImageView *arrowIcon;
    NSString *productName;
    NSString *unit;
    NSString *vendrNme;
    UIButton *btn,*btn1,*ordernow,*askPrice,*openInstagram,*btn2,*confrmBtn;
    UILabel *cartLbl,*sorry,*you,*blackLbl,*whiteBack,*costText3,*costText2;
    UIImageView *Image1,*prod,*Image2,*Image4,*prodImage,*cart_img,*image3;
    NSString *staffPic;


    
}
@end

@implementation mention_details



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    singlogin = [Singleton instance];
    
    
    _litem_tableview.delegate=self;
    _litem_tableview.dataSource=self;
    _litem_tableview.backgroundColor=[UIColor clearColor];
    
    NSArray *detail_array=[[NSArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"mentions"]];
    item_array=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<detail_array.count; i++) {
        
        if ([[[detail_array objectAtIndex:i] valueForKey:@"uid"] isEqualToString:_idstr]) {
            
            [item_array addObject:[detail_array objectAtIndex:i]];
            
        }
        
    }
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        _arebic_back.hidden=YES;
        _title_lbl.text=@"Mentions";
        _arebicTittle_lbl.hidden=YES;
        
    }else{
        _back.hidden=YES;
        _arebicTittle_lbl.text=@"توصيات من الأصدقاء";
        _title_lbl.hidden=YES;
    }
    
    
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 400;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
    
    
    return item_array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    mentionDetails_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"details_cell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"mentionDetails_Cell" bundle:nil] forCellReuseIdentifier:@"details_cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"details_cell"];
    }

    
    UILabel *name1=(UILabel *)[cell viewWithTag:1];
    name1.text=[NSString stringWithFormat:@"%@",[[item_array objectAtIndex:indexPath.row]objectForKey:@"vname"]];

    
    UIImageView *img=(UIImageView *)[cell viewWithTag:2];
    NSString *strImageUrl1 = [[item_array objectAtIndex:indexPath.row]objectForKey:@"productimage"];
    [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@""]];
  
    
    UILabel *product_name=(UILabel *)[cell viewWithTag:3];
    product_name.text=[NSString stringWithFormat:@"%@",[[item_array objectAtIndex:indexPath.row]objectForKey:@"productname"]];
    
    
    
    if ([[[item_array objectAtIndex:indexPath.row]objectForKey:@"productprice"] isEqualToString:@"0"]) {
        UILabel *product_price=(UILabel *)[cell viewWithTag:4];
        product_price.text=@"Sold Out";
        
       UIButton *cart_btn=(UIButton *)[cell viewWithTag:7];
        cart_btn.hidden=YES;
        UIImageView *img=(UIImageView *)[cell viewWithTag:6];
        img.hidden=YES;
    }else{
    
    UILabel *product_price=(UILabel *)[cell viewWithTag:4];
    product_price.text=[NSString stringWithFormat:@"%@",[[item_array objectAtIndex:indexPath.row]objectForKey:@"productprice"]];
    
        UIButton *cart_btn=(UIButton *)[cell viewWithTag:7];
        cart_btn.hidden=NO;
     
        [cart_btn addTarget:self action:@selector(addtocart:) forControlEvents:UIControlEventTouchUpInside];
        
        cart_btn.tag=indexPath.row;
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:6];
        img.hidden=NO;
        
    }
    
    UITextView *product_desc=(UITextView *)[cell viewWithTag:5];
    product_desc.text=[NSString stringWithFormat:@"%@",[[item_array objectAtIndex:indexPath.row]objectForKey:@"productdesc"]];
    
    
    return cell;
    
    
    
}


-(void)addtocart:(UIButton *)sender{
    
    int a = [sender tag];
    
  
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addtocart.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *productidStr=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"pid"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",productidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *imagee=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"productimage"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",imagee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *price1=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"productprice"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",price1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *pic1=[[NSString alloc]initWithFormat:@"%@",singlogin.userproilePic];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userprofilepic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",pic1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *object=[[NSString alloc]initWithFormat:@"%@",singlogin.usrObjctId];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",object] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *object1=[[NSString alloc]initWithFormat:@"%@",singlogin.objectIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",object1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
//        NSString *askprice1=[[NSString alloc]initWithFormat:@"%@",askForPrice];
//        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"%@",askprice1] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
                   NSString *statusidStr=@"0";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        
        NSString *final=@"1";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantityprod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",final] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *name11=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"productname"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",name11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"vimage"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"vid"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",[[item_array objectAtIndex:a]objectForKey:@"vname"]];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        NSInteger j = [singlogin.totalCart integerValue];
        j = j+1 ;
        singlogin.totalCart = [NSString stringWithFormat:@"%ld",(long)j];
        NSLog(@"%@",singlogin.totalCart) ;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Product added to cart" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [ProgressHUD showSuccess:@""];
        
        
    }

}

//-(void)cartTotalView
//{
//    if (singlogin.userid == nil)
//    {
//        chrtLbl.hidden=YES;
//    }
//    else
//    {
//        if ([singlogin.totalCart isEqualToString:@"0"])
//        {
//            chrtLbl.hidden=YES;
//        }
//        else if (singlogin.totalCart ==nil)
//        {
//            chrtLbl.hidden=YES;
//        }
//        else
//        {
//            chrtLbl.hidden=NO;
//            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
//            chrtLbl.clipsToBounds=YES;
//            chrtLbl.text=singlogin.totalCart;
//            
//        }
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)backAR_action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
