//
//  Vendor_Shop_RegtAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 01/05/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface Vendor_Shop_RegtAR : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>

{
    
    Singleton *singlogin;
    
    /* -------- Array ------- */
    
    NSMutableArray *vendorNameArray,*dataArray,*aray_2;
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    
    NSMutableArray *imageArr,*img2;
    
    NSArray *sectionAnimals;
    
    
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2,*stringTwo3;
    NSString *itme;
    
    NSString *instaNameSave, *shopNmberSave, *shopCatSave, *emailSave;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    
    
    IBOutlet UIImageView *profile;
    
    IBOutlet UIView *thanksView;
    
    BSKeyboardControls *keyboardControls;
    
    UIImageView  *Image2;
    
    IBOutlet UIActivityIndicatorView *indicatr;
    
    IBOutlet UITextField *emailText;
    
    IBOutlet UIView *showTable;
    
    IBOutlet UITextField *instaNameText;
    
    IBOutlet UITextField *shopNmbrText;
    
    IBOutlet UITextField *shopCatText;

}

@end
