//
//  User_LoginAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface User_LoginAR : UIViewController<UITextFieldDelegate>

{
    Singleton *singlogin;
    
    NSString *forgotPass,*usremail,*newPasswrd;
    
    /* -------- Outtlets -------- */
    
    IBOutlet UITextField *emai;
    IBOutlet UITextField *passwrd;
    IBOutlet UIActivityIndicatorView *indicatr;
    NSMutableArray *cartListArray;

}
- (IBAction)termsAction:(id)sender;

@end
