//
//  ShopSearch.m
//  Vetrina
//
//  Created by Amit Garg on 5/7/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "ShopSearch.h"
#import "AsyncImageView.h"
#import "Search.h"
#import "JSON.h"
#import "AsyncImageView.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "Vendor_Detaillist.h"
#import "ItemSearch.h"
#import "ViewController.h"
#import "All_SignIn.h"
#import "CartVendorList.h"
#import "CartProductList.h"
#import "ProgressHUD.h"
#import "Favorites.h"
#import "RecentView.h"
#import "Vendor_Product.h"
#import "Vendor_items.h"
#import "UIImageView+WebCache.h"
#import "WSCoachMarksView.h"

@interface ShopSearch ()
{
   IBOutlet UIView *navView;
   IBOutlet UIView *vendorViewBar;
    UITapGestureRecognizer *tapRecognizer;
    UIView *new;
}
@end

@implementation ShopSearch
@synthesize indicator;
- (void)viewDidLoad

{
    [super viewDidLoad];
    singletonn=[Singleton instance];
    
    CGRect screen=[[UIScreen mainScreen] bounds];
   
    
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    
    NSString *st= [[NSUserDefaults standardUserDefaults] valueForKey:@"coachMarker"];

    if ([st isEqualToString:@"one"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"two" forKey:@"coachMarker"];

    
     new = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screen.size.width  , screen.size.height)];
      [self.view addSubview:new];
    
    
    
    UIImageView *arraow=[[UIImageView alloc]initWithFrame:CGRectMake(screen.size.width-80, 65, 55, 55)];
    
    arraow.image=[UIImage imageNamed:@"arrow-down"];
    [new addSubview:arraow];
    
    UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(screen.size.width-270, 90, 200, 40)];
    lblMsg.text=@"List by SHOPS or ITEMS";
        lblMsg.font=[UIFont fontWithName:@"OpenSans-Bold" size:14];
    lblMsg.textColor=[UIColor blackColor];
    lblMsg.backgroundColor=[UIColor whiteColor];
    lblMsg.textAlignment=NSTextAlignmentCenter;
    [new addSubview:lblMsg];
  
    lblMsg.layer.cornerRadius=3.0f;
    lblMsg.layer.masksToBounds=YES;
    [self.view addGestureRecognizer:tapRecognizer];
    
    new.backgroundColor=[UIColor colorWithWhite:0.f alpha:0.6];
      }
   
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, searchText.frame.size.height)];
    searchText.leftView = leftView1;
    searchText.leftViewMode = UITextFieldViewModeAlways;
    [searchText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
   
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;

    
    // Tableview///
    
    searchTable.layer.shadowRadius = 5.0f;
    searchTable.layer.shadowOffset = CGSizeMake(0, 2);
    searchTable.layer.shadowColor = [UIColor blackColor].CGColor;
    searchTable.layer.shadowOpacity = 0.6f;

    
    
    
//    shopesBtn.layer.cornerRadius = 5.0;
//    itemsBtm.layer.masksToBounds = YES;
//    itemsBtm.layer.cornerRadius = 5.0;
//    itemsBtm.layer.borderWidth = 2.0;
//    itemsBtm.layer.borderColor = [[UIColor whiteColor]CGColor];
//    itemsBtm.clipsToBounds = YES;
//    itemsBtm.layer.opaque = NO;
    
    searchTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    [self vendorNameList];
    [self accessTokenList];
    
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];

    
    view1.hidden=YES;
    chrtLbl.hidden=YES;
    msg_countLbl.hidden=YES;
    msg_countVen.hidden=YES;
    catl_lbl.hidden=YES;
    
    if ([singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        navView.hidden = YES;
        vendorViewBar.hidden = NO;
        
        msg_countVen.hidden=YES;
        catl_lbl.hidden=YES;
        if (singletonn.count_msg >0) {
            
            msg_countVen.hidden=NO;
            msg_countVen.layer.cornerRadius=8.0f;
            msg_countVen.layer.masksToBounds=YES;
            msg_countVen.text=[NSString stringWithFormat:@"%d",singletonn.count_msg];
        }
       
        if (singletonn.count_cart >0) {
            
            
            catl_lbl.hidden=NO;
            catl_lbl.layer.cornerRadius=8.0f;
            catl_lbl.layer.masksToBounds=YES;
            catl_lbl.text=[NSString stringWithFormat:@"%d",singletonn.count_cart];
            
            
        }
        
        

        
        
    }
    else if ([singletonn.loginStatus isEqualToString:@"User Login"])
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        msg_countLbl.hidden=YES;
        
        if (singletonn.count_msg >0) {
            
            msg_countLbl.hidden=NO;
            msg_countLbl.layer.cornerRadius=8.0f;
            msg_countLbl.layer.masksToBounds=YES;
            msg_countLbl.text=[NSString stringWithFormat:@"%d",singletonn.count_msg];
        }
        
        

        [self cartTotalView];
        
    }
    else
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        msg_countLbl.hidden=YES;

        [self cartTotalView];
        
    }

    NSArray *fields = @[searchText];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    [keyboardControls setDoneTitle:@"Search"];

    
    
    
}

//- (void)coachMarksView:(MPCoachMarksView*)coachMarksView willNavigateToIndex:(NSUInteger)index{
//    
//}
//- (void)coachMarksView:(MPCoachMarksView*)coachMarksView didNavigateToIndex:(NSUInteger)index{
//    
//}
//- (void)coachMarksViewWillCleanup:(MPCoachMarksView*)coachMarksView{
//    
//}
//- (void)coachMarksViewDidCleanup:(MPCoachMarksView*)coachMarksView{
//    
//}
//- (void)coachMarksViewDidClicked:(MPCoachMarks *)coachMarksView atIndex:(NSInteger)index{
//    
//}

-(void)tapped
{
    [self.view removeGestureRecognizer:tapRecognizer];
    new.hidden=YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)okay:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)vendorNameList
{
    
    
    NSString *post = [NSString stringWithFormat:@"vendorname=%@",searchText.text];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/search.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"search  list"]isEqual:@"No vendor found"])
    {
        array1 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array1 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"search  list"]];
        
        
    }
    if (array1.count==0)
    {
        noPrductView.hidden= NO;
       [searchTable setHidden:YES];
    }
    else
    {
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [array1 sortedArrayUsingDescriptors:sortedarray1];
        [searchTable setHidden:NO];
        noPrductView.hidden= YES;
    }
    [ProgressHUD showSuccess:@""];
//    if([singletonn.searchShopArray count]==0)
//    {
////        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"No Shops available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
////        [alert show];
//        noPrductView.hidden= NO;
//        [searchTable setHidden:YES];
//    }
//    else
//    {
//       array1=[[NSMutableArray alloc]init];
//       array1=singletonn.searchShopArray;
//       NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
//        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
//         sortedarray1 = [array1 sortedArrayUsingDescriptors:sortedarray1];
//        
//        noPrductView.hidden= YES;
//
//    
////    imagearrayLIst = [[NSMutableArray alloc]init];
////    for(int i =0;i<sortedarray1.count;i++)
////    {
////        image2 =[[UIImageView alloc]init];
////        UIGraphicsBeginImageContext(image2.image.size);
////        {
////            CGContextRef ctx = UIGraphicsGetCurrentContext();
////            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
////            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, image2.image.size.height));
////            CGContextConcatCTM(ctx, trnsfrm);
////            CGContextBeginPath(ctx);
////            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height));
////            CGContextClip(ctx);
////            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height), image2.image.CGImage);
////            image2.image = UIGraphicsGetImageFromCurrentImageContext();
////            UIGraphicsEndImageContext();
////        }
////        [[AsyncImageLoader sharedLoader] cancelLoadingURL:image2.imageURL];
////        image2.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"imagee"]]];
////        [imagearrayLIst addObject:image2];
////    }
////    imageArray=[[NSMutableArray alloc]init];
////    imageArray=imagearrayLIst;
//}
    [searchTable reloadData];
    
}


-(void)cartTotalView
{
    if (singletonn.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singletonn.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singletonn.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singletonn.totalCart;
            
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sortedarray1 count];;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    for(UIView *v in cell.contentView.subviews)
    {
        [v removeFromSuperview];
    }
    singletonn.vendorNameList=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    arrowIcon =[[UIImageView alloc]init];
    image2 =[[UIImageView alloc]init];
    image3 = [[UIImageView alloc]init];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            image2.frame = CGRectMake(10, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 200, 30)];
            costText1.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            arrowIcon.frame=CGRectMake(300, 18, 8, 13);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 320, 1)];
            image3.frame = CGRectMake(20, -13, 44, 44);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            image2.frame = CGRectMake(10, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 200, 30)];
            costText1.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            arrowIcon.frame=CGRectMake(300, 18, 8, 13);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 320, 1)];
            image3.frame = CGRectMake(20, -13, 44, 44);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            image2.frame = CGRectMake(10, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 280, 30)];
            costText1.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            arrowIcon.frame=CGRectMake(355,18, 8, 13);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 375, 1)];
            image3.frame = CGRectMake(22, -14, 44, 44);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            image2.frame = CGRectMake(10, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 300, 30)];
            costText1.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            arrowIcon.frame=CGRectMake(394,18, 8, 13);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
            image3.frame = CGRectMake(20, -13, 44, 44);
        }
    }
    else
    {
        arrowIcon.frame=CGRectMake(684, 20, 7, 12);
    }
    
    
//    [[AsyncImageLoader sharedLoader] cancelLoadingURL:image2.imageURL];
//    image2.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"imagee"]]];
    NSString *strImageUrl1 = [[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"imagee"];
    [[NSUserDefaults standardUserDefaults] setObject:strImageUrl1 forKey:@"ven_pic"];
    [image2 sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
 //   image2 = [imagearrayLIst objectAtIndex:indexPath.row];
    image2.layer.borderWidth=1.0f;
    image2.layer.borderColor=[UIColor whiteColor].CGColor;
    image2.clipsToBounds=YES;
    [cell.contentView addSubview:image2];
    
    
    
    
    staffPic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"staffpick"];
    
    
    if ([staffPic isEqualToString:@"yes"])
    {
        image3.hidden = NO;
    }
    else
    {
        image3.hidden = YES;
    }
    image3.image=[UIImage imageNamed:@"PremiumClub.png"];
    [cell.contentView addSubview:image3];

    
    
    costText1.text =[NSString stringWithFormat:@"%@",singletonn.vendorNameList];
    costText1.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    costText1.backgroundColor =[UIColor clearColor];
    costText1.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
    costText1.numberOfLines = 1;
    costText1.textAlignment = NSTextAlignmentLeft;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:costText1];
    arrowIcon.image=[UIImage imageNamed:@"right-normal.png"];
    [cell.contentView addSubview:arrowIcon];
    lineLbl.backgroundColor =[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    [cell.contentView addSubview:lineLbl];
    cell.backgroundColor =[UIColor  whiteColor ];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    singletonn.venName = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    singletonn.SlctVenId=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"id"];
    singletonn.vendorProfilepic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"imagee"];
    singletonn.fUserId = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"instagramid"];
    singletonn.vEnPhone = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"phoneno"];
    singletonn.objectIdStr = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];
    singletonn.vid = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"username_instagramacnt"];

    [self insta];
    
    [self performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
     [indicator startAnimating];
}
-(void)vendorlist
{
        Vendor_Detaillist *vender = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:nil];
        [self.navigationController pushViewController:vender animated:NO];
        [indicator stopAnimating];
        [ProgressHUD showSuccess:@""];

    
}

-(void)insta
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",singletonn.fUserId);
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,singletonn.fUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        singletonn.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            NSDictionary *dynamicDict=[[NSDictionary alloc]init];
            dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
            
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
        }
    }
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singletonn.newarayList1 = newdatalist;
}


-(void)accessTokenList

{
    NSURL * url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/tokenlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        //If data were received
        if (data)
        {
            //Convert to string
            NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSLog(@"GetData--%@",result);
            NSDictionary *eventarray=[result JSONValue];
            NSLog(@"GetDatadictt--%@",eventarray);
            
            if(![[eventarray objectForKey:@"list"] isEqual:@"No subcategory available"])
            {
                array5=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"list"]];
            }
            else
            {
                array5=[[NSMutableArray alloc]init];
                
            }
            if([array5 count]==0)
            {
                
            }
            else
            {
                accesToken = [[array5 objectAtIndex:0]valueForKey:@"accesstoken"];
            }
            
        }
        //No data received
        else
        {
            NSString *errorText;
            //Specific error
            if (error)
                errorText = [error localizedDescription];
            //Generic error
            else
                errorText = @"An error occurred when downloading the list of issues. Please check that you are connected to the Internet.";
            
            //Show error
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [ProgressHUD showError:@""];

            [indicator stopAnimating];
        }
    }];
    NSLog(@"Done");
}


- (IBAction)chart:(id)sender
{
    if (singletonn.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [ProgressHUD showError:@""];

        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    }
}


-(void)nextpage
{
    CartProductList *cart = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:nil];
    [self.navigationController pushViewController:cart animated:YES];
    [ProgressHUD showSuccess:@""];

    [indicator setHidden:YES];
    [indicator stopAnimating];
}


- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singletonn.loginfrom = @"cart";
    All_SignIn *signIn = [[All_SignIn alloc]initWithNibName:@"All_SignIn" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}


- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}


- (IBAction)back:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(backBtn) withObject:nil afterDelay:0.5f];
}

-(void)backBtn
{
    [self.navigationController popViewControllerAnimated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)itemBtn:(id)sender
{     [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(item) withObject:nil afterDelay:0];
}


-(void)item
{
    ItemSearch *items = [[ItemSearch alloc]initWithNibName:@"ItemSearch" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];

    [indicator stopAnimating];
}


-(BOOL)textFieldShouldReturn:(UISearchBar *)textField
{
    [searchText resignFirstResponder];
    return TRUE;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [searchText resignFirstResponder];
    
}


- (IBAction)textDidChange:(id)textField
{
    
    NSString * match = searchText.text;
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"fullname_shopname CONTAINS[c] %@", match];
    NSArray *searchArray1,*listFiles1;
    searchArray1=[singletonn.searchShopArray mutableCopy];
    listFiles1 = [NSArray arrayWithArray:[searchArray1 filteredArrayUsingPredicate:predicate]];
   
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"fullname_shopname"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [listFiles1 sortedArrayUsingDescriptors:sortDescriptors];
    sortedarray1=[sortedArray mutableCopy];
    imagearrayLIst = [[NSMutableArray alloc]init];
//    for(int i =0;i<sortedarray1.count;i++)
//    {
//        image2 =[[UIImageView alloc]init];
//        UIGraphicsBeginImageContext(image2.image.size);
//        {
//            CGContextRef ctx = UIGraphicsGetCurrentContext();
//            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, image2.image.size.height));
//            CGContextConcatCTM(ctx, trnsfrm);
//            CGContextBeginPath(ctx);
//            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height));
//            CGContextClip(ctx);
//            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height), image2.image.CGImage);
//            image2.image = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//        }
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:image2.imageURL];
//        //[indicator stopAnimating];
//        image2.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"imagee"]]];
//        [imagearrayLIst addObject:image2];
//    }

    if ([searchText.text isEqualToString:@""])
    {
        array1=[[NSMutableArray alloc]init];
        array1=singletonn.searchShopArray;
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"fullname_shopname" ascending:YES];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [array1 sortedArrayUsingDescriptors:sortedarray1];
      //  imagearrayLIst=[[NSMutableArray alloc]init];
      //  imagearrayLIst=imageArray;
    }
    
    [searchTable reloadData];
}
#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendorNameList) withObject:nil afterDelay:0.2f];

  //  [self vendorNameList];
    [searchTable reloadData];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
      [keyboardControls setActiveField:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
}


-(void)viewWillAppear:(BOOL)animated
{
    [self cartTotalView];
}


- (IBAction)favrut:(id)sender
{
    if (singletonn.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [ProgressHUD showError:@""];
        
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(favrt) withObject:nil afterDelay:0.5f];
    }

}



-(void)favrt
{
    Favorites *view = [[Favorites alloc]initWithNibName:@"Favorites" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];

}






- (IBAction)chat:(id)sender
{
    if (singletonn.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [ProgressHUD showError:@""];
        
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(chatview) withObject:nil afterDelay:0.5f];
    }
}


-(void)chatview
{
    RecentView *chat = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:chat animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)vendorProductbtn:(id)sender
{
    
    if ([singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorProductBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorProductBtn
{
    Vendor_Product *view = [[Vendor_Product alloc]initWithNibName:@"Vendor_Product" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorChat:(id)sender
{
    if ([singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorchatBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorchatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorItems:(id)sender
{
    if ([singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorItemsBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorItemsBtn
{
    Vendor_items *view = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)homeBtn:(id)sender
{
    
   
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
    
}


-(void)home
{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
