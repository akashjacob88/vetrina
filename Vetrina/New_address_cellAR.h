//
//  New_address_cellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/27/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface New_address_cellAR : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIImageView *locationImage;

@end
