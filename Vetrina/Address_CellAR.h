//
//  Address_CellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/27/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Address_CellAR : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;

@end
