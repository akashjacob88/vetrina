//
//  UploadpicAR.h
//  Vetrina
//
//  Created by Amit Garg on 9/21/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface UploadpicAR : UIViewController
{
    Singleton * signLogin;
    
    /* ----------- All Arrays ---------- */
    
    NSMutableArray *imagesArray;
    
    NSMutableArray *imgArray,*cartListArray;
    
    NSMutableArray *animationNrmlViewImg;
    
    NSMutableArray *swipImagesArray;
    
    /* ----------- IBoutlets ---------- */
    
    IBOutlet UIButton *startShopingBtn;
}

@end
