//
//  Vendor_ProductCell.m
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_ProductCell.h"

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

#import "AppConstant.h"
#import "converter.h"

@implementation Vendor_ProductCell

{
    PFObject *recent;
}
@synthesize msgTxt, timeLbl,totalLbl;


- (void)bindData:(PFObject *)recent_
{
    recent = recent_;
   
    
    NSString * naem = recent[@"lastMessage"];

    msgTxt.text = naem;

    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:recent[PF_RECENT_UPDATEDACTION]];
    timeLbl.text = TimeElapsed(seconds);
    int counter = [recent[PF_RECENT_COUNTER] intValue];
    totalLbl.text = (counter == 0) ? @"" : [NSString stringWithFormat:@"%d new", counter];

}



- (void)awakeFromNib

{
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
