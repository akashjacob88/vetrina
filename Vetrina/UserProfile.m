//
//  UserProfile.m
//  Vetrina
//
//  Created by Amit Garg on 9/25/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "UserProfile.h"
#include "mentions.h"
#import "Walkthrough.h"
#import "User_Settings.h"

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ProgressHUD.h"

#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"
#import "Base64.h"
#import "JSON.h"
#import "PremiumItems.h"
#import "UIImageView+WebCache.h"
#import "ARSpeechActivity.h"
#import "Friends_view.h"
#import "orderHistory.h"

@interface UserProfile ()<UITextFieldDelegate>

{
    IBOutlet UILabel *imageLbl;
    
}
@property (nonatomic, strong) UITextField *textField;

@end

@implementation UserProfile

@synthesize test;

- (void)viewDidLoad
{
    [super viewDidLoad];
    singlogin = [Singleton instance];
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    

  
    NSString *strImageUrl1 = singlogin.userproilePic;
    [profilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
    profilePic.layer.masksToBounds = YES;
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.layer.opaque = NO;

    
//    profilePic.layer.masksToBounds = YES;
//    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
//    profilePic.clipsToBounds = YES;
//    [profilePic setAlpha:0.7];
//    profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
//    profilePic.layer.shadowOffset = CGSizeMake(-.5f, .2f);
//    profilePic.layer.shadowRadius = 3;
//    UIBezierPath *path2 = [UIBezierPath bezierPathWithRect:profilePic.bounds];
//    profilePic.layer.shadowPath = path2.CGPath;
//    profilePic.layer.shadowOpacity = 0.8;
    
    
    view1.layer.shadowRadius = 2.0f;
    view1.layer.shadowOffset = CGSizeMake(0, 2);
    view1.layer.shadowColor = [UIColor blackColor].CGColor;
    view1.layer.shadowOpacity = 0.5f;
    
    

    
    view2.layer.shadowRadius = 2.0f;
    view2.layer.shadowOffset = CGSizeMake(0, 2);
    view2.layer.shadowColor = [UIColor blackColor].CGColor;
    view2.layer.shadowOpacity = 0.5f;
    


    // Do any additional setup after loading the view from its nib.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)back:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(back) withObject:nil afterDelay:0];
}

-(void)back
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self.navigationController popViewControllerAnimated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)wishListAct:(id)sender
{
    
    [self performSelector:@selector(wishlist) withObject:nil afterDelay:0];
}


-(void)wishlist
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    self.textField = [[UITextField alloc] init];
    [self.textField setDelegate:self];
    [self.textField setText:@"“I’m using Vetrina, the shopping app that has everything”: https://VetrinaApp.com"];
    [self.textField setFrame:CGRectMake((screenSize.width/2)-135, (screenSize.height/2)-90, 270, 60)];
    self.textField.textColor = [UIColor clearColor];
    self.textField.enabled = NO;
    [self.textField setReturnKeyType:UIReturnKeyDone];
    [self.view addSubview:self.textField];
    
    
    ARSpeechActivity *speechActivity = [[ARSpeechActivity alloc] init];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[self.textField.text] applicationActivities:@[speechActivity]];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:activityVC animated:YES completion:nil];
    }
    else
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        NSLog(@"%f",self.view.frame.size.width/2);
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }


}

- (IBAction)friendsAct:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(friend) withObject:nil afterDelay:0.2f];
}


-(void)friend
{
    Friends_view *view = [[Friends_view alloc]initWithNibName:@"Friends_view" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)mentionAct:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];

    mentions *obj=[[mentions alloc]initWithNibName:@"mentions" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
    [ProgressHUD showSuccess:@""];

}


- (IBAction)settignAct:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:0.2f];
}


-(void)home
{
    User_Settings *view = [[User_Settings alloc]initWithNibName:@"User_Settings" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)logoutAct:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Log Out" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 10;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 10)
    {
        if (buttonIndex==0)
        {
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"passWord"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"instagramID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"english"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"arabic"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            singlogin.loginStatus = nil;
            singlogin.userid = nil;
            
            
            [QBRequest logOutWithSuccessBlock:^(QBResponse *response) {
                
                NSLog(@"QuickBlox Logout Successfully");
                
                [self unsub];
                
                
                
                
                // Successful logout
            } errorBlock:^(QBResponse *response) {
                // Handle error
            }];
            
            
//            [PFUser logOut];
//            ParsePushUserResign();
//            PostNotification(NOTIFICATION_USER_LOGGED_OUT);
//            LoginUser(self);
            Walkthrough *home = [[Walkthrough alloc]initWithNibName:@"Walkthrough" bundle:NULL];
            [self.navigationController pushViewController:home animated:YES];
        }
    }
    
}

-(void)unsub{
    NSString *deviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [QBRequest unregisterSubscriptionForUniqueDeviceIdentifier:deviceUdid successBlock:^(QBResponse *response) {
        // Unsubscribed successfully
    } errorBlock:^(QBError *error) {
        // Handle error
    }];
}
- (IBAction)addImageuser:(id)sender
{
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@"Choose Imgae"
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Gallery",@"Camera",nil];
    actionsheet1.tag=12;
    [actionsheet1 showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
    }
    
    if (buttonIndex==1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypeCamera;
                camerapicker.allowsEditing=YES;
                camerapicker.automaticallyAdjustsScrollViewInsets=YES;
                
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
        
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error acessing camera"
                                  message:@"Device does not support a camera"
                                  delegate:nil
                                  cancelButtonTitle:@"Dismiss"
                                  otherButtonTitles:nil];
            alert.tag=44;
            [alert show];
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(actionsheet1.tag==12)
    {
        cimage=[info objectForKey:UIImagePickerControllerEditedImage];
        
        CGRect rect = CGRectMake(0,0,cimage.size.width/2,cimage.size.height/2);
        UIGraphicsBeginImageContext( rect.size );
        [cimage drawInRect:rect];
        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageDataForResize = UIImagePNGRepresentation(picture1);
        UIImage *pimage=[UIImage imageWithData:imageDataForResize];
        
        picture = ResizeImage(pimage, 250, 250);
        thumbnail = ResizeImage(pimage, 60, 60);
        
        [profilePic setImage:pimage];
        
        
    }
    [camerapicker dismissViewControllerAnimated:YES completion:nil];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self updatePic];
}


-(void)updatePic

{
    
    
    [self PROFILEPIC];
    
    NSData *myimagedata = UIImageJPEGRepresentation(profilePic.image, 90);
    
    
    
    
    
    
    
    
    
    
//    PFFile *filePicture = [PFFile fileWithName:@"picture.jpg" data:UIImageJPEGRepresentation(picture, 0.6)];
//    [filePicture saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    PFFile *fileThumbnail = [PFFile fileWithName:@"thumbnail.jpg" data:UIImageJPEGRepresentation(thumbnail, 0.6)];
//    [fileThumbnail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    PFUser *user = [PFUser currentUser];
//    user[PF_USER_PICTURE] = filePicture;
//    user[PF_USER_THUMBNAIL] = fileThumbnail;
//    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//
    
    
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/edituserimg.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[Base64 encode:myimagedata]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSMutableArray *valu = [val valueForKey:@"message"];
    
     NSString *venPic=[[valu objectAtIndex:0] valueForKey:@"photo"];
    singlogin.userproilePic = venPic;
    
    [ProgressHUD showSuccess:@""];
}


-(void)PROFILEPIC{
    
    NSData *myimagedata = UIImageJPEGRepresentation(profilePic.image, 90);

   // NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed: @"profile-img"], 0.8f);
    
    [QBRequest TUploadFile: myimagedata fileName: @"ProfilePicture"
               contentType: @"image/jpeg"
                  isPublic: YES successBlock: ^ (QBResponse * response, QBCBlob * blob) {
                      
                      // File uploaded, do something
                      // if blob.isPublic == YES
                      NSString * url = [blob publicUrl];
                      
                      NSLog(@"%@",url);
                      
                      
                      QBUpdateUserParameters *params = [QBUpdateUserParameters new];
                      params.blobID = [blob ID];
                      params.customData=url;
                      [QBRequest updateCurrentUser:params successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                          // success block
                      } errorBlock:^(QBResponse * _Nonnull response) {
                          // error block
                          NSLog(@"Failed to update user: %@", [response.error reasons]);
                      }];
                      
                      
                      
                      
                      
                      
                      
                      
                  }
               statusBlock: ^ (QBRequest * request, QBRequestStatus * status) {
                   // handle progress
               }
                errorBlock: ^ (QBResponse * response) {
                    NSLog(@"error: %@", response.error);
                }
     ];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)oderHistory_Action:(id)sender {
    
    orderHistory *cart=[[orderHistory alloc]initWithNibName:@"orderHistory" bundle:nil];
    [self.navigationController pushViewController:cart animated:NO];
        
    
    
}
@end
