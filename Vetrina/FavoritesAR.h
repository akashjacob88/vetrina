//
//  FavoritesAR.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface FavoritesAR : UIViewController

{
    Singleton *singemplist,*singlogin;
    
    IBOutlet UILabel *count_lbl;
    IBOutlet UITableView *vendorTabl;
    IBOutlet UIActivityIndicatorView *indicator;
    
    UITableView *subcategories;
    UIImageView *Image2,*arrowIcon;
    
    NSDictionary *dynamicDict,*dynamicDict2;
    NSMutableArray *arra_1,*aray_2,*newdatalist,*aray_3,*aray_4,*imagearrayLIst,*array_5,*array;
    BOOL isfilter;
    NSString *name2,*desc2,*catid,*subname2,*subdesc2,*subcatid,*scatName,*accesToken;
    UILabel *costText1,*lbl1;
    NSArray *sortedArray,*sortedArray1;
    
    NSString *venListStatus,*appear;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UIView *viewImprt;
    IBOutlet UIButton *shop;
    IBOutlet UIButton *itemsOutlet;
}

@property (nonatomic, strong) NSString *addproduct;


@end
