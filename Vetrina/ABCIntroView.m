//
//  IntroView.m
//  DrawPad
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import "ABCIntroView.h"

@interface ABCIntroView () <UIScrollViewDelegate>
@property (strong, nonatomic)  UIScrollView *scrollView;
@property (strong, nonatomic)  UIPageControl *pageControl;
@property UIView *holeView;
@property UIView *circleView;
@property UIButton *doneButton;

@end

@implementation ABCIntroView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
                self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
                self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.9, self.frame.size.width, 10)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
                self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
                self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.9, self.frame.size.width, 10)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];
                self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];
                self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.9, self.frame.size.width, 10)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];
                self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];
                self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.9, self.frame.size.width, 10)];
            }
        }
        
        backgroundImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:backgroundImageView];
        
        self.scrollView.pagingEnabled = YES;
        [self addSubview:self.scrollView];
        
       
        self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        [self addSubview:self.pageControl];
    
        [self createViewOne];
        [self createViewTwo];
        [self createViewThree];
        
        
        
        self.pageControl.numberOfPages = 3;
        self.scrollView.contentSize = CGSizeMake(self.frame.size.width*3, self.scrollView.frame.size.height);
        
        //This is the starting point of the ScrollView
        CGPoint scrollPoint = CGPointMake(0, 0);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
    return self;
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
   CGFloat pageWidth = CGRectGetWidth(self.bounds);
   CGFloat pageFraction = self.scrollView.contentOffset.x / pageWidth;
   self.pageControl.currentPage = roundf(pageFraction);
    
}


-(void)createViewOne
{
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
            imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
            imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];
            imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];
            imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];

        }
    }

    
    view1.backgroundColor=[UIColor clearColor];
       
    
    imageview1.backgroundColor=[UIColor clearColor];
    imageview1.contentMode = UIViewContentModeScaleAspectFit;
    imageview1.image = [UIImage imageNamed:@"sti01.png"];
    [view1 addSubview:imageview1];
    
//    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width*.1, self.frame.size.height*.7, self.frame.size.width*.8, 60)];
//    descriptionLabel.text = [NSString stringWithFormat:@"Description for First Screen."];
//    descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18.0];
//    descriptionLabel.textColor = [UIColor whiteColor];
//    descriptionLabel.textAlignment =  NSTextAlignmentCenter;
//    descriptionLabel.numberOfLines = 0;
//    [descriptionLabel sizeToFit];
//    [view addSubview:descriptionLabel];
    
  //  CGPoint labelCenter = CGPointMake(self.center.x, self.frame.size.height*.7);
   // descriptionLabel.center = labelCenter;
    
    self.scrollView.delegate = self;
    [self.scrollView addSubview:view1];
    
}


-(void)createViewTwo
{
    
    CGFloat originWidth = self.frame.size.width;
    CGFloat originHeight = self.frame.size.height;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(originWidth, 0, originWidth, originHeight)];
    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.05, self.frame.size.width*.8, 60)];
//    titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
//    titleLabel.text = [NSString stringWithFormat:@"DropShot"];
//    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:40.0];
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.textAlignment =  NSTextAlignmentCenter;
//    titleLabel.numberOfLines = 0;
//    [view addSubview:titleLabel];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            imageview2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            imageview2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            imageview2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            imageview2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];
        }
    }

    imageview2.contentMode = UIViewContentModeScaleAspectFit;
    imageview2.image = [UIImage imageNamed:@"sti02.png"];
    [view addSubview:imageview2];
    
//    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width*.1, self.frame.size.height*.7, self.frame.size.width*.8, 60)];
//    descriptionLabel.text = [NSString stringWithFormat:@"Description for Second Screen."];
//    descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18.0];
//    descriptionLabel.textColor = [UIColor whiteColor];
//    descriptionLabel.textAlignment =  NSTextAlignmentCenter;
//    descriptionLabel.numberOfLines = 0;
//    [descriptionLabel sizeToFit];
//    [view addSubview:descriptionLabel];
//    
//    CGPoint labelCenter = CGPointMake(self.center.x, self.frame.size.height*.7);
//    descriptionLabel.center = labelCenter;
    
    self.scrollView.delegate = self;
    [self.scrollView addSubview:view];
    
}

-(void)createViewThree

{
    
    CGFloat originWidth = self.frame.size.width;
    CGFloat originHeight = self.frame.size.height;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(originWidth*2, 0, originWidth, originHeight)];
    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.05, self.frame.size.width*.8, 60)];
//    titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
//    titleLabel.text = [NSString stringWithFormat:@"Shaktaya"];
//    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:40.0];
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.textAlignment =  NSTextAlignmentCenter;
//    titleLabel.numberOfLines = 0;
//    [view addSubview:titleLabel];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            imageview3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            imageview3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            imageview3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 375)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            imageview3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 414, 430)];
        }
    }

    
    
    imageview3.contentMode = UIViewContentModeScaleAspectFit;
    imageview3.image = [UIImage imageNamed:@"sti02.png"];
    [view addSubview:imageview3];
    imageview3.contentMode = UIViewContentModeScaleAspectFit;
    imageview3.image = [UIImage imageNamed:@"sti03.png"];
    [view addSubview:imageview3];
    
//    
//    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width*.1, self.frame.size.height*.7, self.frame.size.width*.8, 60)];
//    descriptionLabel.text = [NSString stringWithFormat:@"Description for Third Screen."];
//    descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18.0];
//    descriptionLabel.textColor = [UIColor whiteColor];
//    descriptionLabel.textAlignment =  NSTextAlignmentCenter;
//    descriptionLabel.numberOfLines = 0;
//    [descriptionLabel sizeToFit];
//    [view addSubview:descriptionLabel];
//    
//    CGPoint labelCenter = CGPointMake(self.center.x, self.frame.size.height*.7);
//    descriptionLabel.center = labelCenter;
    
    self.scrollView.delegate = self;
    [self.scrollView addSubview:view];
    
}



@end