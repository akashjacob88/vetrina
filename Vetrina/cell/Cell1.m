
#import "Cell1.h"

@implementation Cell1
@synthesize titleLabel,arrowImageView,iconImg;

- (void)dealloc
{
    
    self.arrowImageView = nil;
    titleLabel = nil;
    iconImg = nil;
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)changeArrowWithUp:(BOOL)up
{
    if (up)
    {
        self.arrowImageView.image = [UIImage imageNamed:@"WhiteArrowUp.png"];
    }
    else
    {
        self.arrowImageView.image = [UIImage imageNamed:@"WhiteArrowDown.png"];
    }
}

@end
