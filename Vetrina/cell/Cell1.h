
#import <UIKit/UIKit.h>

@interface Cell1 : UITableViewCell



@property (nonatomic,retain)IBOutlet UIImageView *arrowImageView;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *lineLbl;

- (void)changeArrowWithUp:(BOOL)up;


@end
