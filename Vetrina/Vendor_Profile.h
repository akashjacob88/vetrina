//
//  Vendor_Profile.h
//  Vetrina
//
//  Created by Amit Garg on 10/3/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Vendor_Profile : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

{
    Singleton *singling;
    IBOutlet UIView *topBar;
    IBOutlet UIImageView *profileVendor;
    IBOutlet UIView *view2Shadow;
    IBOutlet UIView *view1Shadow;
    
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    UIImageView  *Image2;
    UIImage *cimage,*cimage2;
    UIImage *picture ;
    UIImage *thumbnail;


    
}

- (IBAction)shopDetailsBtn:(id)sender;
- (IBAction)VendorSettingBtn:(id)sender;
- (IBAction)logOutBtn:(id)sender;
- (IBAction)orderHistory:(id)sender;



@end
