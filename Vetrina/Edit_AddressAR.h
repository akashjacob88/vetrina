//
//  Edit_AddressAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/27/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Edit_AddressAR : UIViewController

{
    
        Singleton *signlogin;
        IBOutlet UITextField *country;
        IBOutlet UITextField *city;
        IBOutlet UITextField *address;
        IBOutlet UITableView *countryTble;
        IBOutlet UITableView *cityTble;
        NSString *btnStr;
    IBOutlet UIButton *dltBtn;
        
        NSMutableArray *arra_1;
    
    /* -------- Arrays ------- */
    
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    NSMutableArray *vendorNameArray,*dataArray;
    NSArray *sectionAnimals;
    
    NSMutableArray *imageArr,*img2;
    
    
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2;
    
    NSString *itme;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    
}

- (IBAction)back_btn:(id)sender;
@property (strong, nonatomic) NSString *editaddres;


@end
