//
//  Vendor_ProductAR.m
//  Vetrina
//
//  Created by Umesh Kumar on 01/05/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_ProductAR.h"
#import "Vendor_itemsAR.h"
#import "Vendor_profileAR.h"
#import "Vendor_ProductCellAR.h"
#import "JSON.h"
#import "Base64.h"
#import "CartVendorProductListAR.h"
#import <Parse/Parse.h>
#import "ChatView.h"
#import "ProgressHUD.h"
#import "RecentView.h"

#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "Home_VetrinaARViewController.h"
#import "UIImageView+WebCache.h"

#import <Parse/Parse.h>
#import "AppConstant.h"

@interface Vendor_ProductAR ()
{
    NSMutableArray *recents;
    IBOutlet UIImageView *profilePic;
    IBOutlet UIView *topBar;
    IBOutlet UIButton *editBtn;
    NSArray *dialog_Objects;


}
@end

@implementation Vendor_ProductAR

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    
      singloging = [Singleton instance];
    
    
    _count_lbl.hidden=YES;
    recents = [[NSMutableArray alloc]init];
    [self loadRecents];
    
    
    
    //    PFQuery *query = [PFQuery queryWithClassName:PF_RECENT_CLASS_NAME];
//    [query whereKey:PF_RECENT_USER equalTo:[PFUser currentUser]];
//    [query includeKey:PF_RECENT_LASTUSER];
//    [query orderByDescending:PF_RECENT_UPDATEDACTION];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (error == nil)
//         {
//             [recents removeAllObjects];
//             [recents addObjectsFromArray:objects];
//             [userListingTable reloadData];
//             [self updateTabCounter];
//         }
//         else [ProgressHUD showError:@"Network error."];
//     }];
    

    
  
    
    NSURL *url1=[NSURL URLWithString:singloging.venPic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePic.image=emppppic;
    profilePic.layer.masksToBounds = YES;
    profilePic.layer.borderWidth = 1.0f;
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.layer.borderColor = [[UIColor clearColor]CGColor];
    profilePic.clipsToBounds = YES;
    profilePic.layer.opaque = NO;
    
    
    
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    

    
    NSLog(@"test--- %@",singloging.loginStatus);
    if ([singloging.loginStatus isEqual:@"User Login"])
    {
        NSLog(@"vendorid=%@",singloging.vid) ;
    }
    else if([singloging.loginStatus isEqual:@"IG Login"])
    {
        NSLog(@"vendorid=%@",singloging.vUniqueId) ;
    }

    vendorName.text= singloging.shopNameVendr;
    
//    orderbtn.layer.cornerRadius = 5.0;
//    
//    itembtn.layer.masksToBounds = YES;
//    itembtn.layer.cornerRadius = 5.0;
//    itembtn.layer.borderWidth = 2.0;
//    itembtn.layer.borderColor = [[UIColor whiteColor]CGColor];
//    itembtn.clipsToBounds = YES;
//    itembtn.layer.opaque = NO;
    novendrView.hidden = YES;
    [self prodListing];

    indicator.hidden=YES;
    [indicator stopAnimating];
    appear=@"yes";
    
}

- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }

    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    _count_lbl.text=[NSString stringWithFormat:@"%d", total];
    _count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singloging.userid == nil)
    {
        _count_lbl.hidden=YES;
        _count_lbl.hidden = YES;
    }
    else
    {
        if ([_count_lbl.text isEqualToString:@"0"])
        {
            _count_lbl.hidden=YES;
            _count_lbl.hidden = YES;
        }
        else if ([_count_lbl.text isEqualToString:@""])
        {
            _count_lbl.hidden=YES;
            _count_lbl.hidden = YES;
        }
        else
        {
            _count_lbl.hidden=NO;
            _count_lbl.hidden = NO;
            _count_lbl.layer.cornerRadius=_count_lbl.frame.size.height/2;
            _count_lbl.clipsToBounds=YES;
            _count_lbl.layer.cornerRadius=_count_lbl.frame.size.height/2;
            _count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }

}

- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singloging.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)SEttings:(id)sender

{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(settings)withObject:Nil afterDelay:2.0f];
}


-(void)settings
{
    Vendor_profileAR *setting = [[Vendor_profileAR alloc]initWithNibName:@"Vendor_profileAR" bundle:nil];
    [self.navigationController pushViewController:setting animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)items:(id)sender

{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(items)withObject:Nil afterDelay:2.0f];
}


-(void)items

{
    Vendor_itemsAR *items = [[Vendor_itemsAR alloc]initWithNibName:@"Vendor_itemsAR" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];

  
}


-(void)prodListing

{
    
    NSString *post1;
    if ([singloging.loginStatus isEqual:@"User Login"])
    {
        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
    }
    else if ([singloging.loginStatus isEqual:@"IG Login"])
    {
        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    }
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        novendrView.hidden = NO;
        userListingTable.hidden = YES;
        [userListingTable reloadData];
    }
    else
    {
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"productname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]initWithKey:@"orderdate"  ascending:NO];
        NSArray *sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
        NSArray *sortedArray2 = [dataArray sortedArrayUsingDescriptors:sortDescriptors1];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray2 mutableCopy];

        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"ordertime" ascending:NO];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        userNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"username"];
            
            NSString *userName1=[[sortedArray objectAtIndex:i]valueForKey:@"username"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"vendorpic"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [userNameArray addObject:userName1];
                [imageArr addObject:imagestr];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"]) {
                    
                    NSInteger a=i-1;
                    NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:userName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        [userListingTable reloadData];
        novendrView.hidden = YES;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [finalArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Vendor_ProductCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell1"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Vendor_ProductCellAR" bundle:nil] forCellReuseIdentifier:@"productCell1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"productCell1"];
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = userListingTable;
        
        refreshControl  = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(getConnections) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = refreshControl;

    }
    cell.backgroundColor=[UIColor whiteColor];
//    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 54, 414, 1)];
//    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
//    [cell.contentView addSubview:v11];
    return cell;
}


-(void)getConnections
{
    [self prodListing];
    [refreshControl endRefreshing];
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(Vendor_ProductCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [userNameArray objectAtIndex:indexPath.row];
    NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
    cell.userName.text = [[sectionAnimals objectAtIndex:0]valueForKey:@"username"];
    NSString *date = [[sectionAnimals objectAtIndex:0]valueForKey:@"orderdate"];
    NSString *time = [[sectionAnimals objectAtIndex:0]valueForKey:@"ordertime"];
    
    cell.msgTxt.text =  [NSString stringWithFormat:@"%@, %@",date,time];
     NSString *strImageUrl = [[sectionAnimals objectAtIndex:0]valueForKey:@"userprofilepic"];

    [cell.profilPic sd_setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
    
    cell.profilPic.layer.masksToBounds = YES;
    cell.profilPic.layer.borderWidth = 1.0f;
    cell.profilPic.layer.cornerRadius = cell.profilPic.frame.size.width/2;
    cell.profilPic.layer.borderColor = [[UIColor clearColor]CGColor];
    cell.profilPic.clipsToBounds = YES;
    cell.profilPic.layer.opaque = NO;
    

    
    
    
    
    
    //cell.totalLbl.text=[NSString stringWithFormat:@"%li",(unsigned long)sectionAnimals.count];
    cell.statusImage.layer.cornerRadius=cell.statusImage.frame.size.height/2;
    cell.statusImage.clipsToBounds=YES;
   // cell.totalLbl.layer.cornerRadius=cell.totalLbl.frame.size.height/2;
   // cell.totalLbl.clipsToBounds=YES;
//    
//    for (i=0; i< recents.count; i++)
//    {
//        
//        NSString *name= [[recents objectAtIndex:i]valueForKey:@"description"];
//        if ([name containsString:cell.userName.text])
//        {
//            cell.msgTxt.text = [[recents objectAtIndex:i]valueForKey:@"lastMessage"];
//            [cell bindData:recents[i]];
//            
//            i = recents.count;
//            
//        }
//    }

    
    for (i=0; i< sectionAnimals.count; i++)
    {
        NSString *statusStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
        
        if ([statusStr isEqualToString:@"2"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Ordered.png"];
        }
        if ([statusStr isEqualToString:@"1"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
            i=sectionAnimals.count;
        }
        if ([statusStr isEqualToString:@"3"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Confirmed.png"];
            i=sectionAnimals.count;
        }
        if ([statusStr isEqualToString:@"4"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Delivered.png"];
            i=sectionAnimals.count;
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    singloging.productArray=[[NSMutableArray alloc]init];
    NSString *sectionTitle = [userNameArray objectAtIndex:indexPath.row];
    singloging.productArray = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
    singloging.usrName = [[singloging.productArray objectAtIndex:0]valueForKey:@"username"];
    singloging.userid = [[singloging.productArray objectAtIndex:0]valueForKey:@"userid"];
    
    singloging.chat_user= [[singloging.productArray objectAtIndex:0]valueForKey:@"userobjectid"];
    
     singloging.namechat =[[singloging.productArray objectAtIndex:0]valueForKey:@"username"];
    
    NSLog(@"%@",singloging.chat_user);
    
    
    
    
    [self performSelector:@selector(nextPage)withObject:Nil afterDelay:0.5f];
}


-(void)nextPage
{
    CartVendorProductListAR *cart=[[CartVendorProductListAR alloc]initWithNibName:@"CartVendorProductListAR" bundle:nil];
    [self.navigationController pushViewController:cart animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] init];
//
//    if (dataArray.count == 0)
//    {
//        
//    }
//    else
//    {
//        NSString *statusStr = [[dataArray objectAtIndex:section]valueForKey:@"statusid"];
//        
//        UILabel *vendor;
//      
//       
//        
//        
//        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
//        {
//            if ([[UIScreen mainScreen] bounds].size.height == 480)
//            {
//                vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 315, 40)];
//                vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:16];
//                
//            }
//            else if ([[UIScreen mainScreen] bounds].size.height == 568)
//            {
//                vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 315, 40)];
//                vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:16];
//                
//                
//            }
//            else if ([[UIScreen mainScreen] bounds].size.height == 667)
//            {
//                vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 370, 40)];
//                vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
//                
//            }
//            else if ([[UIScreen mainScreen] bounds].size.height == 736)
//            {
//                vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 409, 40)];
//                vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
//            }
//        }
//        
//        
//        if ([statusStr isEqualToString:@"2"])
//        {
//            vendor.text =[NSString stringWithFormat:@"أوامر"];
//        }
//        if ([statusStr isEqualToString:@"1"])
//        {
//            vendor.text =[NSString stringWithFormat:@"اسأل عن سعر"];
//        }
//        if ([statusStr isEqualToString:@"3"])
//        {
//            vendor.text =[NSString stringWithFormat:@"في انتظار التسليم"];
//        }
//        if ([statusStr isEqualToString:@"4"])
//        {
//            vendor.text =[NSString stringWithFormat:@"تم التوصيل"];
//        }
//        
//        vendor.textColor =[UIColor darkGrayColor];
//        vendor.backgroundColor =[UIColor clearColor];
//        vendor.numberOfLines = 1;
//        vendor.textAlignment = NSTextAlignmentRight;
//        [headerView addSubview:vendor];
//        headerView.backgroundColor=[UIColor clearColor];
//    }
//    return headerView;
//}
//
//
//-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40;
//}
//


-(void)viewWillAppear:(BOOL)animated

{
    if (![appear isEqualToString:@"yes"])
    {
        [self prodListing];
    }
    appear=@"";
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Chat:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(chtBtn)withObject:Nil afterDelay:2.0f];
}

-(void)chtBtn
{
    RecentView *view = [[ RecentView alloc] initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)home:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeBtn)withObject:Nil afterDelay:2.0f];
}


-(void)homeBtn
{
    Home_VetrinaARViewController *items = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)edit:(id)sender
{
    if([editBtn.currentTitle isEqualToString:@"تمام"])
    {
        [super setEditing:NO animated:NO];
        [userListingTable setEditing:NO animated:NO];
        [editBtn setTitle:@"تعديل" forState:UIControlStateNormal];
        [userListingTable reloadData];
    }
    else if([editBtn.currentTitle isEqualToString:@"تعديل"])
    {
        [super setEditing:YES animated:YES];
        [userListingTable setEditing:YES animated:YES];
        [editBtn setTitle:@"تمام" forState:UIControlStateNormal];
        [userListingTable reloadData];
    }
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"حذف" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        NSArray *sectionAnimals= [dataArray objectAtIndex:indexPath.section];
                                        orderId =[sectionAnimals valueForKey:@"orderid"];
                                        [ProgressHUD show:@"Loading..." Interaction:NO];
                                        [self performSelector:@selector(deleteView1) withObject:nil afterDelay:0.5f];
                                    }];
    button.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:1.0];
    return @[button];
}


-(void)deleteView1
{
    NSString *post = [NSString stringWithFormat:@"orderid=%@",orderId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delorder.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
    [self prodListing];
    [userListingTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([editBtn.currentTitle isEqualToString:@"تمام"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
