//
//  Premium_cell.h
//  Vetrina
//
//  Created by Amit Garg on 5/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Premium_cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *vendorPic;
@property (strong, nonatomic) IBOutlet UILabel *vendorName;

@end
