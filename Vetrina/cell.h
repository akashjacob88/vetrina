//
//  cell.h
//  Vetrina
//
//  Created by Amit Garg on 11/2/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cell : NSObject

{
    BOOL dirty;
    
    // An array of NSDictionary of NSString -> PFFieldOperation.
    // Each dictionary has a subset of the object's keys as keys, and the
    // changes to the value for that key as its value.
    // There is always at least one dictionary of pending operations.
    // Every time a save is started, a new dictionary is added to the end.
    // Whenever a save completes, the new data is put into fetchedData, and
    // a dictionary is removed from the start.
    NSMutableArray *arrayObject;
    
    // Our best estimate as to what the current data is, based on
    // the last fetch from the server, and the set of pending operations.
    NSMutableDictionary *arraObjct;
}

@end
