//
//  CartProductAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CartProductAR.h"
#import "CartProductCellAR.h"
#import "AsyncImageView.h"
#import "JSON.h"
#import "Base64.h"
#import "showordeAR.h"
#import "AddressCellAR.h"
#import "New_address_cellAR.h"

#import "ChatView.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "CartVendorCellAR.h"
#import "ShowOrderCellAR.h"
#import "RecentView.h"
#import "ChatView.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "push.h"
#import "Home_VetrinaARViewController.h"
#import "FavoritesAR.h"
#import "RecentView.h"
#import "User_ProfileAR.h"

#import <Parse/Parse.h>
#import "AppConstant.h"
#import "chat_New.h"

#import "Vendor_DetaillistAR.h"
@interface CartProductAR ()
{
    NSMutableArray *users;
    IBOutlet UIView *topBar;
    IBOutlet UIImageView *profilePicuser;
    NSMutableArray *recents;
    UIActivityIndicatorView * mySpinner;
    NSString *city1;
    NSString *country1;

    NSArray *dialog_Objects;
    QBUUser *currentUser;
    NSMutableArray *users_array;
    QBChatDialog *chatDialog;
   //  NSArray *dialog_Objects22;
}

@end

@implementation CartProductAR


- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }
    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singletonn.userid == nil)
    {
        count_lbl.hidden=YES;
        count_lbl.hidden = YES;
    }
    else
    {
        if ([count_lbl.text isEqualToString:@"0"])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else if ([count_lbl.text isEqualToString:@""])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else
        {
            count_lbl.hidden=NO;
            count_lbl.hidden = NO;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singletonn.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}

-(void)handleRefresh{
    
    NSLog(@"Refreshed");
    // NSLog(@"%ld",(long)pull_Count);
    UIActivityIndicatorView  *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, productListTable.frame.size.height + 150);
    [self.view addSubview: activityIndicator];
    activityIndicator.color = [UIColor blackColor];
    [activityIndicator startAnimating];
    [self prodListing];
    [refreshControl endRefreshing];
}

- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users21) {
        
        
        [users_array addObjectsFromArray:users21];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
        
        [seen addObject:[NSString stringWithFormat:@"%ld",o]];
        NSUInteger y = 0;
        
        while (y < [users_array count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[users_array objectAtIndex:y];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [users_array removeObjectAtIndex:y];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                y++;
            }
        }
        
        
        
        
        [self newChat];
        
    } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}

-(void)chat

{
    // myei4MTHya
    
    currentUser = [QBUUser user];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
    //    NSInteger i = [ [ NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"] ] integerValue ];
    

    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            [self retrieveAllUsersFromPage:1];
            
        }
    }
     
     
     
     ];
    
    
}



-(void)newChat{
    
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
    
    for (int x=0; x<users_array.count; x++) {
        
        
        QBUUser *user1   =[QBUUser user];
        
        user1 =[users_array objectAtIndex:x];
        
        
        if (user1.ID==o) {
            [self chat22];
            break;
            
        }
        
        
        
    }
    
    
    [ProgressHUD showSuccess:@"Cannot Connect To Vendor, try again"];

    
    
    
}

-(void)chat22{
    
    
    
    
    
    
    QBChatDialog *test_dialog = nil;
    
    
    NSString *str=[[NSString alloc]init];
    str =@"new";
    
    
    for (int y=0; y<dialog_Objects.count; y++) {
        
        
        test_dialog=[dialog_Objects objectAtIndex:y];
        
        
        NSLog(@"%@",test_dialog.occupantIDs);
        
        NSArray *objs=[[NSArray alloc]initWithArray:test_dialog.occupantIDs];
        
        
        
        
        
        
        for (int x =0; x<1; x++) {
            NSInteger c =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:0]] integerValue ];
            NSInteger d =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:1]] integerValue ];
            NSInteger a = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
            NSInteger b = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
            
            
            if (c==a ) {
                
                if (d ==b) {
                    str=@"got";
                    break;
                }
                
            }else if (c==b) {
                
                if (d==a) {
                    str=@"got";
                    
                    break;
                }
                
                
            }
            
        }
        
        if ([str isEqualToString:@"got"]) {
            break;
        }
        
    }
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    
    // singletonn.chat_user=[[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
    
    chatDialog.occupantIDs = @[@(o) ];
    
    
    
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        
        [self actionChat:createdDialog.ID];
        
        
        
        
    } errorBlock:^(QBResponse *response) {
        
        [ProgressHUD showSuccess:@""];
        
    }];
    
    //    chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
    //
    //    chatDialog.name = @"Chat with Bob, Sam, Garry";
    //
    //    chatDialog.occupantIDs = @[@(9045183), @(9045219)]; // change id with your register user's id
    //
    //    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
    //
    //
    //          NSLog(@"No Error: %@", response);
    //        [self chat1];
    //
    //
    //    } errorBlock:^(QBResponse *response) {
    //
    //          NSLog(@"Error: %@", response);
    //
    //    }];
    
    
    
}

- (void)viewDidLoad

{
    [super viewDidLoad];
    singletonn=[Singleton instance];
    vendorNameLbl.text=singletonn.vendorName;
    NSLog(@"%@",singletonn.productArray);
    
    users_array=[[NSMutableArray alloc]init];
    
    count_lbl.hidden=YES;
    recents=[[NSMutableArray alloc]init];
    [self loadRecents];
    // TopBar View///
    refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [productListTable addSubview:refreshControl];

    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    country_cityView.hidden=YES;
    
    NSURL *url1=[NSURL URLWithString:singletonn.userproilePic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePicuser.image=emppppic;
    
    if (emppppic==nil) {
        
        profilePicuser.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        
        
    }
    profilePicuser.layer.masksToBounds = YES;
    profilePicuser.layer.cornerRadius = profilePicuser.frame.size.width/2;
    profilePicuser.layer.opaque = NO;
    
    
    users = [[NSMutableArray alloc]init];
    
    
 //   mySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
   
    mySpinner =[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 -15, self.view.frame.size.height/2 -15, 30, 30)];
    
    [mySpinner setColor:[UIColor blackColor]];
  //  mySpinner.backgroundColor=[UIColor whiteColor];
    mySpinner.hidesWhenStopped = YES;
    [self.view addSubview:mySpinner];
   // [mySpinner startAnimating];
    
    
    if ([singletonn.loginStatus isEqualToString:@"User Login"] || [singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        
//        
//        
//        NSUInteger limit = 1000;
//        __block NSUInteger skip = 0;
//        
//       // [ProgressHUD show:@"Loading..." Interaction:NO];
//      
//        
//        
//        PFUser *user = [PFUser currentUser];
//        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
//        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
//        [query orderByAscending:PF_USER_FULLNAME];
//        //  [query setLimit:1000];
//        [query setLimit: limit];
//        [query setSkip: skip];
//        
//        [mySpinner startAnimating];

//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//         {
//             if (error == nil)
//             {
//                 [users addObjectsFromArray:objects];
//                 NSLog(@"%lu  111  ",(unsigned long)objects.count);
//                 skip += limit;
//                 [query setSkip: skip];
//                 
//                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//
//                     if (error == nil){
//                         [users addObjectsFromArray:objects];
//                         skip += limit;
//                         [query setSkip: skip];
//                         
//                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                             
//                             if (error == nil){
//                                 [users addObjectsFromArray:objects];
//                                 skip += limit;
//                                 [query setSkip: skip];
//                                 
//                                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                                     
//                                     if (error == nil){
//                                         [users addObjectsFromArray:objects];
//                                         skip += limit;
//                                         [query setSkip: skip];
//                                         
//                                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                                             
//                                             if (error == nil){
//                                                 
//                                                 [users addObjectsFromArray:objects];
//                                                 skip += limit;
//                                                 [query setSkip: skip];
//                                                 [mySpinner stopAnimating];
//
//                                             }
//                                             
//                                             
//                                         }];
//                                         
//                                         
//                                     }
//                                     
//                                     
//                                 }];
//                                 
//                                 
//                             }
//                             
//                             
//                         }];
//                         
//                         
//                     }
//                     
//                     
//                 }];
//                 
//                 
//                 NSLog(@"%lu  444",(unsigned long)objects.count);
//                 [mySpinner stopAnimating];
//
//             }
//             else [ProgressHUD showError:@"Network error."];
//         }];
    }
    
    
    

    
    
    showOrderLbl.hidden=YES;
    
    countyTbl.hidden = YES;
    cityTbl.hidden = YES;
    countyTbl.sectionHeaderHeight=0;
    countyTbl.sectionFooterHeight=0;
    cityTbl.sectionHeaderHeight=0;
    cityTbl.sectionFooterHeight=0;
    [self prodListing];
    noItmeView.hidden = YES;
    replyView.hidden=YES;
    confirmView.hidden=YES;
    orderStr=@"no";
    [self addressListing];
    addressView.hidden=YES;
    newAddressView.hidden=YES;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, countryTxt.frame.size.height)];
    countryTxt.leftView = leftView1;
    countryTxt.leftViewMode = UITextFieldViewModeAlways;
    [countryTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, addressTxt.frame.size.height)];
    addressTxt.leftView = leftView2;
    addressTxt.leftViewMode = UITextFieldViewModeAlways;
    [addressTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, cityTxt.frame.size.height)];
    cityTxt.leftView = leftView3;
    cityTxt.leftViewMode = UITextFieldViewModeAlways;
    [cityTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        [productListTable reloadData];
        count_lbl.hidden = YES;
        editBtn.hidden=YES;
//        noItmeView.hidden = NO;
        UILabel *sorry=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 -75, self.view.frame.size.height/2  - 100, 150, 50)];
        [self.view addSubview:sorry];
        sorry.text=@"Sorry !";
        
        sorry.textAlignment = NSTextAlignmentCenter;
        [sorry setFont:[UIFont fontWithName:@"Pacifico" size:20]];

        UILabel *sorry1=[[UILabel alloc]initWithFrame:CGRectMake(10, sorry.frame.origin.y + sorry.frame.size.height +10 , self.view.frame.size.width-20, 50)];
        [self.view addSubview:sorry1];
        sorry1.text=@"You haven't ordered anything yet";
        sorry.textColor=[UIColor whiteColor];
        sorry1.textColor=[UIColor whiteColor];

        sorry1.textAlignment = NSTextAlignmentCenter;

        [sorry1 setFont:[UIFont fontWithName:@"OpenSans" size:18]];

    }
    else
    {
        
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"orderdate"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]initWithKey:@"ordertime"  ascending:YES];
        NSArray *sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
        NSArray *sortedArray2 = [dataArray sortedArrayUsingDescriptors:sortDescriptors1];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray2 mutableCopy];

        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"vendorname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"vendorpic"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [vendorNameArray addObject:vendorName1];
                [imageArr addObject:imagestr];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName1];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName1];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:vendorName1];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        noItmeView.hidden = YES;
        [productListTable reloadData];
    }
    
    NSInteger value=0;
    for( i =0;i<dataArray.count;i++)
    {
        NSString *status=[[dataArray objectAtIndex:i]valueForKey:@"statusid"];
        if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] )
        {
            value=value+1;
        }
    }
    singletonn.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(IBAction)Back:(id)sender
{
    
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(profile) withObject:nil afterDelay:0.0f];
    
  //  [self.navigationController popViewControllerAnimated:YES];
}
-(void)profile
{
    User_ProfileAR *srch = [[User_ProfileAR alloc]initWithNibName:@"User_ProfileAR" bundle:nil];
    [self.navigationController pushViewController:srch animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(void)addressListing

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/addresslistuser.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        addressArray=[[NSMutableArray alloc]init];
    }
    else
    {
        addressArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([orderStr isEqualToString:@"yes"])
    {
        return 1;
    }
    else
    {
        return [finalArray count];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            return 1;
        }
        else if ([btnStr isEqualToString:@"2"])
        {
            return [arra_1 count];
        }
        else
        {
            return  [addressArray count];
        }
        
    }
    else
    {
        NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
        NSArray *sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
        NSString *btnstr1 = [[sectionAnimals objectAtIndex:0]valueForKey:@"statusid"];
        if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
        {
            return 1+1;
        }
        else
        {
            return [sectionAnimals count]+1;
        }
    }
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([addAdress isEqualToString:@"1"])
        {
            

            New_address_cellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_3"];
            
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"New_address_cellAR" bundle:nil] forCellReuseIdentifier:@"cell_3"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_3"];
            }
            if ([btnStr isEqualToString:@"1"])
            {
                cell.label.text = [[countyArra objectAtIndex:1]valueForKey:@"a_country"];
            }
            else if ([btnStr isEqualToString:@"2"])
            {
                cell.label.text = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"a_city"];
                cell.locationImage.hidden=YES;
            }
            
            return cell;
        }
        
        else
        {
            AddressCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"AddressCellAR" bundle:nil] forCellReuseIdentifier:@"AddressCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
            }
            
            NSString *addressStrr = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
            [cell.addressBtn setTitle:addressStrr forState:UIControlStateNormal];
            [cell.addressBtn addTarget:self action:@selector(addressClick:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    }
    
    else
    {
        if (indexPath.row==0)
        {
            CartVendorCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"cartCell1"];
            
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"CartVendorCellAR" bundle:nil] forCellReuseIdentifier:@"cartCell1"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"cartCell1"];
            }
            
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            cell.vendorName.text = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorname"];
            cell.totalLbl.text=[NSString stringWithFormat:@"%li",(unsigned long)sectionAnimals.count];
            cell.vendorImage.layer.cornerRadius=cell.vendorImage.frame.size.height/2;
            cell.vendorImage.clipsToBounds=YES;
            cell.totalLbl.layer.cornerRadius=cell.totalLbl.frame.size.height/2;
            cell.totalLbl.clipsToBounds=YES;
            cell.totalLbl.layer.borderWidth = 2.0;
            cell.totalLbl.layer.borderColor = [UIColor whiteColor].CGColor;
            
            [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.vendorImage.imageURL];
            cell.vendorImage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
            
            
            
            for (i=0; i< sectionAnimals.count;i++)
            {
                NSString *statusStr1= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
                untiteld= [[sectionAnimals objectAtIndex:i]valueForKey:@"productname"];
                if ([untiteld isEqualToString:@"Untitled"])
                {
                    cell.chatLbl.hidden = YES;
                    cell.totalLbl.hidden = YES;
                    i=sectionAnimals.count;
                    
                }
                if ([statusStr1 isEqualToString:@"0"])
                {
                    cell.chatLbl.hidden = NO;
                    cell.totalLbl.hidden = YES;
                    i=sectionAnimals.count;
                }

            }
            
            
            for (i=0; i< sectionAnimals.count;i++)
            {
                NSString *statusStr1= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
                NSString *unitPriceStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"unitprice"];
                NSString *askStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"negotiable_price"];
                
                if ([statusStr1 isEqualToString:@""])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:nil])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:@"0"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:@"2"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Ordered.png"];
                }
                if ([statusStr1 isEqualToString:@"3"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Confirmed.png"];
                }
                if ([statusStr1 isEqualToString:@"4"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Delivered.png"];
                }
                if ([statusStr1 isEqualToString:@"1"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                    i=sectionAnimals.count;
                }
                else
                {
                    if ([unitPriceStr isEqualToString:@""])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals.count;
                    }
                    if ([unitPriceStr isEqualToString:nil])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals.count;
                    }
                    if ([askStr isEqualToString:@"Ask for Price"])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals.count;
                    }
                    if ([askStr isEqualToString:nil])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals.count;
                    }
                    
                    
                }
                cell.backgroundColor=[UIColor whiteColor];
                UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 54, 414, 1)];
                v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
                [cell.contentView addSubview:v11];
                return cell;
            }
        }
        
        else
        {
            NSString *sectionTitle1 = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle1];
            NSString *btnstr1 = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"statusid"];
            if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
            {
                ShowOrderCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_1"];
                if (!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"ShowOrderCellAR" bundle:nil] forCellReuseIdentifier:@"Cell_1"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_1"];
                }
                
                return cell;
            }
            else
            {
                
                CartProductCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell1"];
                if (!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"CartProductCellAR" bundle:nil] forCellReuseIdentifier:@"productCell1"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"productCell1"];
                }
                
                NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                
                //cell.prodName.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"productname"];
                untiteld = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"productname"];
                cell.prodName.text = untiteld;
                NSString *askprice = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"negotiable_price"];
                NSString *price = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"unitprice"];
                
                if ([askprice isEqualToString:@"Ask for Price"])
                {
                    cell.priceLbl.text=@"Ask for Price";
                }
                else if ([askprice isEqualToString:nil])
                {
                    cell.priceLbl.text=@"Ask for Price";
                }
                else
                {
                    //                float val=[price integerValue];
                    //                price=[NSString stringWithFormat:@"%0.3f KD",val];
                    cell.priceLbl.text=price;
                }
                
                
                
                
                
                
                cell.qtyLbl.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"quantityprod"];
                NSString *status = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"statusid"];
                
                if ([status isEqualToString:@"2"]||[status isEqualToString:@"3"]||[status isEqualToString:@"4"] )
                {
                    cell.qtyLbl.userInteractionEnabled=NO;
                }
                else
                {
                    cell.qtyLbl.userInteractionEnabled=YES;
                }
                
                [cell.qtyLbl setDelegate:self];
                NSArray *fields = @[cell.qtyLbl];
                keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
                [keyboardControls setDelegate:self];
                
                
                [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
                cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"prodimage"]]];
                UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
                v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
                [cell.contentView addSubview:v11];
                cell.backgroundColor=[UIColor whiteColor];
                return cell;
            }
        }
        return nil;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([addAdress isEqualToString:@"1"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            countryTxt.text = [[countyArra objectAtIndex:1]valueForKey:@"a_country"];
            country1 = [[countyArra objectAtIndex:1]valueForKey:@"a_country"];

            singletonn.uId = [[countyArra objectAtIndex:1]valueForKey:@"id"];
            countyTbl.hidden = NO;
            
                btnStr = @"2";
                [self city];
            
        }
        else if ([btnStr isEqualToString:@"2"])
        {
            
            city1=[[NSString alloc]init];

            
            city1 = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"a_city"];
         
            countryTxt.text=[NSString stringWithFormat:@"%@ , %@",city1,country1];
            countyTbl.hidden = YES;
            country_cityView.hidden=YES;
        }
    }
    else
    {
        if (indexPath.row == 0)
        {
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            for (i=0; i< sectionAnimals.count;i++)
            {
                statusStr0= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
                untiteld= [[sectionAnimals objectAtIndex:i]valueForKey:@"productname"];
                if ([untiteld isEqualToString:@"Untitled"])
                {
                    i = sectionAnimals.count;
                }
                else
                {
                    untiteld = @"";
                }
            }
            if ([untiteld isEqualToString:@"Untitled"])
            {
                
            }
            if ([statusStr0 isEqualToString:@"0"])
            {
                
                singletonn.venName = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorname"];
                singletonn.objectIdStr = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorobjectid"];
                singletonn.SlctVenId = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorid"];
                singletonn.vendorNameList = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorname"];
                [self performSelector:@selector(Objectuser) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
                [indicator startAnimating];
            }
            else
            {
                singletonn.SlctVenId = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorid"];
                singletonn.vendorNameList = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorname"];
                
                 singletonn.namechat=singletonn.vendorNameList;
                singletonn.objectIdStr = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"vendorobjectid"];
                
                NSString *str=[[NSString alloc]init];
                str =@"no user";
                singletonn.chat_user = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"vendorobjectid"];
                
                
                
                [self chat];
                
                //[self performSelector:@selector(Objectuser) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
                [indicator startAnimating];
            }
        }
        else
        {
            singletonn.productArray = [[NSMutableArray alloc]init];
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            singletonn.productArray = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            NSString *btnstr1 = [[singletonn.productArray objectAtIndex:0]valueForKey:@"statusid"];
            singletonn.orderId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"orderid"];
            if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
            {
                [self performSelector:@selector(showOrder) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
            }
            
        }
    }
    
}


-(void)showOrder
{
    showordeAR *show1=[[showordeAR alloc]initWithNibName:@"showordeAR" bundle:nil];
    [self.navigationController pushViewController:show1 animated:NO];
    [ProgressHUD showSuccess:@""];
    
}


- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}



-(void)Objectuser

{
    
    
    Vendor_DetaillistAR *chatView = [[Vendor_DetaillistAR alloc]initWithNibName:@"Vendor_DetaillistAR" bundle:nil];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
//    NSString *str=[[NSString alloc]init];
//    str =@"no user";
//    singletonn.objectIdStr = [[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
//    for ( i=0; i< users.count; i++)
//    {
//        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//        if ([name containsString:singletonn.objectIdStr])
//        {
//            
//            
//            str=@"user found";
//
//            PFUser *user1 = [PFUser currentUser];
//            PFUser *user2 = users[i];
//            NSString *groupId = StartPrivateChat(user1, user2);
//            [self actionChat:groupId];
//            i = users.count;
//        }
//    }
//    
//    
//    if ([str isEqualToString:@"no user"]) {
//        
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"No User Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//        [ProgressHUD showSuccess:@""];
//}
}

- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
//    ChatView *chatView = [[ChatView alloc] initWith:groupId];
//    chatView.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatView animated:YES];
//    [ProgressHUD showSuccess:@""];
}




-(void)addressClick:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *cellIndexPath = [addressTable indexPathForCell:cell];
     addressStr=[NSString stringWithFormat:@"%@,%@,%@",[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"address"],[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"city"],[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"a_countryname"]];
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(orderCartView) withObject:nil afterDelay:0.5f];
}


- (NSInteger)numberOfSections

{
    return 1;
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([orderStr isEqualToString:@"yes"])
    {
        return 50;
        
    }
    else{
        if (indexPath.row==0)
        {
            return 55;
            
        }
        else
        {
            return 50;
            
        }
    }
}



- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    
    if ([orderStr isEqualToString:@"yes"]) {
        
        UIButton *newAddress;
        UIView *linee;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(99, 35, 106, 1)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(87, 36, 129, 1)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(127, 35, 105, 1)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(147, 35, 105, 1)];
            }
        }
        newAddress.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:17];
        [newAddress setTitle:@"إضافة عنوان جديد" forState:UIControlStateNormal];
        [newAddress setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [newAddress addTarget:self action:@selector(newAddressBtn:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:newAddress];
        linee.backgroundColor=[UIColor whiteColor];
        [headerView addSubview:linee];
    }
    else
    {
        UIButton *statusBtn;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
                
            }
        }
        
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
        NSArray *sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
        
        
        for ( i=0; i< sectionAnimals.count; i++)
        {
            statusStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
            
            // NSString *unitPriceStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"unitprice"];
            NSString *askPriceStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"negotiable_price"];
            
            if ([statusStr isEqualToString:@""])
            {
                statusStr=@"طلب";
            }
            if ([statusStr isEqualToString:nil])
            {
                statusStr=@"طلب";
            }
            if ([statusStr isEqualToString:@"0"])
            {
                statusStr=@"طلب";
            }
            if ([statusStr isEqualToString:@"2"])
            {
                statusStr=@"في انتظار تأكيد";
            }
            if ([statusStr isEqualToString:@"3"])
            {
                statusStr=@"في انتظار التسليم";
                //      [editBtn setTitle:@"SHOW ORDER" forState:UIControlStateNormal];
                //     showOrderLbl.hidden=NO;
            }
            if ([statusStr isEqualToString:@"4"])
            {
                statusStr=@"تم التوصيل";
                //      [editBtn setTitle:@"SHOW ORDER" forState:UIControlStateNormal];
                //      showOrderLbl.hidden=NO;
                i=sectionAnimals.count;
            }
            if ([statusStr isEqualToString:@"1"])
            {
                statusStr=@"الانتظار للحصول على السعر";
                i=sectionAnimals.count;
            }
            else
            {
                if ([askPriceStr isEqualToString:@"Ask for Price"])
                {
                    statusStr=@"ASK FOR PRICE";
                    i=sectionAnimals.count;
                }
                if ([askPriceStr isEqualToString:nil])
                {
                    statusStr=@"ASK FOR PRICE";
                    i=sectionAnimals.count;
                }
                
            }
        }
        
        
        
        statusBtn.tag=section;
        [statusBtn setTitle:statusStr forState:UIControlStateNormal];
        [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        if ([statusStr isEqualToString:@"طلب"]||[statusStr isEqualToString:@"ASK FOR PRICE"]) {
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
        }
        else
        {
            [statusBtn setBackgroundColor:[UIColor clearColor]];
            statusBtn.layer.borderColor=[UIColor whiteColor].CGColor;
            statusBtn.layer.borderWidth=1.5f;
            statusBtn.clipsToBounds=YES;
        }
        
        if ([statusStr isEqualToString:@"طلب"]) {
            [statusBtn addTarget:self action:@selector(orderBtn:) forControlEvents:UIControlEventTouchUpInside];
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
        }
        if ([statusStr isEqualToString:@"ASK FOR PRICE"])
        {
            [statusBtn addTarget:self action:@selector(AskBtn:) forControlEvents:UIControlEventTouchUpInside];
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
        }
        if ([statusStr isEqualToString:@"في انتظار التسليم"] || [statusStr isEqualToString:@"إرسال التسليم"]) {
            [headerView addSubview:statusBtn];
        }
        else
        {
            if (sectionAnimals.count==0)
            {
            }
            else
            {
                [headerView addSubview:statusBtn];
            }
        }
        statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
        
        if([editBtn.currentTitle isEqualToString:@"تمام"])
        {
            statusBtn.enabled = NO;
        }
        else if([editBtn.currentTitle isEqualToString:@"تعديل"])
        {
            statusBtn.enabled = YES;
            
        }
        
    }
    
    headerView.backgroundColor=[UIColor clearColor];
    
    return headerView;
}


-(void)newAddressBtn:(UIButton *)sender

{
    addAdress = @"1";
    newAddressView.hidden=NO;
}

-(void)orderBtn:(UIButton *)sender
{
    orderStr=@"yes";
    NSInteger section=sender.tag;
    if (addressArray.count==0)
    {
        addAdress = @"1";
        newAddressView.hidden=NO;
    }
    else
    {
        addressView.hidden=NO;
        
        [addressTable reloadData];
    }
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    NSArray *sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    singletonn.userid=[[sectionAnimals objectAtIndex:0]valueForKey:@"userid"];
    singletonn.venId=[[sectionAnimals objectAtIndex:0]valueForKey:@"vendorid"];
    singletonn.userNameStr=[[sectionAnimals objectAtIndex:0]valueForKey:@"username"];
    singletonn.prodNameStr=[[sectionAnimals objectAtIndex:0]valueForKey:@"productname"];
singletonn.objectIdStr=[[sectionAnimals objectAtIndex:0]valueForKey:@"vendorobjectid"];
    
}

-(void)Confirm:(id)sender
{
    [cityTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
    [addressTxt resignFirstResponder];

    
    if (cityTxt.text.length > 0 && countryTxt.text.length > 0 && addressTxt.text.length > 0)
    {
        indicator.hidden=NO;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(confirmAddressView) withObject:nil afterDelay:0.5f];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)countyBtn:(id)sender
{
    country_cityView.hidden=NO;
    countyTbl.hidden = NO;
    btnStr = @"1";

    [self country];
    [countyTbl reloadData];
}


-(void)country

{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/countrylist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    countyArra = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",countyArra);
    
    [countyTbl reloadData];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"country"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [countyArra sortedArrayUsingDescriptors:sortDescriptors];
    countyArra=[[NSMutableArray alloc]init];
    countyArra = [sortedArray1 mutableCopy];
    [countyTbl reloadData];
    
}


- (IBAction)cityBtn:(id)sender
{
    
    
    
    
    
//    cityTbl.hidden = NO;
//    btnStr = @"2";
//    [self city];
//    [cityTbl reloadData];
}

-(void)city

{
    NSString *post = [NSString stringWithFormat:@"countryid=%@",singletonn.uId];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/citylist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No list available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"a_city"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [arra_1 sortedArrayUsingDescriptors:sortDescriptors];
    arra_1=[[NSMutableArray alloc]init];
    arra_1 = [sortedArray1 mutableCopy];
    [countyTbl reloadData];
}


-(void)confirmAddressView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/adduseradd.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",singletonn.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *countryStrr=[[NSString alloc]initWithFormat:@"%@",country1];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"country\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",countryStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *phone=[[NSString alloc]initWithFormat:@"%@",cityTxt.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",phone] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *cityStrr=[[NSString alloc]initWithFormat:@"%@",city1];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"city\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",cityStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    addressStr=[[NSString alloc]initWithFormat:@"%@",addressTxt.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"address\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",addressStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    [self orderCartView];
    
}


-(void)orderCartView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addorder.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",singletonn.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",singletonn.venId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSDate * now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm:ss a"];
    NSString *currentTime = [formatter stringFromDate:now];
    
    NSDate * now2 = [NSDate date];
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"dd-MM-yyyy"];
    NSString *currentDate = [formatter2 stringFromDate:now2];
    
    
    NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *orderdate2=[[NSString alloc]initWithFormat:@"%@, %@,%@, Phone no. %@",city1,country1,addressStr,cityTxt.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"useraddress\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderdate2] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
//    NSString *addressStr1=[[NSString alloc]initWithFormat:@"%@",addressStr];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"useraddress\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",addressStr1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *number = [NSMutableString stringWithCapacity:10];
    for (i = 0U; i < 10; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [number appendFormat:@"%C", c];
    }
    NSLog(@"%@",number);
    
    NSString *orderIdStr=[[NSString alloc]initWithFormat:@"%@_%@",singletonn.userNameStr,number];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
//        singletonn.objectIdStr = [[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
//        NSLog(@"%@",singletonn.objectIdStr);
        
        
        
        
        NSString *message =[NSString stringWithFormat:@"%@ wants to order %@",singletonn.userNameStr,singletonn.prodNameStr                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:singletonn.objectIdStr successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];
        

        
        
//        for ( i=0; i< users.count; i++)
//        {
//            name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//            if ([name containsString:singletonn.objectIdStr])
//            {
//                PFUser *user1 = [PFUser currentUser];
//                PFUser *user2 = users[i];
//                NSString *groupId = StartPrivateChat(user1, user2);
//                NSString *text = @"Wants to order";
//                // [self actionChat:groupId];
//                SendPushNotification(groupId, text);
//                
//                i = users.count;
//            }
//        }

        
    }
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    addressView.hidden=YES;
    newAddressView.hidden=YES;
    confirmView.hidden=NO;
    orderStr=@"no";
    [self prodListing];
    [productListTable reloadData];
    
}

-(void)AskBtn:(UIButton *)sender
{
    arrayAsk=[[NSMutableArray alloc]init];
    NSInteger section=sender.tag;
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    arrayAsk = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(ask) withObject:nil afterDelay:0.5f];
    
}

-(void)ask

{
    for ( i=0; i< arrayAsk.count; i++)
    {
        NSString *unitPriceStr= [[arrayAsk objectAtIndex:i]valueForKey:@"unitPriceStr"];
        NSString *askPriceStr= [[arrayAsk objectAtIndex:i]valueForKey:@"negotiable_price"];
        cartIdStr=[[arrayAsk objectAtIndex:i]valueForKey:@"id"];
        if ([unitPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if ([unitPriceStr isEqualToString:nil])
        {
            [self askView];
        }
        if ([unitPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if ([unitPriceStr isEqualToString:nil])
        {
            [self askView];
        }
        if ([askPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if ([askPriceStr isEqualToString:nil])
        {
            [self askView];
        }
        
        
    }
    
    indicator.hidden=YES;
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    replyView.hidden=NO;
    [self prodListing];
    [productListTable reloadData];
    
}



-(IBAction)OK:(id)sender
{
    replyView.hidden=YES;
    confirmView.hidden=YES;
 

    
}





-(void)askView
{
    NSString *post = [NSString stringWithFormat:@"cartid=%@&userid=%@",cartIdStr,singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/askforprice.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if ([addAdress isEqualToString:@"1"])
    {
        return 0;
    }
    else
    {
        return 45;
    }
}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (IBAction)Edit:(id)sender
{
    
    if ([editBtn.currentTitle isEqualToString:@"SHOW ORDER"]) {
        showordeAR *show1=[[showordeAR alloc]initWithNibName:@"showordeAR" bundle:nil];
        [self.navigationController pushViewController:show1 animated:NO];
    }
    else
    {
        if([editBtn.currentTitle isEqualToString:@"تمام"])
        {
            [super setEditing:NO animated:NO];
            [productListTable setEditing:NO animated:NO];
            [editBtn setTitle:@"تعديل" forState:UIControlStateNormal];
            [productListTable reloadData];
            
        }
        else if([editBtn.currentTitle isEqualToString:@"تعديل"])
        {
            [super setEditing:YES animated:YES];
            [productListTable setEditing:YES animated:YES];
            [editBtn setTitle:@"تمام" forState:UIControlStateNormal];
            [productListTable reloadData];
            
        }
    }
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"حذف" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        if (indexPath.row==0)
                                        {
                                            
                                            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                                            NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                                            singletonn.userid =[[sectionAnimals objectAtIndex:0]valueForKey:@"userid"];
                                            singletonn.venId =[[sectionAnimals objectAtIndex:0]valueForKey:@"vendorid"];
                                            statusStrr =[[sectionAnimals objectAtIndex:0]valueForKey:@"statusid"];
                                            
                                            if (/*[statusStrr isEqualToString:@"2"]||*/[statusStrr isEqualToString:@"3"])
                                            {
                                                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot delete this product because you already have a pending order with this vendor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alrt show];
                                            }
                                            else
                                            {
                                                indicator.hidden=NO;
                                                [indicator startAnimating];
                                                [ProgressHUD show:@"Loading..." Interaction:NO];
                                                [self performSelector:@selector(deleteView1) withObject:nil afterDelay:0.5f];
                                            }
                                        }
                                        else
                                        {
                                            
                                            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                                            NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                                            statusStrr =[[sectionAnimals objectAtIndex:0]valueForKey:@"statusid"];
                                            cartIdStr =[[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"id"];
                                            if (/*[statusStrr isEqualToString:@"2"]||*/[statusStrr isEqualToString:@"3"])
                                            {
                                                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot delete this product because you already have a pending order with this vendor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alrt show];
                                            }
                                            else
                                            {
                                                indicator.hidden=NO;
                                                [indicator startAnimating];
                                                [ProgressHUD show:@"Loading..." Interaction:NO];
                                                
                                                [self performSelector:@selector(deleteView) withObject:nil afterDelay:0.5f];
                                                
                                            }
                                        }
                                    }];
    button.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:1.0];
    
    return @[button];
}

-(void)deleteView1
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@&userid=%@&statusid=%@",singletonn.venId,singletonn.userid,statusStrr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delvendorcart.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
    [self prodListing];
    [productListTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}


-(void)deleteView
{
    NSString *post = [NSString stringWithFormat:@"cartid=%@",cartIdStr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delcart.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    [self prodListing];
    [productListTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([editBtn.currentTitle isEqualToString:@"تمام"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [addressTxt resignFirstResponder];
    [cityTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
    [textField resignFirstResponder];
    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [addressTxt resignFirstResponder];
    [cityTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == sendMessageTxt || textField == addressTxt || textField == cityTxt)
    {
        [self animateTextField:textField up:YES];
    }
    //if ([editBtn.currentTitle isEqualToString:@"DONE"])
    else
    {
        currentValue=textField.text;
        [keyboardControls setActiveField:textField];
        
    }
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == addressTxt)
    {
        [self animateTextField:textField up:NO];
    }
    else if (textField==cityTxt){
        
        [self animateTextField:textField up:NO];

        
    }
    
    
    else
    {
        
        quantStr=textField.text;
        if (![currentValue isEqualToString:quantStr])
        {
            
            UITableViewCell *cell = (UITableViewCell *)textField.superview.superview;
            NSIndexPath *cellIndexPath = [productListTable indexPathForCell:cell];
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:cellIndexPath.section];
            NSArray *sectionAnimals = [[finalArray objectAtIndex:cellIndexPath.section]valueForKey:sectionTitle];
            cartIdStr=[[sectionAnimals objectAtIndex:cellIndexPath.row-1]valueForKey:@"id"];
            indicator.hidden=NO;
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];
            
            [self performSelector:@selector(editCartView) withObject:nil afterDelay:0.2f];
        }
    }
}


-(void)editCartView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartquant.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",quantStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
        
    }
    [self prodListing];
    [productListTable reloadData];
    
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    
}

- (IBAction)chat:(id)sender
{
  
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(chatbb) withObject:nil afterDelay:0.5f];
}


-(void)chatbb
{
RecentView *View = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:View animated:NO];
    [ProgressHUD showSuccess:@""];

}

- (IBAction)favt:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(fav) withObject:nil afterDelay:0.5f];
}


-(void)fav
{
    FavoritesAR *fav = [[FavoritesAR alloc]initWithNibName:@"FavoritesAR" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)home:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(homeBtn) withObject:nil afterDelay:0.5f];
}


-(void)homeBtn
{
    Home_VetrinaARViewController *fav = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    [ProgressHUD showSuccess:@""];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel_cntryCityView:(id)sender {
    
    country_cityView.hidden=YES;
    
    
}
@end
