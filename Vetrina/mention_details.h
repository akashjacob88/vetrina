//
//  mention_details.h
//  Vetrina
//
//  Created by Amit Garg on 1/30/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@interface mention_details : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    Singleton *singlogin;
    
}
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *arebic_back;
@property (strong, nonatomic) IBOutlet UILabel *title_lbl;
- (IBAction)back_action:(id)sender;
- (IBAction)backAR_action:(id)sender;
@property (nonatomic) NSString *idstr;
@property (strong, nonatomic) IBOutlet UITableView *litem_tableview;
@property (strong, nonatomic) IBOutlet UILabel *arebicTittle_lbl;



@end
