//
//  HomeAR_Cell1.m
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "HomeAR_Cell1.h"

@implementation HomeAR_Cell1

@synthesize titleLabel,arrowImageView,iconImg;


- (void)dealloc
{
    
    self.arrowImageView = nil;
    titleLabel = nil;
    iconImg = nil;
    [super dealloc];
}

- (void)awakeFromNib

{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)changeArrowWithUp:(BOOL)up
{
    if (up)
    {
        self.arrowImageView.image = [UIImage imageNamed:@"WhiteArrowUp.png"];
    }
    else
    {
        self.arrowImageView.image = [UIImage imageNamed:@"WhiteArrowDown.png"];
    }
}


@end
