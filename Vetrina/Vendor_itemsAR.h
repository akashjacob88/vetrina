//
//  Vendor_itemsAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/1/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Vendor_itemsAR : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIScrollViewDelegate,UITextFieldDelegate>

{
    Singleton *singloging;
    IBOutlet UILabel *shopNme;
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    UILabel *vendor;
    
    IBOutlet UILabel *count_lbl;
    IBOutlet UITableView *itemTables;
    IBOutlet UIButton *itemsbtn;
    IBOutlet UIButton *orderBtn;
    
    NSMutableArray *prodArray,*dataArray,*newArray,*array,*finalArray,*vendorNameArray,*imageArr;
    NSInteger i;
    NSString *stringOne,*stringTwo,*newString;
    NSDictionary *dynamicDict;
    NSArray *sectionAnimals;
    IBOutlet UIActivityIndicatorView *indicator;
    UIImage *cimage,*cimage2,*cimage3,*cimage4,*cimage5;
    NSString *priceDec,*soldout,*price;

    IBOutlet UILabel *vendorChatLblk;
}

@end
