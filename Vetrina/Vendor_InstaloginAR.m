//
//  Vendor_InstaloginAR.m
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_InstaloginAR.h"
#import "JSON.h"
#import "Vendor_Shop_RegtAR.h"
#import "Vendor_ProductAR.h"
#define kAccessToken         @"access_token="
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#define kBaseURL @"https://instagram.com/"
#define kAuthenticationURL @"oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
//#define kClientID @"4af4e24014f943aaaf16781b45ebf803"
#define kClientID @"c0fa0e5317f543098de48be60c4b7c69"

#define kRedirectURI @"http://VetrinaApp.com"
#import <Parse/Parse.h>
#import "AppConstant.h"
#import "ProgressHUD.h"
#import "push.h"
#import "Home_VetrinaARViewController.h"


@interface Vendor_InstaloginAR ()

@end

@implementation Vendor_InstaloginAR

- (void)viewDidLoad
{
    [super viewDidLoad];
    singlogin=[Singleton instance];
    
    [indicator stopAnimating];
    webView2.hidden= YES;
    
    singlogin.vImgUrl = [[NSMutableArray alloc]init];
    [webView2 sizeToFit];
    webView2.scrollView.scrollEnabled = YES;
    newdatalist = [[NSMutableArray alloc]init];
    
    [self logout];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:animated];
    self.title = @"Instagram Login";
    return;
}


- (IBAction)loginInsta:(id)sender

{
    [webView2 setHidden:NO];
    [indicator startAnimating];
    NSString* urlString = [kBaseURL stringByAppendingFormat:kAuthenticationURL,kClientID,kRedirectURI];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [webView2 loadRequest:request];
}


#pragma mark -- WebView Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString* urlString = [[request URL] absoluteString];
    NSURL *Url = [request URL];
    NSArray *UrlParts = [Url pathComponents];
    
    // runs a loop till the user logs in with Instagram and after login yields a token for that Instagram user
    // do any of the following here
    if ([UrlParts count] == 1)
    {
        
        NSRange tokenParam = [urlString rangeOfString: kAccessToken];
        if (tokenParam.location != NSNotFound)
        {
            NSString* token = [urlString substringFromIndex: NSMaxRange(tokenParam)];
            // NSString *followsID = @"395834289";
            // If there are more args, don't include them in the token:
            NSLog(@"ACCESS TOKEN %@",token);
            singlogin.accesstokenNew=token;
            NSRange endRange = [token rangeOfString: @"&"];
            
            if (endRange.location != NSNotFound)
                token = [token substringToIndex: endRange.location];
            
            if ([token length] > 0 )
            {
                
                NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/self/media/recent/?access_token=%@", kInstagramAPIBaseURL,token];
                NSURL * url=[NSURL URLWithString:userInfoUrl];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLResponse *response = NULL;
                NSError *requestError = NULL;
                NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
                
                NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
                //  NSLog(@"GetData--%@",data);
                
                NSDictionary *value=[data JSONValue];
                NSMutableArray *userDict = [value objectForKey:@"data"];
                NSLog(@"User Dict %@",userDict);
                //
                NSMutableArray *user = [[userDict  objectAtIndex:0]valueForKey:@"user"];
                NSLog(@"user bio %@",user);
                NSString* userId=[user valueForKey:@"id"];
                NSString* userFullName=[user valueForKey:@"full_name"];
                NSString* userProfilePic=[user valueForKey:@"profile_picture"];
                NSString *userName = [user valueForKey:@"username"];
                NSLog(@"USER NAME= %@",userName);
                singlogin  = [Singleton instance];
                singlogin.vIGUsername = userName;
                singlogin.vIGUserId = userId;
                singlogin.vIGFullname = userFullName;
                singlogin.vIGProfilePic = userProfilePic;
                NSLog(@"Singleton --- %@",userFullName);
                img4 = [NSString stringWithFormat:@"%@",singlogin.vIGProfilePic];
                NSURL *url2 = [NSURL URLWithString:img4];
                NSData *data2 = [NSData dataWithContentsOfURL:url2];
                pro = [[UIImage alloc] initWithData:data2];
                
                NSMutableArray *img = [userDict valueForKeyPath:@"images"];
                
                NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
                
                NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
                NSLog(@"Pic Links %@",righturl);
                singlogin  = [Singleton instance];
                singlogin.vImgUrl = righturl;
                NSLog(@"%lu",(unsigned long)[singlogin.vImgUrl count]);
                
                NSString* userInfoUrl1 = [NSString stringWithFormat:@"%@/v1/users/self/follows/?access_token=%@", kInstagramAPIBaseURL,token];
                NSURL * url1=[NSURL URLWithString:userInfoUrl1];
                NSURLRequest *request1= [NSURLRequest requestWithURL:url1];
                NSURLResponse *response1 = NULL;
                NSError *requestError1 = NULL;
                NSData *responseData1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&requestError1];
                
                NSString *data1=[[NSString alloc]initWithData:responseData1 encoding:NSUTF8StringEncoding];
                NSDictionary *value1=[data1 JSONValue];
                NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
                NSLog(@"dadadad %@",userDict1);
                NSMutableArray *userName1 = [userDict1  valueForKey:@"username"];
                NSLog(@"user bio %@",userName1);
                NSMutableArray *fullName1 = [userDict1  valueForKey:@"full_name"];
                NSMutableArray *profilePic = [userDict1  valueForKey:@"profile_picture"];
                NSMutableArray *folowUsrID=[userDict1 valueForKey:@"id"];
                singlogin.vFolowUserName = userName1;
                singlogin.vFolowFullName = fullName1;
                singlogin.vFolowUserId = folowUsrID;
                singlogin.vFolowProfilePic = profilePic;
                [self signUpShop];
            }
        }
        else
        {
            [indicator stopAnimating];
            [ProgressHUD showError:@""];
        }
        return NO;
    }
    return YES;
}


- (void) webViewDidStartLoad:(UIWebView *)webView

{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

}


- (void)webViewDidFinishLoad:(UIWebView *)webView

{
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];

}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error

{
    if (error.code == 102)
        return;
    if (error.code == -1009 || error.code == -1005)
    {
        //        _completion(kNetworkFail,kPleaseCheckYourInternetConnection);
    }
    else
    {
        //        _completion(kError,error.description);
    }
    //back to main page
}


-(void)signUpShop

{
    NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/vendorreg.php"];
    
    NSLog(@"strURL:%@", strULR);
    
    NSURL * url=[NSURL URLWithString:strULR];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.vIGUsername];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *fullnamestr = [[NSString alloc]initWithFormat:@"%@", singlogin.vIGFullname];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fullname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",fullnamestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usreIdstr = [[NSString alloc]initWithFormat:@"%@", singlogin.vIGUserId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"instagramid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",usreIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *proiflestr= [[NSString alloc]initWithFormat:@"%@", singlogin.vIGProfilePic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagee\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",proiflestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest setHTTPBody:body];
    
    NSString *objectIdstr= [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableArray *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSString *uniqueid=[[val objectAtIndex:0]valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[[val objectAtIndex:0] valueForKey:@"phoneno"];
    NSString *uName=[[val objectAtIndex:0] valueForKey:@"username_instagramacnt"];
    singlogin.usrName=uName;
    singlogin.vUniqueId=uniqueid;
    singlogin.userid=uniqueid;
    singlogin.vPhone = phoneno;
    
    
    
    NSString *msg = [[val valueForKey:@"message"]objectAtIndex:0];
    
    NSLog(@"msg -- %@",msg);
    
    if([[[val valueForKey:@"message"] objectAtIndex:0] isEqual:@"User Already Exist"])
    {
        [self LoginIG];
        [indicator stopAnimating];
        [ProgressHUD showSuccess:@""];

    }
    else
    {
        Vendor_Shop_RegtAR *shop = [[Vendor_Shop_RegtAR alloc]initWithNibName:@"Vendor_Shop_RegtAR" bundle:nil];
        [self.navigationController pushViewController:shop animated:YES];
        [ProgressHUD showSuccess:@""];

        [indicator stopAnimating];
    }
}


-(void)Edit

{
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editobjectid.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singlogin.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *objectIdstr= [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    
    NSMutableArray *valu = [val valueForKey:@"detail"];
    
    
    
    NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *objectid=[[valu objectAtIndex:0] valueForKey:@"objectid"];
    singlogin.objectIdStr = objectid;
    
    
    
    
}

-(void)EditProduct_withObjectid

{
//    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editproductobjectid.php"];
//    
//    NSLog(@"strURL:%@", strURL);
//    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
//    [theLoginRequest setHTTPMethod:@"POST"];
//    
//    NSMutableData *body = [NSMutableData data];
//    NSString *boundry = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
//    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
//    
//    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singlogin.vUniqueId];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *objectIdstr= [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    
//    [theLoginRequest  setHTTPBody:body];
//    
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
//    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    
//    
//    NSLog(@"print mystring==%@",returnString);
//    
//    NSMutableDictionary *val=[returnString JSONValue];
//    
//    NSLog(@"GetDatadictt--%@",val);
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@&objectid=%@",singlogin.vUniqueId,singlogin.objectIdStr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editproductobjectid.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSLog(@"%@",eventarray);

    
    
}


-(void)LoginIG

{
    NSString *post = [NSString stringWithFormat:@"instagramid=%@&logintype=%@",singlogin.vIGUserId,@"instagram"];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlogin2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
   // NSArray *val = [eventarray valueForKey:@"0"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[eventarray  valueForKey:@"phoneno"];
    NSString *uName=[eventarray  valueForKey:@"username_instagramacnt"];
    NSString *shopName=[eventarray  valueForKey:@"fullname_shopname"];
    NSString *image=[eventarray  valueForKey:@"imagee"];
    singlogin.mainCat=[eventarray  valueForKey:@"main_catname"];
    singlogin.mainSubCat=[eventarray  valueForKey:@"main_subcatname"];
    singlogin.shopDec=[eventarray  valueForKey:@"descr"];
    singlogin.subId=[eventarray  valueForKey:@"subid"];
    singlogin.pid = [eventarray  valueForKey:@"staffpick"];
    singlogin.catid = [eventarray  valueForKey:@"main_catid"];
    singlogin.subCatid = [eventarray valueForKey:@"main_subcatid"];
    
    singlogin.venPic = image;
    singlogin.vendorName=uName;
    singlogin.vUniqueId=uniqueid;
    singlogin.userid=uniqueid;
    singlogin.vPhone = phoneno;
    singlogin.shopNameVendr = shopName;
    singlogin.loginStatus = @"IG Login";
    if([neww isEqualToString:@"False"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"User Login" message:@"This email id is not registered! please waiting for approval!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
    }
    else
    {
        if ([singlogin.vUniqueId isEqualToString:@"1653"])
        {
            //singlogin.loginStatus = @"Admin";
            //            Admin *admin=[[Admin alloc]initWithNibName:@"Admin" bundle:NULL];
            //            [self.navigationController pushViewController:admin animated:YES];
        }
        else
        {
            [self Edit];
            [self EditProduct_withObjectid];
            
            
            Home_VetrinaARViewController *product=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
            [self.navigationController pushViewController:product animated:YES];
            
            [[NSUserDefaults standardUserDefaults] setObject: singlogin.vIGUserId forKey:@"VinstagramID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [ProgressHUD showSuccess:@""];
            [indicator stopAnimating];
        }
    }
}


-(void)logout

{
    NSURL *url = [NSURL URLWithString:@"https://instagram.com/"];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSEnumerator *enumerator = [[cookieStorage cookiesForURL:url] objectEnumerator];
    NSHTTPCookie *cookie = nil;
    while ((cookie = [enumerator nextObject]))
    {
        [cookieStorage deleteCookie:cookie];
    }
}


- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
