//
//  AppDelegate.h
//  Vetrina
//
//  Created by Amit Garg on 4/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Singleton *singlogin;
    NSMutableArray *cartListArray;
}

@property (strong, nonatomic) UIWindow *window;


@end

