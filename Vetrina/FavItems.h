//
//  FavItems.h
//  Vetrina
//
//  Created by Amit Garg on 6/11/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface FavItems : UIViewController


{
    Singleton *singlogin;
    IBOutlet UITableView *itemTble;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    IBOutlet UILabel *chrtLbl;
    NSMutableArray *arra_1;
    NSArray *sortedarray1,*sortedarray2;
    UIImageView *Image2, *img3;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr;
    NSString *cartIdStr,*quantStr;
    
    NSString *searchStr,*soldout,*askforPrice,*staffPic,*name,*objectIdStr;
    NSMutableArray *cartListArray;
    
    IBOutlet UIView *topbsr;
    IBOutlet UIView *view2;
}

@end
