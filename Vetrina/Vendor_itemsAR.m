//
//  Vendor_itemsAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/1/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_itemsAR.h"
#import "Vendor_ProductAR.h"
#import "EdititemsAR.h"
#import "Vendor_settingAR.h"
#import "Vendor_InstapicsAR.h"
#import "Vendor_ItemCellAR.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "EdititemsAR.h"
#import "Vendor_ItemCellAR.h"
#import "ProgressHUD.h"
#import "Home_VetrinaARViewController.h"
#import "RecentView.h"
#import "Vendor_profileAR.h"

#import <Parse/Parse.h>
#import "AppConstant.h"

@interface Vendor_itemsAR ()
{
    IBOutlet UIImageView *profilePice;
    IBOutlet UIView *topBar;
    NSMutableArray *recents;
 NSArray *dialog_Objects;
    
}

@end

@implementation Vendor_itemsAR








- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }

    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singloging.userid == nil)
    {
        count_lbl.hidden=YES;
        count_lbl.hidden = YES;
    }
    else
    {
        if ([count_lbl.text isEqualToString:@"0"])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else if ([count_lbl.text isEqualToString:@""])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else
        {
            count_lbl.hidden=NO;
            count_lbl.hidden = NO;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singloging.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}
-(void)prodListing123
{
    //     NSString *post1;
    //    if ([singloging.loginStatus isEqual:@"User Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
    //    }
    //    else if ([singloging.loginStatus isEqual:@"IG Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    //    }
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSMutableArray *data_arr;
    
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        data_arr=[[NSMutableArray alloc]init];
    }
    else
    {
        data_arr = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    
    
    if (data_arr.count==0) {
        vendorChatLblk.hidden=YES;
        
        
    }else{
        int count1=0;
        
        for (int a=0; a<data_arr.count; a++) {
            
            NSString *aarra =[[data_arr objectAtIndex:a] valueForKey:@"statusid"];
            
            if ([aarra isEqualToString:@"2"]) {
                count1++;
            }
            
        }
        
        if (count1 == 0) {
            vendorChatLblk.hidden=YES;
        }else{
            
            
            vendorChatLblk.hidden=NO;
            vendorChatLblk.text=[NSString stringWithFormat:@"%d",count1];
            vendorChatLblk.layer.cornerRadius=8.f;
            vendorChatLblk.layer.masksToBounds=YES;
            
        }
    }
    
    
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    singloging = [Singleton instance];
    
    
    recents =[[NSMutableArray alloc]init];
    [self prodListing123];
    [self loadRecents];
    count_lbl.hidden=YES;
    
    
    NSURL *url1=[NSURL URLWithString:singloging.venPic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePice.image=emppppic;
    profilePice.hidden = NO;
    profilePice.layer.masksToBounds = YES;
    profilePice.layer.borderWidth = 1.0f;
    profilePice.layer.cornerRadius = profilePice.frame.size.width/2;
    profilePice.layer.borderColor = [[UIColor clearColor]CGColor];
    profilePice.clipsToBounds = YES;
    profilePice.layer.opaque = NO;
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    

    
    shopNme.text= singloging.shopNameVendr;
    
    itemsbtn.layer.cornerRadius = 5.0;
    
    orderBtn.layer.masksToBounds = YES;
    orderBtn.layer.cornerRadius = 5.0;
    orderBtn.layer.borderWidth = 2.0;
    orderBtn.layer.borderColor = [[UIColor whiteColor]CGColor];
    orderBtn.clipsToBounds = YES;
    orderBtn.layer.opaque = NO;
    
    indicator.hidden=YES;
    [indicator stopAnimating];
    [self prodListing];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)stafpicNo{
    
    
    NSString *post = [NSString stringWithFormat:@"id=%@",singloging.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/staffpics_no.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    
}
-(void)stafpicNo2{
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/staffpics_novendor.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    
}
- (IBAction)Settings:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(settings)withObject:Nil afterDelay:2.0f];
}


-(void)settings
{
    Vendor_profileAR *usr=[[Vendor_profileAR alloc]initWithNibName:@"Vendor_profileAR" bundle:NULL];
    [self.navigationController pushViewController:usr animated:NO];
    [ProgressHUD showSuccess:@""];
    
}


- (IBAction)Add:(id)sender
{
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@"Add Product"
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Instagram",@"Camera ",@"Camera Roll",nil];
    actionsheet1.tag=12;
    [actionsheet1 showInView:self.view];
//    EdititemsAR *item = [[EdititemsAR alloc]initWithNibName:@"EdititemsAR" bundle:nil];
//    [self.navigationController pushViewController:item animated:NO];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==12)
    {
        
        if (buttonIndex==0)
        {
            Vendor_InstapicsAR *vInstPics = [[Vendor_InstapicsAR alloc]initWithNibName:@"Vendor_InstapicsAR" bundle:nil];
            singloging.selectInstaPic1 = @"2";
            [self.navigationController pushViewController:vInstPics animated:NO];
        }
        if (buttonIndex==2)
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                
                //cameraimages.backgroundColor=[UIColor  grayColor];
                camerapicker =[[UIImagePickerController alloc]init];
                
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }
        }
        if (buttonIndex==1)
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    camerapicker =[[UIImagePickerController alloc]init];
                    camerapicker.delegate=self;
                    camerapicker.sourceType=UIImagePickerControllerSourceTypeCamera;
                    camerapicker.allowsEditing=YES;
                    [self presentViewController:camerapicker animated:YES
                                     completion:nil];
                }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"Error acessing camera"
                                      message:@"Device does not support a camera"
                                      delegate:nil
                                      cancelButtonTitle:@"Dismiss"
                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    cimage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    CGRect rect = CGRectMake(0,0,cimage.size.width/2,cimage.size.height/2);
    UIGraphicsBeginImageContext( rect.size );
    [cimage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageDataForResize = UIImagePNGRepresentation(picture1);
    UIImage *pimage=[UIImage imageWithData:imageDataForResize];
    singloging.imageCamera = [[UIImageView alloc]init];
    [singloging.imageCamera setImage: pimage];
    
    [camerapicker dismissViewControllerAnimated:YES completion:nil];
    
    EdititemsAR *items = [[EdititemsAR alloc]initWithNibName:@"EdititemsAR" bundle:nil];
    [self.navigationController pushViewController:items animated:YES];
}


-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/productlistingvendor.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"productlist"] isEqual:@"No product available"])
    {
        finalArray=[[NSMutableArray alloc]init];
        
         [self stafpicNo];
         [self stafpicNo2];
        
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"product list"]];
    }
    if([dataArray count]==0)
    {
        [itemTables reloadData];
    }
    else
    {
        
        
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"prodname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"subcatname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"subcatname"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"subcatname"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"prodimg1"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                
                [vendorNameArray addObject:vendorName1];
                
                [imageArr addObject:imagestr];
                
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"subcatname"];
                    
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"subcatname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"subcatname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:vendorName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        [itemTables reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    return [sectionAnimals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Vendor_ItemCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"VendorItemAR"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Vendor_ItemCellAR" bundle:nil] forCellReuseIdentifier:@"VendorItemAR"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"VendorItemAR"];
    }
    cell.backgroundColor=[UIColor whiteColor];
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(Vendor_ItemCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
    sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
    cell.prodName.text = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodname"];
    priceDec = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    soldout = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    
    if ([priceDec isEqualToString:@"Ask for Price"])
    {
        cell.priceLbl.text = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    }
    else if ([soldout isEqualToString:@"SOLD OUT"])
    {
        cell.priceLbl.text = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    }
    else
    {
        cell.priceLbl.text = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
        
    }
    cell.qtyLbl.text = [[sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"quantity"];
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
    cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sectionAnimals objectAtIndex:indexPath.row]objectForKey:@"prodimg1"]]];


}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
    singloging.sectionAnimals = [[NSMutableArray alloc]init];
    singloging.sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
    singloging.prodIdStr = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    singloging.eProdName = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodname"];
    singloging.eDesc = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prod_desc"];
    singloging.eQuant = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"quantity"];
    singloging.ePrice = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    singloging.eCat = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"catname"];
    singloging.eSubCat = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"subcatname"];
    singloging.epImg1 = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodimg1"];
    singloging.epImg2 = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodimg2"];
    singloging.epImg3 = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodimg3"];
    singloging.epImg4 = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodimg4"];
    singloging.epImg5 = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"prodimg5"];
    singloging.eCatId = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"catid"];
    singloging.eSubCatId = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"subcatid"];
    singloging.askforprice = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    singloging.soldout = [[singloging.sectionAnimals objectAtIndex:indexPath.row]valueForKey:@"soldout"];

    
    
    
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(nextPage) withObject:nil afterDelay:0.5f];
}

-(void)nextPage
{
    EdititemsAR *edit=[[EdititemsAR alloc]initWithNibName:@"EdititemsAR" bundle:nil];
    edit.editItem=@"edit";
    [ProgressHUD showSuccess:@""];
    [self.navigationController pushViewController:edit animated:YES];
    indicator.hidden=YES;
    [indicator stopAnimating];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *headerView = [[UIView alloc] init];
   
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
            vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
            vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 359, 40)];
            vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 398, 40)];
            vendor.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
        }
    }
    

    
    NSString *vendorName11 = [vendorNameArray objectAtIndex:section];
    vendor.text =[NSString stringWithFormat:@"%@",vendorName11];
    vendor.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1];
    vendor.backgroundColor =[UIColor clearColor];
    vendor.numberOfLines = 1;
    vendor.textAlignment = NSTextAlignmentRight;
    [headerView addSubview:vendor];
    headerView.backgroundColor=[UIColor clearColor];
    return headerView;

}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([singloging.nextPage isEqualToString:@"yes"])
    {
        singloging.nextPage=@"";
    }
}



- (IBAction)orders:(id)sender

{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(product)withObject:Nil afterDelay:0.5f];

    
}


-(void)product
{
    Vendor_ProductAR *product = [[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
    [self.navigationController pushViewController:product animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)chat:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(chatBtm)withObject:Nil afterDelay:0.5f];
}

-(void)chatBtm
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)home:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeBtm)withObject:Nil afterDelay:0.5f];
}

-(void)homeBtm
{
    Home_VetrinaARViewController *view = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
