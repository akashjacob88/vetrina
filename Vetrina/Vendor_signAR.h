//
//  Vendor_signAR.h
//  Vetrina
//
//  Created by Amit Garg on 6/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Vendor_signAR : UIViewController<UITextFieldDelegate>

{
    Singleton *singlogin;
    
    
}
@property (strong, nonatomic) IBOutlet UITextField *fieldName;
@property (strong, nonatomic) IBOutlet UITextField *fieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *fieldEmail;

- (IBAction)termsAction:(id)sender;

@end
