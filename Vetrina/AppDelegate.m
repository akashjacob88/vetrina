


//
//  AppDelegate.m
//  Vetrina
//
//  Created by Amit Garg on 4/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "AppDelegate.h"
#import "Walkthrough.h"
#import <Parse/Parse.h>
#import "Premium_Club.h"
#import "UploadPic.h"
#import "UploadpicAR.h"
#import "ViewController.h"
#import "JSON.h"
#import "ProgressHUD.h"
#import "Home_VetrinaARViewController.h"
#import <Quickblox/Quickblox.h>
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    singlogin =[Singleton instance];
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
   
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker"];
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker1"];
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker_ar"];
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker1_ar"];
    
    
   // [Parse setApplicationId:@"aj3ZiSBjq5GPFmDgETyItIDUX0m3PFRHr1kDlxI1" clientKey:@"qi2FPgLYLSRACQCgyLGOj4CXoz2wWzLJUbFVtXHj"];
//28 jan 2016
//    [Parse setApplicationId:@"XfW43e3lyh02vqRUJ1ayrXUYGDQXPxqN8S7j5fJR" clientKey:@"SWJyrUOpMmzK0DyvrmJbBjJNHM3bylb0O5dF9CEj"];
    
    
    
//    
//    [QBSettings setApplicationID:35399];
//    [QBSettings setAuthKey:@"eeJKRZtg8ZcFTZq"];
//    [QBSettings setAuthSecret:@"C5Sy7ukTU9UUfj-"];
//    [QBSettings setAccountKey:@"y2DDRacq5psqkXwYvYFL"];

    
    [QBSettings setApplicationID:37534];
    [QBSettings setAuthKey:@"j-JxeK535zNHRgy"];
    [QBSettings setAuthSecret:@"5e2E3YX85QhQH9a"];
    [QBSettings setAccountKey:@"tZDrsxUJ3m1nipVHk4yM"];
    
    
    
     [Parse setApplicationId:@"tE1hMWky9S9xRExbRlnPbkzg2gJN4g69RYItDJ5r" clientKey:@"cvQKOW4C1fZ4h087e2CWJ4n9JzA3aPs8LYpeeDZq"];
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
  
   
    [[PFUser currentUser] fetch];
    
    if ([PFUser currentUser])
        NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
        {
//            UploadPic *walk = [[UploadPic alloc] initWithNibName:@"UploadPic" bundle:nil];
//            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
//            self.window.rootViewController = nav;
            if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
            {
                
                [self autoLogingEmail];
                
            }
            else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
            {
                //[self autoLoginInstagram];
            }
            else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
            {
                [self LoginIG];
            }
            else
            {
                Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
                self.window.rootViewController = nav;
            }

        }
        else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
            {
                
                [self autoLogingEmail1];
                
            }
            else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
            {
                //[self autoLoginInstagram];
            }
            else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
            {
                [self LoginIG1];
            }
            else
            {
                Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
                self.window.rootViewController = nav;
                [ProgressHUD showSuccess:@""];
            }

        }
        else
        {
            Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
            self.window.rootViewController = nav;
        }
    }
//    else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
//    {
//        //[self autoLoginInstagram];
//    }
    else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
        {
            //            UploadPic *walk = [[UploadPic alloc] initWithNibName:@"UploadPic" bundle:nil];
            //            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
            //            self.window.rootViewController = nav;
            if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
            {
                
                [self autoLogingEmail];
                
            }
            else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
            {
                //[self autoLoginInstagram];
            }
            else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
            {
                [self LoginIG];
            }
            else
            {
                Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
                self.window.rootViewController = nav;
            }
            
        }
        else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
            {
                
                [self autoLogingEmail1];
                
            }
            else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
            {
                //[self autoLoginInstagram];
            }
            else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
            {
                [self LoginIG1];
            }
            else
            {
                Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
                self.window.rootViewController = nav;
                [ProgressHUD showSuccess:@""];
            }
    }
    else
    {
        Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
        self.window.rootViewController = nav;
    }

    }
    else
    {
        Walkthrough *walk = [[Walkthrough alloc] initWithNibName:@"Walkthrough" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:walk];
        self.window.rootViewController = nav;
    }
    
    [self.window makeKeyAndVisible];
    return YES;
}


//vendor

-(void)autoLogingEmail1
{
    
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"email"];
    NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
    [[NSUserDefaults standardUserDefaults] setObject:savedPassword forKey:@"pass_word"];

    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",savedUsername,savedPassword];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique User ID %@",uniqueid);
    NSString *uName=[eventarray  valueForKey:@"username"];
    NSString *uaddress=[eventarray  valueForKey:@"address"];
    singlogin.rate_status =[eventarray valueForKey:@"rate_status"];
    singlogin.check = [eventarray valueForKey:@"check"];
    singlogin.usrObjctId = [eventarray valueForKey:@"userobjectid"];
    NSString *venPic=[eventarray valueForKey:@"photo"];
    singlogin.userproilePic = venPic;
    singlogin.homAddress=uaddress;
    singlogin.usrName=uName;
    singlogin.userid=uniqueid;
    
    singlogin.userNameStr=uName;
    singlogin.user_id=[eventarray valueForKey:@"userobjectid"];

    
    [self prodListing1];
    
    if([neww isEqualToString:@"False"])
    {
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;        [ProgressHUD showSuccess:@""];
    }
    else
    {
        singlogin.loginStatus = @"User Login";
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;        [ProgressHUD showSuccess:@""];
    }
}

-(void)prodListing1
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if (cartListArray.count==0)
    {
        
    }
    else
    {
        NSInteger value=0;
        for(NSInteger i =0;i<cartListArray.count;i++)
        {
            NSString *status=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
            if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] )
            {
                value=value+1;
            }
        }
        singlogin.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    }
}



-(void)LoginIG1

{
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"VinstagramID"];
    
    NSString *post = [NSString stringWithFormat:@"instagramid=%@&logintype=%@",savedUsername,@"instagram"];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlogin2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    // NSArray *val = [eventarray valueForKey:@"0"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[eventarray  valueForKey:@"phoneno"];
    NSString *uName=[eventarray  valueForKey:@"username_instagramacnt"];
    NSString *shopName=[eventarray  valueForKey:@"fullname_shopname"];
    NSString *image=[eventarray  valueForKey:@"imagee"];
    singlogin.mainCat=[eventarray  valueForKey:@"main_catname"];
    singlogin.mainSubCat=[eventarray  valueForKey:@"main_subcatname"];
    singlogin.shopDec=[eventarray  valueForKey:@"descr"];
    singlogin.subId=[eventarray  valueForKey:@"subid"];
    singlogin.pid = [eventarray  valueForKey:@"staffpick"];
    singlogin.catid = [eventarray  valueForKey:@"main_catid"];
    singlogin.subCatid = [eventarray valueForKey:@"main_subcatid"];
    singlogin.objectIdStr = [eventarray valueForKey:@"objectid"];
    singlogin.vIGUserId =[eventarray valueForKey:@"instagramid"];
    
    singlogin.venPic = image;
    singlogin.vendorName=uName;
    singlogin.vUniqueId=uniqueid;
    singlogin.userid=uniqueid;
    singlogin.vPhone = phoneno;
    singlogin.shopNameVendr = shopName;
    
    singlogin.userNameStr=shopName;
    singlogin.user_id=[eventarray valueForKey:@"objectid"];
    
    //signLogin.loginStatus = @"IG Login";
    if([neww isEqualToString:@"False"])
    {
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
        [ProgressHUD showError:@""];
    }
    else
    {
        singlogin.loginStatus = @"IG Login";
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
        [ProgressHUD showSuccess:@""];
        
    }
}



//user

-(void)autoLogingEmail
{
    
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"email"];
    NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
    [[NSUserDefaults standardUserDefaults] setObject:savedPassword forKey:@"pass_word"];

    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",savedUsername,savedPassword];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique User ID %@",uniqueid);
    NSString *uName=[eventarray  valueForKey:@"username"];
    NSString *uaddress=[eventarray  valueForKey:@"address"];
    singlogin.rate_status =[eventarray valueForKey:@"rate_status"];
    singlogin.check = [eventarray valueForKey:@"check"];
    singlogin.usrObjctId = [eventarray valueForKey:@"userobjectid"];
    NSString *venPic=[eventarray valueForKey:@"photo"];
    singlogin.userproilePic = venPic;
    singlogin.homAddress=uaddress;
    singlogin.usrName=uName;
    singlogin.userNameStr=uName;
    singlogin.userid=uniqueid;
     singlogin.user_id=[eventarray valueForKey:@"userobjectid"];
    [self prodListing];
    
    if([neww isEqualToString:@"False"])
    {
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
    }
    else
    {
        singlogin.loginStatus = @"User Login";
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
        [ProgressHUD showSuccess:@""];
    }
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if (cartListArray.count==0)
    {
        
    }
    else
    {
        NSInteger value=0;
        for(NSInteger i =0;i<cartListArray.count;i++)
        {
            NSString *status=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
            if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] )
            {
                value=value+1;
            }
        }
        singlogin.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    }
}



-(void)LoginIG

{
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"VinstagramID"];
    
    NSString *post = [NSString stringWithFormat:@"instagramid=%@&logintype=%@",savedUsername,@"instagram"];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlogin2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    // NSArray *val = [eventarray valueForKey:@"0"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[eventarray  valueForKey:@"phoneno"];
    NSString *uName=[eventarray  valueForKey:@"username_instagramacnt"];
    NSString *shopName=[eventarray  valueForKey:@"fullname_shopname"];
    NSString *image=[eventarray  valueForKey:@"imagee"];
    singlogin.mainCat=[eventarray  valueForKey:@"main_catname"];
    singlogin.mainSubCat=[eventarray  valueForKey:@"main_subcatname"];
    singlogin.shopDec=[eventarray  valueForKey:@"descr"];
    singlogin.subId=[eventarray  valueForKey:@"subid"];
    singlogin.pid = [eventarray  valueForKey:@"staffpick"];
    singlogin.catid = [eventarray  valueForKey:@"main_catid"];
    singlogin.subCatid = [eventarray valueForKey:@"main_subcatid"];
    singlogin.objectIdStr = [eventarray valueForKey:@"objectid"];
    singlogin.vIGUserId =[eventarray valueForKey:@"instagramid"];
    
    singlogin.venPic = image;
    singlogin.vendorName=uName;
    singlogin.vUniqueId=uniqueid;
    singlogin.userid=uniqueid;
    singlogin.vPhone = phoneno;
    singlogin.shopNameVendr = shopName;
    
    singlogin.userNameStr=shopName;
    singlogin.user_id=[eventarray valueForKey:@"objectid"];

    
    //signLogin.loginStatus = @"IG Login";
    if([neww isEqualToString:@"False"])
    {
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
        [ProgressHUD showError:@""];
    }
    else
    {
        singlogin.loginStatus = @"IG Login";
        ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:home];
        self.window.rootViewController = nav;
        [ProgressHUD showSuccess:@""];
        
    }
}






- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    application.applicationIconBadgeNumber = 0;
    
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
   
//    [PFPush storeDeviceToken:deviceToken];
//    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error)
//     {
//         if (succeeded)
//             NSLog(@"Successfully subscribed to broadcast channel!");
//         else
//             NSLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
//     }];
//    
//    [[PFInstallation currentInstallation] addUniqueObject:@"pushType" forKey:@"channel"];
//    [[PFInstallation currentInstallation] saveEventually];
 
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    [currentInstallation saveInBackground];

//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    currentInstallation.channels = @[@"global"];
//    [currentInstallation saveInBackground];
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"device_token"];
    
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = deviceToken;
    
    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
        
        
        NSLog(@"%ld",(long)response.status);
        
    } errorBlock:^(QBResponse *response) {
        
        NSLog(@"%ld",(long)response.error);

        
    }];

    
    
}

- (void)application:(UIApplication *)application   didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
{
    
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"])
    {
    }
    else if ([identifier isEqualToString:@"answerAction"])
    {
        
    }
}




- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    if (error.code == 3010)
    {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    }
    else
    {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
  // [[UIApplication sharedApplication] setApplicationIconBadgeNumber:99];
    NSLog(@"%@", userInfo);
    [PFPush handlePush:userInfo];

//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:nil cancelButtonTitle:@"Reject" otherButtonTitles:@"Accept",nil];
//    [alertView show];

}


@end
