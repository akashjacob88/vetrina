//
//  CartVendorProductListAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface CartVendorProductListAR : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

{
    Singleton *singletonn;
    IBOutlet UITableView *prodListingTable;
    IBOutlet UILabel *userNameLbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITextField *sendMessageTxt;
    NSString *statusStr;
    
    IBOutlet UILabel *userAddressLbl;
    IBOutlet UIView *whatsappView;
    IBOutlet UIView *locationView;
    NSString *jointText,*orderIdStr,*name;
    NSMutableArray *dataArray,*finalArray;
    NSMutableArray *valu;


}

@end
