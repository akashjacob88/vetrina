//
//  Walkthrough.h
//  Vetrina
//
//  Created by Amit Garg on 4/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "Reachability.h"



@interface Walkthrough : UIViewController


{
 
    Singleton * signLogin;
    
    /* ----------- All Arrays ---------- */
    
    NSMutableArray *imagesArray;
    
    NSMutableArray *imgArray,*cartListArray;
    
    NSMutableArray *animationNrmlViewImg;
    
    NSMutableArray *swipImagesArray;
    
    /* ----------- IBoutlets ---------- */
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    IBOutlet UIImageView *slideImageView;
    
    IBOutlet UIButton *strtShoppingoult;
    
    IBOutlet UIPageControl *pageControl;

}

/* ----------- IBAction  ---------- */

- (IBAction)swipeHandle:(UISwipeGestureRecognizer *)sender;

- (IBAction)startShopping:(id)sender;

- (IBAction)arebic:(id)sender;





@end
