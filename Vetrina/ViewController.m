//
//  DiYuListViewController.m
//  IYLM
//
//  Created by Jian-Ye on 12-10-30.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "ViewController.h"
#import "Cell1.h"
#import "Cell2.h"
#import "JSON.h"
#import "All_SignIn.h"
#import "User_Settings.h"
#import "Favorites.h"
#import "ShopSearch.h"
#import "AsyncImageView.h"
#import "Subcatgory.h"
#import "CartVendorList.h"
#import "CartProductList.h"
#import "Premium_Club.h"
#import "ProgressHUD.h"
#import "SignUpScreen.h"
#import "RecentView.h"
#import "UserProfile.h"
#import "Vendor_Product.h"
#import "Vendor_items.h"
#import "Vendor_Profile.h"
#import "UIImageView+WebCache.h"

#import <Quickblox/Quickblox.h>
#import <Parse/Parse.h>
#import "AppConstant.h"

@interface ViewController()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *navView;
    IBOutlet UIView *topBar;
    NSMutableArray *recents;
    NSArray *dialog_Objects;
    NSData *dt;

    
}

@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;

@end

@implementation ViewController
@synthesize isOpen,selectIndex,indicatorView;

- (void)dealloc
{
   // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex = nil;
    settingIcon =nil;
    img33 =nil;
    indicatorView = nil;
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self)
    {
        
    }
    return self;
}

-(void)prodListing123
{
    //     NSString *post1;
    //    if ([singloging.loginStatus isEqual:@"User Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
    //    }
    //    else if ([singloging.loginStatus isEqual:@"IG Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    //    }
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singlogin.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSMutableArray *data_arr;
    
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        data_arr=[[NSMutableArray alloc]init];
    }
    else
    {
        data_arr = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    
    
    if (data_arr.count==0) {
        vendorChatLblk.hidden=YES;
        singlogin.count_cart=0;

        
    }else{
        int count1=0;
        
        for (int a=0; a<data_arr.count; a++) {
            
            NSString *aarra =[[data_arr objectAtIndex:a] valueForKey:@"statusid"];
            
            if ([aarra isEqualToString:@"2"]) {
                count1++;
            }
            
        }
        
        if (count1 == 0) {
            vendorChatLblk.hidden=YES;
            singlogin.count_cart=0;

        }else{
            
            
            vendorChatLblk.hidden=NO;
            vendorChatLblk.text=[NSString stringWithFormat:@"%d",count1];
            singlogin.count_cart=count1;
            vendorChatLblk.layer.cornerRadius=8.f;
            vendorChatLblk.layer.masksToBounds=YES;
            
        }
    }
    
    
    
    
}



- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singlogin.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}


- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }
    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    chatLbl.text=[NSString stringWithFormat:@"%d", total];
   // chatLbl.text=[NSString stringWithFormat:@"%d", total];
    _count_lbl_user.text=[NSString stringWithFormat:@"%d", total];
    
    singlogin.count_msg=total;
    
    if (singlogin.userid == nil)
    {
        chatLbl.hidden=YES;
        chatLbl.hidden = YES;
    }
    else
    {
        if ([chatLbl.text isEqualToString:@"0"])
        {
            chatLbl.hidden=YES;
            chatLbl.hidden = YES;
        }
        else if ([chatLbl.text isEqualToString:@""])
        {
            chatLbl.hidden=YES;
            chatLbl.hidden = YES;
        }
        else
        {
            chatLbl.hidden=NO;
            chatLbl.hidden = NO;
            chatLbl.layer.cornerRadius=chatLbl.frame.size.height/2;
            chatLbl.clipsToBounds=YES;
            chatLbl.layer.cornerRadius=chatLbl.frame.size.height/2;
            chatLbl.clipsToBounds=YES;
            
            
            _count_lbl_user.hidden=NO;
            _count_lbl_user.hidden = NO;
            _count_lbl_user.layer.cornerRadius=_count_lbl_user.frame.size.height/2;
            _count_lbl_user.clipsToBounds=YES;
            _count_lbl_user.layer.cornerRadius=_count_lbl_user.frame.size.height/2;
            _count_lbl_user.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}



- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users) {
        
        
        [recents addObjectsFromArray:users];
        
        
        NSMutableSet *seen = [NSMutableSet set];
     //   [seen addObject:[NSString stringWithFormat:@"%lu",(unsigned long)singlogin.uId]];
      //  NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.user_id] integerValue ];

        [seen addObject:[NSString stringWithFormat:@"%@",singlogin.user_id]];
        NSUInteger x = 0;
        
        while (x < [recents count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[recents objectAtIndex:x];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [recents removeObjectAtIndex:x];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                x++;
            }
        }
        
        
        
        
        
        
        
        
      //  [self chat];
        
        
        
        //    userNumber += users.count;
        
        
        //        if (pageInformation.totalEntries > userNumber) {
        //
        //
        //            [self retrieveAllUsersFromPage:pageInformation + 1];
        //        }
    } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}


- (void)viewDidLoad

{
    [super viewDidLoad];
    singlogin = [Singleton instance];
    signIn.layer.masksToBounds = YES;
    signIn.layer.cornerRadius = 5.0;
    signIn.layer.borderWidth = 2.0;
    signIn.layer.borderColor = [[UIColor whiteColor]CGColor];
    signIn.clipsToBounds = YES;
    signIn.layer.opaque = NO;
    
    
   

    [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"language"];

    recents=[[NSMutableArray alloc]init];
    
    
//    QBUUser *abc=[QBSession currentSession].currentUser;
//    
//    NSLog(@"here is id -------------------------------------------- %ld",(long)abc.customData);
//    
//    NSString *crr=abc.customData;
//    [settingIcon sd_setImageWithURL:[NSURL URLWithString:crr] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
//    NSInteger pro_pic = [[NSString stringWithFormat:@"%ld",(long)abc.blobID] integerValue];
    
  
    
//    [QBRequest downloadFileWithUID:[NSString stringWithFormat:@"%ld",(long)pro_pic] successBlock:^ (QBResponse *response , NSData *data) {
//        
//        
//        dt=data;
//        
//        UIImageView *im=[[UIImageView alloc]initWithFrame:CGRectMake(90, 5, 60, 60)];
//        im.image=[UIImage imageWithData:data];
//        //im.backgroundColor=[UIColor orangeColor];
//        [topBar addSubview:im];
//        
//        
//        
//        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"%@",str);
//        
//        
//
//        
//    
//    }statusBlock:nil errorBlock:nil];
//    
    
    
    vendorChatLblk.hidden = YES;
    chatLbl.hidden = YES;
    _count_lbl_user.hidden=YES;
    
    [self signIn];
    [self catlist];
    view1.hidden=YES;
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        navView.hidden = YES;
        vendorViewBar.hidden = NO;
        [self retrieveAllUsersFromPage:1];
        [self loadRecents];
        [self prodListing123];

    }
    else if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [self searchShops];
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
       [self retrieveAllUsersFromPage:1];
        [self loadRecents];

        [self cartTotalView];

    }
    else
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        [self cartTotalView];

    }

    
    
    self.expansionTableView.layer.shadowRadius = 8.0f;
    self.expansionTableView.layer.shadowOffset = CGSizeMake(0, 3);
    self.expansionTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.expansionTableView.layer.shadowOpacity = 0.8f;
    
    
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    [self.navigationController setNavigationBarHidden:YES];

    
    
    [indicatorView stopAnimating];
    [indicatorView setHidesWhenStopped:YES];
    
    
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        
//                [self searchItems];
//                [self searchShops];
//        //set your image on main thread.
//        
//    });
    
        
}


-(void)searchItems

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlist.php"];
    // NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlist2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        singlogin.searchItemArray=[[NSMutableArray alloc]init];
        singlogin.searchItemArray=searchArray;
    }
}


-(void)searchShops

{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
     NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlist.php"];
   // NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/new_premium_venlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        singlogin.searchShopArray=[[NSMutableArray alloc]init];
        singlogin.searchShopArray=searchArray;
    }
}


#pragma mark - Cart Label Update

-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
        }
    }
}


#pragma mark - Category Service

-(void)catlist
{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/subcatlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    
    if (dataArray.count > 0)
    {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"subcatname"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [sortedArray1 mutableCopy];
    
    
    NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"catname" ascending:YES];
    NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
    sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
    
    NSLog(@"sorted array is %@",sortedArray);
    
    newArray=[[NSMutableArray alloc]init];
    array=[[NSMutableArray alloc]init];
    finalArray=[[NSMutableArray alloc]init];
    
    vendorNameArray=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
    
    for( i =0;i<sortedArray.count;i++)
    {
        stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
        
        NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"catname"];
        NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
        if (![stringOne isEqualToString:stringTwo ])
        {
            [vendorNameArray addObject:vendorName1];
            
            [imageArr addObject:imagestr];

            stringTwo = stringOne;
            
            if ([newString isEqualToString:@"newString"])
            {
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];

                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            if ([newString isEqualToString:@"oneString"])
            {
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            newString=@"oneString";
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
        }
        else
        {
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
            newString=@"newString";
        }
    }
    
    NSInteger a=i-1;
    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];
    dynamicDict=[[NSMutableDictionary alloc]init];
    [dynamicDict setValue:newArray forKey:vendorName];
    [finalArray addObject:dynamicDict];
    
    dynamicDict=[[NSMutableDictionary alloc]init];
    }
    else
    {
        finalArray = [[NSMutableArray alloc]init];
        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
    }
    
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex.section == section)
        {
                NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
                sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
                return [sectionAnimals count]+1;
        }
    }
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0)
    {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"subcatname"];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        cell.titleLabel.text = sectionTitle;
    [cell.iconImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]] placeholderImage:nil];
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            
        }
        else
        {
            if (!self.selectIndex)
            {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
           [ProgressHUD show:@"Loading..." Interaction:NO];
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            
            itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
            singlogin.subName = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"subcatname"];
            singlogin.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
            [self performSelector:@selector(subcatgory)withObject:Nil afterDelay:0.2f];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)subcatgory

{
    [ProgressHUD showSuccess:@""];
    Subcatgory *sub  = [[Subcatgory alloc]initWithNibName:@"Subcatgory" bundle:nil];
    [self.navigationController pushViewController:sub animated:NO];
    [indicatorView stopAnimating];
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex.section;
    
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
	NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	
    for ( i = 1; i < contentCount + 1; i++)
    {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
	[rowToInsert release];
	
	[self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
   
    if ([sectionTitle isEqualToString:@"My Favorites"])
    {
        
        cell.arrowImageView.image=[UIImage imageNamed:@"arrow_right.png"];
        cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
        
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [indicatorView startAnimating];
        
        if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
        {
            [self performSelector:@selector(favrt)withObject:Nil afterDelay:0.2f];
        }
        else
        {
          view1.hidden = NO;
           [ProgressHUD showError:@""];
         [indicatorView stopAnimating];
            
        }
        
    }
    else if ([sectionTitle isEqualToString:@"Premium Club"])
    {
        cell.arrowImageView.image=[UIImage imageNamed:@"arrow_right.png"];
        cell.arrowImageView.contentMode=UIViewContentModeScaleAspectFit;
        [indicatorView startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(premium)withObject:Nil afterDelay:0.2f];
        
    }
    else
    {
     if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }

}


#pragma mark - Button Actions


-(void)premium
{
    [ProgressHUD showSuccess:@""];

    Premium_Club *club = [[Premium_Club alloc]initWithNibName:@"Premium_Club" bundle:nil];
    singlogin.primium = @"";
    [self.navigationController pushViewController:club animated:YES];
    [indicatorView stopAnimating];
    indicatorView.hidden = YES;
}



-(void)signIn

{
    if ([singlogin.loginStatus isEqual:@"User Login"])
    {
        signIn.hidden = YES;
        settingIcon.hidden = NO;
        NSString *strImageUrl1 = singlogin.userproilePic;
        
        
        
        
        if (strImageUrl1==[NSNull class] || strImageUrl1.length<1 || [strImageUrl1 isKindOfClass:Nil]) {
            
        }else
        [settingIcon sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        settingIcon.layer.masksToBounds = YES;
        settingIcon.layer.cornerRadius = settingIcon.frame.size.width/2;
        settingIcon.layer.opaque = NO;
    }
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        signIn.hidden = YES;
        settingIcon.hidden = NO;
        NSString *strImageUrl1 = singlogin.venPic;
        [settingIcon sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        settingIcon.layer.masksToBounds = YES;
        settingIcon.layer.borderWidth = 1.0f;
        settingIcon.layer.cornerRadius = settingIcon.frame.size.width/2;;
        settingIcon.layer.borderColor = [[UIColor clearColor]CGColor];
        settingIcon.clipsToBounds = YES;
        settingIcon.layer.opaque = NO;
    }
    else
    {
        settingIcon.hidden = YES;
        cart.hidden = YES;
        signIn.hidden = NO;
    }
}


-(void) signUp

{
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [self performSelector:@selector(setting)withObject:Nil afterDelay:0.2f];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        indicatorView.hidden = NO;
        [indicatorView startAnimating];
    }
    else if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [self performSelector:@selector(VendorSetting)withObject:Nil afterDelay:0.2f];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        indicatorView.hidden = NO;
        [indicatorView startAnimating];
    }
    else
    {
       SignUpScreen *back = [[SignUpScreen alloc]initWithNibName:@"SignUpScreen" bundle:NULL];
       [self.navigationController pushViewController:back animated:NO];
    }
}


-(void)setting
{
    

    UserProfile *usr=[[UserProfile alloc]initWithNibName:@"UserProfile" bundle:NULL];
    usr.test=@"no";
    [self.navigationController pushViewController:usr animated:NO];
    indicatorView.hidden = YES;
    [indicatorView stopAnimating];
    [ProgressHUD showSuccess:@""];

}


-(void)VendorSetting
{
 
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(backBtn) withObject:nil afterDelay:0.2f];
}

-(void)backBtn
{
    Vendor_Profile *usr=[[Vendor_Profile alloc]initWithNibName:@"Vendor_Profile" bundle:NULL];
    [self.navigationController pushViewController:usr animated:NO];
    indicatorView.hidden = YES;
    [indicatorView stopAnimating];
    [ProgressHUD showSuccess:@""];
}








- (IBAction)signBtn:(id)sender

{
    singlogin.loginfrom = @"loging";
    [self signUp];
}

- (IBAction)search:(id)sender
{
    [indicatorView setHidden:NO];
    [indicatorView startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeMethod) withObject:nil afterDelay:0.2f];
}


-(void)homeMethod

{
    ShopSearch *shopVen = [[ShopSearch alloc]initWithNibName:@"ShopSearch" bundle:NULL];
    [self.navigationController pushViewController:shopVen animated:NO];
    [indicatorView stopAnimating];
    indicatorView.hidden=YES;
    [ProgressHUD showSuccess:@""];
}

- (IBAction)chartBtn:(id)sender
{
    if (singlogin.userid == nil)
    {
        [indicatorView stopAnimating];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        [indicatorView setHidden:NO];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [indicatorView startAnimating];
        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.2f];
    }
}
-(void)nextpage
{
    [ProgressHUD showSuccess:@""];

    CartProductList *home = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:NULL];
    [self.navigationController pushViewController:home animated:NO];
    [indicatorView setHidden:YES];
    [indicatorView stopAnimating];
}

- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singlogin.loginfrom = @"cart";
    SignUpScreen *signInn = [[SignUpScreen alloc]initWithNibName:@"SignUpScreen" bundle:NULL];
    [self.navigationController pushViewController:signInn animated:NO];
}
- (IBAction)noThanks:(id)sender
{
    [indicatorView stopAnimating];
    [indicatorView setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}



- (IBAction)myFavrtBtn:(id)sender

{
    [indicatorView startAnimating];
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(favrt)withObject:Nil afterDelay:0.2f];
    }
    else
    {
        view1.hidden = NO;
        [indicatorView stopAnimating];
    }
   
}
-(void)favrt

{
    Favorites *fav = [[Favorites alloc]initWithNibName:@"Favorites" bundle:nil];
    [self.navigationController pushViewController:fav animated:NO];
    [ProgressHUD showSuccess:@""];

    [indicatorView stopAnimating];
}



- (IBAction)chat:(id)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
       [self performSelector:@selector(chatBtn)withObject:Nil afterDelay:0.2f];
    }
    else
    {
        view1.hidden = NO;
        [indicatorView stopAnimating];
    }

}


-(void)chatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    
}



- (IBAction)permotionalBtn:(id)sender
{
}

-(void)viewWillAppear:(BOOL)animated

{
    
    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
        
    }];
    
     [self cartTotalView];
    [self signIn];
    [self.navigationController setNavigationBarHidden:YES];

}


- (IBAction)vendorProductbtn:(id)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorProductBtn)withObject:Nil afterDelay:0.2f];
    }
    else
    {
        view1.hidden = NO;
        [indicatorView stopAnimating];
    }
    
}


-(void)vendorProductBtn
{
    Vendor_Product *view = [[Vendor_Product alloc]initWithNibName:@"Vendor_Product" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorChat:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorchatBtn)withObject:Nil afterDelay:0.2f];
    }
    else
    {
        view1.hidden = NO;
        [indicatorView stopAnimating];
    }
    
}


-(void)vendorchatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorItems:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorItemsBtn)withObject:Nil afterDelay:0.2f];
    }
    else
    {
        view1.hidden = NO;
        [indicatorView stopAnimating];
    }
    
}


-(void)vendorItemsBtn
{
    Vendor_items *view = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


@end
