//
//  FavItems.m
//  Vetrina
//
//  Created by Amit Garg on 6/11/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "FavItems.h"
#import "ViewController.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "ProgressHUD.h"
#import "CartProductList.h"
#import "CustomItem_CellTableViewCell.h"
#import "Vendor_Detaillist.h"
#import "ShopSearch.h"
#import "AppConstant.h"
#import "push.h"
#import <Parse/Parse.h>
#import "recent.h"
#import "RecentView.h"


@interface FavItems ()
{
    NSMutableArray *users;
}

@end

@implementation FavItems

- (void)viewDidLoad

{
    [super viewDidLoad];
    singlogin = [Singleton instance];
    itemsLbl.layer.cornerRadius = 5.0;
    users = [[NSMutableArray alloc]init];
    
    
    // TopBar View///
    
    topbsr.layer.shadowRadius = 2.0f;
    topbsr.layer.shadowOffset = CGSizeMake(0, 2);
    topbsr.layer.shadowColor = [UIColor blackColor].CGColor;
    topbsr.layer.shadowOpacity = 0.5f;

    
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
    {
//        PFUser *user = [PFUser currentUser];
//        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
//        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
//        [query orderByAscending:PF_USER_FULLNAME];
//        [query setLimit:1000];
//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//         {
//             if (error == nil)
//             {
//                 [users removeAllObjects];
//                 [users addObjectsFromArray:objects];
//                 [itemTble reloadData];
//             }
//             else [ProgressHUD showError:@"Network error."];
//         }];
    }

    shoplab.layer.masksToBounds = YES;
    shoplab.layer.cornerRadius = 5.0;
    shoplab.layer.borderWidth = 2.0;
    shoplab.layer.borderColor = [[UIColor whiteColor]CGColor];
    shoplab.clipsToBounds = YES;
    shoplab.layer.opaque = NO;
    
    chrtLbl.hidden=YES;
    [self cartTotalView];
    [self vendornamelisting];
}


-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
        }
    }
}


-(void)vendornamelisting

{
    
    
    NSString *post = [NSString stringWithFormat:@"userid=%@", singlogin.userid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlistfav.php"];
    
    NSLog(@"PostData--%@",post);

    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No vendor available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    if([arra_1 count]==0)
    {
        
        view2.hidden = NO;
        [itemTble setHidden:YES];
    }
    else
    {
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"id" ascending:NO];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [arra_1 sortedArrayUsingDescriptors:sortedarray1];
        
        NSLog(@"--------%@",sortedarray1);
        
        [itemTble setHidden:NO];
        view2.hidden = YES;
        [itemTble reloadData];
    }
}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sortedarray1 count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 495;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    CustomItem_CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell6"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CustomItem_CellTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell6"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell6"];
    }
    return cell;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(CustomItem_CellTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectional = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    cell.vendrNme.text =[NSString stringWithFormat:@"%@",sectional];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
    cell.vendorpic.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"vendorpic"]]];
    cell.vendorpic.layer.masksToBounds = YES;
    cell.vendorpic.layer.cornerRadius = 20.0;
    cell.vendorpic.layer.opaque = NO;
    
    NSString *sectional2 = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"prodname"];
    cell.productName.text =[NSString stringWithFormat:@"%@",sectional2];
    
    askforPrice = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    soldout = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    
    
    if ([askforPrice isEqualToString:@"Ask for Price"])
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    }
    else if ([soldout isEqualToString:@"SOLD OUT"])
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    }
    else
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    }
    
    
    staffPic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"staffpick"];
    
    
    if ([staffPic isEqualToString:@"yes"])
    {
        cell.premiumClub.hidden = NO;
    }
    else
    {
        cell.premiumClub.hidden = YES;
    }
    
    NSString *sectional4 = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"prod_desc"];
    cell.productDic.text =[NSString stringWithFormat:@"%@",sectional4];
    
    if ([soldout isEqualToString:@"SOLD OUT"])
    {
        cell.cartBtn.hidden = YES;
        cell.btnLbl.hidden = YES;
    }
    else
    {
        [cell.cartBtn addTarget:self action:@selector(orderBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell.sendBtn addTarget:self action:@selector(VendrBtn:) forControlEvents:UIControlEventTouchUpInside];
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:img3.imageURL];
    cell.itmeImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prodimg1"]]];
}



-(void)VendrBtn:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell * )sender.superview.superview;
    NSIndexPath *indexPath = [itemTble indexPathForCell:cell];
    singlogin.SlctVenId=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
    singlogin.venName = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    singlogin.objectIdStr = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];
    
    //[indicator setHidden:NO];
    //[indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(detailbtn) withObject:Nil afterDelay:2.0f];
}


-(void)detailbtn

{
    Vendor_Detaillist *vender = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:nil];
    [self.navigationController pushViewController:vender animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(void)orderBtn:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *cellIndexPath = [itemTble indexPathForCell:cell];
    productIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"id"];
    prodImgStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"prodimg1"];
    unitPriceStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"unitprice"];
    productNameStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"prodname"];
    vendorImgStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorpic"];
    vendorIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorid"];
    vendorNameStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorname"];
    objectIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"objectid"];
    
    askforPrice=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"negotiable_price"];
    if ([askforPrice isEqualToString:@"Ask for Price"])
    {
        unitPriceStr=@"";
    }
    
    [self prodListing];
    
    if (cartListArray.count==0) {
        [self addtocart];
        
    }
    else
    {
        NSString *exist;
        for (NSInteger i=0; i<cartListArray.count; i++)
        {
            NSString *productIdStr1=[[cartListArray objectAtIndex:i]valueForKey:@"productid"];
            cartIdStr=[[cartListArray objectAtIndex:i]valueForKey:@"id"];
            quantStr=[[cartListArray objectAtIndex:i]valueForKey:@"quantityprod"];
            
            if ([productIdStr1 isEqualToString:productIdStr])
            {
                exist=@"yes";
                i=cartListArray.count;
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"Product is already in cart! Do you want to increase quantity?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Add"
                                                        otherButtonTitles:@"Cancel", nil];
                [myAlert show];
                
                
                
            }
        }
        if ([exist isEqualToString:@"yes"]) {
            
        }
        else
        {
            
            NSString *match;
            for (NSInteger i=0; i<cartListArray.count; i++)
            {
                NSString *vendoridS=[[cartListArray objectAtIndex:i]valueForKey:@"vendorid"];
                NSString *statusStr=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
                
                if ([vendoridS isEqualToString:vendorIdStr])
                {
                    if ([statusStr isEqualToString:@"0"] || [statusStr isEqualToString:@"1"] ) {
                        match=@"yes";
                        i=cartListArray.count;
                        [ProgressHUD show:@"Loading..." Interaction:NO];
                        
                        [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
                        
                    }
                    else
                    {
                        match=@"donot";
                        i=cartListArray.count;
                        
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Your previous order is under being processed.Kindly Wait!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else
                {
                    match=@"no";
                }
            }
            if ([match isEqualToString:@"no"]) {
                [ProgressHUD show:@"Loading..." Interaction:NO];
                
                [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
                
            }
            
        }
    }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(editCartView) withObject:nil afterDelay:0.5f];
    }
    else if (buttonIndex==1)
    {
        
    }
}


-(void)editCartView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartquant.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSInteger aa=[quantStr integerValue];
    aa=aa+1;
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%li",(long)aa];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
    }
    [ProgressHUD showSuccess:@""];
}



-(void)addtocart
{
    if (singlogin.userid == nil)
    {
        
        NSLog(@"Please Login to add cart");
    }
    else
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addtocart.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *productidStr=[[NSString alloc]initWithFormat:@"%@",productIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",productidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *imagee=[[NSString alloc]initWithFormat:@"%@",prodImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",imagee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *askprice1=[[NSString alloc]initWithFormat:@"%@",askforPrice];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",askprice1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *pic1=[[NSString alloc]initWithFormat:@"%@",singlogin.userproilePic];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userprofilepic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",pic1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        
        
        NSString *price1=[[NSString alloc]initWithFormat:@"%@",unitPriceStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",price1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        if ([askforPrice isEqualToString:@"Ask for Price"])
        {
            NSString *statusidStr=@"1";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            for (NSInteger i=0; i< users.count; i++)
            {
//                name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//                if ([name containsString:objectIdStr])
//                {
//                    PFUser *user1 = [PFUser currentUser];
//                    PFUser *user2 = users[i];
//                    NSString *groupId = StartPrivateChat(user1, user2);
//                    NSString *text = @"Asked for price";
//                    // [self actionChat:groupId];
//                    SendPushNotification(groupId, text);
//                    i = users.count;
//                }
            }

        }
        else
        {
            NSString *statusidStr=@"0";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSString *final=@"1";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantityprod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",final] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *name11=[[NSString alloc]initWithFormat:@"%@",productNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",name11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",vendorImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",vendorIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",vendorNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        
        NSInteger j = [singlogin.totalCart integerValue];
        j = j+1 ;
        singlogin.totalCart = [NSString stringWithFormat:@"%ld",(long)j];
        chrtLbl.text=singlogin.totalCart;
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Product added to cart" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [self cartTotalView];
        
    }
    
    [ProgressHUD showSuccess:@""];
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}


- (IBAction)back:(id)sender

{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
}


- (IBAction)search:(id)sender
{
    ShopSearch *shop = [[ShopSearch alloc]initWithNibName:@"ShopSearch" bundle:nil];
    [self.navigationController pushViewController:shop animated:YES];
}


- (IBAction)cart:(id)sender
{
    
    [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
}



-(void)nextpage
{
    CartProductList *home = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:NULL];
    [self.navigationController pushViewController:home animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)shop:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self cartTotalView];
    [self.navigationController setNavigationBarHidden:YES];

}


- (IBAction)chat:(id)sender
{
    [self performSelector:@selector(cht)withObject:Nil afterDelay:0.2f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
}


-(void)cht
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)home:(id)sender
{
    
    [self performSelector:@selector(home2)withObject:Nil afterDelay:0.2f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
}


-(void)home2
{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}









- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
