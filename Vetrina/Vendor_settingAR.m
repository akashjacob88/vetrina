//
//  Vendor_settingAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/1/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_settingAR.h"
#import "Base64.h"
#import "JSON.h"
#import "BSKeyboardControls.h"
#import "Home_VetrinaARViewController.h"
#import "Vendor_setting.h"
#import "Vendor_ProductAR.h"
#import "Walkthrough.h"
#import "ViewController.h"
#import "HomeAR_Cell1.h"
#import "HomeAR_Cell2.h"
#import "AsyncImageView.h"
#import "ProgressHUD.h"
#import "Vendor_Product.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>


#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"



@interface Vendor_settingAR ()<UITableViewDataSource,UITableViewDelegate>

{
    IBOutlet UIView *topBar;
    IBOutlet UILabel *nameShwdowLbl;
    IBOutlet UILabel *taxtViewShedow;
    IBOutlet UILabel *catShedow;
    IBOutlet UIButton *confrmShedow;
    
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex1;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;

@end

@implementation Vendor_settingAR

@synthesize expansionTableView,isOpen,selectIndex1;
- (void)dealloc
{
    // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex1 = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;

    singling = [Singleton instance];
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    nameShwdowLbl.layer.shadowRadius = 2.0f;
    nameShwdowLbl.layer.shadowOffset = CGSizeMake(0, 2);
    nameShwdowLbl.layer.shadowColor = [UIColor blackColor].CGColor;
    nameShwdowLbl.layer.shadowOpacity = 0.5f;
    
    taxtViewShedow.layer.shadowRadius = 2.0f;
    taxtViewShedow.layer.shadowOffset = CGSizeMake(0, 2);
    taxtViewShedow.layer.shadowColor = [UIColor blackColor].CGColor;
    taxtViewShedow.layer.shadowOpacity = 0.5f;
    
    confrmShedow.layer.shadowRadius = 2.0f;
    confrmShedow.layer.shadowOffset = CGSizeMake(0, 2);
    confrmShedow.layer.shadowColor = [UIColor blackColor].CGColor;
    confrmShedow.layer.shadowOpacity = 0.5f;
    
    
    catShedow.layer.shadowRadius = 2.0f;
    catShedow.layer.shadowOffset = CGSizeMake(0, 2);
    catShedow.layer.shadowColor = [UIColor blackColor].CGColor;
    catShedow.layer.shadowOpacity = 0.5f;
    
    
    
    vendorName.text = singling.shopNameVendr;
    instaLbl.text = singling.vendorName;
    phoneText.text = singling.vPhone;
    
    
 //   ShowDec.text = singling.shopDec;
    view1.hidden = YES;
    cat1.text = singling.mainCat;
    subcat1.text = singling.mainSubCat;
    [indicater stopAnimating];
    
    NSArray *fields = @[vendorName];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    
    NSURL *url1=[NSURL URLWithString:singling.venPic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    vendorImg.image=emppppic;
    vendorImg.layer.masksToBounds = YES;
    vendorImg.layer.cornerRadius = 45.0;
    vendorImg.layer.opaque = NO;
    
    [scrolVIew  setScrollEnabled:YES];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            scrolVIew.frame=CGRectMake(0,64, 320,416);
            
            [scrolVIew setContentSize:CGSizeMake(320, 517)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            scrolVIew.frame=CGRectMake(0, 64, 320, 504);
            
            [scrolVIew setContentSize:CGSizeMake(320, 604)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            scrolVIew.frame=CGRectMake(0, 64,self.view.frame.size.width,603);
            
            [scrolVIew setContentSize:CGSizeMake(320, 704)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            scrolVIew.frame=CGRectMake(0, 64, self.view.frame.size.width, 672);
            
            [scrolVIew setContentSize:CGSizeMake(320, 772)];
        }
    }
    else
    {
        
    }
    [scrolVIew setAutoresizesSubviews:YES];
    [scrolVIew setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleWidth];
}


#pragma mark - Servise

- (IBAction)addproduct2:(id)sender

{
    view1.hidden = NO;
    [self catlist];
   
    addBtnStr = @"2";
   
    
}


-(void)catlist
{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/subcatlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    if (dataArray.count > 0)
    {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"a_subcatname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"a_catname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        subCatArry=[[NSMutableArray alloc]init];
        
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"a_catname"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
            NSString *catidStr=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
            
            
            
            
            if (![stringOne isEqualToString:stringTwo ])
            {
                
                [vendorNameArray addObject:vendorName1];
                [subCatArry addObject:catidStr];
                
                
                
                [imageArr addObject:imagestr];
                
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];
                    
                    
                    
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:catidStr];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];
                    
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:catidStr];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *catidStr=[[sortedArray objectAtIndex:a]valueForKey:@"catid"];
        
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:catidStr];
        [finalArray addObject:dynamicDict];
    }
    else
    {
        finalArray = [[NSMutableArray alloc]init];
        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
        
    }
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
    
    [expansionTableView reloadData];
    
    
}


#pragma mark - Table view data source and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex1.section == section)
        {
            //   return [[[_dataList objectAtIndex:section] objectForKey:@"list"] count]+1;;
            
            NSString *sectionTitle = [subCatArry objectAtIndex:section];
            sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
            return [sectionAnimals count]+1;
            
            
        }
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex1.section == indexPath.section&&indexPath.row!=0)
    {
        HomeAR_Cell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell2" bundle:nil] forCellReuseIdentifier:@"Cell_Ar2"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        }
        
        NSString *sectionTitle = [subCatArry objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        //     NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"list"];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"a_subcatname"];
        return cell;
    }
    else
    {
        HomeAR_Cell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell1" bundle:nil] forCellReuseIdentifier:@"Cell_Ar1"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        }
        //    NSString *name = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        
        cell.titleLabel.text = sectionTitle;
        
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:Img.imageURL];
//        cell.iconImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
//        [cell changeArrowWithUp:([self.selectIndex1 isEqual:indexPath]?YES:NO)];
        
        
        cell.titleLabel.textColor=[UIColor whiteColor];
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:Img.imageURL];
        cell.iconImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
        [cell changeArrowWithUp:([self.selectIndex1 isEqual:indexPath]?YES:NO)];
        return cell;

        
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex1])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex1 = nil;
            
        }
        else
        {
            if (!self.selectIndex1)
            {
                self.selectIndex1 = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
//        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
//        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        NSString *sectionTitle = [subCatArry objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
        singling.catid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
        
        NSLog(@"%@",singling.catid);
        
        singling.mainCat =[[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_catname"];
        NSLog(@"%@",singling.mainCat);

        
        singling.mainSubCat = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_subcatname"];
        NSLog(@"%@",singling.mainSubCat);

        
        singling.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
        NSLog(@"%@",singling.subCatid);

        
        if ([addBtnStr isEqualToString:@"2"])
        {
            cat1.text = singling.mainCat;
            subcat1.text = singling.mainSubCat;
        }

        
        
        view1.hidden = YES;
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    HomeAR_Cell1 *cell = (HomeAR_Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex1];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex1.section;
    
    NSString *sectionTitle = [subCatArry objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];

    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex1 = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)close:(id)sender
{
    view1.hidden = YES;
}


- (IBAction)back:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"آسف!" message:@"Changes have not been saved, are you sure you want to leave?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 9;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (alertView.tag == 10)
    {
        if (buttonIndex==0)
        {
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"VinstagramID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [PFUser logOut];
            ParsePushUserResign();
            PostNotification(NOTIFICATION_USER_LOGGED_OUT);
            LoginUser(self);
            singling.loginStatus = nil;
            singling.userid = nil;
            Home_VetrinaARViewController *home = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
            [self.navigationController pushViewController:home animated:YES];
            
        }
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    
    [vendorName resignFirstResponder];
    [phoneText resignFirstResponder];
    
    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [vendorName resignFirstResponder];
    [phoneText resignFirstResponder];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == vendorName || textField == phoneText )
    {
        [self animateTextField:textField up:YES];
    }
    [keyboardControls setActiveField:textField];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField==vendorName || textField==phoneText)
    {
        [self animateTextField:textField up:NO];
    }
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)animateTextView:(UITextView *)textView up:(BOOL)up
{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView==ShowDec)
    {
        [self animateTextView:textView up:YES];
    }
    
    if ([textView.text isEqualToString:@"تفاصيل المحل"])
    {
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView==ShowDec)
    {
        [self animateTextView:textView up:NO];
    }
    
    if ([textView.text isEqualToString:@""])
    {
        textView.text = @"تفاصيل المحل";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}


- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}


- (IBAction)logoutBtn:(id)sender

{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Log Out" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 10;
}





- (IBAction)ComfrmBtn:(id)sender

{
    if (vendorName.text.length > 0 && cat1.text.length > 0 && subcat1.text.length > 0 )
     {
         
         [ProgressHUD show:@"Loading..." Interaction:NO];
         
         [self performSelector:@selector(Submitted)withObject:Nil afterDelay:3.0f];
     }
     else
     {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
     }
}


-(void)Submitted

{
///////////////////////////////////
    
    
    
    
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editvendor1.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singling.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *shopNamestr=[[NSString alloc]initWithFormat:@"%@",vendorName.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fullname_shopname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",shopNamestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSString *shopDecstr=[[NSString alloc]initWithFormat:@"%@",ShowDec.text];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"descr\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",shopDecstr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSString *phonestr=[[NSString alloc]initWithFormat:@"%@",phoneText.text];
    //    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[[NSString stringWithFormat:@"%@",phonestr] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *catIDstr=[[NSString alloc]initWithFormat:@"%@",singling.catid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"main_catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catIDstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *catstr=[[NSString alloc]initWithFormat:@"%@",singling.mainCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"main_catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatIdstr=[[NSString alloc]initWithFormat:@"%@",singling.subCatid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"main_subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatstr=[[NSString alloc]initWithFormat:@"%@",singling.mainSubCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"main_subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subid=[[NSString alloc]initWithFormat:@"%@",singling.subId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subid] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[[Base64 encode:myimagedata]dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSMutableArray *valu = [val valueForKey:@"detail"];
    
    
    
    NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    //  NSString *phoneno=[[valu objectAtIndex:0] valueForKey:@"phoneno"];
    NSString *uName=[[valu objectAtIndex:0] valueForKey:@"fullname_shopname"];
    NSString *shopDec=[[valu objectAtIndex:0] valueForKey:@"descr"];
    //NSString *venPic=[[valu objectAtIndex:0] valueForKey:@"imagee"];
    NSString *catname=[[valu objectAtIndex:0] valueForKey:@"main_catname"];
    NSString *subcatname=[[valu objectAtIndex:0] valueForKey:@"main_subcatname"];
    NSString *catid=[[valu objectAtIndex:0] valueForKey:@"main_catid"];
    NSString *subcatid=[[valu objectAtIndex:0] valueForKey:@"main_subcatid"];
    
    
    
    singling.shopNameVendr=uName;
    singling.vUniqueId=uniqueid;
    singling.shopDec=shopDec;
    //singling.vPhone = phoneno;
    // singling.venPic = venPic;
    singling.mainCat = catname;
    singling.mainSubCat = subcatname;
    singling.catid = catid;
    singling.subCatid  = subcatid;
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Profile is Edited" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
    [ProgressHUD showSuccess:@""];
    [indicater stopAnimating];
    
    
 ///////////////////////////////////
//    NSData *myimagedata = UIImageJPEGRepresentation(vendorImg.image, 90);
    
 /*
    
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editvendor1.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singling.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *shopNamestr=[[NSString alloc]initWithFormat:@"%@",vendorName.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fullname_shopname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",shopNamestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *shopDecstr=[[NSString alloc]initWithFormat:@"%@",ShowDec.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"descr\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",shopDecstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSString *phonestr=[[NSString alloc]initWithFormat:@"%@",phoneText.text];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",phonestr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([addBtnStr isEqualToString:@"2"])
    {
        NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singling.catid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    //    NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singling.catName];
        NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singling.mainCat];

        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singling.subCatid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
     //   NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singling.subName];
        
        NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singling.mainSubCat];

        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    else
    {
        NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singling.catid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singling.mainCat];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singling.subCatid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singling.mainSubCat];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
//    NSString *subid=[[NSString alloc]initWithFormat:@"%@",singling.subId];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",subid] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[Base64 encode:myimagedata]dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSMutableArray *valu = [val valueForKey:@"detail"];
    
    
    
    NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
   // NSString *phoneno=[[valu objectAtIndex:0] valueForKey:@"phoneno"];
    NSString *uName=[[valu objectAtIndex:0] valueForKey:@"fullname_shopname"];
    NSString *shopDec=[[valu objectAtIndex:0] valueForKey:@"descr"];
    //NSString *venPic=[[valu objectAtIndex:0] valueForKey:@"imagee"];
    NSString *catname=[[valu objectAtIndex:0] valueForKey:@"main_catname"];
    NSString *subcatname=[[valu objectAtIndex:0] valueForKey:@"main_subcatname"];
    NSString *catid=[[valu objectAtIndex:0] valueForKey:@"main_catid"];
    NSString *subcatid=[[valu objectAtIndex:0] valueForKey:@"main_subcatid"];
    
    
    
    
    singling.shopNameVendr=uName;
    singling.vUniqueId=uniqueid;
    singling.shopDec=shopDec;
//    singling.vPhone = phoneno;
//    singling.venPic = venPic;
    singling.mainCat = catname;
    singling.mainSubCat = subcatname;
    singling.catid = catid;
    singling.subCatid  = subcatid;
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Profile is Edited" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
    [ProgressHUD showSuccess:@""];

  
  */
  
  }



- (IBAction)SlectimageBtn:(id)sender

{
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@""
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Gallery",@"Camera",nil];
    actionsheet1.tag=12;
    [actionsheet1 showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
    }
    
    if (buttonIndex==1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypeCamera;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
        
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error acessing camera"
                                  message:@"Device does not support a camera"
                                  delegate:nil
                                  cancelButtonTitle:@"Dismiss"
                                  otherButtonTitles:nil];
            alert.tag=44;
            [alert show];
        }
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(actionsheet1.tag==12)
    {
        cimage=[info objectForKey:UIImagePickerControllerEditedImage];
        
        CGRect rect = CGRectMake(0,0,cimage.size.width/2,cimage.size.height/2);
        UIGraphicsBeginImageContext( rect.size );
        [cimage drawInRect:rect];
        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageDataForResize = UIImagePNGRepresentation(picture1);
        UIImage *pimage=[UIImage imageWithData:imageDataForResize];
        [vendorImg setImage:pimage];
    }
    [camerapicker dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)English:(id)sender

{
    Vendor_Product *setting = [[Vendor_Product alloc]initWithNibName:@"Vendor_Product" bundle:nil];
    [self.navigationController pushViewController:setting animated:YES];

}

- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
