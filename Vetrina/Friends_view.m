//
//  Friends_view.m
//  Vetrina
//
//  Created by Zappy Dhiiman on 29/08/1937 Saka.
//  Copyright © 1937 Saka Amit Garg. All rights reserved.
//

#import "Friends_view.h"
#import "JSON.h"
#import "BSKeyboardControls.h"
#import "ProgressHUD.h"
#import "friends_cell.h"
#import "AsyncImageView.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "CartVendorList.h"
#import "CartProductList.h"
#import "ProgressHUD.h"
#import "AppConstant.h"
#import "push.h"
#import "User_friends.h"
#import "SDWebImage/SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "wishlist_ViewController.h"
@interface Friends_view ()<BSKeyboardControlsDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSString *searchStr;
    BSKeyboardControls *keyboardControls;
    IBOutlet UIView *bottomView;
    IBOutlet UILabel *label1;
    NSMutableArray *users,*chatArr;
    NSMutableArray *array4;
    NSMutableArray *friend_array;
    NSMutableArray *array5;
    NSString *friend_status;

    
}

@end

@implementation Friends_view



-(void)friendList
{
    
    
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/friend_req_list.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray=[data JSONValue];
    
    
    if (eventarray.count<1)
    {
        friend_array = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array5 = [[NSMutableArray alloc]initWithArray:eventarray];
//        [[NSUserDefaults standardUserDefaults] setObject:array3 forKey:@"request_list"];
      //  array4=[[NSMutableArray alloc]init];
        friend_array=[[NSMutableArray alloc]init];
        
        for (int i=0; i<array5.count; i++) {
            if ([[[array5 objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"sent"]) {
              //  [array4 addObject:[array3 objectAtIndex:i]];
            }else{
                [friend_array addObject:[array5 objectAtIndex:i]];
                
            }
        }
        
        
        
        
    }
    
    
    [ProgressHUD showSuccess:@"" Interaction:YES];
    
    if (friend_array.count==0)
    {
         friend_status=@"friends available";
         [friends_table reloadData];
    }
    else
    {
       friend_status=@"friends available";
        [friends_table reloadData];
        
        
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singlogin = [Singleton instance];
    // TopBar View///
     users = [[NSMutableArray alloc]init];
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        bottomView.hidden = YES;
        topBar.hidden = YES;
        arebicTopBar.layer.shadowRadius = 2.0f;
        arebicTopBar.layer.shadowOffset = CGSizeMake(0, 2);
        arebicTopBar.layer.shadowColor = [UIColor blackColor].CGColor;
        arebicTopBar.layer.shadowOpacity = 0.5f;
    }
    else
    {
        bottomView.hidden = YES;
        arebicTopBar.hidden= YES;
        topBar.layer.shadowRadius = 2.0f;
        topBar.layer.shadowOffset = CGSizeMake(0, 2);
        topBar.layer.shadowColor = [UIColor blackColor].CGColor;
        topBar.layer.shadowOpacity = 0.5f;
    }
   
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
    {
//        PFUser *user = [PFUser currentUser];
//        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
//        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
//        [query orderByAscending:PF_USER_FULLNAME];
//        [query setLimit:1000];
//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//         {
//             if (error == nil)
//             {
//                 [users removeAllObjects];
//                 [users addObjectsFromArray:objects];
//                 [friends_table reloadData];
//             }
//             else [ProgressHUD showError:@"Network error."];
//         }];
    }

    // Tableview///
    
    friends_table.layer.shadowRadius = 3.0f;
    friends_table.layer.shadowOffset = CGSizeMake(0, 2);
    friends_table.layer.shadowColor = [UIColor blackColor].CGColor;
    friends_table.layer.shadowOpacity = 0.5f;
    
    
    friends_table.sectionFooterHeight = 0;
    friends_table.sectionHeaderHeight = 0;

    
    NSArray *fields = @[searchText,arabicSrch];
    keyboardControls = [[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    searchStr=@"no";
    searchText.hidden=YES;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, searchText.frame.size.height)];
    searchText.leftView = leftView1;
    searchText.leftViewMode = UITextFieldViewModeAlways;
    [searchText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftViewq = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, arabicSrch.frame.size.height)];
    arabicSrch.leftView = leftViewq;
    arabicSrch.leftViewMode = UITextFieldViewModeAlways;
    [arabicSrch setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];

    [self requstFrinds];
    [self requstRecev];
    [self friendList];
}



-(void)requstFrinds
{
    
    
    NSString *post = [NSString stringWithFormat:@"user_sent=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/friend_req_sent.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"Vendor list"]isEqual:@"No list available"])
    {
        array2 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array2 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        
        
    }
}



-(void)requstRecev
{
    
    
    NSString *post = [NSString stringWithFormat:@"user_received=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/friend_req_recv.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"Vendor list"]isEqual:@"No list available"])
    {
        array3 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array3 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        [[NSUserDefaults standardUserDefaults] setObject:array3 forKey:@"request_list"];
        array4=[[NSMutableArray alloc]init];

        for (int i=0; i<array3.count; i++) {
            if ([[[array3 objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"sent"]) {
                [array4 addObject:[array3 objectAtIndex:i]];
            }else{

            }
        }
        
        
        
        
    }
    
    if (array4.count==0)
    {
        bottomView.hidden = YES;
    }
    else
    {
        bottomView.hidden= NO;
        
        NSInteger x = array4.count;
        
        NSString *str = [NSString stringWithFormat:@"YOU HAVE %ld FRIEND REQUEST",(long)x];
        
        label1.text = str;
    }
}














-(BOOL)textFieldShouldReturn:(UISearchBar *)textField
{
    [searchText resignFirstResponder];
    return TRUE;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [searchText resignFirstResponder];
    
}


#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    friend_status=@"no";

    
    [self.view endEditing:YES];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendorNameList1) withObject:nil afterDelay:0.2f];
    
    //  [self vendorNameList];
    [friends_table reloadData];

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [keyboardControls setActiveField:textField];
}


- (IBAction)dearch:(id)sender
{
    
    
    if ([searchStr isEqualToString:@"no"])
    {
        friend_status=@"no";

        searchText.hidden=NO;
        
        [serach setBackgroundImage:[UIImage imageNamed:@"Close.png"] forState:UIControlStateNormal];
        searchStr=@"yes";
        [searchText becomeFirstResponder];
    }
    else if ([searchStr isEqualToString:@"yes"])
    {
        [self friendList];
        
        searchText.hidden=YES;
        [serach setBackgroundImage:[UIImage imageNamed:@"search_white.png"] forState:UIControlStateNormal];
        searchStr=@"no";
        searchText.text = @"";
                if ([searchText.text isEqualToString:@""])
                {
                    [array removeAllObjects];
                    [friends_table reloadData];
                }
        [searchText resignFirstResponder];
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    return 60;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)vendorNameList1
{
    NSString *post;
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        post   = [NSString stringWithFormat:@"username=%@",arabicSrch.text];
    }
    else
    {
         post   = [NSString stringWithFormat:@"username=%@",searchText.text];
    }
    
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/searchuser.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"search  list"]isEqual:@"No vendor found"])
    {
        array = [[NSMutableArray alloc]init];
        
    }
    else
    {
        NSArray *arry123=[[NSArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        NSString *id_str  =   singlogin.userid;
        array=[[NSMutableArray alloc]init];
        for (int i =0; i<arry123.count; i++) {
            if ([[[arry123 objectAtIndex:i] valueForKey:@"id"] isEqualToString:id_str]) {
            }else{
                [array addObject:[arry123 objectAtIndex:i]];

            }
        
        
        }
        
        
        
        
        
        
        
        
        
    }
    if (array.count==0)
    {
        
        [friends_table setHidden:YES];
    }
    else
    {
        
        [friends_table setHidden:NO];

    }
    
    [ProgressHUD showSuccess:@""];
    [friends_table reloadData];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([friend_status isEqualToString:@"friends available"]) {
    
        return friend_array.count;
    }
    
    return [array count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([friend_status isEqualToString:@"friends available"]) {
        
        static NSString *CellIdentifier = @"Cell_identifier";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            
            
        }
        
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)

        {
            
            
            NSString *id_str  =   singlogin.userid;
            
            if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str]) {
                
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-55, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_img"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-265, 10 , 200, 40)];
                name1.tag=1;
                name1.textAlignment = NSTextAlignmentRight;

               //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_username"]];
                
                if (([cell.contentView viewWithTag:1]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                }
                
                [cell.contentView addSubview:name1];
                
            }else{
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-55, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_photo"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width - 265 , 10 , 200, 40)];
                name1.tag=1;
                name1.textAlignment = NSTextAlignmentRight;

                //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_username"]];
                
                if (([cell.contentView viewWithTag:1]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                }
                
                [cell.contentView addSubview:name1];
                
            }
            
            
        }
        else{
        
        NSString *id_str  =   singlogin.userid;
        
        if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str]) {
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
            NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_img"];
            [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
            img.layer.masksToBounds = YES;
            img.layer.cornerRadius = 25.0;;
            [cell addSubview:img];
            img.backgroundColor=[UIColor grayColor];
            
            UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(60 , 10 , 200, 40)];
            name1.tag=1;
            name1.textAlignment=NSTextAlignmentLeft;
            //  name1.backgroundColor=[UIColor lightGrayColor];
            name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_username"]];
            
            if (([cell.contentView viewWithTag:1]))
            {
                [[cell.contentView viewWithTag:1]removeFromSuperview];
            }
            
            [cell.contentView addSubview:name1];
            
        }else{
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
        NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_photo"];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 25.0;;
        [cell addSubview:img];
        img.backgroundColor=[UIColor grayColor];
        
        UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(60 , 10 , 200, 40)];
        name1.tag=1;
            name1.textAlignment=NSTextAlignmentLeft;

        //   name1.backgroundColor=[UIColor lightGrayColor];
        name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_username"]];
       
            if (([cell.contentView viewWithTag:1]))
            {
                [[cell.contentView viewWithTag:1]removeFromSuperview];
            }
            
            [cell.contentView addSubview:name1];
            
        }
            
            
        }

        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       // tableView.allowsMultipleSelectionDuringEditing = NO;

        return cell;

        
        
    }
    
    
    
    friends_cell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"friends_cell" bundle:nil] forCellReuseIdentifier:@"friendCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"friendCell"];
        
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(friends_cell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([friend_status isEqualToString:@"friends available"]) {

    }
    else{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    
    {
        NSString *sectional = [[array objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.auserName.text =[NSString stringWithFormat:@"%@",sectional];
        
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:3];
        NSString *strImageUrl1 = [[array objectAtIndex:indexPath.row]objectForKey:@"photo"];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 25.0;
        [cell addSubview:img];
        img.backgroundColor=[UIColor grayColor];
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.auserImage.imageURL];
//        cell.auserImage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[array objectAtIndex:indexPath.row]objectForKey:@"photo"]]];
//        cell.auserImage.layer.masksToBounds = YES;
//        cell.auserImage.layer.cornerRadius = 25.0;
//        cell.auserImage.layer.opaque = NO;
        
        cell.areqastedBtn.layer.masksToBounds = YES;
        cell.areqastedBtn.layer.cornerRadius = 5.0;
        cell.areqastedBtn.layer.opaque = NO;
        cell.arqustBtn.layer.masksToBounds = YES;
        cell.arqustBtn.layer.cornerRadius = 5.0;
        cell.arqustBtn.layer.opaque = NO;
        cell.areqastedBtn.hidden = YES;
        
        cell.userImage.hidden=YES;
        cell.userName.hidden = YES;
        cell.reqastedBtn.hidden = YES;
        cell.rqustBtn.hidden = YES;

        
        NSString *strid = [[array objectAtIndex:indexPath.row]valueForKey:@"id"];
        
        for(NSInteger i =0;i<array2.count;i++)
        {
            NSString *usrid = [[array2 objectAtIndex:i]valueForKey:@"user_received"];
            if ([strid isEqualToString:usrid])
            {
                cell.arqustBtn.hidden = YES;
                cell.areqastedBtn.hidden = NO;
                i= array2.count;
            }
            else
            {
                cell.areqastedBtn.hidden = YES;
                cell.arqustBtn.hidden = NO;
            }
        }
        [cell.arqustBtn addTarget:self action:@selector(openInstagram:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {NSString *sectional = [[array objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.userName.text =[NSString stringWithFormat:@"%@",sectional];
        
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.userImage.imageURL];
//        cell.userImage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[array objectAtIndex:indexPath.row]objectForKey:@"photo"]]];
//        cell.userImage.layer.masksToBounds = YES;
//        cell.userImage.layer.cornerRadius = 25.0;
//        cell.userImage.layer.opaque = NO;
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:2];
        NSString *strImageUrl1 = [[array objectAtIndex:indexPath.row]objectForKey:@"photo"];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 25.0;;
        [cell addSubview:img];
        img.backgroundColor=[UIColor grayColor];

        
        cell.reqastedBtn.layer.masksToBounds = YES;
        cell.reqastedBtn.layer.cornerRadius = 5.0;
        cell.reqastedBtn.layer.opaque = NO;
        cell.rqustBtn.layer.masksToBounds = YES;
        cell.rqustBtn.layer.cornerRadius = 5.0;
        cell.rqustBtn.layer.opaque = NO;
        cell.reqastedBtn.hidden = YES;
        
        cell.auserImage.hidden=YES;
        cell.auserName.hidden = YES;
        cell.areqastedBtn.hidden = YES;
        cell.arqustBtn.hidden = YES;
        
        
        NSString *strid = [[array objectAtIndex:indexPath.row]valueForKey:@"id"];
        
        for(NSInteger i =0;i<array2.count;i++)
        {
            NSString *usrid = [[array2 objectAtIndex:i]valueForKey:@"user_received"];
            if ([strid isEqualToString:usrid])
            {
                cell.rqustBtn.hidden = YES;
                cell.reqastedBtn.hidden = NO;
                i= array2.count;
            }
            else
            {
                cell.reqastedBtn.hidden = YES;
                cell.rqustBtn.hidden = NO;
            }
            
            
            
        }
        [cell.rqustBtn addTarget:self action:@selector(openInstagram:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [ProgressHUD show:@"Deleting" Interaction:NO];
        
        
        NSString *user_sent=[[friend_array objectAtIndex:indexPath.row] valueForKey:@"user_sent"];
         NSString *user_received=[[friend_array objectAtIndex:indexPath.row] valueForKey:@"user_received"];
        
        
        
        NSString *post = [NSString stringWithFormat:@"user_sent=%@&user_received=%@",user_sent,user_received];
        
        NSLog(@"get data=%@",post);
        
        NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delete_friendreq.php?"];
        
        NSLog(@"PostData--%@",post);
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSArray *eventarray=[data JSONValue];
        
        NSLog(@"%@",eventarray);
       

        [self friendList];
        
        
        
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([friend_status isEqualToString:@"no"]){
        
        
        NSLog(@"pressed");
        
    }else{
        
        [ProgressHUD show:@"Loading" Interaction:NO];
        
        
        NSString *id_str  =   singlogin.userid;

        if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str]) {

            [[NSUserDefaults standardUserDefaults] setObject:[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received"] forKey:@"wish_id"];
            
        }else{
            
             [[NSUserDefaults standardUserDefaults] setObject:[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] forKey:@"wish_id"];
            
            
        }
//        int a=(int)indexPath.row;
//        
//        
//        NSLog(@"%@",friend_array);
        
        
      //  [ProgressHUD show:@"Loading" Interaction:NO];
        
        wishlist_ViewController *view = [[wishlist_ViewController alloc]initWithNibName:@"wishlist_ViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:NO];
 
        
    }
    
    
}


-(void)openInstagram:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell * )sender.superview.superview;
    NSIndexPath *indexPath = [friends_table indexPathForCell:cell];
   NSString *idstr =[[array objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addfriendrequest.php"];
    
    NSLog(@"strURL:%@", strULR);
    
    
    NSURL * url=[NSURL URLWithString:strULR];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.userid];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_sent\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *emailstr = [[NSString alloc]initWithFormat:@"%@",idstr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_received\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",emailstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventarray=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    
    NSMutableArray *val = [[eventarray valueForKey:@"detail"]objectAtIndex:0];
    
    NSString *objectid = [val valueForKey:@"user_received_objectid"];
 //  singlogin.objectIdStr=[val valueForKey:@"user_received_objectid"];
    NSString *uname = [val valueForKey:@"user_sent_username"];

    
    NSString *message =[NSString stringWithFormat:@"%@ sent you friend request",uname                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    NSMutableDictionary *aps = [NSMutableDictionary dictionary];
    [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
    [aps setObject:message forKey:QBMPushMessageAlertKey];
    [payload setObject:aps forKey:QBMPushMessageApsKey];
    
    QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
    
    // Send push to users with ids 292,300,1395
    [QBRequest sendPush:pushMessage toUsers:objectid successBlock:^(QBResponse *response, QBMEvent *event) {
        // Successful response with event
        
        
        
    } errorBlock:^(QBError *error) {
        
        
        
        // Handle error
    }];

    
    
//    for (NSInteger i=0; i< users.count; i++)
//    {
//        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//        if ([name containsString:objectid])
//        {
//            PFUser *user1 = [PFUser currentUser];
//            PFUser *user2 = users[i];
//            NSString *groupId = StartPrivateChat(user1, user2);
//            NSString *text = @"Sent you a friend request";
//            // [self actionChat:groupId];
//            SendPushNotification(groupId, text);
//            i = users.count;
//        }
//    }
    [self requstFrinds];
    [friends_table reloadData];
}





- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)friendReqst:(id)sender {
    
    User_friends *obj=[[User_friends alloc]initWithNibName:@"User_friends" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];

    
    
}









- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
