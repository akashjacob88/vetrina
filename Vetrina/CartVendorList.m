//
//  CartVendorList.m
//  Vetrina
//
//  Created by Amit Garg on 5/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CartVendorList.h"
#import "JSON.h"
#import "CartVendorCell.h"
#import "AsyncImageView.h"
#import "CartProductList.h"
@interface CartVendorList ()

@end

@implementation CartVendorList

- (void)viewDidLoad
{
    [super viewDidLoad];
    indicator.hidden=YES;
    [indicator stopAnimating];
    singletonn=[Singleton instance];
    view1.hidden=YES;
    [self prodListing];
    appear=@"yes";
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




-(IBAction)Back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];

    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        [vendorListTable reloadData];
        editBtn.hidden=YES;
        view1.hidden=NO;
    }
    else
    {
        
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"productname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"vendorname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"vendorpic"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [vendorNameArray addObject:vendorName1];
                [imageArr addObject:imagestr];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:vendorName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        [vendorListTable reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [finalArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartVendorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CartVendorCell" bundle:nil] forCellReuseIdentifier:@"CartCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
    }
    cell.backgroundColor=[UIColor whiteColor];
    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 54, 414, 1)];
    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    [cell.contentView addSubview:v11];
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(CartVendorCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.row];
    NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
    cell.vendorName.text = [[sectionAnimals objectAtIndex:0]valueForKey:@"vendorname"];
    cell.totalLbl.text=[NSString stringWithFormat:@"%li",(unsigned long)sectionAnimals.count];
    cell.vendorImage.layer.cornerRadius=cell.vendorImage.frame.size.height/2;
    cell.vendorImage.clipsToBounds=YES;
    cell.totalLbl.layer.cornerRadius=cell.totalLbl.frame.size.height/2;
    cell.totalLbl.clipsToBounds=YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.vendorImage.imageURL];
    cell.vendorImage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.row]]];
    
    
    for (i=0; i< sectionAnimals.count; i++)
    {
       NSString *statusStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
        NSString *unitPriceStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"unitprice"];
        
      
        if ([statusStr isEqualToString:@""])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
        }
        if (statusStr == nil)
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
        }
        if ([statusStr isEqualToString:@"0"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
        }
        if ([statusStr isEqualToString:@"2"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"OrderedBadge.png"];
        }
        if ([statusStr isEqualToString:@"3"]) 
        {
            cell.statusImage.image=[UIImage imageNamed:@"Confirmed.png"];
        }
        if ([statusStr isEqualToString:@"4"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Delivered.png"];
        }
        if ([statusStr isEqualToString:@"1"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
            i=sectionAnimals.count;
        }
        else
        {
            if ([unitPriceStr isEqualToString:@""])
            {
                cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                i=sectionAnimals.count;
            }
            if (unitPriceStr == nil)
            {
                cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                i=sectionAnimals.count;
            }
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    singletonn.productArray=[[NSMutableArray alloc]init];
    NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.row];
    singletonn.productArray = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
    singletonn.vendorName = [[singletonn.productArray objectAtIndex:0]valueForKey:@"vendorname"];
    singletonn.venId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"vendorid"];
    [self performSelector:@selector(nextPage)withObject:Nil afterDelay:0.5f];
    [indicator startAnimating];
    indicator.hidden=NO;
}
-(void)nextPage
{
    CartProductList *cart=[[CartProductList alloc]initWithNibName:@"CartProductList" bundle:nil];
    [self.navigationController pushViewController:cart animated:YES];
    [indicator stopAnimating];
    [indicator setHidden:YES];
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (IBAction) EditTable:(id)sender
{
    if([editBtn.currentTitle isEqualToString:@"DONE"])
    {
        [super setEditing:NO animated:NO];
        [vendorListTable setEditing:NO animated:NO];
        [editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
        [vendorListTable reloadData];
        
    }
    else if([editBtn.currentTitle isEqualToString:@"EDIT"])
    {
        [super setEditing:YES animated:YES];
        [vendorListTable setEditing:YES animated:YES];
        [editBtn setTitle:@"DONE" forState:UIControlStateNormal];
        [vendorListTable reloadData];
        
    }
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.row];
                                        NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
                                        singletonn.userid =[[sectionAnimals objectAtIndex:0]valueForKey:@"userid"];
                                        singletonn.venId =[[sectionAnimals objectAtIndex:0]valueForKey:@"vendorid"];
                                        statusStrr =[[sectionAnimals objectAtIndex:0]valueForKey:@"statusid"];
                                        index=indexPath.row;
                                        indicator.hidden=NO;
                                        [indicator startAnimating];
                                        [self performSelector:@selector(deleteView) withObject:nil afterDelay:0.5f];
                                    }];
    button.backgroundColor = [UIColor redColor];
    
    return @[button];
}

-(void)deleteView
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@&userid=%@&statusid=%@",singletonn.venId,singletonn.userid,statusStrr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delvendorcart.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    [finalArray removeObjectAtIndex:index];
    NSLog(@"%@",eventarray);
    [vendorListTable reloadData];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([editBtn.currentTitle isEqualToString:@"DONE"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    if (![appear isEqualToString:@"yes"]) {
        [self prodListing];
    }
    appear=@"";
}

@end
