//
//  Vendor_Login.h
//  Vetrina
//
//  Created by Amit Garg on 5/18/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Vendor_Login : UIViewController

{
    Singleton *signlogin;
    IBOutlet UITextField *passwor;
    IBOutlet UITextField *email;
}
- (IBAction)termsAction:(id)sender;

@end
