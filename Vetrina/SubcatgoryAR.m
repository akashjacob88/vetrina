//
//  SubcatgoryAR.m
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "SubcatgoryAR.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "SubCat_ItemsAR.h"
#import "Sorry_cartAR.h"
#import "SearchAR.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "Vendor_DetaillistAR.h"
#import "All_SignInAR.h"
#import "ProgressHUD.h"
#import "CartProductAR.h"
#import "Vendor_itemsAR.h"
#import "Vendor_ProductAR.h"
#import "Home_VetrinaARViewController.h"
#import "RecentView.h"
#import "Sorry_cartAR.h"
#import "FavoritesAR.h"
#import "UIImageView+WebCache.h"



@interface SubcatgoryAR ()
{
    IBOutlet UIView *topBar;
    IBOutlet UIView *userView;
    IBOutlet UIView *vendorView;
    UITapGestureRecognizer *tapRecognizer;
    UIView *new;
    
}

@end

@implementation SubcatgoryAR

- (void)viewDidLoad
{

    [super viewDidLoad];

    singloging = [Singleton instance];
    subName.text = singloging.subName;
    
    shopesBtn.layer.cornerRadius = 5.0;
    
//    itemsBtm.layer.masksToBounds = YES;
//    itemsBtm.layer.cornerRadius = 5.0;
//    itemsBtm.layer.borderWidth = 2.0;
//    itemsBtm.layer.borderColor = [[UIColor whiteColor]CGColor];
//    itemsBtm.clipsToBounds = YES;
//    itemsBtm.layer.opaque = NO;

    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    // Tableview///
    
    vendorTbl.layer.shadowRadius = 5.0f;
    vendorTbl.layer.shadowOffset = CGSizeMake(0, 2);
    vendorTbl.layer.shadowColor = [UIColor blackColor].CGColor;
    vendorTbl.layer.shadowOpacity = 0.6f;
    vendorTbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self vendorNameList];
    [self accessTokenList];
    
    
    
    [indicator stopAnimating];
    
    if ([singloging.loginStatus isEqualToString:@"IG Login"])
    {
        userView.hidden = YES;
        vendorView.hidden = NO;
    }
    else if ([singloging.loginStatus isEqualToString:@"User Login"])
    {
        userView.hidden = NO;
        vendorView.hidden = YES;
        chrtLbl.hidden=YES;
        [self cartTotalView];
    }
    else
    {
        userView.hidden = NO;
        vendorView.hidden = YES;
        chrtLbl.hidden=YES;
        [self cartTotalView];
        
    }

    
    NSArray *fields = @[searchText];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    view1.hidden=YES;
   
    searchStr=@"no";
    searchText.hidden=YES;
    
    
    
    
    [searchText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
    
    
    
    CGRect screen=[[UIScreen mainScreen] bounds];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    
    NSString *st= [[NSUserDefaults standardUserDefaults] valueForKey:@"coachMarker1_ar"];
    
    if ([st isEqualToString:@"one"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"two" forKey:@"coachMarker1_ar"];

    
    new = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screen.size.width  , screen.size.height)];
    [self.view addSubview:new];
    
    
    
    UIImageView *arraow=[[UIImageView alloc]initWithFrame:CGRectMake(18, 60, 55, 55)];
    
    arraow.image=[UIImage imageNamed:@"arrow-top"];
    [new addSubview:arraow];
    
        UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(65, 85, 200, 40)];
        lblMsg.text=@"بدل بين المحلات والمنتجات";
        lblMsg.font=[UIFont fontWithName:@"DroidArabicKufi-Bold" size:14];
        
        lblMsg.textColor=[UIColor blackColor];
    lblMsg.backgroundColor=[UIColor whiteColor];
    lblMsg.textAlignment=NSTextAlignmentCenter;
    [new addSubview:lblMsg];
    
    lblMsg.layer.cornerRadius=3.0f;
    lblMsg.layer.masksToBounds=YES;
    [self.view addGestureRecognizer:tapRecognizer];
    
    new.backgroundColor=[UIColor colorWithWhite:0.f alpha:0.6];

    
    
    }
    
    
    
    
}
-(void)tapped
{
    [self.view removeGestureRecognizer:tapRecognizer];
    new.hidden=YES;
}
- (IBAction)okay:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cartTotalView
{
    if (singloging
        .userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singloging.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singloging.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singloging.totalCart;
            
        }
    }
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)vendorNameList

{
    NSString *post = [NSString stringWithFormat:@"subcatid=%@", singloging.subCatid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlistsubcat.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"list list"]isEqual:@"No list available"])
    {
        array1 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array1 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        searchArray=[[NSMutableArray alloc]init];
        searchArray=array1;
        
    }
    
    if([array1 count]==0)
    {
        noItmView.hidden = NO;
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"No list available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
    }
    else
    {
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [array1 sortedArrayUsingDescriptors:sortedarray1];
        
        
        NSLog(@"--------%@",sortedarray1);
        
        imagearrayLIst = [[NSMutableArray alloc]init];
        
        for(int i =0;i<sortedarray1.count;i++)
        {
            image2 =[[UIImageView alloc]init];
//            UIGraphicsBeginImageContext(image2.image.size);
//            {
//                CGContextRef ctx = UIGraphicsGetCurrentContext();
//                CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//                trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, image2.image.size.height));
//                CGContextConcatCTM(ctx, trnsfrm);
//                CGContextBeginPath(ctx);
//                CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height));
//                CGContextClip(ctx);
//                CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height), image2.image.CGImage);
//                image2.image = UIGraphicsGetImageFromCurrentImageContext();
//                UIGraphicsEndImageContext();
//            }
//            [[AsyncImageLoader sharedLoader] cancelLoadingURL:image2.imageURL];
//            //[indicator stopAnimating];
            NSString *strImageUrl1 = [[sortedarray1 objectAtIndex:i]objectForKey:@"imagee"];
            
            [image2 sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"logo.png"]];
//            image2.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"imagee"]]];
            [imagearrayLIst addObject:image2];
        }
        imageSearchArray=[[NSMutableArray alloc]init];
        imageSearchArray=imagearrayLIst;
        noItmView.hidden = YES;
        
        [vendorTbl reloadData];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return 1 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [sortedarray1 count];;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    for(UIView *v in cell.contentView.subviews)
    {
        [v removeFromSuperview];
    }
    singloging.vendorNameList=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    
    arrowIcon =[[UIImageView alloc]init];
    UIGraphicsBeginImageContext(arrowIcon.image.size);
    {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
        trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, arrowIcon.image.size.height));
        CGContextConcatCTM(ctx, trnsfrm);
        CGContextBeginPath(ctx);
        CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, arrowIcon.image.size.width, arrowIcon.image.size.height));
        CGContextClip(ctx);
        CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, arrowIcon.image.size.width, arrowIcon.image.size.height), arrowIcon.image.CGImage);
        arrowIcon.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
      image3 = [[UIImageView alloc]init];

    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            image2.frame = CGRectMake(270, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 200, 30)];
            costText1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            arrowIcon.frame=CGRectMake(-10, -3, 55, 55);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 320, 1)];
             image3.frame = CGRectMake(250, -8, 44, 44);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            image2.frame = CGRectMake(270, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 200, 30)];
            costText1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            arrowIcon.frame=CGRectMake(-10, -3, 55, 55);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 320, 1)];
             image3.frame = CGRectMake(250, -8, 44, 44);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            image2.frame = CGRectMake(328, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 260, 30)];
            costText1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            arrowIcon.frame=CGRectMake(-10,-3, 55, 55);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 375, 1)];
             image3.frame = CGRectMake(310, -8, 44, 44);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            image2.frame = CGRectMake(364, 5, 40, 40);
            image2.layer.cornerRadius=image2.frame.size.width /2;
            costText1 =[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 300, 30)];
            costText1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            arrowIcon.frame=CGRectMake(-10, -3, 55, 55);
            lineLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
             image3.frame = CGRectMake(335, -4, 44, 44);
        }
    }
    else
    {
        arrowIcon.frame=CGRectMake(684, 20, 7, 12);
    }
    image2 = [imagearrayLIst objectAtIndex:indexPath.row];
    image2.layer.borderWidth=1.0f;
    image2.layer.borderColor=[UIColor whiteColor].CGColor;
    image2.clipsToBounds=YES;
    [cell.contentView addSubview:image2];
    
    
    staffPic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"staffpick"];
    
    
    if ([staffPic isEqualToString:@"yes"])
    {
        image3.hidden = NO;
    }
    else
    {
        image3.hidden = YES;
    }
    image3.image=[UIImage imageNamed:@"PremiumClub.png"];
    [cell.contentView addSubview:image3];

    
    costText1.text =[NSString stringWithFormat:@"%@",singloging.vendorNameList];
    costText1.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    costText1.backgroundColor =[UIColor clearColor];
    costText1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
    costText1.numberOfLines = 1;
    costText1.textAlignment = NSTextAlignmentRight;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:costText1];
    
    arrowIcon.image=[UIImage imageNamed:@"ArrowLeft.png"];
    [cell.contentView addSubview:arrowIcon];
    
    lineLbl.backgroundColor =[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    [cell.contentView addSubview:lineLbl];
    
    
    cell.backgroundColor =[UIColor  whiteColor ];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    singloging.venName = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"fullname_shopname"];
    singloging.SlctVenId=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
    singloging.vid=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"username_instagramacnt"];
    singloging.vendorProfilepic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"imagee"];
    [self performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [indicator startAnimating];
}


-(void)vendorlist

{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@", singloging.SlctVenId];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlistvendor.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    if ([[eventarray objectForKeyedSubscript:@"list"]isEqual:@"No list available"])
    {
        array2 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array2 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        
    }
    if([array2 count]==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"آسف!" message:@"لا يوجد لديك منتجات" delegate:nil cancelButtonTitle:@"تمام" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        singloging.fUserId = [[array2 objectAtIndex:0]valueForKey:@"instagramid"];
        singloging.vEnPhone =[[array2 objectAtIndex:0]valueForKey:@"phoneno"];
        singloging.objectIdStr = [[array2 objectAtIndex:0]valueForKey:@"objectid"];
        [self insta];
        Vendor_DetaillistAR *vender = [[Vendor_DetaillistAR alloc]initWithNibName:@"Vendor_DetaillistAR" bundle:nil];
        [self.navigationController pushViewController:vender animated:YES];
        [ProgressHUD showSuccess:@""];
        [indicator stopAnimating];

    }
}


-(void)insta
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",singloging.fUserId);
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,singloging.fUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        singloging.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        
        NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            NSDictionary *dynamicDict=[[NSDictionary alloc]init];
            dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
            
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
        }
    }
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singloging.newarayList1 = newdatalist;
}


-(void)accessTokenList

{
    NSURL * url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/tokenlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        //If data were received
        if (data)
        {
            //Convert to string
            NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSLog(@"GetData--%@",result);
            NSDictionary *eventarray=[result JSONValue];
            NSLog(@"GetDatadictt--%@",eventarray);
            
            if(![[eventarray objectForKey:@"list"] isEqual:@"No subcategory available"])
            {
                array5=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"list"]];
            }
            else
            {
                array5=[[NSMutableArray alloc]init];
                
            }
            if([array5 count]==0)
            {
                
            }
            else
            {
                accesToken = [[array5 objectAtIndex:0]valueForKey:@"accesstoken"];
            }
            
        }
        //No data received
        else
        {
            NSString *errorText;
            //Specific error
            if (error)
                errorText = [error localizedDescription];
            //Generic error
            else
                errorText = @"An error occurred when downloading the list of issues. Please check that you are connected to the Internet.";
            
            //Show error
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            [indicator stopAnimating];
        }
    }];
    NSLog(@"Done");
}


- (IBAction)search:(id)sender
{
    
        if ([searchStr isEqualToString:@"no"])
        {
            searchText.hidden=NO;
            subName.hidden=YES;
            [searchBtn setBackgroundImage:[UIImage imageNamed:@"Close.png"] forState:UIControlStateNormal];
            searchStr=@"yes";
            [searchText becomeFirstResponder];
        }
        else if ([searchStr isEqualToString:@"yes"])
        {
            searchText.hidden=YES;
            subName.hidden=NO;
            [searchBtn setBackgroundImage:[UIImage imageNamed:@"search_white.png"] forState:UIControlStateNormal];
            searchStr=@"no";
            [searchText resignFirstResponder];
            
        }
    

}


- (IBAction)chart:(id)sender

{
    if (singloging.userid == nil)
    {
        [indicator stopAnimating];
         [ProgressHUD showError:@""];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    }
}


-(void)nextpage
{
    CartProductAR *cart=[[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:nil];
    [self.navigationController pushViewController:cart animated:YES];
    [ProgressHUD showSuccess:@""];
    [indicator setHidden:YES];
    [indicator stopAnimating];
}


- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singloging.loginfrom = @"cart";
     All_SignInAR *signIn = [[All_SignInAR alloc]initWithNibName:@"All_SignInAR" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}

- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}

- (IBAction)items:(id)sender

{     [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(item) withObject:nil afterDelay:0.3f];
}
    
    
-(void)item
    
{
    SubCat_ItemsAR *items = [[SubCat_ItemsAR alloc]initWithNibName:@"SubCat_ItemsAR" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
     [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


-(BOOL)textFieldShouldReturn:(UISearchBar *)textField
{
    [searchText resignFirstResponder];
    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [searchText resignFirstResponder];
}


- (IBAction)textDidChange:(id)textField
{
    NSString * match = searchText.text;
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"fullname_shopname CONTAINS[c] %@", match];
    NSArray *searchArray1,*listFiles1;
    searchArray1=[searchArray mutableCopy];
    listFiles1 = [NSArray arrayWithArray:[searchArray1 filteredArrayUsingPredicate:predicate]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"fullname_shopname"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [listFiles1 sortedArrayUsingDescriptors:sortDescriptors];
    sortedarray1=[sortedArray mutableCopy];
    imagearrayLIst = [[NSMutableArray alloc]init];
    for(int i =0;i<sortedarray1.count;i++)
    {
        image2 =[[UIImageView alloc]init];
        UIGraphicsBeginImageContext(image2.image.size);
        {
            CGContextRef ctx = UIGraphicsGetCurrentContext();
            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, image2.image.size.height));
            CGContextConcatCTM(ctx, trnsfrm);
            CGContextBeginPath(ctx);
            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height));
            CGContextClip(ctx);
            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, image2.image.size.width, image2.image.size.height), image2.image.CGImage);
            image2.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:image2.imageURL];
        //[indicator stopAnimating];
        image2.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"imagee"]]];
        [imagearrayLIst addObject:image2];
    }
    
    if ([searchText.text isEqualToString:@""])
    {
        array1=[[NSMutableArray alloc]init];
        array1=searchArray;
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"fullname_shopname" ascending:YES];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [array1 sortedArrayUsingDescriptors:sortedarray1];
        imagearrayLIst=[[NSMutableArray alloc]init];
        imagearrayLIst=imageSearchArray;
    }
    
    [vendorTbl reloadData];
}
#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [keyboardControls setActiveField:textField];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
}



- (IBAction)homeBtn:(id)sender
{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self vendorNameList];
}


-(void)home
{
    Home_VetrinaARViewController *view = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)favrtBtn:(id)sender
{
    if (singloging.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(fav) withObject:nil afterDelay:0];
    }
}


-(void)fav
{
    FavoritesAR *view = [[FavoritesAR alloc]initWithNibName:@"FavoritesAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)userChat:(id)sender
{
    if (singloging.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {

    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(chat) withObject:nil afterDelay:0];
    }
}


-(void)chat
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)vendorItems:(id)sender
{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendoritm) withObject:nil afterDelay:0];
}


-(void)vendoritm
{
    Vendor_itemsAR *view = [[Vendor_itemsAR alloc]initWithNibName:@"Vendor_itemsAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)vendorProduct:(id)sender
{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendorProd) withObject:nil afterDelay:0];
}


-(void)vendorProd
{
    Vendor_ProductAR *view = [[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(void)viewWillAppear:(BOOL)animated

{
    [self cartTotalView];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
