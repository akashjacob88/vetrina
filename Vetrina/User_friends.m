//
//  User_friends.m
//  Vetrina
//
//  Created by Zappy Dhiiman on 04/09/1937 Saka.
//  Copyright © 1937 Saka Amit Garg. All rights reserved.
//

#import "User_friends.h"
#import "userfriend_Cell.h"
#import "AsyncImageView.h"
#import "UIImageView+WebCache.h"
#import <Parse/Parse.h>
#import "common.h"
#import "JSON.h"
#import "userfriendAR.h"
@interface User_friends ()
{
    NSArray *list;
    NSMutableArray *array4;
}
@end

@implementation User_friends

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    singlogin = [Singleton instance];

//    list= [[NSArray alloc ] init];
//    
//    list=[[NSUserDefaults standardUserDefaults] objectForKey:@"request_list"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        _topBar.hidden = YES;
        _topbar_arebic.layer.shadowRadius = 2.0f;
        _topbar_arebic.layer.shadowOffset = CGSizeMake(0, 2);
        _topbar_arebic.layer.shadowColor = [UIColor blackColor].CGColor;
        _topbar_arebic.layer.shadowOpacity = 0.5f;
    }
    else
    {
        _topbar_arebic.hidden= YES;
        _topBar.layer.shadowRadius = 2.0f;
        _topBar.layer.shadowOffset = CGSizeMake(0, 2);
        _topBar.layer.shadowColor = [UIColor blackColor].CGColor;
        _topBar.layer.shadowOpacity = 0.5f;
    }
    
    
    
    
    [self requstRecev];
    
    
}
-(void)requstRecev
{
    
    
    NSString *post = [NSString stringWithFormat:@"user_received=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/friend_req_recv.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"Vendor list"]isEqual:@"No list available"])
    {
        array3 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array3 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        array4=[[NSMutableArray alloc]init];

        
        for (int i=0; i<array3.count; i++) {
            if ([[[array3 objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"sent"]) {
                [array4 addObject:[array3 objectAtIndex:i]];

            }
        }
        
        [_table_view reloadData];
        
    }
    
  }

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array4.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{

    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        
        userfriendAR *cell = [tableView dequeueReusableCellWithIdentifier:@"UserfriendAR"];

        
     
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"userfriendAR" bundle:nil] forCellReuseIdentifier:@"UserfriendAR"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"UserfriendAR"];
            
            
        }
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:3];
        img.frame=CGRectMake(self.view.frame.size.width-65, 5, 60,60) ;
        NSString *strImageUrl1 = [[array4 valueForKey:@"user_sent_photo"] objectAtIndex:indexPath.row];
        
        img.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        img.backgroundColor=[UIColor grayColor];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 30.0f;
        
        //     img.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[array objectAtIndex:indexPath.row]objectForKey:@"photo"]]];
        
        // [img setImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[list valueForKey:@"user_received_img"] objectAtIndex:indexPath.row]]]];
        
        
        UILabel *lbl_Post=(UILabel *)[cell viewWithTag:1];
       // lbl_Post.tag=1;
        lbl_Post.textAlignment = NSTextAlignmentRight;

        
        
       // UILabel *lbl_Post=(UILabel *)[cell viewWithTag:1];
        lbl_Post.text=[[array4 valueForKey:@"user_sent_username"] objectAtIndex:indexPath.row];
        [lbl_Post setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
        //  lbl_Post.backgroundColor=[UIColor yellowColor];
        
        UIButton *request_btn=(UIButton *)[cell viewWithTag:2];;
        [request_btn setTitle:@"Accept" forState:UIControlStateNormal];
        [request_btn setTintColor:[UIColor whiteColor]];
        request_btn.titleLabel.textColor=[UIColor whiteColor];
        [request_btn setBackgroundColor:[UIColor colorWithRed:119/255.f green:218/255.f blue:172/255.f alpha:1.0]];
        request_btn.layer.cornerRadius=5.0;
        request_btn.layer.masksToBounds=YES;
        request_btn.tag=indexPath.row;
        [request_btn addTarget:self action:@selector(accept_request:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
        [cell.layer setBorderWidth:1.0f];
        // [cell.layer setCornerRadius:7.5f];
        [cell.layer setShadowOffset:CGSizeMake(0, 1)];
        [cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [cell.layer setShadowRadius:8.0];
        [cell.layer setShadowOpacity:0.8];
        
        [cell.layer setMasksToBounds:NO];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;


        return cell;

        
        
    }else{
        
        userfriend_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendList_cell"];

        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"userfriend_Cell" bundle:nil] forCellReuseIdentifier:@"friendList_cell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"friendList_cell"];
            
            
        }
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:3];
        img.frame=CGRectMake(10, 5, 60,60) ;
        NSString *strImageUrl1 = [[array4 valueForKey:@"user_sent_photo"] objectAtIndex:indexPath.row];
        
        img.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        img.backgroundColor=[UIColor grayColor];
        [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 30.0f;
        
        //     img.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[array objectAtIndex:indexPath.row]objectForKey:@"photo"]]];
        
        // [img setImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[list valueForKey:@"user_received_img"] objectAtIndex:indexPath.row]]]];
        
        UILabel *lbl_Post=(UILabel *)[cell viewWithTag:1];
        lbl_Post.text=[[array4 valueForKey:@"user_sent_username"] objectAtIndex:indexPath.row];
        [lbl_Post setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
        //  lbl_Post.backgroundColor=[UIColor yellowColor];
        
        UIButton *request_btn=(UIButton *)[cell viewWithTag:2];;
        [request_btn setTitle:@"Accept" forState:UIControlStateNormal];
        [request_btn setTintColor:[UIColor whiteColor]];
        request_btn.titleLabel.textColor=[UIColor whiteColor];
        [request_btn setBackgroundColor:[UIColor colorWithRed:119/255.f green:218/255.f blue:172/255.f alpha:1.0]];
        request_btn.layer.cornerRadius=5.0;
        request_btn.layer.masksToBounds=YES;
        request_btn.tag=indexPath.row;
        [request_btn addTarget:self action:@selector(accept_request:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
        [cell.layer setBorderWidth:1.0f];
        // [cell.layer setCornerRadius:7.5f];
        [cell.layer setShadowOffset:CGSizeMake(0, 1)];
        [cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
        [cell.layer setShadowRadius:8.0];
        [cell.layer setShadowOpacity:0.8];
        
        [cell.layer setMasksToBounds:NO];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        
        
        
        return cell;

        
    }
    
    
    
    
    
}


-(void)accept_request:(id)sender{
    
    
    int i =(int)[sender tag];
    NSString *sent_id=[[array4 valueForKey:@"id"] objectAtIndex:i];
    
        NSString *post = [NSString stringWithFormat:@"requestid=%@&&status=accept",sent_id];
        
        NSLog(@"get data=%@",post);
        
        NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editrequest.php"];
        
        NSLog(@"PostData--%@",post);
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSDictionary *eventarray=[data JSONValue];
        
    NSLog(@"%@",eventarray);
    
    if ([[[[eventarray valueForKey:@"detail"] valueForKey:@"status"] objectAtIndex:0] isEqualToString:@"accept"]) {
        
        
        NSMutableArray *val = [[eventarray valueForKey:@"detail"]objectAtIndex:0];
        
        NSString *objectid = [val valueForKey:@"user_sent_userobjectid"];
        //  singlogin.objectIdStr=[val valueForKey:@"user_received_objectid"];
        NSString *uname = [val valueForKey:@"user_received_username"];
        

        
        
        
        NSString *message =[NSString stringWithFormat:@"%@ accepted your friend request",uname                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:objectid successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];

        
        
        
        
        UIAlertView *arelt=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"Request Accepted" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [arelt show];

        
        
        [self requstRecev];

        
    }else{
        UIAlertView *arelt=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"Please try again" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"cancel", nil];
        [arelt show];
    }
    
    
    
//        if ([[eventarray objectForKeyedSubscript:@"Vendor list"]isEqual:@"No list available"])
//        {
//            array2 = [[NSMutableArray alloc]init];
//            
//        }
//        else
//        {
//            array2 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
//            
//            
//        }
   
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(userfriend_Cell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ar_back_btn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)back_btn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
@end
