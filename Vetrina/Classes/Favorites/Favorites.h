//
//  Favorites.h
//  Vetrina
//
//  Created by Amit Garg on 4/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Favorites : UIViewController

{
    Singleton *singemplist,*singlogin;

    IBOutlet UITableView *vendorTabl;
    IBOutlet UIActivityIndicatorView *indicator;

    UITableView *subcategories;
    UIImageView *Image2,*arrowIcon;
 
    NSDictionary *dynamicDict,*dynamicDict2;
    NSMutableArray *arra_1,*aray_2,*newdatalist,*aray_3,*aray_4,*imagearrayLIst,*array_5,*array;
    BOOL isfilter;
    NSString *name2,*desc2,*catid,*subname2,*subdesc2,*subcatid,*scatName,*accesToken;
    UILabel *costText1,*lbl1;
    NSArray *sortedArray,*sortedArray1;
    NSString *venListStatus;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UIView *viewImprt;
    IBOutlet UIView *view2;
    NSString *appear;
    IBOutlet UIButton *shop;
    IBOutlet UIButton *itemsOutlet;
    IBOutlet UILabel *count_lbl;
}

@property (nonatomic, strong) NSString *addproduct;
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;





@end
