//
//  Favorites.m
//  Vetrina
//
//  Created by Amit Garg on 4/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Favorites.h"
#import "ShopSearch.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "Vendor_Detaillist.h"
#import "FavCustomCell.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "CartVendorList.h"
#import "All_SignIn.h"
#import "User_login_insta.h"
#import "CartProductList.h"
#import "ProgressHUD.h"
#import "FavItems.h"
#import "ViewController.h"
#import "RecentView.h"
#import "PCRapidSelectionView.h"
#import "PremiumItems.h"
#import "UserProfile.h"

#import <Parse/Parse.h>
#import "AppConstant.h"

@interface Favorites ()
{
    IBOutlet UIView *topBar;
    
    IBOutlet UIImageView *profilePic;
    NSMutableArray *recents;
    NSArray *dialog_Objects;

}

@end

@implementation Favorites





- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }

    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singlogin.userid == nil)
    {
        count_lbl.hidden=YES;
        count_lbl.hidden = YES;
    }
    else
    {
        if ([count_lbl.text isEqualToString:@"0"])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else if ([count_lbl.text isEqualToString:@""])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else
        {
            count_lbl.hidden=NO;
            count_lbl.hidden = NO;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singlogin.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}



- (void)viewDidLoad

{
    [super viewDidLoad];
    
    singlogin = [Singleton instance];
//    shop.layer.cornerRadius = 5.0;
//    itemsOutlet.layer.masksToBounds = YES;
//    itemsOutlet.layer.cornerRadius = 5.0;
//    itemsOutlet.layer.borderWidth = 2.0;
//    itemsOutlet.layer.borderColor = [[UIColor whiteColor]CGColor];
//    itemsOutlet.clipsToBounds = YES;
//    itemsOutlet.layer.opaque = NO;
    
    recents=[[NSMutableArray alloc]init];
    count_lbl.hidden=YES;

    
    [self loadRecents];
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    // Tableview///
    
    vendorTabl.layer.shadowRadius = 3.0f;
    vendorTabl.layer.shadowOffset = CGSizeMake(0, 2);
    vendorTabl.layer.shadowColor = [UIColor blackColor].CGColor;
    vendorTabl.layer.shadowOpacity = 0.5f;
 
    NSURL *url1=[NSURL URLWithString:singlogin.userproilePic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePic.image=emppppic;
    if (emppppic==nil) {
        
        profilePic.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        
        
    }
    
    profilePic.layer.masksToBounds = YES;
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.layer.opaque = NO;
    
    
    if ([singlogin.check isEqualToString:@"1"])
    {
        viewImprt.hidden = YES;
    }
    else
    {
        viewImprt.hidden = NO;
    }
    
    vendorTabl.sectionFooterHeight = 0;
    vendorTabl.sectionHeaderHeight = 0;
    [indicator stopAnimating];
    
     singlogin=[Singleton instance];
    [self vendornamelisting];
    [self accessTokenList];
    vendorTabl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    view1.hidden=YES;
    chrtLbl.hidden=YES;
    [self cartTotalView];
    appear=@"yes";

}


- (IBAction)items:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
}


-(void)home
{
    PremiumItems *items =[[PremiumItems alloc]initWithNibName:@"PremiumItems" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];
}




-(void)cartTotalView
{
    
    
  //  [ProgressHUD show:@""];
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
            
        }
    }
}


-(void)vendornamelisting
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/favvendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No vendor available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    

    if([arra_1 count]==0)
    {
        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You don’t have any favorite shops" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
        view2.hidden = NO;
        [vendorTabl setHidden:YES];
    }
    else
    {
        view2.hidden = YES;

        [vendorTabl setHidden:NO];

        [vendorTabl reloadData];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arra_1 count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    FavCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell3"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"FavCustomCell" bundle:nil] forCellReuseIdentifier:@"Cell3"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell3"];
        
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(FavCustomCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(openContextMenu:)];
    longPress.minimumPressDuration = 0.2;
    longPress.cancelsTouchesInView = YES;
    [cell.contentView addGestureRecognizer:longPress];
    
    NSString *sectional = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    cell.vendrLbl.text =[NSString stringWithFormat:@"%@",sectional];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
    cell.vendorImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arra_1 objectAtIndex:indexPath.row]objectForKey:@"vendorimage"]]];
    cell.vendorImg.layer.masksToBounds = YES;
    cell.vendorImg.layer.cornerRadius = 20.0;
    cell.vendorImg.layer.opaque = NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    singlogin.fUserId = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramid"];
    singlogin.venInstaName = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramaccount"];
    singlogin.SlctVenId=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
    singlogin.venName=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    singlogin.objectIdStr = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];
    singlogin.vEnPhone = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"phoneno"];
    [self insta];
    [self performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
    [indicator startAnimating];
}


-(void)vendorlist

{
    Vendor_Detaillist *vInstPics = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:NULL];
    [self.navigationController pushViewController:vInstPics animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 50;
}


-(void)insta
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",singlogin.fUserId);
        
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,singlogin.fUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        singlogin.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            dynamicDict=[[NSMutableDictionary alloc]init];
          
            dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
        }
    }
    //NSLog(@"Dynamic ---- %@",dynamicDict);
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singlogin.newarayList1 = newdatalist;
}


-(void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"View Disapear");
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
}


-(void)accessTokenList
{
    
    NSURL * url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/tokenlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        //If data were received
        if (data) {
            //Convert to string
            NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSLog(@"GetData--%@",result);
            NSDictionary *eventarray=[result JSONValue];
            NSLog(@"GetDatadictt--%@",eventarray);
            
            if(![[eventarray objectForKey:@"list"] isEqual:@"No subcategory available"])
            {
                array_5=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"list"]];
            }
            else
            {
                array_5=[[NSMutableArray alloc]init];
                
            }
            if([array_5 count]==0)
            {
                
            }
            else
            {
                accesToken = [[array_5 objectAtIndex:0]valueForKey:@"accesstoken"];
            }
            
        }
        //No data received
        else {
            NSString *errorText;
            //Specific error
            if (error)
                errorText = [error localizedDescription];
            //Generic error
            else
                errorText = @"An error occurred when downloading the list of issues. Please check that you are connected to the Internet.";
            
            //Show error
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            //Hide activity indicator
            //[self clearIssuesAccessoryView];
        }
    }];
    
    
    
    NSLog(@"Done");
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)chart:(id)sender
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    }
}


-(void)nextpage
{
    CartProductList *home = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:NULL];
    [self.navigationController pushViewController:home animated:NO];
    [ProgressHUD showSuccess:@""];
    
    [indicator setHidden:YES];
    [indicator stopAnimating];
}


- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singlogin.loginfrom = @"cart";
    All_SignIn *signIn = [[All_SignIn alloc]initWithNibName:@"All_SignIn" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}


- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}



- (IBAction)search:(id)sender
{
     UserProfile *srch = [[UserProfile alloc]initWithNibName:@"UserProfile" bundle:nil];
     [self.navigationController pushViewController:srch animated:NO];
}


- (IBAction)instagram:(id)sender
{
    //[self check];
    User_login_insta *login = [[User_login_insta alloc]initWithNibName:@"User_login_insta" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}


-(void)check

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcheck.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
   
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
}



- (void)dealloc

{
    [vendorTabl release];
    [super dealloc];
}


-(void)viewWillAppear:(BOOL)animated

{
    if (![appear isEqualToString:@"yes"])
    {
        [self vendornamelisting];
        [viewImprt setHidden:YES];
        [self cartTotalView];
    }
    appear=@"";
    [self.navigationController setNavigationBarHidden:YES];

}

- (IBAction)home:(id)sender
{
    [self performSelector:@selector(home2)withObject:Nil afterDelay:0.2f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
}


-(void)home2
{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)chat:(id)sender
{
    
    [self performSelector:@selector(cht)withObject:Nil afterDelay:0.2f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
}


-(void)cht
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (void)openContextMenu:(UILongPressGestureRecognizer *)gesture
{
    NSIndexPath *indexPath = [self indexPathForGesture:gesture];
    
     typeof(self) weakSelf = self;
    [PCRapidSelectionView viewForParentView:self.navigationController.view currentGuestureRecognizer:gesture interactive:YES options:@[@"Show Detail",[NSString stringWithFormat:@"Unlike %@",[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"]]] title:@"Quick Select" completionHandler:^(NSInteger selectedIndex)
     {
         if (selectedIndex == NSNotFound)
         {
             NSLog(@"Cancelled");
         }
         if (selectedIndex == 0)
         {
             [ProgressHUD show:@"Loading..." Interaction:NO];
             //[weakSelf performSegueWithIdentifier:@"showDetail" sender:indexPath];
             singlogin.fUserId = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramid"];
             singlogin.venInstaName = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramaccount"];
             singlogin.SlctVenId=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
             singlogin.venName=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
             singlogin.objectIdStr = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];
             
             
             [self insta];
             
             [weakSelf performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
             
             
             [indicator startAnimating];
         }
         if (selectedIndex == 1)
         {
             [weakSelf tableView:vendorTabl commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
            
         }
     }];
}


-(void)delFavorite

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/delfavvendor.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.SlctVenId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
    [vendorTabl reloadData];
}


- (NSIndexPath *)indexPathForGesture:(UIGestureRecognizer *)gesture
{
    return [vendorTabl indexPathForCell:(id)gesture.view.superview];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        singlogin.SlctVenId=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
        singlogin.venName=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
        [self delFavorite];
        [arra_1 removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
