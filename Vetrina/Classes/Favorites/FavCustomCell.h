//
//  FavCustomCell.h
//  Vetrina
//
//  Created by Amit Garg on 4/22/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavCustomCell : UITableViewCell

{
}
@property (strong, nonatomic) IBOutlet UIImageView *vendorImg;
@property (strong, nonatomic) IBOutlet UILabel *vendrLbl;
@property (strong, nonatomic) IBOutlet UILabel *shopDec;

@end
