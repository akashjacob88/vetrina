//
//  All_SignIn.m
//  Vetrina
//
//  Created by Amit Garg on 4/3/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "All_SignIn.h"
#import "User_Login.h"
#import "JSON.h"
#import "Base64.h"
#import "ViewController.h"
#import "Vendor_Instagramlogin.h"
#import "Walkthrough.h"
#import <Parse/Parse.h>
#import "AppConstant.h"
#import "push.h"
#import "ProgressHUD.h"
#import "SignUp_Shop.h"
#import "AppDelegate.h"
#import <Quickblox/Quickblox.h>


@interface All_SignIn ()
{
    IBOutlet UIImageView *proimg;
    
}

@end

@implementation All_SignIn


- (void)viewDidLoad

{
    [super viewDidLoad];
    
    singemplist1 = [Singleton instance];
    singlogin = [Singleton instance];
    
    [indicatr stopAnimating];
    
    
    
//    proimg = [[UIImageView alloc]init];
//    proimg.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
    
    
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, email.frame.size.height)];
    email.leftView = leftView1;
    email.leftViewMode = UITextFieldViewModeAlways;
    [email setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, pasword.frame.size.height)];
    pasword.leftView = leftView2;
    pasword.leftViewMode = UITextFieldViewModeAlways;
    [pasword setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, userName.frame.size.height)];
    userName.leftView = leftView3;
    userName.leftViewMode = UITextFieldViewModeAlways;
    [userName setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");

    if ([singlogin.loginfrom isEqualToString:@"cartback"])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        
    }
    [self.navigationController setNavigationBarHidden:YES];
}


            //---------------- UITextField Methods ------------------\\


-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [email resignFirstResponder];
    [pasword resignFirstResponder];
    [userName resignFirstResponder];
    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [email resignFirstResponder];
    [userName resignFirstResponder];
    [pasword resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];

}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == email || textField == pasword || textField == userName)
    {
        [self animateTextField:textField up:YES];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField==email || textField==pasword || textField==userName )
    {
        [self animateTextField:textField up:NO];
    }
}

                    //------------------ IBAction and Login/SignUp Proccess -------------------\\


- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)signUp:(id)sender

{
    if (email.text && userName.text && pasword.text.length > 0)
    {
        
        if (userName.text.length < 3) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter username minimum 3 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }else if (pasword.text.length < 8){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter Password minimum 8 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else{
        
        
         [ProgressHUD show:@"Loading" Interaction:NO];
        
        [indicatr setHidden:NO];
        [indicatr startAnimating];
        [self performSelector:@selector(actionRegister) withObject:Nil afterDelay:2.0f];
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}





- (void)actionRegister
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
   
    
    
    QBUUser *user1 = [QBUUser user];
    user1.login = [email.text lowercaseString];
    user1.password = pasword.text;
    user1.fullName = userName.text;

    
    [QBRequest signUp:user1 successBlock:^(QBResponse *response, QBUUser *user) {
        
       // [ProgressHUD showSuccess:@"Succeed."];
        singlogin.uId =[NSString stringWithFormat:@"%lu",(unsigned long)user.ID] ;
       
    
       // user.blobID=user.ID;
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        [self actionLogin];
        
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        [ProgressHUD showSuccess:@"Try Again"];
        NSLog(@"error: %@", response.error);
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"SignUp" message:@"Username has already taken" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        
        [alert show];
        
        
        
        
    }];
//
    
    
    
    
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
 //   OnzhLkWRVq
   // OnzhLkWRVq
   }



- (void)actionLogin
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *email23 =[email.text lowercaseString];
    NSString *password =  pasword.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email23 length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];
    
    
    
    [QBRequest logInWithUserLogin:email23 password:password successBlock:^(QBResponse *response, QBUUser *user) {
        
        
        
        
        
        singlogin.user_active=user;
        
        singlogin.uId=[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
        NSLog(@"%@",singlogin.uId);
        singlogin.userNameStr=user.fullName;
        
        [self register1];
        [self dismissViewControllerAnimated:YES completion:nil];

       
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
        [ProgressHUD showError:@""];
        
    }];
    
    
}


-(void)action2{
    
    NSString *name		= userName.text;
    NSString *password	= pasword.text;
    NSString *email1     = [email.text lowercaseString];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([name length] == 0)		{ [ProgressHUD showError:@"Name must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    if ([email1 length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Please wait..." Interaction:NO];
    
    PFUser *user = [PFUser user];
    user.username = email1;
    user.password = password;
    user.email = email1;
    user[PF_USER_EMAILCOPY] = email1;
    user[PF_USER_FULLNAME] = name;
    user[PF_USER_FULLNAME_LOWER] = [name lowercaseString];
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (error == nil)
         {
             ParsePushUserAssign();
             [ProgressHUD showSuccess:@"Succeed."];
             [[PFUser currentUser] fetch];
             if ([PFUser currentUser])
                 NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
             singlogin.uId = [[PFUser currentUser] objectId];
             [self register1];
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else [ProgressHUD showError:error.userInfo[@"error"]];
     }];

    
}


-(void)register1

{
   
            emailLogin = email.text;
            passwordLogin = pasword.text;
            singlogin.userNameStr = userName.text;
    [[NSUserDefaults standardUserDefaults] setObject:passwordLogin forKey:@"pass_word"];

//            NSData *myImageData = UIImageJPEGRepresentation(proimg.image, 90);
   //      NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/userregg.php"];

            NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/userreg1.php"];
            
            NSLog(@"strURL:%@", strULR);
            
            NSURL * url=[NSURL URLWithString:strULR];
            NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
            [theLoginRequest setHTTPMethod:@"POST"];
            
            NSMutableData *body = [NSMutableData data];
            NSString *boundry = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
            [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
            
            NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.userNameStr];
            [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSString *emailstr = [[NSString alloc]initWithFormat:@"%@", email.text];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"%@",emailstr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSString *paswrdstr = [[NSString alloc]initWithFormat:@"%@", pasword.text];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [[NSString stringWithFormat:@"%@",paswrdstr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
            NSString *objectIdstr = [[NSString alloc]initWithFormat:@"%@", singlogin.uId];
           [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
           [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding ]];
           [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
           [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    

    
            [theLoginRequest setHTTPBody:body];
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            
            NSLog(@"print mystring==%@",returnString);
            
            NSDictionary *eventarray=[returnString JSONValue];
            
            NSLog(@"GetDatadictt--%@",eventarray);
            
            NSString *msg = [[eventarray valueForKey:@"message"]objectAtIndex:0];
            
            NSLog(@"msg -- %@",msg);
    
    [ProgressHUD showSuccess:@"Succeed"];
            
            if([[[eventarray valueForKey:@"message"] objectAtIndex:0] isEqual:@"User Already Exist"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"User already exists." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                NSMutableArray *details=[eventarray valueForKey:@"detail"];
                NSLog(@"GetArray--%@",details);
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thanks!" message:@"Have fun shopping" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [self login];
                
                
                //ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
               // singlogin.loginStatus = @"User Login";
                //[self.navigationController pushViewController:home animated:YES];

//                Walkthrough *walk = [[Walkthrough alloc ]initWithNibName:@"Walkthrough" bundle:nil];
//                [self.navigationController pushViewController:walk animated:YES];
                [indicatr stopAnimating];
            }
}


-(void)login

{
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",emailLogin,passwordLogin];
    
    NSLog(@"get data=%@",post);
 //   NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin1.php"];

    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    
    NSLog(@"GetData--%@",data);
    
    NSDictionary *eventarray=[data JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSString *uName=[eventarray  valueForKey:@"username"];
    NSString *uaddress=[eventarray  valueForKey:@"address"];
    NSString *usrObjtId=[eventarray  valueForKey:@"userobjectid"];
    NSString *venPic=[eventarray valueForKey:@"photo"];
    singlogin.userproilePic = venPic;

    NSLog(@"Unique User ID %@",uniqueid);
    singlogin.userid=uniqueid;
    singlogin.usrName=uName;
    singlogin.homAddress=uaddress;
    singlogin.usrObjctId = usrObjtId;
    singlogin.userNameStr=uName;
    singlogin.user_id=usrObjtId;
    
    if([neww isEqualToString:@"False"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"User Login" message:@"This email id is not registered! please create new account!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if ([singlogin.loginfrom isEqualToString:@"cart"])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            
            
            
            NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            
            QBMSubscription *subscription = [QBMSubscription subscription];
            subscription.notificationChannel = QBMNotificationChannelAPNS;
            subscription.deviceUDID = deviceIdentifier;
            subscription.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey: @"device_token"];
            
            [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                
                
                [self PROFILEPIC ];
                
                ViewController *home=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                singlogin.loginStatus = @"User Login";
                [self.navigationController pushViewController:home animated:YES];
                
                
                
                NSLog(@"%ld",(long)response.status);
                
            } errorBlock:^(QBResponse *response) {
                
                NSLog(@"%ld",(long)response.error);
                
                
            }];

            
            
            
           
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject: emailLogin forKey: @"email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject: passwordLogin forKey: @"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)PROFILEPIC{
   
    
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed: @"CircledUserMaleFilled.png"], 0.8f);
    
    [QBRequest TUploadFile: imageData fileName: @"ProfilePicture"
               contentType: @"image/jpeg"
                  isPublic: YES successBlock: ^ (QBResponse * response, QBCBlob * blob) {
                      
                      // File uploaded, do something
                      // if blob.isPublic == YES
                      NSString * url = [blob publicUrl];
                      
                      NSLog(@"%@",url);
                      
                      
                      QBUpdateUserParameters *params = [QBUpdateUserParameters new];
                      params.blobID = [blob ID];
                      params.customData=url;
                      [QBRequest updateCurrentUser:params successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                          // success block
                      } errorBlock:^(QBResponse * _Nonnull response) {
                          // error block
                          NSLog(@"Failed to update user: %@", [response.error reasons]);
                      }];
                      
                      
                      
                      
                      
                      
                      
                      
                  }
               statusBlock: ^ (QBRequest * request, QBRequestStatus * status) {
                   // handle progress
               }
                errorBlock: ^ (QBResponse * response) {
                    NSLog(@"error: %@", response.error);
                }
     ];

}

- (IBAction)alredyMembr:(id)sender

{
    User_Login *userLogin = [[User_Login alloc]initWithNibName:@"User_Login" bundle:nil];
    [self.navigationController pushViewController:userLogin animated:NO];
}


- (IBAction)venderSignIn:(id)sender

{
    SignUp_Shop *login = [[SignUp_Shop alloc]initWithNibName:@"SignUp_Shop" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}


-(void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"View Disapear");
}





- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)termsAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://VetrinaApp.com/PrivacyPolicy.html"]];
    
}
@end
