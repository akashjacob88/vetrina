//
//  All_SignIn.h
//  Vetrina
//
//  Created by Amit Garg on 4/3/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface All_SignIn : UIViewController<UIImagePickerControllerDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>

{
    Singleton *singemplist1,*singlogin;
    NSString *emailLogin, *passwordLogin;
    
    // ---------- IBOutlet ----------- \\
    
    UIImage *cimage,*cimage2;

    IBOutlet UIImageView *profile, *backviewimage;
    IBOutlet UITextField *email;
    IBOutlet UITextField *pasword;
    IBOutlet UITextField *userName;
    
    IBOutlet UIActivityIndicatorView *indicatr;
}
- (IBAction)termsAction:(id)sender;




@end
