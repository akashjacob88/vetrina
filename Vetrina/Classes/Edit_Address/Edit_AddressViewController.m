//
//  Edit_AddressViewController.m
//  Vetrina
//
//  Created by Amit Garg on 4/23/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Edit_AddressViewController.h"
#import "JSON.h"
#import "New_address_cell.h"
#import "ProgressHUD.h"
#import "Cell1.h"
#import "Cell2.h"
#import "UIImageView+WebCache.h"


@interface Edit_AddressViewController ()<UITableViewDataSource,UITableViewDelegate>

{
    
}


@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex1;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;

@end

@implementation Edit_AddressViewController
@synthesize isOpen,selectIndex1;


- (void)dealloc
{
    // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex1 = nil;

    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self)
    {
        
    }
    return self;
}





- (void)viewDidLoad

{
    [super viewDidLoad];

    signlogin = [Singleton instance];
      
    countryView.hidden = YES;
    // TopBar View///
    
    topbar.layer.shadowRadius = 2.0f;
    topbar.layer.shadowOffset = CGSizeMake(0, 2);
    topbar.layer.shadowColor = [UIColor blackColor].CGColor;
    topbar.layer.shadowOpacity = 0.5f;
    
    

    
    
    centerBar.layer.shadowRadius = 2.0f;
    centerBar.layer.shadowOffset = CGSizeMake(0, 2);
    centerBar.layer.shadowColor = [UIColor blackColor].CGColor;
    centerBar.layer.shadowOpacity = 0.5f;


    
    
    if ([self.editaddres isEqualToString:@"edit"])
    {
        dltBtn.hidden=NO;
        country.text= [NSString stringWithFormat:@"%@, %@",signlogin.cityAddr,signlogin.countryAddres];
        city.text = signlogin.userFonNo;
        address.text = signlogin.addresaddr;

    }
    else
    {
        dltBtn.hidden=YES;

    }
    [self catlist];
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, country.frame.size.height)];
    country.leftView = leftView1;
    country.leftViewMode = UITextFieldViewModeAlways;
    [country setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, city.frame.size.height)];
    city.leftView = leftView3;
    city.leftViewMode = UITextFieldViewModeAlways;
    [city setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView4 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, address.frame.size.height)];
    address.leftView = leftView4;
    address.leftViewMode = UITextFieldViewModeAlways;
    [address setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];

    self.expansionTableView.layer.shadowRadius = 8.0f;
    self.expansionTableView.layer.shadowOffset = CGSizeMake(0, 3);
    self.expansionTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.expansionTableView.layer.shadowOpacity = 0.8f;
    
    
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    [self.navigationController setNavigationBarHidden:YES];

}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)back:(id)sender

{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Changes have not been saved, are you sure you want to leave?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
        [alert show];
        alert.tag = 10;
}

- (IBAction)countryBtn:(id)sender

{
//    countryTble.hidden = NO;
//    [self country];
//    btnStr = @"1";
//    [countryTble reloadData];
    [city resignFirstResponder];
    [address resignFirstResponder];

    [UIView beginAnimations:nil context:nil];
    self.view.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    [city resignFirstResponder];
    [address resignFirstResponder];

    countryView.hidden = NO;
}


#pragma mark - Category Service

-(void)catlist
{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/citylistall.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    
    if (dataArray.count > 0)
    {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"city"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"countryname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"countryid"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"countryname"];
           // NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [vendorNameArray addObject:vendorName1];
                
               // [imageArr addObject:imagestr];
                
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
                    
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName];
                    [finalArray addObject:dynamicDict];
                    
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:vendorName];
        [finalArray addObject:dynamicDict];
        
        dynamicDict=[[NSMutableDictionary alloc]init];
    }
    else
    {
        finalArray = [[NSMutableArray alloc]init];
        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
    }
    
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex1.section == section)
        {
            NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
            sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
            return [sectionAnimals count]+1;
        }
    }
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex1.section == indexPath.section&&indexPath.row!=0)
    {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"city"];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        cell.titleLabel.text = sectionTitle;
        UIImage *image = [UIImage imageNamed:@"locationWhite.png"];
        cell.iconImg.image = image;
        [cell changeArrowWithUp:([self.selectIndex1 isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex1])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex1 = nil;
            
        }
        else
        {
            if (!self.selectIndex1)
            {
                self.selectIndex1 = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"countryid"];
       signlogin.cityAddr = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"city"];
        signlogin.countryAddres = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"countryname"];
        NSString *name = [NSString stringWithFormat:@"%@, %@",signlogin.cityAddr,signlogin.countryAddres];
        country.text = name;
        countryView.hidden = YES;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}





- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex1];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex1.section;
    
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {
        [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex1 = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    else
    {
        if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}



- (IBAction)confrmBtn:(id)sender

{
    if ([self.editaddres isEqualToString:@"edit"])
    {
        if (country.text.length > 0 && city.text > 0 && address.text.length > 0)
        {
            [indicator setHidden:NO];
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];

            [self performSelector:@selector(editaddress)withObject:Nil afterDelay:0.5f];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        if (country.text.length > 0 && city.text.length > 0 && address.text.length > 0)
        {
            [indicator setHidden:NO];
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];

            [self performSelector:@selector(confirmAddressView) withObject:Nil afterDelay:0.5f];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}


-(void)editaddress
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/edituseradd.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *prodidStr = [[NSString alloc]initWithFormat:@"%@", signlogin.adresId];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"addressid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",prodidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",signlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *countryStrr=[[NSString alloc]initWithFormat:@"%@", signlogin.countryAddres];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"country\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",countryStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *cityStrr=[[NSString alloc]initWithFormat:@"%@", signlogin.cityAddr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"city\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",cityStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSString *addressStr=[[NSString alloc]initWithFormat:@"%@",address.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"address\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",addressStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    NSString *phoneStr=[[NSString alloc]initWithFormat:@"%@",city.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",phoneStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
   
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    signlogin.nextPage=@"yes";
    [self.navigationController popViewControllerAnimated:YES];
    [ProgressHUD showSuccess:@"Success"];
    [indicator stopAnimating];

}


-(void)confirmAddressView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/adduseradd.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",signlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *countryStrr=[[NSString alloc]initWithFormat:@"%@", signlogin.countryAddres];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"country\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",countryStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *cityStrr=[[NSString alloc]initWithFormat:@"%@", signlogin.cityAddr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"city\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",cityStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSString *addressStr=[[NSString alloc]initWithFormat:@"%@",address.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"address\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",addressStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    NSString *phoneStr=[[NSString alloc]initWithFormat:@"%@",city.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",phoneStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    signlogin.nextPage=@"yes";
    [self.navigationController popViewControllerAnimated:YES];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [city resignFirstResponder];
    [address resignFirstResponder];
    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [city resignFirstResponder];
    [address resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == address || textField == city)
    {
        [self animateTextField:textField up:YES];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField == address || textField == city)
    {
        [self animateTextField:textField up:NO];
    }
}


- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deletBtn:(id)sender
{
   
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to delete this address?" delegate:self cancelButtonTitle:@"Delete" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 9;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
            [indicator setHidden:NO];
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];

            [self performSelector:@selector(deleteProduct)withObject:Nil afterDelay:0.5f];

        }
    }
   else if (alertView.tag == 10)
    {
        if (buttonIndex==0)
        {
            if ([self.editaddres isEqualToString:@"edit"])
            {
                signlogin.nextPage=@"yes";
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}




    


-(void)deleteProduct
{
    NSString *post = [NSString stringWithFormat:@"addressid=%@",signlogin.adresId];
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/deluseraddress.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    indicator.hidden=YES;
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
  [self.navigationController popViewControllerAnimated:YES];
    signlogin.nextPage=@"yes";
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Address has been deleted!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}


- (IBAction)close:(id)sender
{
    countryView.hidden = YES;
}



@end
