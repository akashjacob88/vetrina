//
//  Edit_AddressViewController.h
//  Vetrina
//
//  Created by Amit Garg on 4/23/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Edit_AddressViewController : UIViewController

{
    Singleton *signlogin;
    IBOutlet UITextField *country;
    IBOutlet UITextField *city;
    IBOutlet UITextField *address;

    IBOutlet UIButton *dltBtn;
    
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableArray *arra_1;
    NSString *btnStr;
    IBOutlet UIView *topbar;
    IBOutlet UIView *centerBar;
    IBOutlet UIView *countryView;

    
    /* -------- Arrays ------- */
    
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    NSMutableArray *vendorNameArray,*dataArray;
    NSArray *sectionAnimals;
    
    NSMutableArray *imageArr,*img2;
    
    
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2;
    
    NSString *itme;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    


}
@property (strong, nonatomic) NSString *editaddres;

- (IBAction)deletBtn:(id)sender;




@end
