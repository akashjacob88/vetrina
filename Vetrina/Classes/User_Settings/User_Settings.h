//
//  User_Settings.h
//  Vetrina
//
//  Created by Amit Garg on 4/9/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <Quickblox/Quickblox.h>
#import <Quickblox/QBAuthHeader.h>

@interface User_Settings : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

{
    Singleton *singlogin;
    
    IBOutlet UITextField *newPass;
    IBOutlet UITextField *confirmPass;
    
    NSMutableArray *addressArray;
    UITextField *user2;
    
    IBOutlet UIView *view1;
    UIImageView *logOutImg, *image2 , *proImage , *changepasImg, *notifictnIMag;
    UIButton *addressBtn, *logoutBtn, *confrmBtn , *changePassBtn, *notifficationBtn;
    
    IBOutlet UIView *topBar;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITableView *addresTbl;
}
- (IBAction)changePassBtn:(id)sender;

- (IBAction)userRegin:(id)sender;
- (IBAction)okay:(id)sender;

@property(strong,nonatomic) NSString *test;
@end
