//
//  Address_Cell.h
//  Vetrina
//
//  Created by Amit Garg on 4/23/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Address_Cell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *AdressLbl;

@end
