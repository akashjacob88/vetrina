//
//  User_Settings.m
//  Vetrina
//
//  Created by Amit Garg on 4/9/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "User_Settings.h"
#import "Address_Cell.h"
#import "ViewController.h"
#import "Edit_AddressViewController.h"
#import "JSON.h"
#import "User_SettingsAR.h"
#import "WalkthroughAR.h"
#import "Walkthrough.h"
#import "Home_VetrinaARViewController.h"

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ProgressHUD.h"

#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"

#import <Quickblox/Quickblox.h>
#import <Quickblox/QBUUser.h>

@interface User_Settings ()
{
    IBOutlet UITextField *oldPasswrd;
    
}

@end

@implementation User_Settings
@synthesize test;



- (void)viewDidLoad

{
    
    
    [super viewDidLoad];
    view1.hidden = YES;
    singlogin = [Singleton instance];
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    user2 = [[UITextField alloc]init];
    user2.text = singlogin.usrName ;
    user2.userInteractionEnabled=NO;
    user2.delegate = self;
    indicator.hidden=YES;
    [indicator stopAnimating];
   // addresTbl.sectionHeaderHeight = 91;
    addresTbl.sectionFooterHeight = 55;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, newPass.frame.size.height)];
    newPass.leftView = leftView1;
    newPass.leftViewMode = UITextFieldViewModeAlways;
    [newPass setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, confirmPass.frame.size.height)];
    confirmPass.leftView = leftView2;
    confirmPass.leftViewMode = UITextFieldViewModeAlways;
    [confirmPass setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, oldPasswrd.frame.size.height)];
    oldPasswrd.leftView = leftView3;
    oldPasswrd.leftViewMode = UITextFieldViewModeAlways;
    [oldPasswrd setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];

    
    [self addressListing];
    
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)back2:(id)sender

{
    view1.hidden = YES;
}

- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Changes have not been saved, are you sure you want to leave?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
//    [alert show];
//    alert.tag = 9;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
       if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
        [self.navigationController popViewControllerAnimated:YES];
        }
    }
       else if (alertView.tag == 10)
       {
           if (buttonIndex==0)
           {
               [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"email"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"passWord"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"instagramID"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
          
               [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"arabic"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"english"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               
               singlogin.loginStatus = nil;
               singlogin.userid = nil;
               [PFUser logOut];
               ParsePushUserResign();
               PostNotification(NOTIFICATION_USER_LOGGED_OUT);
               LoginUser(self);
               Walkthrough *home = [[Walkthrough alloc]initWithNibName:@"Walkthrough" bundle:NULL];
               [self.navigationController pushViewController:home animated:YES];
           }
       }

}


-(void)addressListing

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/addresslistuser.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        addressArray=[[NSMutableArray alloc]init];
    }
    else
    {
        addressArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    
    [addresTbl reloadData];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return addressArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    Address_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell4"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Address_Cell" bundle:nil] forCellReuseIdentifier:@"Cell4"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell4"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(Address_Cell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    cell.AdressLbl.text = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    singlogin.countryAddres = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"country"];
    singlogin.cityAddr = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"city"];
    singlogin.addresaddr=[[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
    singlogin.adresId = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    singlogin.userFonNo = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"phoneno"];
    
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(nextPage) withObject:nil afterDelay:0.5f];
}


-(void)nextPage
{
    Edit_AddressViewController *edit = [[Edit_AddressViewController alloc]initWithNibName:@"Edit_AddressViewController" bundle:nil];
    edit.editaddres = @"edit";
    [self.navigationController pushViewController:edit animated:YES];
    indicator.hidden=YES;
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat ff;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                ff= 91.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                ff=  181.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                ff=  181.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                ff=  91.0f;
            }
        }
    return  ff;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *headerView = [[UIView alloc] init];
    UILabel *userSettings;
    UIView *anitame;
    UILabel *addressLbl;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            user2.frame = CGRectMake(50, 0, 304, 45);
            [user2 setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:92.0f/255 green:99.0f/255 blue:103.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 46, 254, 45)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(0, 0, 50, 45);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(0, 46, 50, 45);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            userSettings = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 304, 30)];
            user2.frame = CGRectMake(50, 40, 309, 50);
            [user2 setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 91, 309, 50)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
            
            anitame = [[UIView alloc]initWithFrame:CGRectMake(0, 91, 359, 50)];
            //            notifficationBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 142, 309, 50)];
            //            [notifficationBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
            
            addressLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 147, 304, 30)];
            
            
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(0, 40, 50, 50);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(0, 91, 50, 50);
            notifictnIMag = [[UIImageView alloc]init];
            notifictnIMag.frame=CGRectMake(0, 142, 50, 50);

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            userSettings = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 304, 30)];
            user2.frame = CGRectMake(50, 40, 309, 50);
            [user2 setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 91, 309, 50)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
            
            anitame = [[UIView alloc]initWithFrame:CGRectMake(0, 91, 359, 50)];
//            notifficationBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 142, 309, 50)];
//            [notifficationBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:18]];
           
            addressLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 147, 304, 30)];
            
            
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(0, 40, 50, 50);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(0, 91, 50, 50);
            notifictnIMag = [[UIImageView alloc]init];
            notifictnIMag.frame=CGRectMake(0, 142, 50, 50);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            user2.frame = CGRectMake(50, 0, 348, 50);
            [user2 setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:92.0f/255 green:99.0f/255 blue:103.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 46, 348, 50)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(0, 0, 50, 50);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(0, 46, 50, 50);
        }
    }
    
    
    
    user2.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:user2];
    
    proImage.image=[UIImage imageNamed:@"profile_icon.png"];
    proImage.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:proImage];
   
    anitame.backgroundColor = [UIColor whiteColor];
    anitame.layer.shadowRadius = 2.0f;
    anitame.layer.shadowOffset = CGSizeMake(0, 2);
    anitame.layer.shadowColor = [UIColor blackColor].CGColor;
    anitame.layer.shadowOpacity = 0.5f;
    [headerView addSubview:anitame];
    
    [changePassBtn setTitle:@"Change Password" forState:UIControlStateNormal];
    [changePassBtn setTitleColor:[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1] forState:UIControlStateNormal];
    changePassBtn.backgroundColor=[UIColor whiteColor];
    changePassBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [changePassBtn addTarget:self action:@selector(changePassBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:changePassBtn];
   
    changepasImg.image=[UIImage imageNamed:@"lock"];
    changepasImg.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:changepasImg];
    
//    notifictnIMag.image=[UIImage imageNamed:@"lock"];
//    notifictnIMag.backgroundColor = [UIColor whiteColor];
//    [headerView addSubview:notifictnIMag];
//    
//    [notifficationBtn setTitle:@"Notifications" forState:UIControlStateNormal];
//    [notifficationBtn setTitleColor:[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1] forState:UIControlStateNormal];
//    notifficationBtn.backgroundColor=[UIColor whiteColor];
//    notifficationBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//     [headerView addSubview:notifficationBtn];
    
   
    
    //[notifficationBtn addTarget:self action:@selector(changePassBtn:) forControlEvents:UIControlEventTouchUpInside];
   
    
    userSettings.text = @"USER SETTINGS";
    [userSettings setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
    userSettings.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    userSettings.backgroundColor=[UIColor clearColor];
    userSettings.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:userSettings];

    
    addressLbl.text = @"ADDRESS";
    [addressLbl setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
    addressLbl.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    addressLbl.backgroundColor=[UIColor clearColor];
    addressLbl.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:addressLbl];

    return headerView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    UIView *footerView = [[UIView alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 304, 45)];
//            [addressBtn.titleLabel setFont:[UIFont systemFontOfSize:20]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 65, 254, 45)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(0, 65, 50, 45);
//            
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 304, 45)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 304, 45)];
//            [addressBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 65, 254, 45)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(0, 65, 50, 45);
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 304, 45)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 359, 50)];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 65, 308, 50)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(0, 65, 50, 50);
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 359, 50)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 398, 50)];
//            [addressBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 70, 348, 50)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(0, 70, 50, 50);
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 130, 398, 50)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        
    }
    
    [addressBtn setTitle:@"ADD NEW ADDRESS" forState:UIControlStateNormal];
    addressBtn.backgroundColor=[UIColor whiteColor];
    [addressBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
    [addressBtn setTitleColor:[UIColor colorWithRed:237.0f/255 green:92.0f/255 blue:118.0f/255 alpha:1] forState:UIControlStateNormal];
    addressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    addressBtn.layer.shadowRadius = 2.0f;
    addressBtn.layer.shadowOffset = CGSizeMake(0, 2);
    addressBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    addressBtn.layer.shadowOpacity = 0.5f;
    [addressBtn addTarget:self action:@selector(addAdrs:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:addressBtn];
    
    
    
//    [logoutBtn setTitle:@"Log out" forState:UIControlStateNormal];
//    [logoutBtn setTitleColor:[UIColor colorWithRed:92.0f/255 green:99.0f/255 blue:103.0f/255 alpha:1] forState:UIControlStateNormal];
//    logoutBtn.backgroundColor=[UIColor whiteColor];
//    logoutBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [logoutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:logoutBtn];
    
//    logOutImg.image=[UIImage imageNamed:@"logout.png"];
//    logOutImg.backgroundColor = [UIColor whiteColor];
//    [footerView addSubview:logOutImg];
//
//    [confrmBtn setTitle:@"CONFIRM" forState:UIControlStateNormal];
//     confrmBtn.backgroundColor = [UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1];
//    [confrmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:confrmBtn];

    return footerView;
}

-(void)confirm:(UIButton *)sender
{
    
    if (user2.text.length > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    [indicator setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(Submitted)withObject:Nil afterDelay:3.0f];
    }
}


-(void)Submitted
{
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editvendor.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *userstr=[[NSString alloc]initWithFormat:@"%@",user2.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",userstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSString *passStr=[[NSString alloc]initWithFormat:@"%@",newPass.text];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",passStr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    [self.navigationController popViewControllerAnimated:YES];
    indicator.hidden = YES;
    [ProgressHUD showSuccess:@"Success."];

    
}


-(void)logout:(UIButton *)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Log Out" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 10;
}


-(void)addAdrs:(UIButton *)sender

{
    singlogin.addresaddr = @"";
    singlogin.countryAddres = @"";
    singlogin.cityAddr = @"";
    singlogin.userFonNo = @"";
    Edit_AddressViewController *edit = [[Edit_AddressViewController alloc]initWithNibName:@"Edit_AddressViewController" bundle:nil];
    [self.navigationController pushViewController:edit animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([singlogin.nextPage isEqualToString:@"yes"])
    {
        [self addressListing];
        singlogin.nextPage=@"";
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)changePassBtn:(UIButton *)sender

{
    view1.hidden = NO;
    
    
    newPass.text=@"";
    oldPasswrd.text=@"";
    confirmPass.text=@"";
    
    [oldPasswrd becomeFirstResponder];
    

   
    
}

- (IBAction)userRegin:(id)sender

{
    [user2 resignFirstResponder];
}

- (IBAction)okay:(id)sender

{
    
    if (newPass.text.length < 1  && confirmPass.text.length < 1 && oldPasswrd.text.length < 1)
    {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    }
    else if (![newPass.text isEqualToString:confirmPass.text])
    {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password does not match" message:@"Ensure the passwords entered are identical. Note that passwords case-sensitive." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    }else if (newPass.text.length<8){
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter Password At least 8 Characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
    }else
    {
    
    NSString *post;
    if ([singlogin.loginStatus isEqual:@"User Login"])
    {
        post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    }
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        post = [NSString stringWithFormat:@"userid=%@",singlogin.igUniqueId];
    }
        
        NSLog(@"get data=%@",post);
        
        
        
        
        
        
    
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editpass.php"];
    NSLog(@"strURL:%@",strURL);
        
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
        
        
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *idstr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",idstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    NSString *namestr=[[NSString alloc]initWithFormat:@"%@",newPass.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"newpassword\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
        
    NSString *oldPass=[[NSString alloc]initWithFormat:@"%@",oldPasswrd.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"oldpassword\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",oldPass] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];

    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
    
        
        
//        
//        QBUUser *user = [QBUUser user];
//        user.ID = 300;
//        user.oldPassword = @"fgrhpass";
//        user.password = @"newpassword";
//        
//        [QBUsers updateUser:user delegate:self];
        
        
        
        
//        QBUUser *myProfile = [QMApi instance].currentUser;
//        myProfile.password = newPassword;
//        myProfile.oldPassword = oldPassword;
//        
//        [[QMApi instance] changePasswordForCurrentUser:myProfile completion:^(BOOL success) {
//            
//            if (success) {
//                // do something:
//            }
//            
//        }];
//        
 
        
        
        
//        NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"email"];
//        NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
//
//        QBUUser *user=[QBUUser user];
//       
//        NSInteger ide=[[NSString stringWithFormat:@"%@",singlogin.user_id] integerValue];
//        user.ID=ide;
//
//        user.oldPassword = @"12345678";
//        user.password = @"123456789";
//        
//        [QBUsers updateUser:user delegate:self];
//
//        
//    
//        
//        
//
        
        QBUUser *user=[QBUUser user];
        user=[QBSession currentSession].currentUser;
      
        QBUpdateUserParameters *updateParameters = [QBUpdateUserParameters new];
    
        updateParameters.oldPassword=oldPasswrd.text;

        updateParameters.password=newPass.text;
    
        
        [QBRequest updateCurrentUser:updateParameters successBlock:^(QBResponse *response, QBUUser *user) {

             [[NSUserDefaults standardUserDefaults]  setObject:newPass.text forKey:@"password"];
            
            NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:savedPassword forKey:@"pass_word"];
        
            NSLog(@"%@",user);
        
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@",response.error);
        }];
       // [QBUsers updateUser:user delegate:self];
        NSLog(@"%@",user);

    [view1 setHidden:YES];
   }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [user2 resignFirstResponder];
    [newPass resignFirstResponder];
    [confirmPass resignFirstResponder];
    [oldPasswrd resignFirstResponder];
    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [user2 resignFirstResponder];
    [newPass resignFirstResponder];
    [confirmPass resignFirstResponder];
    [oldPasswrd resignFirstResponder];
}


- (IBAction)arabicAR:(id)sender
{
       NSString *str = @"1";
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey: @"english"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:str forKey: @"arabic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker_ar"];
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker1_ar"];

    
    Home_VetrinaARViewController *view = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];

}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == newPass || textField == confirmPass || textField == oldPasswrd)
    {
        [self animateTextField:textField up:YES];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == newPass || textField == confirmPass || textField == oldPasswrd)
    {
        [self animateTextField:textField up:NO];
    }
}


@end
