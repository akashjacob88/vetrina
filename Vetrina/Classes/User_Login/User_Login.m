//
//  User_Login.m
//  Vetrina
//
//  Created by Amit Garg on 4/3/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "User_Login.h"
#import "JSON.h"
#import <Parse/Parse.h>
#import "ViewController.h"
#import "Walkthrough.h"
#import "AppConstant.h"
#import "push.h"
#import "ProgressHUD.h"
#import <Quickblox/Quickblox.h>


@interface User_Login ()

@end

@implementation User_Login

- (void)viewDidLoad

{
    [super viewDidLoad];
    singlogin = [Singleton instance];
    
    [indicatr stopAnimating];
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, emai.frame.size.height)];
    emai.leftView = leftView1;
    emai.leftViewMode = UITextFieldViewModeAlways;
    [emai setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];

    UIView *leftview2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, passwrd.frame.size.height)];
    passwrd.leftView = leftview2;
    passwrd.leftViewMode = UITextFieldViewModeAlways;
    [passwrd setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


/* ---------------------------- IBAction Buttons ----------------------------- */

- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}
                                    /* -------------- Services Source ---------------- */

         /* -------------- Login Proccess ---------------- */

- (IBAction)login:(id)sender

{
    if ([emai.text isEqualToString:@""]||[passwrd.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"User Login" message:@"Fill in the required information!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [indicatr startAnimating];
        [ProgressHUD show:@"Signing in..." Interaction:NO];
        [self actionLogin];
    }
    
}

- (void)actionLogin
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *email = [emai.text lowercaseString];
    NSString *password = passwrd.text;
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"pass_word"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"]);

    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];
    
    
    [QBRequest logInWithUserLogin:email password:password successBlock:^(QBResponse *response, QBUUser *user) {
        
        
        [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user.fullName]];
        
        
        singlogin.user_active=user;
        
        singlogin.uId=[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
        NSLog(@"%@",singlogin.uId);
        singlogin.userNameStr=user.fullName;
        [self performSelector:@selector(loginView)withObject:Nil afterDelay:1.0f];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
     //   [self action2];
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
         [ProgressHUD showError:@""];
    }];

    
    
    
//    [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error)
//     {
//         if (user != nil)
//         {
//             ParsePushUserAssign();
//             [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user[PF_USER_FULLNAME]]];
//            
//             [self performSelector:@selector(loginView)withObject:Nil afterDelay:1.0f];
//
//             [self dismissViewControllerAnimated:YES completion:nil];
//         }
//         else [ProgressHUD showError:error.userInfo[@"error"]];
//     }];
    
//    NSString *email = [emai.text lowercaseString];
//    NSString *password = passwrd.text;
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
//    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    [ProgressHUD show:@"Signing in..." Interaction:NO];
//    [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error)
//     {
//         if (user != nil)
//         {
//             ParsePushUserAssign();
//             [ProgressHUD showSuccess:@"Succeed."];[[PFUser currentUser] fetch];
//             if ([PFUser currentUser])
//                 NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
//             singlogin.uId = [[PFUser currentUser] objectId];
//             [self loginView];
//
//             [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user[PF_USER_FULLNAME]]];
//             [self dismissViewControllerAnimated:YES completion:nil];
//         }
//         else [ProgressHUD showError:error.userInfo[@"error"]];
//     }];
////     [self performSelector:@selector(loginView)withObject:Nil afterDelay:1.0f];
}

-(void)action2{
    
    NSString *email = [emai.text lowercaseString];
    NSString *password = passwrd.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];
    
    
    
    
    [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error)
     {
         if (user != nil)
         {
             ParsePushUserAssign();
             [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user[PF_USER_FULLNAME]]];
             
             [self performSelector:@selector(loginView)withObject:Nil afterDelay:1.0f];
             
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else [ProgressHUD showError:error.userInfo[@"error"]];
     }];
    
    
}

-(void)loginView

{
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",emai.text,passwrd.text];
    
    NSLog(@"get data=%@",post);
  //  NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin1.php"];

    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLenght = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLenght forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventArray = [data JSONValue];
    
    NSString *neww = [eventArray valueForKey:@"Login"];
    //NSArray *val = [eventArray valueForKey:@"0"];
    
    NSString *uniqueid = [eventArray valueForKey:@"id"];
    NSString *uName = [eventArray valueForKey:@"username"];
    NSString *uAdress = [eventArray valueForKey:@"address"];
    singlogin.check = [eventArray valueForKey:@"check"];
    singlogin.rate_status =[eventArray valueForKey:@"rate_status"];
    singlogin.usrObjctId = [eventArray valueForKey:@"userobjectid"];
    NSString *venPic=[eventArray valueForKey:@"photo"];
    singlogin.userproilePic = venPic;
    NSLog(@"Unique User Id %@",uniqueid);
    singlogin.userid = uniqueid;
    singlogin.usrName = uName;
    singlogin.userNameStr=uName;

    singlogin.homAddress = uAdress;
     singlogin.user_id=[eventArray valueForKey:@"userobjectid"];
    [self prodListing];
    if ([neww isEqualToString:@"False"])
    {
        [indicatr stopAnimating];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"User Login" message:@"This email id is not registered! please create new account!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if ([singlogin.loginfrom isEqualToString:@"cart"])
        {
            singlogin.loginStatus = @"User Login";
            singlogin.loginfrom = @"cartback";
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            
            
            
            NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            
            QBMSubscription *subscription = [QBMSubscription subscription];
            subscription.notificationChannel = QBMNotificationChannelAPNS;
            subscription.deviceUDID = deviceIdentifier;
            subscription.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey: @"device_token"];
            
            [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                
                
                
                ViewController *home = [[ViewController alloc]initWithNibName:@"ViewController" bundle:NULL];
                singlogin.loginStatus = @"User Login";
                [self.navigationController pushViewController:home animated:YES];
                
                
                NSLog(@"%ld",(long)response.status);
                
            } errorBlock:^(QBResponse *response) {
                
                NSLog(@"%ld",(long)response.error);
                
                
            }];

            
            
            
            
          
         
        }
    }

    
    [[NSUserDefaults standardUserDefaults] setObject:emai.text forKey:@"email"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:passwrd.text forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [ProgressHUD showSuccess:@""];
    [indicatr stopAnimating];

}
         /* -------------- Forgot Password Proccess ---------------- */

- (IBAction)forgotPass:(id)sender

{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"To reset your password, enter the email address you use to sign in to Vetrina." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
    alertView.tag = 2;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
    
    NSString *alphabet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    forgotPass = [NSMutableString stringWithCapacity:10];
    for (NSUInteger i=0U; i<10; i++)
    {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
//        [forgotPass appendFormat:@"%C", c];
        
    }
        NSLog(@"random string %@",forgotPass);
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 2 && buttonIndex==1)
    {
        UITextField *alertTextFiled = [alertView textFieldAtIndex:0];
        
        NSLog(@"alerttextfiled - %@",alertTextFiled.text);
        
        usremail = alertTextFiled.text;
        NSString *subString = [[usremail componentsSeparatedByString:@"@"] objectAtIndex:0];
        NSLog(@"id---- %@",subString);
        
        newPasswrd = [subString stringByAppendingString:forgotPass];
        NSLog(@"%@", newPasswrd);
      //  [indicatr startAnimating];
              [self forgotPassword];
        
    }
}


-(void)forgotPassword

{
    
    if ([usremail isEqualToString:@""]) {
       
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] ;
        [alertView show];
    }else{
    
    
    
//    UIActivityIndicatorView  *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
//    [self.view addSubview: activityIndicator];
//    activityIndicator.color = [UIColor blackColor];
//    [self.view addSubview:activityIndicator];
//    [self.view bringSubviewToFront:activityIndicator];
//    [activityIndicator startAnimating];
    
    indicatr.hidden = NO;
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [indicatr startAnimating];

    NSString *post1 = [NSString stringWithFormat:@"email=%@",usremail];
    NSLog(@"get data=%@",post1);
    
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/forgotpass.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url1=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url1];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *emailStr=[[NSString alloc]initWithFormat:@"%@",usremail];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",emailStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *passstrr=[[NSString alloc]initWithFormat:@"%@",newPasswrd];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",passstrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *eventarray1=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray1);
    
    [indicatr stopAnimating];
    [indicatr setHidesWhenStopped:YES];
    [ProgressHUD showSuccess:@""];

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your new password is sent at your given email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [indicatr stopAnimating];

    [alert show];
        
        
        
    }
}

-(void)mailPassword
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina/sendmail.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url1=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url1];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *emailStr=[[NSString alloc]initWithFormat:@"%@",usremail];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",emailStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *passstrr=[[NSString alloc]initWithFormat:@"%@",newPasswrd];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",passstrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *eventarray1=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray1);
    [indicatr stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your new password is sent at your given email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];

}
                        /* -------------- UITextField Method ---------------- */

-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    
    [emai resignFirstResponder];
    [passwrd resignFirstResponder];
    
    return TRUE;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField==emai || textField== passwrd)
    {
        [self animateTextField:textField up:YES];
        
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField==emai)
    {
        [self animateTextField:textField up:NO];
    }
    if(textField==passwrd)
    {
        [self animateTextField:textField up:NO];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    [emai resignFirstResponder];
    [passwrd resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up


{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.6f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"View Disapear");
    
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if (cartListArray.count==0) {
        
    }
    else
    {
    NSInteger value=0;
    for(NSInteger i =0;i<cartListArray.count;i++)
    {
        NSString *status=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
        if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] ) {
            value=value+1;
        }
        
     }
        singlogin.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    }
}



- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)termsAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://VetrinaApp.com/PrivacyPolicy.html"]];

}
@end
