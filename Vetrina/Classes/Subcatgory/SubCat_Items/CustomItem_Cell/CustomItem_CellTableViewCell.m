//
//  CustomItem_CellTableViewCell.m
//  Vetrina
//
//  Created by Amit Garg on 4/25/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CustomItem_CellTableViewCell.h"

@implementation CustomItem_CellTableViewCell



-(void)layoutSubviews
{
    [self cardsatup];
}


-(void)cardsatup
{
    [self.backlabl setAlpha:1];
    self.backlabl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.backlabl.layer.masksToBounds = NO;
    self.backlabl.layer.cornerRadius = 2; // redius corners
    self.backlabl.layer.shadowOffset = CGSizeMake(0, .2f);
    self.backlabl.layer.shadowRadius = 3;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.backlabl.bounds];
    self.backlabl.layer.shadowPath = path.CGPath;
    self.backlabl.layer.shadowOpacity = 0.8;
}


- (void)awakeFromNib
{
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
