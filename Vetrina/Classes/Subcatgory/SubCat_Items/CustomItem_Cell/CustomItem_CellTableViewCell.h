//
//  CustomItem_CellTableViewCell.h
//  Vetrina
//
//  Created by Amit Garg on 4/25/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomItem_CellTableViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UIImageView *vendorpic;
@property (strong, nonatomic) IBOutlet UILabel *vendrNme;
@property (strong, nonatomic) IBOutlet UIImageView *itmeImg;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UITextView *productDic;
@property (strong, nonatomic) IBOutlet UIButton *cartBtn;
@property (strong, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) IBOutlet UILabel *btnLbl;
@property (strong, nonatomic) IBOutlet UIImageView *premiumClub;
@property (strong, nonatomic) IBOutlet UIView *cellView;

@property (strong, nonatomic) IBOutlet UILabel *backlabl;
@property (strong, nonatomic) IBOutlet UIImageView *likeImage;









@end
