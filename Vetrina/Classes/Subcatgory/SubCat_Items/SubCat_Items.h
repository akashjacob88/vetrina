//
//  SubCat_Items.h
//  Vetrina
//
//  Created by Amit Garg on 4/25/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
#include <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SubCat_Items : UIViewController<UITableViewDataSource,UITableViewDelegate,BSKeyboardControlsDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    SLComposeViewController*compose;
    BSKeyboardControls *keyboardControls;
    Singleton *singlogin;
    IBOutlet UITableView *itemsTbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    IBOutlet UILabel *subcatName;
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;

    UIImageView *Image2, *img3;
    
    NSMutableArray *aray_2;
    NSArray *sortedarray1,*sortedarray2;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    NSMutableArray *cartListArray;
    NSString *cartIdStr,*quantStr,*prodid;
    IBOutlet UILabel *chrtLbl;
    NSString *searchStr,*soldout,*askforPrice,*staffPic,*name,*objectIdStr;
    IBOutlet UIButton *searchBtn;
    IBOutlet UITextField *searchText;
    NSMutableArray *searchArray,*imageArray,*imageArra2,*newdatalist,*imageDataSource;

    IBOutlet UIView *nolistView;
    IBOutlet UILabel *noitemview;
    IBOutlet UIView *topBar;
    NSString *reson;
    IBOutlet UIButton *reportBtn1,*reportBtn2,*reportBtn3;
    UIImageView *imagefile1,*imagefile2,*imagefile3,*imagefile4,*imagefile5;
    UIImage *image1,*image2,*image3,*image4,*image5;

}
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;
- (IBAction)textDidChange:(id)textField;



@end
