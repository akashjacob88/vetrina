//
//  Vendor_Detaillist.h
//  Vetrina
//
//  Created by Amit Garg on 4/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <Parse/Parse.h>
#include <Social/Social.h>


@interface Vendor_Detaillist : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>

{
    Singleton *singlogin;
    SLComposeViewController*compose;
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;

    IBOutlet UILabel *vendrNme;
    IBOutlet UILabel *cartLbl,*sorry,*you,*blackLbl,*whiteBack,*costText3,*costText2;
    IBOutlet UIButton *likeBtn;
    IBOutlet UITableView *productTbl;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UIView *thnxxView;
    
    IBOutlet UIView *reportView;
    IBOutlet UIButton *reportBtn1;
    IBOutlet UIButton *reportBtn2;
    IBOutlet UIButton *reportBtn3,*reportBtn01,*reportBtn02,*reportBtn03;
    NSString *reson;
    NSString *name;
    IBOutlet UIView *topbar;
    
    IBOutlet UIButton *vendorChat;
    
    
    UIButton *btn,*btn1,*ordernow,*askPrice,*openInstagram,*btn2,*confrmBtn;

    UIImage *pro1,*pro2,*emppppic;
    UIImageView *Image3,*venderImge,*image3;
    UIImageView *Image1,*prod,*Image2,*Image4,*prodImage,*arrowIcon,*cart_img;

    NSString *name2,*desc2,*catid,*navLogin,*productName,*venName,*unit,*quantity,*subcatid,*chckLbl,*staffPic;
    NSString *delivery11,*minimum11,*img3,*unitPrice,*deliverPrice,*rquirement,*vendorID,*vendorPic,*prodimag,*img4,*img5,*img6,*amountPay,*orderDate,*orderTime,*dmndQty,*cellLbl,*productname,*prodId,*selectedImageMediaId,*likeStatus,*vendorpic_browserLink,*statusIDe,*productAlreadyStatus,*vendorFeedStatus,*prodid;

    NSMutableArray *tapArray,*newdatalist;
    NSMutableArray *imagearrayLIst,*data1,*prodImageArrayList,*likeArray,*totalArray,*followsImageArrayList,*array2Value;
    NSArray *sortedarray1;
    NSMutableArray *arra_1,*aray_2,*arraySorted;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr,*askForPrice,*soldout,*v_objct;
    
    IBOutlet UIView *twilloView;

    NSMutableArray *cartListArray;
    NSString *cartIdStr,*quantStr;
    IBOutlet UILabel *chrtLbl;

}

@property( readwrite, assign ) NSInteger * data_id;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)okay:(id)sender;

- (IBAction)Okay2:(id)sender;


- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;


NSString*		StartPrivateChat		(PFUser *user1, PFUser *user2);
NSString*		StartMultipleChat		(NSMutableArray *users);

//-------------------------------------------------------------------------------------------------------------------------------------------------
void		CreateRecentItem		(PFUser *user, NSString *groupId, NSString *description);

//-------------------------------------------------------------------------------------------------------------------------------------------------
void			UpdateRecentCounter		(NSString *groupId, NSInteger amount, NSString *lastMessage);
void			ClearRecentCounter		(NSString *groupId);




@end
