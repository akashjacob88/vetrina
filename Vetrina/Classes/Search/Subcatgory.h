//
//  Subcatgory.h
//  Vetrina
//
//  Created by Amit Garg on 4/13/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
@interface Subcatgory : UIViewController<BSKeyboardControlsDelegate,UIGestureRecognizerDelegate>

{
    BSKeyboardControls *keyboardControls;

    Singleton *singloging, *singemplist;
    IBOutlet UIButton *shopesBtn;
    
    IBOutlet UIButton *itemsBtm;
    NSMutableArray *array1, *array5, *array2;
    NSArray *sortedarray1,*sortedarray2;
    
    NSString *accesToken,*staffPic;
    
    NSMutableArray *imagearrayLIst, *newdatalist;
    
    UIImageView *image2, *arrowIcon,*image3;
    UILabel *costText1, *lineLbl;
    
    IBOutlet UIView *noListView;
    IBOutlet UILabel *subName;
    IBOutlet UITableView *vendorTbl;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn,*searchBtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UITextField *searchText;
    NSMutableArray *searchArray,*imageSearchArray;
    NSString *searchStr;
    IBOutlet UIView *noItmView;

    IBOutlet UIView *topBar;
}
- (IBAction)itemBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)textDidChange:(id)textField;
- (IBAction)search:(id)sender;
- (IBAction)chart:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;

@end


