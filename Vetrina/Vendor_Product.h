//
//  Vendor_Product.h
//  Vetrina
//
//  Created by Amit Garg on 4/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Vendor_Product : UIViewController
{
    Singleton *singloging;
    IBOutlet UIButton *orderbtn;
    IBOutlet UIButton *itembtn;
    IBOutlet UILabel *vendorName;
    NSMutableArray *finalArray,*dataArray,*newArray,*array,*userNameArray,*imageArr;
    NSInteger i;
    NSString *stringOne,*newString,*stringTwo,*orderId;
    NSMutableDictionary *dynamicDict;
    IBOutlet UITableView *userListingTable;
    IBOutlet UIActivityIndicatorView *indicator;
    NSString *appear;
    IBOutlet UIView *noOrderView;
    IBOutlet UIView *topBar;
    UIRefreshControl *refreshControl;
    IBOutlet UIButton *editBtn;
    IBOutlet UIButton *settingOut;

}
@property (strong, nonatomic) IBOutlet UILabel *count_lbl;

- (void)loadRecents;
- (IBAction)items:(id)sender;
@end
