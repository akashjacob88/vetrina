//
//  orderHistory.m
//  Vetrina
//
//  Created by Amit Garg on 3/30/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import "orderHistory.h"
#import "JSON.h"
#import "ProgressHUD.h"
#import "Singleton.h"
#include "UIImageView+WebCache.h"
#import "chat_New.h"


@interface orderHistory ()
{
    NSInteger i;

    UITableView *table_View;
    CGRect screenRect;
    UIView *mainView2;
    Singleton *singlogin;
    NSString *user_id;
    NSArray *dataArray;
    
    NSArray *dialog_Objects;
    QBUUser *currentUser;
    NSMutableArray *users_array;
    QBChatDialog *chatDialog;
    int abc;
    NSString *stringOne,*newString,*stringTwo,*orderId;
    NSMutableArray *finalArray,*newArray,*array,*userNameArray,*imageArr;
    NSMutableDictionary *dynamicDict;
    
    UITableView *detail_table;
    NSArray *abc123;
    
    NSMutableArray *dateAr;
    NSMutableArray *timear;
}
@end

@implementation orderHistory

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void)loadrecents{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    

    
}

- (void)viewDidLoad {
    [super viewDidLoad];

     singlogin = [Singleton instance];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        
        [_english_Btn setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        _arebic_btn.hidden=YES;
        _english_Btn.hidden=NO;
        
        
        _arebicOrderLbl.hidden=YES;
        _englishOrderLbl.hidden=NO;
        
     //   [_arebic_btn setImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"] forState:UIControlStateNormal];

        
        
    }else{
        
        _arebicOrderLbl.hidden=NO;
        _englishOrderLbl.hidden=YES;
        
        _arebic_btn.hidden=NO;
        _english_Btn.hidden=YES;

        
     //   [_english_Btn setImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"] forState:UIControlStateNormal];
        [_arebic_btn setImage:[UIImage imageNamed:@"back1.png"] forState:UIControlStateNormal];
    }

    
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
       
        
        NSString *post = [NSString stringWithFormat:@"vendorid=%@",singlogin.vUniqueId];
        NSLog(@"get data=%@",post);
        NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendor_neworderlist.php"];
        NSLog(@"PostData--%@",post);
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSMutableDictionary *eventarray =[data JSONValue];
        if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
        {
            dataArray=[[NSMutableArray alloc]init];
        }
        else
        {
            dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        }
        
        
        NSMutableArray *array1=[[NSMutableArray alloc]init];
        
        for (int y=(int)dataArray.count - 1; y>=0; y--) {
            
            
            [array1 addObject:[dataArray objectAtIndex:y]];
            
        }
        
        dataArray=array1;
    
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        userNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        dateAr=[[NSMutableArray alloc]init];
        timear=[[NSMutableArray alloc]init];

        
        for( i =0;i<dataArray.count;i++)
        {
            stringOne=[[dataArray objectAtIndex:i]valueForKey:@"orderid"];
            
            NSString *userName1=[[dataArray objectAtIndex:i]valueForKey:@"username"];
            NSString *imagestr=[[dataArray objectAtIndex:i]valueForKey:@"userprofilepic"];
            NSString *d=[[dataArray objectAtIndex:i]valueForKey:@"orderdate"];
            NSString *t=[[dataArray objectAtIndex:i]valueForKey:@"ordertime"];
            
            if (![stringOne isEqualToString:stringTwo ])
            {
                [userNameArray addObject:userName1];
                [imageArr addObject:imagestr];
                [dateAr addObject:d];
                [timear addObject:t];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[dataArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[dataArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        
        if (dataArray.count>0) {
            
        
        
        
        NSInteger a=i-1;
        NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"username"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:userName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";

        
        
        
        }else{
            
            
            
            
            
            
        }
        
        
        
        
        
        
        
        
        [self loadrecents];
        
        
        
    }
    else if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
      
        NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
        NSLog(@"get data=%@",post);
        NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/order_userlist.php"];
        NSLog(@"PostData--%@",post);
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSMutableDictionary *eventarray =[data JSONValue];
        if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
        {
            dataArray=[[NSMutableArray alloc]init];
            
        }
        else
        {
            dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        }
 
      
        NSMutableArray *array1=[[NSMutableArray alloc]init];
        
        for (int y = (int)dataArray.count - 1; y>=0; y--) {
            
            
            [array1 addObject:[dataArray objectAtIndex:y]];
            
        }
        
        dataArray=array1;
    
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        userNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        dateAr=[[NSMutableArray alloc]init];
        timear=[[NSMutableArray alloc]init];
        for( i =0;i<dataArray.count;i++)
        {
            stringOne=[[dataArray objectAtIndex:i]valueForKey:@"orderid"];
            
            NSString *userName1=[[dataArray objectAtIndex:i]valueForKey:@"vendorname"];
            NSString *imagestr=[[dataArray objectAtIndex:i]valueForKey:@"vendorpic"];
            NSString *d=[[dataArray objectAtIndex:i]valueForKey:@"orderdate"];
            NSString *t=[[dataArray objectAtIndex:i]valueForKey:@"ordertime"];

            
            
            if (![stringOne isEqualToString:stringTwo ])
            {
                [userNameArray addObject:userName1];
                [imageArr addObject:imagestr];
                
                [dateAr addObject:d];
                [timear addObject:t];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[dataArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[dataArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        
        if (dataArray.count>0) {

        
        
        NSInteger a=i-1;
        NSString *userName=[[dataArray objectAtIndex:a]valueForKey:@"vendorname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:userName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";

        
        }
        
        [self loadrecents];

    
    }
    else
    {
      
        
        
    }
    

    
    
    
    
    
    
    screenRect=[[UIScreen mainScreen] bounds];

    table_View=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenRect.size.width, screenRect.size.height-64)];
    [self.view addSubview:table_View];
    table_View.delegate=self;
    table_View.dataSource=self;
    table_View.separatorColor = [UIColor clearColor];
    table_View.backgroundColor=[UIColor clearColor];








}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView==detail_table) {
        
        return abc123.count;
    }
    
    return finalArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==detail_table) {
        
        return 60;
    }
    
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"cellID";
    UITableViewCell *cell;
    
        cell = [tableView dequeueReusableCellWithIdentifier:
                cellIdentifier];
        
        
        if (cell == nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
    
    if (tableView==detail_table) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
        {
            
            
            UILabel *qty_lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 30)];
            [cell addSubview:qty_lbl];
            qty_lbl.text=@"QTY";
            qty_lbl.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
            qty_lbl.textColor=[UIColor whiteColor];
            
            qty_lbl.textAlignment=NSTextAlignmentCenter;
            
            
            UILabel *qtyNo=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 70, 29)];
            [cell addSubview:qtyNo];
            qtyNo.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"quantityprod"];
            qtyNo.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
            qtyNo.textColor=[UIColor whiteColor];
            qtyNo.textAlignment=NSTextAlignmentCenter;
            
            UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(screenRect.size.width-55 , 5, 50, 50)];
            
            
            [cell addSubview:profile_img];
            
            NSString *strImageUrl1;
            
            
            
            strImageUrl1  =[[abc123 objectAtIndex:indexPath.row] valueForKey:@"prodimage"];
            
            
            
            
            
            
            
            
            [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
            
            
            UILabel *ItemName=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-210, 10, 150, 25)];
            [cell addSubview:ItemName];
            ItemName.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"productname"];
            ItemName.backgroundColor=[UIColor whiteColor];
            
            ItemName.textAlignment=NSTextAlignmentRight;
            
            
            
            
            UILabel *ItemPrice=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-210, 35, 150, 12)];
            [cell addSubview:ItemPrice];
            ItemPrice.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"unitprice"];
            ItemPrice.backgroundColor=[UIColor whiteColor];
            
            ItemPrice.textAlignment=NSTextAlignmentRight;

            
            UILabel *line1=[[UILabel alloc]initWithFrame:CGRectMake(70  , 59, screenRect.size.width-70, 1)];
            [cell addSubview:line1];
            line1.backgroundColor=[UIColor grayColor];
            
            
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            cell.backgroundColor=[UIColor whiteColor];
            return cell;

            
 
        }else{
            
            
            
        
        UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(5 , 5, 50, 50)];
        
        
        [cell addSubview:profile_img];
        
        NSString *strImageUrl1;
        
        
            
            strImageUrl1  =[[abc123 objectAtIndex:indexPath.row] valueForKey:@"prodimage"];
        
            
        
        
        
        
        
        
        [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
        
        
        
        UILabel *ItemName=[[UILabel alloc]initWithFrame:CGRectMake(65, 10, 150, 25)];
        [cell addSubview:ItemName];
        ItemName.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"productname"];
        ItemName.backgroundColor=[UIColor whiteColor];

        
            UILabel *ItemPrice=[[UILabel alloc]initWithFrame:CGRectMake(65, 35, 150, 12)];
            [cell addSubview:ItemPrice];
            ItemPrice.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"unitprice"];
        ItemPrice.backgroundColor=[UIColor whiteColor];

            UILabel *qty_lbl=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-70, 0, 70, 30)];
            [cell addSubview:qty_lbl];
            qty_lbl.text=@"QTY";
            qty_lbl.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
            qty_lbl.textColor=[UIColor whiteColor];
        
            qty_lbl.textAlignment=NSTextAlignmentCenter;
        
        
            UILabel *qtyNo=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-70, 30, 70, 29)];
        [cell addSubview:qtyNo];
            qtyNo.text=[[abc123 objectAtIndex:indexPath.row] valueForKey:@"quantityprod"];
            qtyNo.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
            qtyNo.textColor=[UIColor whiteColor];
            qtyNo.textAlignment=NSTextAlignmentCenter;
        
//        
//        UILabel *line11=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-70  , 59, 70, 1)];
//        [cell addSubview:line11];
//        line11.backgroundColor=[UIColor whiteColor];

        
        UILabel *line1=[[UILabel alloc]initWithFrame:CGRectMake(0  , 59, screenRect.size.width-70, 1)];
        [cell addSubview:line1];
        line1.backgroundColor=[UIColor grayColor];

        
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        cell.backgroundColor=[UIColor whiteColor];
        return cell;
        
        }
            
            
    }else{
    
    
    UIView *mainView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, screenRect.size.width-10, 190)];
    [cell addSubview:mainView];
    
    mainView.backgroundColor=[UIColor whiteColor];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:mainView.bounds];
    mainView.layer.masksToBounds = NO;
    mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    mainView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    mainView.layer.shadowOpacity = 0.5f;
    mainView.layer.shadowPath = shadowPath.CGPath;
    

        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil){
            
            
            
            UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(screenRect.size.width-70, 10, 60, 60)];
            
            profile_img.layer.cornerRadius=30.0f;
            profile_img.layer.masksToBounds=YES;
            [mainView addSubview:profile_img];
            
            NSString *strImageUrl1;
            
            UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-230, 10, 150, 60)];
            [mainView addSubview:name];
            
            name.textAlignment=NSTextAlignmentRight;
            
            UIButton *chatBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
            [mainView addSubview:chatBtn];
            
            chatBtn.tag=indexPath.row;
            [chatBtn setImage:[UIImage imageNamed:@"PinkChat"] forState:UIControlStateNormal];
            
            
            [chatBtn addTarget:self action:@selector(chatting:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                
                
                strImageUrl1  =[imageArr objectAtIndex:indexPath.row];
                
                
                name.text=[userNameArray objectAtIndex:indexPath.row] ;
                
            }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
                
                
                strImageUrl1  =[imageArr objectAtIndex:indexPath.row];
                
                
                name.text=[userNameArray objectAtIndex:indexPath.row] ;
                
                
            }
            
            
            
            
            
            
            [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
            
            
            
            UILabel *line=[[UILabel alloc]initWithFrame:CGRectMake(0, 72, screenRect.size.width-10, 1)];
            [mainView addSubview:line];
            line.backgroundColor=[UIColor grayColor];
            
            
            
            
            UILabel *orederDate=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 78, 130, 25)];
            [mainView addSubview:orederDate];
            orederDate.text=[dateAr objectAtIndex:indexPath.row];
            
            
            UILabel *line01=[[UILabel alloc]initWithFrame:CGRectMake(0  , 105, mainView.frame.size.width, 1)];
            [mainView addSubview:line01];
            line01.backgroundColor=[UIColor grayColor];
            
            
            
            
            UILabel *orederTime=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 110, 130, 25)];
            [mainView addSubview:orederTime];
            orederTime.text=[timear objectAtIndex:indexPath.row];
            
            
            UILabel *line10=[[UILabel alloc]initWithFrame:CGRectMake(0  , 140, mainView.frame.size.width, 1)];
            [mainView addSubview:line10];
            line10.backgroundColor=[UIColor grayColor];
            
            
            
            
            UIButton *showOrder=[[UIButton alloc]initWithFrame:CGRectMake( 0, 145, screenRect.size.width, 37)];
            [mainView addSubview:showOrder];
            
            [showOrder setTitle:@"Show Order" forState:UIControlStateNormal];
            [showOrder setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            showOrder.titleLabel.font=[UIFont fontWithName:@"OpenSans-Bold" size:18];
            
            
            [showOrder addTarget:self action:@selector(show_order:) forControlEvents:UIControlEventTouchUpInside];
            showOrder.tag=indexPath.row;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
            

            
            
            
            
            
        }else{

    
    
    
    UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
    
    profile_img.layer.cornerRadius=30.0f;
    profile_img.layer.masksToBounds=YES;
    [mainView addSubview:profile_img];

    NSString *strImageUrl1;

    UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(80, 10, 150, 60)];
    [mainView addSubview:name];
   
    UIButton *chatBtn=[[UIButton alloc]initWithFrame:CGRectMake( screenRect.size.width-70, 10, 60, 60)];
    [mainView addSubview:chatBtn];
    
    chatBtn.tag=indexPath.row;
    [chatBtn setImage:[UIImage imageNamed:@"PinkChat"] forState:UIControlStateNormal];
    
    
    [chatBtn addTarget:self action:@selector(chatting:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        
        
        strImageUrl1  =[imageArr objectAtIndex:indexPath.row];

        
         name.text=[userNameArray objectAtIndex:indexPath.row] ;
        
    }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
        
        
        strImageUrl1  =[imageArr objectAtIndex:indexPath.row];
        
        
        name.text=[userNameArray objectAtIndex:indexPath.row] ;


    }

    
    
    
    
    
    [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
    
 
    
    UILabel *line=[[UILabel alloc]initWithFrame:CGRectMake(0, 72, screenRect.size.width-10, 1)];
    [mainView addSubview:line];
    line.backgroundColor=[UIColor grayColor];
            
            
            
            UILabel *orederDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 76, 130, 25)];
            [cell addSubview:orederDate];
            orederDate.text=[dateAr objectAtIndex:indexPath.row];
            
            
            
            UILabel *line01=[[UILabel alloc]initWithFrame:CGRectMake(0  , 105, mainView.frame.size.width, 1)];
            [mainView addSubview:line01];
            line01.backgroundColor=[UIColor grayColor];
            
            
            
            
            UILabel *orederTime=[[UILabel alloc]initWithFrame:CGRectMake(10, 110, 130, 25)];
            [mainView addSubview:orederTime];
            orederTime.text=[timear objectAtIndex:indexPath.row];
            
            UILabel *line11=[[UILabel alloc]initWithFrame:CGRectMake(0  , 140, mainView.frame.size.width, 1)];
            [mainView addSubview:line11];
            line11.backgroundColor=[UIColor grayColor];

            
            
            
            
            
            
    
    UIButton *showOrder=[[UIButton alloc]initWithFrame:CGRectMake( 0, 145, screenRect.size.width, 37)];
    [mainView addSubview:showOrder];

    [showOrder setTitle:@"Show Order" forState:UIControlStateNormal];
    [showOrder setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    showOrder.titleLabel.font=[UIFont fontWithName:@"OpenSans-Bold" size:18];
    
    
    [showOrder addTarget:self action:@selector(show_order:) forControlEvents:UIControlEventTouchUpInside];
    showOrder.tag=indexPath.row;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    }
        
        
        
        
        
     }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor=[UIColor whiteColor];

    return cell;
        
        
        
   
    
    
    
        
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
   
    
    
    
    
}


-(void)chatting:(id)sender{
    
abc=(int)[sender tag];
    
    
    [ProgressHUD show:@"Loading" Interaction:NO];
    
    
    [self chat];
    
    
}



-(void)show_order:(id)sender{
    
    
    int a=(int)[sender tag];
    NSLog(@"%@",[finalArray objectAtIndex:a]);
    
    
    dataArray=[[NSArray alloc]init];
    dataArray=[finalArray objectAtIndex:a];
    
    NSString *uname=[userNameArray objectAtIndex:a];
    
    abc123=[[NSArray alloc]init];
    abc123=[dataArray valueForKey:uname];
    
    mainView2=[[UIView alloc]initWithFrame:CGRectMake(0  , 0, screenRect.size.width, screenRect.size.height)];
    [self.view addSubview:mainView2];
    
    mainView2.backgroundColor=[UIColor whiteColor];
    
    UIView *innerView=[[UIView alloc]initWithFrame:CGRectMake(0  , 15, screenRect.size.width,270)];
    [mainView2 addSubview:innerView];
    
    
    innerView.backgroundColor=[UIColor whiteColor];

    
    
    
    UILabel *line1=[[UILabel alloc]initWithFrame:CGRectMake(0  , 60, innerView.frame.size.width, 1)];
    [innerView addSubview:line1];
    line1.backgroundColor=[UIColor grayColor];

    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
    
        UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(screenRect.size.width-60 , 5, 50, 50)];
        profile_img.layer.cornerRadius=25.0f;
        profile_img.layer.masksToBounds=YES;
        
        [innerView addSubview:profile_img];
        
        NSString *strImageUrl1;
        
        
        
        if ([singlogin.loginStatus isEqualToString:@"IG Login"])
        {
            
            
            strImageUrl1  =[[abc123 objectAtIndex:0] valueForKey:@"userprofilepic"];
            
            
            
        }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
            
            strImageUrl1  =[[abc123 objectAtIndex:0] valueForKey:@"userprofilepic"];
            
        }
        
        
        
        
        
        
        [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
        
        
        
        UILabel *ItemName=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-220, 10, 150, 35)];
        [innerView addSubview:ItemName];
        ItemName.text=[[abc123 objectAtIndex:0] valueForKey:@"username"];
        
        ItemName.textAlignment=NSTextAlignmentRight;
        ItemName.backgroundColor=[UIColor whiteColor];
        
        UILabel *orederDate=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 65, 130, 25)];
        [innerView addSubview:orederDate];
        orederDate.text=[[abc123 objectAtIndex:0] valueForKey:@"orderdate"];
        
        
        UILabel *line01=[[UILabel alloc]initWithFrame:CGRectMake(0  , 90, innerView.frame.size.width, 1)];
        [innerView addSubview:line01];
        line01.backgroundColor=[UIColor grayColor];

        
        
        
        UILabel *orederTime=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 95, 130, 25)];
        [innerView addSubview:orederTime];
        orederTime.text=[[abc123 objectAtIndex:0] valueForKey:@"ordertime"];
    
        
        UILabel *line10=[[UILabel alloc]initWithFrame:CGRectMake(0  , 120, innerView.frame.size.width, 1)];
        [innerView addSubview:line10];
        line10.backgroundColor=[UIColor grayColor];

        
        
//        UILabel *dateLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 5, 50, 25)];
//        [innerView addSubview:dateLbl];
//        dateLbl.text=@"Date";
//
//        UILabel *Timelbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 35, 50, 25)];
//        [innerView addSubview:Timelbl];
//        Timelbl.text=@"Time";
//        
//        orederDate.font=[UIFont fontWithName:@"OpenSans" size:12];
//        orederTime.font=[UIFont fontWithName:@"OpenSans" size:12];
//
//        
//        Timelbl.font=[UIFont fontWithName:@"OpenSans" size:12];
//        dateLbl.font=[UIFont fontWithName:@"OpenSans" size:12];

        
    }else{
        
        UIImageView *profile_img=[[UIImageView alloc]initWithFrame:CGRectMake(5 , 5, 50, 50)];
        profile_img.layer.cornerRadius=25.0f;
        profile_img.layer.masksToBounds=YES;
        
        [innerView addSubview:profile_img];
        
        NSString *strImageUrl1;
        
        
        
        if ([singlogin.loginStatus isEqualToString:@"IG Login"])
        {
            
            
            strImageUrl1  =[[abc123 objectAtIndex:0] valueForKey:@"userprofilepic"];
            
            
            
        }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
            
            strImageUrl1  =[[abc123 objectAtIndex:0] valueForKey:@"userprofilepic"];
            
        }
        
        
        
        
        
        
        [profile_img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
        
        
        
        UILabel *ItemName=[[UILabel alloc]initWithFrame:CGRectMake(75, 10, 150, 35)];
        [innerView addSubview:ItemName];
        ItemName.text=[[abc123 objectAtIndex:0] valueForKey:@"username"];
        
        
        ItemName.backgroundColor=[UIColor whiteColor];
        
        UILabel *orederDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 65, 130, 25)];
        [innerView addSubview:orederDate];
        orederDate.text=[[abc123 objectAtIndex:0] valueForKey:@"orderdate"];
        
        
        
        UILabel *line01=[[UILabel alloc]initWithFrame:CGRectMake(0  , 90, innerView.frame.size.width, 1)];
        [innerView addSubview:line01];
        line01.backgroundColor=[UIColor grayColor];
        
        

        
        UILabel *orederTime=[[UILabel alloc]initWithFrame:CGRectMake(10, 95, 130, 25)];
        [innerView addSubview:orederTime];
        orederTime.text=[[abc123 objectAtIndex:0] valueForKey:@"ordertime"];
        
        UILabel *line11=[[UILabel alloc]initWithFrame:CGRectMake(0  , 120, innerView.frame.size.width, 1)];
        [innerView addSubview:line11];
        line11.backgroundColor=[UIColor grayColor];
        
        
//        UILabel *dateLbl=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-200, 5, 60, 25)];
//        [innerView addSubview:dateLbl];
//        dateLbl.text=@"Date";
//        
//        UILabel *Timelbl=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-200, 35, 60, 25)];
//        [innerView addSubview:Timelbl];
//        Timelbl.text=@"Time";
//
//        Timelbl.font=[UIFont fontWithName:@"OpenSans" size:12];
//        dateLbl.font=[UIFont fontWithName:@"OpenSans" size:12];
//
//        orederDate.font=[UIFont fontWithName:@"OpenSans" size:12];
//        orederTime.font=[UIFont fontWithName:@"OpenSans" size:12];
        
    }
    
    
   

//    UILabel *ItemPrice=[[UILabel alloc]initWithFrame:CGRectMake(65, 35, 150, 12)];
//    [innerView addSubview:ItemPrice];
//    ItemPrice.text=[[dataArray objectAtIndex:a] valueForKey:@"unitprice"];
    
//    UILabel *qty_lbl=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-70, 0, 70, 30)];
//    [innerView addSubview:qty_lbl];
//    qty_lbl.text=@"QTY";
//    qty_lbl.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
//    qty_lbl.textColor=[UIColor whiteColor];
//    
//    qty_lbl.textAlignment=NSTextAlignmentCenter;
//    
//    
//    UILabel *qtyNo=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-70, 30, 70, 30)];
//    [innerView addSubview:qtyNo];
//    qtyNo.text=[[dataArray objectAtIndex:a] valueForKey:@"quantityprod"];
//    qtyNo.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
//    qtyNo.textColor=[UIColor whiteColor];
//    qtyNo.textAlignment=NSTextAlignmentCenter;
    
    
    
    
    
    UITextView *addressView=[[UITextView alloc]initWithFrame:CGRectMake(2, 125, innerView.frame.size.width-4, 50)];
    
    addressView.text=[[abc123 objectAtIndex:0] valueForKey:@"useraddress"];
    
    [innerView addSubview:addressView];
    
    addressView.selectable=NO;
    addressView.editable=NO;
    
    addressView.scrollEnabled=NO;
    addressView.font=[UIFont fontWithName:@"OpenSans" size:12];
    
    
    
    
    UILabel *line2=[[UILabel alloc]initWithFrame:CGRectMake(0  , 175, innerView.frame.size.width, 1)];
    [innerView addSubview:line2];
    line2.backgroundColor=[UIColor grayColor];
    
    
    UILabel *line21=[[UILabel alloc]initWithFrame:CGRectMake(0  , innerView.frame.size.height-1, innerView.frame.size.width, 1)];
    [innerView addSubview:line21];
    line21.backgroundColor=[UIColor grayColor];

    
    
    

    UIButton *okay=[[UIButton alloc]initWithFrame:CGRectMake( 0, mainView2.frame.size.height-65,innerView.frame.size.width, 65)];
    [mainView2 addSubview:okay];
    
    okay.backgroundColor=[UIColor colorWithRed:119/255.f green:218/255.f blue:172/255.f alpha:1.0];
    [okay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okay addTarget:self action:@selector(hide_view) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        
        [okay setTitle:@"تمام" forState:UIControlStateNormal];

        
        
    }else{
        
        
        [okay setTitle:@"OKAY" forState:UIControlStateNormal];

        
    }
    
    
    
    detail_table=[[UITableView alloc]initWithFrame:CGRectMake(0, innerView.frame.origin.y+innerView.frame.size.height, innerView.frame.size.width, mainView2.frame.size.height-innerView.frame.size.height-70)];
    [mainView2 addSubview:detail_table];
    detail_table.delegate=self;
    detail_table.dataSource=self;
    
    [detail_table reloadData];
    detail_table.separatorColor=[UIColor clearColor];
    
    
    UILabel *delv=[[UILabel alloc]initWithFrame:CGRectMake(2 , 180, 150, 42)];
    [innerView addSubview:delv];
    delv.text=@"Delivery Charge";
    
    
    
    
    UILabel *delv_charge=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 180, 110, 35)];
    [innerView addSubview:delv_charge];

    delv_charge.text=[[abc123 objectAtIndex:0] valueForKey:@"deliveryprice"];
    
    
    UILabel *lineVer1=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-122  , 176, 1, 100)];
    [innerView addSubview:lineVer1];
    lineVer1.backgroundColor=[UIColor grayColor];
    
    
    
    
    UILabel *line3=[[UILabel alloc]initWithFrame:CGRectMake(0  , 225, innerView.frame.size.width, 1)];
    [innerView addSubview:line3];
    line3.backgroundColor=[UIColor grayColor];

    
    UILabel *total=[[UILabel alloc]initWithFrame:CGRectMake(2 , 225, 150, 35)];
    [innerView addSubview:total];
    total.text=@"Total";
    
    
    
    
    UILabel *total_charge=[[UILabel alloc]initWithFrame:CGRectMake(screenRect.size.width-120, 225, 110, 35)];
    [innerView addSubview:total_charge];
    
    total_charge.text=[[abc123 objectAtIndex:0] valueForKey:@"amountpay"];

    
 

    
}


-(void)hide_view{
    
    
    mainView2.hidden=YES;
    
    
}

    

- (IBAction)english_btnAction:(id)sender {

    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        
        
        
    }



}

- (IBAction)arebic_btnAction:(id)sender {
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        
    }else{
        
        
        [self.navigationController popViewControllerAnimated:YES];

        
    }
    
    
}

-(void)chat

{
    
    currentUser = [QBUUser user];
    
    NSInteger o;
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        
        dataArray=[[NSArray alloc]init];
        dataArray=[finalArray objectAtIndex:abc];
        
        NSString *uname=[userNameArray objectAtIndex:abc];
        
        abc123=[[NSArray alloc]init];
        abc123=[dataArray valueForKey:uname];
        
        
        o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"vendorobjectid"]] integerValue ];
        
        
    }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
        
       
        
        dataArray=[[NSArray alloc]init];
        dataArray=[finalArray objectAtIndex:abc];
        
        NSString *uname=[userNameArray objectAtIndex:abc];
        
        abc123=[[NSArray alloc]init];
        abc123=[dataArray valueForKey:uname];
        
        
        o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"userobjectid"]] integerValue ];
         }
    
    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"]);
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            [self retrieveAllUsersFromPage:1];
            
        }
    }
     
     
     
     ];
    
    
    
    
    
    
    
}


- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    //    ChatView *chatView = [[ChatView alloc] initWith:groupId];
    //    chatView.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:chatView animated:YES];
    
    
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
}

- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users21) {
        
        
        [users_array addObjectsFromArray:users21];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.usrObjctId] integerValue ];
        
        [seen addObject:[NSString stringWithFormat:@"%ld",o] ];
        NSUInteger i = 0;
        
        while (i < [users_array count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[users_array objectAtIndex:i];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [users_array removeObjectAtIndex:i];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                i++;
            }
        }
        
        
        
        [self newChat];
        
        
         } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}


-(void)newChat{
    
    QBChatDialog *test_dialog;
    
    for (int i=0; i<dialog_Objects.count; i++) {
        
        
        test_dialog=[dialog_Objects objectAtIndex:i];
        
        
        
        
    }
    
    
    
    
    
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    
    
    
    
    
    NSInteger o;
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"userobjectid"]] integerValue ];
        
        singlogin.namechat=[[abc123 objectAtIndex:0] valueForKey:@"username"];
        singlogin.chat_user=[[abc123 objectAtIndex:0] valueForKey:@"userobjectid"];
        
    }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
        
        
        o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"vendorobjectid"]] integerValue ];
        singlogin.chat_user=[[abc123 objectAtIndex:0] valueForKey:@"vendorobjectid"];
singlogin.namechat=[[abc123 objectAtIndex:0] valueForKey:@"vendorname"];
    }
    

    chatDialog.occupantIDs = @[@(o) ];
    
    
    
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        
        [self actionChat:createdDialog.ID];
        
        
        
        
    } errorBlock:^(QBResponse *response) {
        
    }];
    
    
    
}


-(void)chat22{
    
    
    
    NSInteger o;
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
      o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"userobjectid"]] integerValue ];

        
    }else if ([singlogin.loginStatus isEqualToString:@"User Login"]){
        
        
          o  = [ [ NSString stringWithFormat: @"%@",[[abc123 objectAtIndex:0] valueForKey:@"vendorobjectid"]] integerValue ];
        
    }

    
    
    
    for (int x=0; x<users_array.count; x++) {
        
        
        QBUUser *user1   =[QBUUser user];
        
        user1 =[users_array objectAtIndex:x];
        
        
        if (user1.ID==o) {
            [self newChat];
            break;
            
        }
        
        
        
    }
    
    
    [ProgressHUD showSuccess:@"Cannot Connect , try again"];
    
    
}






@end














