//
//  IntroView.h
//  ABCIntroView
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABCIntroViewDelegate <NSObject>


@end

@interface ABCIntroView : UIView

{
    UIImageView *backgroundImageView;
    UIView *view1;
    UIImageView *imageview1,*imageview2,*imageview3;
}

@property id<ABCIntroViewDelegate> delegate;




@end
