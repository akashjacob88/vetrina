//
//  CartVendorList.h
//  Vetrina
//
//  Created by Amit Garg on 5/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@interface CartVendorList : UIViewController
{
    Singleton *singletonn;
    IBOutlet UITableView *vendorListTable;
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableArray *finalArray,*dataArray,*newArray,*array,*vendorNameArray,*imageArr;
    NSInteger i;
    NSString *stringOne,*newString,*stringTwo,*statusStrr;
    NSMutableDictionary *dynamicDict;
    IBOutlet UIButton *editBtn;
    NSInteger index;
    IBOutlet UIView *view1;
    NSString *appear;
}
-(IBAction)Back:(id)sender;
@end
