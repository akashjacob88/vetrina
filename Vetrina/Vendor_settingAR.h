//
//  Vendor_settingAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/1/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface Vendor_settingAR : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,BSKeyboardControlsDelegate>

{
    Singleton *singling;
    
    BSKeyboardControls *keyboardControls;
    
    NSMutableArray *vendorNameArray,*dataArray,*aray_2;
    NSMutableArray *newArray,*subCatArry;
    NSMutableArray *array,*finalArray;
    
    NSMutableArray *imageArr,*img2;
    UIImageView *Img;
    NSArray *sectionAnimals;
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2,*stringTwo3;
    NSString *itme;
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    NSString *instaNameSave, *shopNmberSave, *shopCatSave, *emailSave ,*addBtnStr;

    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    
    UIImage *cimage,*cimage2;
    
    IBOutlet UITextField *phoneText;
    
    IBOutlet UIScrollView *scrolVIew;
    
    IBOutlet UITextField *vendorName;
    
    IBOutlet UIImageView *vendorImg;
    IBOutlet UITextView *ShowDec;
    IBOutlet UILabel *instaLbl;

    IBOutlet UIView *view1;
    IBOutlet UILabel *subcat1;
    IBOutlet UILabel *cat1;
    IBOutlet UIActivityIndicatorView *indicater;
}

@end
