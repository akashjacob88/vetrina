//
//  DeclarePriceAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "DeclarePriceAR.h"
#import "DeclarePriceCellAR.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "Base64.h"
#import "Vendor_ProductAR.h"
#import "ProgressHUD.h"
#import "ChatView.h"
#import "ProgressHUD.h"

#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "push.h"

@interface DeclarePriceAR ()
{
    NSMutableArray *users;
}

@end

@implementation DeclarePriceAR


- (void)viewDidLoad

{
    [super viewDidLoad];
    singletonn=[Singleton instance];
    [declarePriceTable reloadData];
    
    users = [[NSMutableArray alloc]init];
    
//    PFUser *user = [PFUser currentUser];
//    PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
//    [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
//    [query orderByAscending:PF_USER_FULLNAME];
//    [query setLimit:1000];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (error == nil)
//         {
//             [users removeAllObjects];
//             [users addObjectsFromArray:objects];
//             [declarePriceTable reloadData];
//         }
//         else [ProgressHUD showError:@"Network error."];
//     }];

    indicator.hidden=YES;
    [indicator stopAnimating];
    declareArray=[[NSMutableArray alloc]init];
    for(NSInteger i =0;i<singletonn.productArray.count;i++)
    {
        NSString *cartt=[[singletonn.productArray objectAtIndex:i]valueForKey:@"id"];
        NSString *unitprice=[[singletonn.productArray objectAtIndex:i]valueForKey:@"unitprice"];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:cartt forKey:@"cartid"];
        [dict setValue:unitprice forKey:@"unitprice"];
        [dict setValue:@"0" forKey:@"current"];
        
        [declareArray addObject:dict];
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [singletonn.productArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeclarePriceCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"PriceCell1"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"DeclarePriceCellAR" bundle:nil] forCellReuseIdentifier:@"PriceCell1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"PriceCell1"];
    }
    
    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
    v11.backgroundColor=[UIColor colorWithRed:85.0f/255 green:85.0f/255 blue:85.0f/255 alpha:1];
    [cell.contentView addSubview:v11];
    cell.backgroundColor=[UIColor whiteColor];
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(DeclarePriceCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.prodName.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"productname"];
//    NSString *price = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
//    
//    if ([price isEqualToString:@""])
//    {
//        cell.priceLbl.text=@"0.00";
//    }
//    else if ([price isEqualToString:nil])
//    {
//        cell.priceLbl.text=@"0.00";
//    }
//    else
//    {
//        cell.priceLbl.text=[NSString stringWithFormat:@"%@",price];
//    }
//    
   [cell.priceLbl setDelegate:self];
    NSArray *fields = @[cell.priceLbl];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
    cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[singletonn.productArray objectAtIndex:indexPath.row]objectForKey:@"prodimage"]]];
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    UIButton *statusBtn;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
            
        }
    }
    [statusBtn setTitle:@"أعلن التسعير" forState:UIControlStateNormal];
    [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    [statusBtn addTarget:self action:@selector(declarePriceBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
    [headerView addSubview:statusBtn];
    headerView.backgroundColor=[UIColor clearColor];
    return headerView;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 45;
}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
    currentValue=textField.text;
    [keyboardControls setActiveField:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
    unitPirceStr=textField.text;
    UITableViewCell *cell = (UITableViewCell *)textField.superview.superview;
    NSIndexPath *cellIndexPath = [declarePriceTable indexPathForCell:cell];
    cartIdStr=[[singletonn.productArray objectAtIndex:cellIndexPath.row]valueForKey:@"id"];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:cartIdStr forKey:@"cartid"];
    [dict setValue:unitPirceStr forKey:@"unitprice"];
    [dict setValue:currentValue forKey:@"current"];
    
    for(NSInteger i =0;i<declareArray.count;i++)
    {
        NSString *cartt=[[declareArray objectAtIndex:i]valueForKey:@"cartid"];
        
        if ([cartt isEqualToString:cartIdStr])
        {
            
            [declareArray removeObjectAtIndex:i];
            [declareArray addObject:dict];
            i=declareArray.count;
        }
    }
}


-(void)declareView1
{
    for(NSInteger i =0;i<declareArray.count;i++)
    {
        cartIdStr=[[declareArray objectAtIndex:i]valueForKey:@"cartid"];
        unitPirceStr=[[declareArray objectAtIndex:i]valueForKey:@"unitprice"];
        currentValue=[[declareArray objectAtIndex:i]valueForKey:@"current"];
        
        NSInteger p1=[unitPirceStr integerValue];
        
        if (p1==0)
        {
            NSLog(@"edit");
        }
        else if ([unitPirceStr isEqualToString:@""])
        {
            NSLog(@"not change");
        }
        else if ([unitPirceStr isEqualToString:nil])
        {
            NSLog(@"not change");
        }
        else
        {
            cartIdStr=[[declareArray objectAtIndex:i]valueForKey:@"cartid"];
            unitPirceStr=[[declareArray objectAtIndex:i]valueForKey:@"unitprice"];
            [self declarePricingView];
        }
    }
    
    if ([update isEqualToString:@"yes"])
    {
        [self prodListing];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    indicator.hidden=YES;
    [indicator stopAnimating];
}



-(void)declarePriceBtn:(UIButton *)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(declareView1) withObject:nil afterDelay:0.5f];
    
}


-(void)declarePricingView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartid.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant1=[[NSString alloc]initWithFormat:@""];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    float price = [unitPirceStr floatValue];
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%0.3f KD",price];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        update=@"yes";
        singletonn.usrObjctId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"userobjectid"];
      
        NSString *message = [ NSString stringWithFormat:@"%@ Declared price of %@",singletonn.userNameStr,prodQuant];
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:singletonn.usrObjctId successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];

//        for (NSInteger i=0; i< users.count; i++)
//        {
//            name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//            if ([name containsString:singletonn.usrObjctId])
//            {
//                PFUser *user1 = users[i];
//                PFUser *user2 = [PFUser currentUser];
//                NSString *groupId = StartPrivateChat(user1, user2);
//                NSString *text = @"Declared price";
//                // [self actionChat:groupId];
//                SendPushNotification(groupId, text);
//                
//                i = users.count;
//            }
//        }
//        
    }
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singletonn.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        Vendor_ProductAR *pro=[[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
        [self.navigationController pushViewController:pro animated:YES];
        
    }
    else
    {
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"productname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        finalArray=[[NSMutableArray alloc]init];
        singletonn.productArray=[[NSMutableArray alloc]init];
        
        for(NSInteger i =0;i<sortedArray.count;i++)
        {
            NSString *stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"userid"];
            if ([stringOne isEqualToString:singletonn.userid])
            {
                finalArray=[sortedArray objectAtIndex:i];
                [singletonn.productArray addObject:finalArray];
            }
            
        }
        
        if (singletonn.productArray.count==0)
        {
            Vendor_ProductAR *pro=[[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
            [self.navigationController pushViewController:pro animated:NO];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
