//
//  Vendor_InstapicsAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_InstapicsAR.h"
#import "JSON.h"
#import "Base64.h"
#import "AsyncImageView.h"
#import "EdititemsAR.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "ProgressHUD.h"


@interface Vendor_InstapicsAR ()

@end

@implementation Vendor_InstapicsAR

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singlogin = [Singleton instance];
    vendrNme.text= singlogin.shopNameVendr;
    
    [super viewDidLoad];
    
    accesToken= singlogin.accesstokenNew;

    totalArray=[[NSMutableArray alloc]init];
    [self instaPicsFetch];
    [self CountImg];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)instaPicsFetch
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",singlogin.vIGUserId);
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,singlogin.vIGUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        singlogin.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        //NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        //NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        // NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            dynamicDict=[[NSMutableDictionary alloc]init];
            dynamicDict = @{@"url":[righturl objectAtIndex:i]};
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
            
        }
        
    }
    
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singlogin.vImgUrl2 = newdatalist;
    
    
}


-(void) CountImg

{
    totalArray = singlogin.vImgUrl2;
    venImageArrayList = [[NSMutableArray alloc]init];
    for(int i =0;i<totalArray.count;i++)
    {
        image =[[UIImageView alloc]init];
        UIGraphicsBeginImageContext(image.image.size);
        {
            CGContextRef ctx = UIGraphicsGetCurrentContext();
            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, image.image.size.height));
            CGContextConcatCTM(ctx, trnsfrm);
            CGContextBeginPath(ctx);
            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, image.image.size.width, image.image.size.height));
            CGContextClip(ctx);
            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, image.image.size.width, image.image.size.height), image.image.CGImage);
            image.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:image.imageURL];
        image.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[totalArray objectAtIndex:i] valueForKey:@"url"]]];
        [venImageArrayList addObject:image];
    }
}


#pragma mark -  TableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    return totalArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
    }
    for(UIView *v in cell.contentView.subviews)
    {
        [v removeFromSuperview];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            image.frame = CGRectMake(0, 0, 305, 305);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            image.frame = CGRectMake(0, 0, 305, 305);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            image.frame = CGRectMake(0, 0, 357, 357);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            image.frame = CGRectMake(0, 0, 395, 395);
        }
        
        else{
            image.frame = CGRectMake(0, 0, 300, 200);
        }
    }
    else
    {
        image.frame = CGRectMake(0, 0, 300, 300);
    }
    image = [venImageArrayList objectAtIndex:indexPath.row];
    [cell.contentView addSubview:image];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            return 313.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            return 313.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            return 365.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            return 403.0f;
        }
        
        else{
            return 160.0f;
        }
    }
    else
    {
        return 455.0f;
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    
    UIView *footerView = [[UIView alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 50)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 50)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        
    }
    
    
    [confrmBtn setTitle:@"LOAD MORE" forState:UIControlStateNormal];
    confrmBtn.backgroundColor = [UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1];
    [confrmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    if (singlogin.pedagori .length > 0)
    {
        [footerView addSubview:confrmBtn];
    }
    else
    {
        
    }

    return footerView;
    
}


-(void)confirm:(UIButton *)sender

{
    [self performSelector:@selector(load) withObject:Nil afterDelay:0.3f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    
}


-(void)load

{
    
    NSURL * url=[NSURL URLWithString:singlogin.pedagori];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    //  NSLog(@"GetData--%@",data);
    
    NSDictionary *value1=[data JSONValue];
    NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
    NSLog(@"dadadad %@",pedagori);
    NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
    NSLog(@"dadadad %@",pedagori1);
    singlogin.pedagori = pedagori1;
    
    NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
    NSLog(@"dadadad %@",userDict1);
    //NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
    
    //NSMutableArray *namef = [usef valueForKeyPath:@"username"];
    
    NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
    // NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
    NSLog(@"image links %@",imgId);
    NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
    NSLog(@"image links %@",img);
    NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
    NSLog(@"hmmm %@",urlvalue);
    NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
    NSLog(@"yooooo %@",righturl);
    newdatalist = [[NSMutableArray alloc]init];
    for (int i=0; i< righturl.count; i++)
    {
        dynamicDict=[[NSMutableDictionary alloc]init];
        dynamicDict = @{@"url":[righturl objectAtIndex:i]};
        
        NSLog(@"Dynamic ---- %@",dynamicDict);
        
        [newdatalist addObject:dynamicDict];
        
    }
    
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singlogin.vImgUrl2= newdatalist;
    [self CountImg];
    [instaImgTble reloadData];
    [instaImgTble setContentOffset:CGPointZero animated:YES];
    [ProgressHUD showSuccess:@""];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat ff;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            ff= 48.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            ff=  48.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            ff=  53.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            ff=  53.0f;
        }
    }
    
    
    return  ff;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    singlogin.selectedPic = [[totalArray objectAtIndex:indexPath.row]valueForKey:@"url"];
    
    if ([singlogin.selectInstaPic1 isEqualToString: @"2"])
    {
        singlogin.selectedPic = [[totalArray objectAtIndex:indexPath.row]valueForKey:@"url"];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(home) withObject:nil afterDelay:2.0f];

        
    }
    
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        singlogin.instagrmStr = @"1";
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    
    
    
}



-(void)home
{
    
    EdititemsAR *item = [[EdititemsAR alloc]initWithNibName:@"EdititemsAR" bundle:nil];
    [self.navigationController pushViewController:item animated:NO];
    [ProgressHUD showSuccess:@""];
    
}






- (IBAction)back:(id)sender

{
    singlogin.instagrmStr = @"";
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
