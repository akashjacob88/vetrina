//
//  CartProductList.m
//  Vetrina
//
//  Created by Amit Garg on 5/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CartProductList.h"
#import "CartProductCell.h"
#import "AsyncImageView.h"
#import "ShowOrder.h"
#import "JSON.h"
#import "AddressCell.h"
#import "CartVendorCell.h"
#import "New_address_cell.h"
#import "ProgressHUD.h"
#import "ShowOrderCell.h"
#import "RecentView.h"

#import "ChatView.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "push.h"
#import "ViewController.h"
#import "Favorites.h"
#import "User_Settings.h"
#import "UserProfile.h"
#import "Cell1.h"
#import "Cell2.h"
#import <Parse/Parse.h>
#import "AppConstant.h"
#import <Quickblox/Quickblox.h>
#import "chat_New.h"
#import "Vendor_Detaillist.h"
@interface CartProductList ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *users,*chatArr;
    IBOutlet UIImageView *profilePicuser;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UIView *countryView;
    NSMutableArray *recents;
    NSString *addre;
    
    NSArray *dialog_Objects;
    QBUUser *currentUser;
    NSMutableArray *users_array;
    QBChatDialog *chatDialog;
    
    
    
    
}

@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex2;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;

@end

@implementation CartProductList
@synthesize isOpen,selectIndex2;


- (void)dealloc
{
    // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex2 = nil;
    
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self)
    {
        
    }
    return self;
}




- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }
    
    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singletonn.userid == nil)
    {
        count_lbl.hidden=YES;
        count_lbl.hidden = YES;
    }
    else
    {
        if ([count_lbl.text isEqualToString:@"0"])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else if ([count_lbl.text isEqualToString:@""])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else
        {
            count_lbl.hidden=NO;
            count_lbl.hidden = NO;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singletonn.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}



- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users21) {
        
        
        [users_array addObjectsFromArray:users21];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
        
        [seen addObject:[NSString stringWithFormat:@"%ld",o]];
        NSUInteger y = 0;
        
        while (y < [users_array count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[users_array objectAtIndex:y];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [users_array removeObjectAtIndex:y];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                y++;
            }
        }
        
        
        
        
        [self newChat];
        
    } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}

-(void)chat
{
    // myei4MTHya
    
    currentUser = [QBUUser user];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
    //    NSInteger i = [ [ NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"] ] integerValue ];
    
    
    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            [self retrieveAllUsersFromPage:1];
            
        }else{
            [ProgressHUD showSuccess:@"try again"];
        }
    }
     
     
     
     ];
    
    
}


-(void)newChat{
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
    
    for (int x=0; x<users_array.count; x++) {
        
        
        QBUUser *user1   =[QBUUser user];
        
        user1 =[users_array objectAtIndex:x];
        
        
        if (user1.ID==o) {
            [self chat22];
            break;
            
        }
        
        
        
    }
    
    
    [ProgressHUD showSuccess:@"Cannot Connect To Vendor, try again"];

}

-(void)chat22{
    
    
    
    
    
    
    QBChatDialog *test_dialog = nil;
    
    
    NSString *str=[[NSString alloc]init];
    str =@"new";
    
    
    for (int y=0; y<dialog_Objects.count; y++) {
        
        
        test_dialog=[dialog_Objects objectAtIndex:y];
        
        
        NSLog(@"%@",test_dialog.occupantIDs);
        
        NSArray *objs=[[NSArray alloc]initWithArray:test_dialog.occupantIDs];
        
        
        
        
        
        
        for (int x =0; x<1; x++) {
            NSInteger c =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:0]] integerValue ];
            NSInteger d =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:1]] integerValue ];
            NSInteger a = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
            NSInteger b = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
            
            
            if (c==a ) {
                
                if (d ==b) {
                    str=@"got";
                    break;
                }
                
            }else if (c==b) {
                
                if (d==a) {
                    str=@"got";
                    
                    break;
                }
                
                
            }
            
        }
        
        if ([str isEqualToString:@"got"]) {
            break;
        }
        
    }
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    
    // singletonn.chat_user=[[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
    
    chatDialog.occupantIDs = @[@(o) ];
    
    
    
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        
        [self actionChat:createdDialog.ID];
        
        
        
        
    } errorBlock:^(QBResponse *response) {
        
        [ProgressHUD showSuccess:@""];
        
    }];
    
    //    chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
    //
    //    chatDialog.name = @"Chat with Bob, Sam, Garry";
    //
    //    chatDialog.occupantIDs = @[@(9045183), @(9045219)]; // change id with your register user's id
    //
    //    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
    //
    //
    //          NSLog(@"No Error: %@", response);
    //        [self chat1];
    //
    //
    //    } errorBlock:^(QBResponse *response) {
    //
    //          NSLog(@"Error: %@", response);
    //
    //    }];
    
    
    
}




-(void)handleRefresh{
    
    NSLog(@"Refreshed");
    // NSLog(@"%ld",(long)pull_Count);
    UIActivityIndicatorView  *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, productListTable.frame.size.height + 150);
    [self.view addSubview: activityIndicator];
    activityIndicator.color = [UIColor blackColor];
    [activityIndicator startAnimating];
    [self prodListing];
    [refreshControl endRefreshing];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    singletonn=[Singleton instance];
    countryView.hidden = YES;
    count_lbl.hidden=YES;
    recents=[[NSMutableArray alloc]init];
    [self loadRecents];
    refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [productListTable addSubview:refreshControl];
    backBtnOut.hidden = NO;
    backBtnOut.layer.masksToBounds = YES;
    backBtnOut.layer.borderWidth = 1.0f;
    backBtnOut.layer.cornerRadius = backBtnOut.frame.size.width/2;
    backBtnOut.layer.borderColor = [[UIColor clearColor]CGColor];
    backBtnOut.clipsToBounds = YES;
    backBtnOut.layer.opaque = NO;
    
    
    users_array=[[NSMutableArray alloc]init];
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    NSURL *url1=[NSURL URLWithString:singletonn.userproilePic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePicuser.image=emppppic;
    if (emppppic==nil) {
        
        profilePicuser.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        
        
    }
    profilePicuser.layer.masksToBounds = YES;
    profilePicuser.layer.cornerRadius = profilePicuser.frame.size.width/2;
    profilePicuser.layer.opaque = NO;
    
    
    NSArray *fields = @[cityTxt,addressTxt];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    vendorNameLbl.text=singletonn.vendorName;
    showOrderLbl.hidden=YES;
    users = [[NSMutableArray alloc]init];
    if ([singletonn.loginStatus isEqualToString:@"User Login"] || [singletonn.loginStatus isEqualToString:@"IG Login"])
    {
        
        
        
        QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
        
        [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
            
            NSLog(@"%@",dialogObjects);
            dialog_Objects=[[NSArray alloc]initWithArray:dialogObjects];
            
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@",response);
            
            
        }];
        
        
        
        
        //
        //        NSUInteger limit = 1000;
        //        __block NSUInteger skip = 0;
        //
        //        [ProgressHUD show:@"Loading..." Interaction:NO];
        //
        //        [indicator startAnimating];
        //
        //
        //        PFUser *user = [PFUser currentUser];
        //        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
        //        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
        //        [query orderByAscending:PF_USER_FULLNAME];
        //        //  [query setLimit:1000];
        //        [query setLimit: limit];
        //        [query setSkip: skip];
        //
        //
        //        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
        //         {
        //             if (error == nil)
        //             {
        //                 [users addObjectsFromArray:objects];
        //                 NSLog(@"%lu  111  ",(unsigned long)objects.count);
        //                 skip += limit;
        //               [query setSkip: skip];
        //
        //                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                     if (error == nil){
        //                     [users addObjectsFromArray:objects];
        //                         skip += limit;
        //                         [query setSkip: skip];
        //
        //                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                             if (error == nil){
        //                                 [users addObjectsFromArray:objects];
        //                                 skip += limit;
        //                                 [query setSkip: skip];
        //
        //                                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                                     if (error == nil){
        //                                         [users addObjectsFromArray:objects];
        //                                         skip += limit;
        //                                         [query setSkip: skip];
        //
        //                                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                                             if (error == nil){
        //
        //                                                 [users addObjectsFromArray:objects];
        //                                                 skip += limit;
        //                                                 [query setSkip: skip];
        //                                                 [indicator stopAnimating];
        //                                                 [ProgressHUD showSuccess:@""];
        //
        //
        //                                             }
        //
        //
        //                                         }];
        //
        //
        //                                     }
        //
        //
        //                                 }];
        //
        //
        //                             }
        //
        //
        //                         }];
        //
        //
        //                     }
        //
        //
        //                 }];
        //
        //                 NSLog(@"%lu  444",(unsigned long)objects.count);
        //
        //             }
        //
        //             //                 [users removeAllObjects];
        //             //                 [users addObjectsFromArray:objects];
        //             // [productListTable reloadData];
        //
        //             else [ProgressHUD showError:@"Network error."];
        //         }];
    }
    
    self.expansionTableView.layer.shadowRadius = 8.0f;
    self.expansionTableView.layer.shadowOffset = CGSizeMake(0, 3);
    self.expansionTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.expansionTableView.layer.shadowOpacity = 0.8f;
    
    
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    
    [self prodListing];
    
    indicator.hidden=YES;
    [indicator stopAnimating];
    [sendMessageTxt setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    replyView.hidden=YES;
    confirmView.hidden=YES;
    orderStr=@"no";
    [self addressListing];
    addressView.hidden=YES;
    newAddressView.hidden=YES;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, countryTxt.frame.size.height)];
    countryTxt.leftView = leftView1;
    countryTxt.leftViewMode = UITextFieldViewModeAlways;
    [countryTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, addressTxt.frame.size.height)];
    addressTxt.leftView = leftView2;
    addressTxt.leftViewMode = UITextFieldViewModeAlways;
    [addressTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, cityTxt.frame.size.height)];
    cityTxt.leftView = leftView3;
    cityTxt.leftViewMode = UITextFieldViewModeAlways;
    [cityTxt setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    [self catlist];
    
}

-(void)getdata{
    
    //
    //    PFQuery *query = [PFQuery queryWithClassName:@"myClassName"];
    //
    //    [ParseProxy findAllObjectsWithQuery:query withBlock:^(NSArray *objects, NSError *error) {
    //        if(!error)
    //        {
    //            NSLog(@"Loaded All Objects: %@",objects);
    //        }
    //    }];
    
}


//+ (void)findAllObjectsWithQuery:(PFQuery *)query withBlock:(void (^)(NSArray *objects, NSError *error))block
//{
//    __block NSMutableArray *allObjects = [NSMutableArray array];
//    __block NSUInteger limit = 1000;
//    __block NSUInteger skip = 0;
//
//    typedef void  (^FetchNextPage)(void);
//    FetchNextPage __weak __block weakPointer;
//
//    FetchNextPage strongBlock = ^(void)
//    {
//        [query setLimit: limit];
//        [query setSkip: skip];
//
//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//            if (!error)
//            {
//                // The find succeeded. Add the returned objects to allObjects
//                [allObjects addObjectsFromArray:objects];
//
//                if (objects.count == limit) {
//                    // There might be more objects in the table. Update the skip value and execute the query again.
//                    skip += limit;
//                    [query setSkip: skip];
//                    // Go get more results
//                    weakPointer();
//                }
//                else
//                {
//                    // We are done so return the objects
//                    block(allObjects, nil);
//                }
//
//            }
//            else
//            {
//                block(nil,error);
//            }
//        }];
//    };
//
//    weakPointer = strongBlock;
//    strongBlock();
//
//}
- (IBAction)chatBtn:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(chatrt) withObject:nil afterDelay:0.5f];
}

-(void)chatrt

{
    RecentView *View = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:View animated:NO];
    [ProgressHUD showSuccess:@""];
}



-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        // [dataArray retain];
    }
    if([dataArray count]==0)
    {
        [productListTable reloadData];
        editBtn.hidden=YES;
        chatBtn.hidden= YES;
        noItmeView.hidden = NO;
    }
    else
    {
        
        NSLog(@"GetDatadictt--%@",dataArray);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"orderdate"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]initWithKey:@"ordertime"  ascending:YES];
        NSArray *sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
        NSArray *sortedArray2 = [dataArray sortedArrayUsingDescriptors:sortDescriptors1];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray2 mutableCopy];
        
        
        
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"vendorname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        vendorNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"vendorname"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"vendorpic"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [vendorNameArray addObject:vendorName1];
                [imageArr addObject:imagestr];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName1];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:vendorName1];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"vendorname"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:vendorName1];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        noItmeView.hidden = YES;
        [productListTable reloadData];
    }
    
    NSInteger value=0;
    for( i =0;i<dataArray.count;i++)
    {
        NSString *status=[[dataArray objectAtIndex:i]valueForKey:@"statusid"];
        if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] )
        {
            value=value+1;
        }
        
    }
    singletonn.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    [self cartTotalView];
}



-(void)cartTotalView
{
    if (singletonn.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singletonn.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singletonn.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singletonn.totalCart;
        }
    }
}





//    NSInteger j = dataArray.count;
//    Singleton.totalCart = [NSString stringWithFormat:@"%ld",(long)j];
//}





-(IBAction)Back:(id)sender
{
    //    User_Settings *alloc = [[User_Settings alloc]initWithNibName:@"User_Settings" bundle:nil];
    //    [self.navigationController pushViewController:alloc animated:NO];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(pofie) withObject:nil afterDelay:0.5f];
}

-(void)pofie
{
    UserProfile *view = [[UserProfile alloc]initWithNibName:@"UserProfile" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(void)addressListing

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/addresslistuser.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        addressArray=[[NSMutableArray alloc]init];
    }
    else
    {
        addressArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            return [finalArray12 count];
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return [finalArray count];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            //            return [countyArra count];
            //        }
            //        else if ([btnStr isEqualToString:@"2"])
            //        {
            //            return [arra_1 count];
            //        }
            
            if (self.isOpen)
            {
                if (self.selectIndex2.section == section)
                {
                    NSString *sectionTitle = [vendorNameArray12 objectAtIndex:section];
                    sectionAnimals = [[finalArray12 objectAtIndex:section]valueForKey:sectionTitle];
                    return [sectionAnimals count]+1;
                }
            }
            return 1;
            
        }
        else
        {
            return  [addressArray count];
        }
    }
    else
    {
        NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
        NSArray *sectionAnimals1 = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
        NSString *btnstr1 = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"statusid"];
        if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
        {
            return 1+1;
        }
        else
        {
            return [sectionAnimals1 count]+1;
        }
    }
    //return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([addAdress isEqualToString:@"1"])
        {
            if (self.isOpen&&self.selectIndex2.section == indexPath.section&&indexPath.row!=0)
            {
                static NSString *CellIdentifier = @"Cell2";
                Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                
                if (!cell)
                {
                    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
                }
                
                NSString *sectionTitle = [vendorNameArray12 objectAtIndex:indexPath.section];
                sectionAnimals = [[finalArray12 objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"city"];
                return cell;
            }
            else
            {
                static NSString *CellIdentifier = @"Cell1";
                Cell1 *cell = (Cell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell)
                {
                    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
                }
                NSString *sectionTitle = [vendorNameArray12 objectAtIndex:indexPath.section];
                cell.titleLabel.text = sectionTitle;
                UIImage *image = [UIImage imageNamed:@"locationWhite.png"];
                cell.iconImg.image = image;
                [cell changeArrowWithUp:([self.selectIndex2 isEqual:indexPath]?YES:NO)];
                return cell;
            }
            
            //            New_address_cell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
            //
            //            if (!cell)
            //            {
            //                [tableView registerNib:[UINib nibWithNibName:@"New_address_cell" bundle:nil] forCellReuseIdentifier:@"cell_1"];
            //                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
            //            }
            //            if ([btnStr isEqualToString:@"1"])
            //            {
            //                cell.lable1.text = [[countyArra objectAtIndex:indexPath.row]valueForKey:@"country"];
            //            }
            //            else if ([btnStr isEqualToString:@"2"])
            //            {
            //                cell.lable1.text = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"city"];
            //            }
            //            return cell;
        }
        else
        {
            AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Address"];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"AddressCell" bundle:nil] forCellReuseIdentifier:@"Address"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"Address"];
            }
            
            NSString *addressStrr = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
            [cell.addressBtn setTitle:addressStrr forState:UIControlStateNormal];
            [cell.addressBtn addTarget:self action:@selector(addressClick:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    }
    
    else
    {
        if (indexPath.row==0)
        {
            CartVendorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
            
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"CartVendorCell" bundle:nil] forCellReuseIdentifier:@"CartCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
            }
            
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            cell.vendorName.text = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorname"];
            cell.totalLbl.text=[NSString stringWithFormat:@"%li",(unsigned long)sectionAnimals.count];
            cell.vendorImage.layer.cornerRadius=cell.vendorImage.frame.size.height/2;
            cell.vendorImage.clipsToBounds=YES;
            cell.totalLbl.layer.cornerRadius=cell.totalLbl.frame.size.height/2;
            cell.totalLbl.clipsToBounds=YES;
            cell.totalLbl.layer.borderWidth = 2.0;
            cell.totalLbl.layer.borderColor = [UIColor whiteColor].CGColor;
            
            [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.vendorImage.imageURL];
            cell.vendorImage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
            
            
            
            for (i=0; i< sectionAnimals1.count;i++)
            {
                untiteld= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"productname"];
                NSString *statusStr1= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"statusid"];
                if ([untiteld isEqualToString:@"Untitled"])
                {
                    cell.chatIcon.hidden = YES;
                    cell.totalLbl.hidden = YES;
                    i=sectionAnimals1.count;
                    
                }
                if ([statusStr1 isEqualToString:@"0"])
                {
                    cell.chatIcon.hidden = NO;
                    cell.totalLbl.hidden = YES;
                    i=sectionAnimals1.count;
                }
            }
            
            
            for (i=0; i< sectionAnimals1.count; i++)
            {
                NSString *statusStr1= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"statusid"];
                NSString *unitPriceStr= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"unitprice"];
                NSString *askStr= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"negotiable_price"];
                
                if ([statusStr1 isEqualToString:@""])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:@""])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:@"0"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                }
                if ([statusStr1 isEqualToString:@"2"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Ordered.png"];
                }
                if ([statusStr1 isEqualToString:@"3"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Confirmed.png"];
                }
                if ([statusStr1 isEqualToString:@"4"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Delivered.png"];
                }
                if ([statusStr1 isEqualToString:@"1"])
                {
                    cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                    i=sectionAnimals1.count;
                }
                else
                {
                    if ([unitPriceStr isEqualToString:@""])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals1.count;
                    }
                    if (unitPriceStr == nil)
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals1.count;
                    }
                    if ([askStr isEqualToString:@"Ask for Price"])
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals1.count;
                    }
                    if (askStr == nil)
                    {
                        cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
                        i=sectionAnimals1.count;
                    }
                }
                cell.backgroundColor=[UIColor whiteColor];
                UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 54, 414, 1)];
                v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
                [cell.contentView addSubview:v11];
                return cell;
            }
        }
        else
        {
            NSString *sectionTitle1 = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle1];
            NSString *btnstr1 = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"statusid"];
            if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
            {
                ShowOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
                if (!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"ShowOrderCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
                }
                
                return cell;
            }
            else
            {
                
                CartProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
                if (!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"CartProductCell" bundle:nil] forCellReuseIdentifier:@"ProductCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
                }
                
                NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                
                //cell.prodName.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"productname"];
                untiteld = [[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"productname"];
                cell.prodName.text = untiteld;
                NSString *askprice = [[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"negotiable_price"];
                NSString *price = [[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"unitprice"];
                
                if ([askprice isEqualToString:@"Ask for Price"])
                {
                    cell.priceLbl.text=@"Ask for Price";
                }
                else if (askprice == nil)
                {
                    cell.priceLbl.text=@"Ask for Price";
                }
                else
                {
                    //                float val=[price integerValue];
                    //                price=[NSString stringWithFormat:@"%0.3f KD",val];
                    cell.priceLbl.text=price;
                }
                
                cell.qtyLbl.text = [[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"quantityprod"];
                NSString *status = [[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"statusid"];
                
                if ([status isEqualToString:@"2"]||[status isEqualToString:@"3"]||[status isEqualToString:@"4"] )
                {
                    cell.qtyLbl.userInteractionEnabled=NO;
                }
                else
                {
                    cell.qtyLbl.userInteractionEnabled=YES;
                }
                
                [cell.qtyLbl setDelegate:self];
                NSArray *fields = @[cell.qtyLbl];
                keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
                [keyboardControls setDelegate:self];
                
                
                [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
                cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sectionAnimals1 objectAtIndex:indexPath.row-1]objectForKey:@"prodimage"]]];
                UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
                v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
                [cell.contentView addSubview:v11];
                cell.backgroundColor=[UIColor whiteColor];
                return cell;
            }
        }
        return nil;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([addAdress isEqualToString:@"1"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            //            countryTxt.text = [[countyArra objectAtIndex:indexPath.row]valueForKey:@"country"];
            //            singletonn.uId = [[countyArra objectAtIndex:indexPath.row]valueForKey:@"id"];
            //            countyTbl.hidden = YES;
            
            if (indexPath.row == 0)
            {
                if ([indexPath isEqual:self.selectIndex2])
                {
                    self.isOpen = NO;
                    [self didSelectCellRowFirstDo:NO nextDo:NO];
                    self.selectIndex2 = nil;
                    
                }
                else
                {
                    if (!self.selectIndex2)
                    {
                        self.selectIndex2 = indexPath;
                        [self didSelectCellRowFirstDo:YES nextDo:NO];
                        
                    }
                    else
                    {
                        
                        [self didSelectCellRowFirstDo:NO nextDo:YES];
                    }
                }
                
            }
            else
            {
                NSString *sectionTitle = [vendorNameArray12 objectAtIndex:indexPath.section];
                sectionAnimals = [[finalArray12 objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                itme = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"countryid"];
                singletonn.cityAddr = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"city"];
                singletonn.countryAddres = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"countryname"];
                NSString *name1 = [NSString stringWithFormat:@"%@, %@",singletonn.cityAddr,singletonn.countryAddres];
                countryTxt.text = name1;
                countryView.hidden = YES;
            }
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        //        else if ([btnStr isEqualToString:@"2"])
        //        {
        //            cityTxt.text = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"city"];
        //            cityTbl.hidden = YES;
        //        }
    }
    else
    {
        
        if (indexPath.row == 0)
        {
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            for (i=0; i< sectionAnimals1.count;i++)
            {
                untiteld= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"productname"];
                statusStr0 = [[sectionAnimals1 objectAtIndex:i]valueForKey:@"statusid"];
                if ([untiteld isEqualToString:@"Untitled"])
                {
                    i = sectionAnimals1.count;
                }
                else
                {
                    untiteld = @"";
                }
            }
            if ([untiteld isEqualToString:@"Untitled"])
            {
                
            }
            if ([statusStr0 isEqualToString:@"0"])
            {
                
                
                singletonn.venName = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorname"];
                singletonn.objectIdStr = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorobjectid"];
                singletonn.SlctVenId = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorid"];
                singletonn.vendorNameList = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorname"];
                [self performSelector:@selector(Objectuser) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
                
                
            }
            else
            {
                singletonn.SlctVenId = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorid"];
                singletonn.vendorNameList = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorname"];
                singletonn.objectIdStr = [[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
                
                // [self performSelector:@selector(Objectuser) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
                
                [indicator startAnimating];
                
                
                singletonn.objectIdStr = [[sectionAnimals1 objectAtIndex:indexPath.row]valueForKey:@"vendorobjectid"];
                
                NSString *str=[[NSString alloc]init];
                str =@"no user";
                singletonn.chat_user = [[sectionAnimals1 objectAtIndex:indexPath.row]valueForKey:@"vendorobjectid"];
                singletonn.namechat=singletonn.vendorNameList;
                
                
                [self chat];
                
                
                
                //                for ( i=0; i< users.count; i++)
                //                {
                //
                //
                //                    //  NSLog(@"%@",singletonn.objectIdStr);
                //                    name= [[users objectAtIndex:i]valueForKey:@"objectId"];
                //                    if ([name containsString:singletonn.objectIdStr])
                //                    {
                //                        str=@"user found";
                //                        PFUser *user1 = [PFUser currentUser];
                //                        PFUser *user2 = users[i];
                //                        NSString *groupId = StartPrivateChat(user1, user2);
                //                        [self actionChat:groupId];
                //                        i = users.count;
                //                    }
                //                }
                
                //                if ([str isEqualToString:@"no user"]) {
                //
                //                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"No User Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                //                    [alert show];
                //                    [ProgressHUD showSuccess:@""];
                //
                //                }
                
            }
        }
        else
        {
            singletonn.productArray = [[NSMutableArray alloc]init];
            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
            singletonn.productArray = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
            NSString *btnstr1 = [[singletonn.productArray objectAtIndex:0]valueForKey:@"statusid"];
            singletonn.orderId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"orderid"];
            if ([btnstr1 isEqualToString:@"3"]|| [btnstr1 isEqualToString:@"4"])
            {
                [self performSelector:@selector(showOrder) withObject:Nil afterDelay:2.0f];
                [ProgressHUD show:@"Loading..." Interaction:NO];
            }
        }
    }
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex2];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex2.section;
    
    NSString *sectionTitle = [vendorNameArray12 objectAtIndex:section];
    sectionAnimals = [[finalArray12 objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {
        [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex2 = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    else
    {
        if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}





-(void)showOrder
{
    ShowOrder *show1=[[ShowOrder alloc]initWithNibName:@"ShowOrder" bundle:nil];
    [self.navigationController pushViewController:show1 animated:NO];
    [ProgressHUD showSuccess:@""];
    
}


- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}



-(void)Objectuser
{
    
    Vendor_Detaillist *chatView = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:nil];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    
    
    
    //    singletonn.objectIdStr = [[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
    //    for ( i=0; i< users.count; i++)
    //    {
    //
    //
    //      //  NSLog(@"%@",singletonn.objectIdStr);
    //        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
    //        if ([name containsString:singletonn.objectIdStr])
    //        {
    //            PFUser *user1 = [PFUser currentUser];
    //            PFUser *user2 = users[i];
    //            NSString *groupId = StartPrivateChat(user1, user2);
    //            [self actionChat:groupId];
    //            i = users.count;
    //        }
    //    }
}


- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
    
    //    ChatView *chatView = [[ChatView alloc] initWith:groupId];
    //    chatView.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:chatView animated:YES];
    //    [ProgressHUD showSuccess:@""];
}



-(void)addressClick:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *cellIndexPath = [addressTable indexPathForCell:cell];
    
    addressStr=[[NSString alloc]init];
    
    addressStr=[NSString stringWithFormat:@"%@, %@,%@ ,Phone no. %@",[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"address"],[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"city"],[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"country"],[[addressArray objectAtIndex:cellIndexPath.row]valueForKey:@"phoneno"]];
    
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    
    addre=[[NSString alloc]initWithString:addressStr];
    
    
    [self performSelector:@selector(orderCartView_english) withObject:nil afterDelay:10.5f];
}


- (NSInteger)numberOfSections

{
    return 1;
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([orderStr isEqualToString:@"yes"])
    {
        if ([btnStr isEqualToString:@"1"])
        {
            return 50;
            
        }
        else
        {
            return 50;
        }
        
    }
    else{
        if (indexPath.row==0)
        {
            return 55;
            
        }
        else
        {
            return 50;
            
        }
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    
    if ([orderStr isEqualToString:@"yes"]) {
        
        UIButton *newAddress;
        UIView *linee;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(99, 35, 106, 1)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(99, 35, 106, 1)];
                
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(127, 35, 105, 1)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                newAddress=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
                linee=[[UIView alloc]initWithFrame:CGRectMake(147, 35, 105, 1)];
            }
        }
        newAddress.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:17];
        [newAddress setTitle:@"New address" forState:UIControlStateNormal];
        [newAddress setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [newAddress addTarget:self action:@selector(newAddressBtn:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:newAddress];
        linee.backgroundColor=[UIColor whiteColor];
        [headerView addSubview:linee];
    }
    else
    {
        UIButton *statusBtn = nil;
        UIView *view = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                view=[[UIView alloc]initWithFrame:CGRectMake(0, 20, 304, 25)];
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                view=[[UIView alloc]initWithFrame:CGRectMake(0, 20, 304, 25)];
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                view=[[UIView alloc]initWithFrame:CGRectMake(0, 20, 359, 25)];
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                view=[[UIView alloc]initWithFrame:CGRectMake(0, 20, 398, 25)];
                statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
            }
        }
        
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
        NSArray *sectionAnimals1 = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
        
        
        for ( i=0; i< sectionAnimals1.count; i++)
        {
            statusStr= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"statusid"];
            
            // NSString *unitPriceStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"unitprice"];
            NSString *askPriceStr= [[sectionAnimals1 objectAtIndex:i]valueForKey:@"negotiable_price"];
            
            if ([statusStr isEqualToString:@""])
            {
                statusStr=@"ORDER";
            }
            if (statusStr == nil)
            {
                statusStr=@"ORDER";
            }
            if ([statusStr isEqualToString:@"0"])
            {
                statusStr=@"ORDER";
            }
            if ([statusStr isEqualToString:@"2"])
            {
                statusStr=@"WAITING FOR CONFIRMATION";
            }
            if ([statusStr isEqualToString:@"3"])
            {
                statusStr=@"WAITING FOR DELIVERY";
                //      [editBtn setTitle:@"SHOW ORDER" forState:UIControlStateNormal];
                //     showOrderLbl.hidden=NO;
            }
            if ([statusStr isEqualToString:@"4"])
            {
                statusStr=@"SENT FOR DELIVERY";
                //      [editBtn setTitle:@"SHOW ORDER" forState:UIControlStateNormal];
                //      showOrderLbl.hidden=NO;
                i=sectionAnimals1.count;
            }
            if ([statusStr isEqualToString:@"1"])
            {
                statusStr=@"WAITING FOR PRICE";
                i=sectionAnimals1.count;
            }
            else
            {
                //                if ([unitPriceStr isEqualToString:@""])
                //                {
                //                    statusStr=@"ASK FOR PRICE";
                //                    i=sectionAnimals.count;
                //                }
                //                if ([unitPriceStr isEqualToString:nil])
                //                {
                //                    statusStr=@"ASK FOR PRICE";
                //                    i=sectionAnimals.count;
                //                }
                if ([askPriceStr isEqualToString:@"Ask for Price"])
                {
                    statusStr=@"ASK FOR PRICE";
                    i=sectionAnimals1.count;
                }
                if (askPriceStr == nil)
                {
                    statusStr=@"ASK FOR PRICE";
                    i=sectionAnimals1.count;
                }
                
            }
        }
        
        
        
        statusBtn.tag=section;
        [statusBtn setTitle:statusStr forState:UIControlStateNormal];
        [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        if ([statusStr isEqualToString:@"ORDER"]||[statusStr isEqualToString:@"ASK FOR PRICE"])
        {
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
            [headerView addSubview:view];
            [view setAlpha:0.8];
            view.backgroundColor = [UIColor whiteColor];
            view.layer.shadowColor = [UIColor blackColor].CGColor;
            view.layer.masksToBounds = NO;
            view.layer.cornerRadius = 2; // redius corners
            view.layer.shadowOffset = CGSizeMake(-2.0f, .2f);
            view.layer.shadowRadius = 3;
            UIBezierPath *path = [UIBezierPath bezierPathWithRect:view.bounds];
            view.layer.shadowPath = path.CGPath;
            view.layer.shadowOpacity = 0.8;
        }
        else
        {
            [statusBtn setBackgroundColor:[UIColor clearColor]];
            statusBtn.layer.borderColor=[UIColor whiteColor].CGColor;
            statusBtn.layer.borderWidth=1.5f;
            statusBtn.clipsToBounds=YES;
        }
        
        if ([statusStr isEqualToString:@"ORDER"])
        {
            [statusBtn addTarget:self action:@selector(orderBtn:) forControlEvents:UIControlEventTouchUpInside];
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
        }
        if ([statusStr isEqualToString:@"ASK FOR PRICE"])
        {
            [statusBtn addTarget:self action:@selector(AskBtn:) forControlEvents:UIControlEventTouchUpInside];
            [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
        }
        if ([statusStr isEqualToString:@"WAITING FOR DELIVERY"] || [statusStr isEqualToString:@"SENT FOR DELIVERY"]) {
            [headerView addSubview:statusBtn];
        }
        else
        {
            if (sectionAnimals1.count==0)
            {
            }
            else
            {
                [headerView addSubview:view];
                [headerView addSubview:statusBtn];
            }
        }
        statusBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
        
        if([editBtn.currentTitle isEqualToString:@"DONE"])
        {
            statusBtn.enabled = NO;
        }
        else if([editBtn.currentTitle isEqualToString:@"EDIT"])
        {
            statusBtn.enabled = YES;
            
        }
        
    }
    
    headerView.backgroundColor=[UIColor clearColor];
    
    return headerView;
}


-(void)newAddressBtn:(UIButton *)sender

{
    //    NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    //    NSString *country = [[NSLocale currentLocale]  displayNameForKey:NSLocaleCountryCode value:countryCode];
    //    NSLog(@"Country Locale:%@  Code:%@ Name:%@", [NSLocale currentLocale] , countryCode, country);
    //    countryTxt.text=country;
    addAdress = @"1";
    newAddressView.hidden=NO;
}


-(void)orderBtn:(UIButton *)sender
{
    orderStr=@"yes";
    NSInteger section=sender.tag;
    if (addressArray.count==0)
    {
        //        NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
        //        NSString *country = [[NSLocale currentLocale]  displayNameForKey:NSLocaleCountryCode value:countryCode];
        //        NSLog(@"Country Locale:%@  Code:%@ Name:%@", [NSLocale currentLocale] , countryCode, country);
        //        countryTxt.text=country;
        addAdress = @"1";
        newAddressView.hidden=NO;
    }
    else
    {
        addressView.hidden=NO;
        
        [addressTable reloadData];
    }
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    NSArray *sectionAnimals1 = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    singletonn.userid=[[sectionAnimals1 objectAtIndex:0]valueForKey:@"userid"];
    singletonn.venId=[[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorid"];
    singletonn.userNameStr=[[sectionAnimals1 objectAtIndex:0]valueForKey:@"username"];
    singletonn.objectIdStr = [[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorobjectid"];
    singletonn.prodNameStr=[[sectionAnimals1 objectAtIndex:0]valueForKey:@"productname"];
    
    
    
}

-(void)Confirm:(id)sender
{
    
    [cityTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
    [addressTxt resignFirstResponder];
    
    
    if (cityTxt.text.length > 0 && countryTxt.text.length > 0 && addressTxt.text.length > 0)
    {
        indicator.hidden=NO;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(confirmAddressView) withObject:nil afterDelay:0.5f];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)countyBtn:(id)sender
{
    
    //    [self country];
    
    btnStr = @"1";
    
    countryView.hidden = NO;
    [self.expansionTableView reloadData];
}


-(void)country

{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/countrylist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    countyArra = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",countyArra);
    
    [countyTbl reloadData];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"country"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [countyArra sortedArrayUsingDescriptors:sortDescriptors];
    countyArra=[[NSMutableArray alloc]init];
    countyArra = [sortedArray1 mutableCopy];
    [countyTbl reloadData];
    
}


- (IBAction)cityBtn:(id)sender
{
    cityTbl.hidden = NO;
    btnStr = @"2";
    [self city];
    [cityTbl reloadData];
}

-(void)city

{
    NSString *post = [NSString stringWithFormat:@"countryid=%@",singletonn.uId];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/citylist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No list available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"city"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [arra_1 sortedArrayUsingDescriptors:sortDescriptors];
    arra_1=[[NSMutableArray alloc]init];
    arra_1 = [sortedArray1 mutableCopy];
    [cityTbl reloadData];
}


-(void)confirmAddressView
{
    
    
    //    if (cityTxt.text.length > 0 && countryTxt.text.length > 0 && addressTxt.text.length > 0)
    
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/adduseradd.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",singletonn.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *countryStrr=[[NSString alloc]initWithFormat:@"%@", singletonn.countryAddres];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"country\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",countryStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *cityStrr=[[NSString alloc]initWithFormat:@"%@", singletonn.cityAddr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"city\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",cityStrr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSString *addressStr1=[[NSString alloc]initWithFormat:@"%@",addressTxt.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"address\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",addressStr1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    NSString *phoneStr=[[NSString alloc]initWithFormat:@"%@",cityTxt.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",phoneStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    addressStr = [[NSString alloc]initWithFormat:@"%@, %@,%@, Phone no. %@",singletonn.cityAddr,singletonn.countryAddres,addressTxt.text,cityTxt.text];
    
    
    
    [self performSelector:@selector(address) withObject:self afterDelay:10.0 ];
    
    
    
}


-(void)address{
    [self orderCartView_english];
    
}





-(void)orderCartView_english
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addorder.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",singletonn.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",singletonn.venId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSDate * now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm:ss a"];
    NSString *currentTime = [formatter stringFromDate:now];
    
    NSDate * now2 = [NSDate date];
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"dd-MM-yyyy"];
    NSString *currentDate = [formatter2 stringFromDate:now2];
    
    
    NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    
    //   NSString *addressStr1=[[NSString alloc]initWithFormat:@"%@",addressStr];
    
    NSString *addr_str1=[[NSString alloc] initWithFormat:@"%@",addressStr];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"useraddress\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",addr_str1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *number = [NSMutableString stringWithCapacity:10];
    for (i = 0U; i < 10; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [number appendFormat:@"%C", c];
    }
    NSLog(@"%@",number);
    
    NSString *orderIdStr=[[NSString alloc]initWithFormat:@"%@_%@",singletonn.userNameStr,number];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
//        singletonn.objectIdStr = [[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
//        
//        objectIdStr
        
        NSString *message =[NSString stringWithFormat:@"%@ wants to order %@",singletonn.userNameStr ,singletonn.prodNameStr                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ];
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:singletonn.objectIdStr successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];
        
        
        
        //        for ( i=0; i< users.count; i++)
        //        {
        //            name= [[users objectAtIndex:i]valueForKey:@"objectId"];
        //            if ([name containsString:singletonn.objectIdStr])
        //            {
        //                PFUser *user1 = [PFUser currentUser];
        //                PFUser *user2 = users[i];
        //                NSString *groupId = StartPrivateChat(user1, user2);
        //                NSString *text = @"Wants to order";
        //                // [self actionChat:groupId];
        //                SendPushNotification(groupId, text);
        //
        //                i = users.count;
        //            }
        //        }
    }
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    addressView.hidden=YES;
    newAddressView.hidden=YES;
    confirmView.hidden=NO;
    orderStr=@"no";
    [self prodListing];
    [productListTable reloadData];
    
}

-(void)AskBtn:(UIButton *)sender
{
    arrayAsk=[[NSMutableArray alloc]init];
    NSInteger section=sender.tag;
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    arrayAsk = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    
    indicator.hidden=NO;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(ask) withObject:nil afterDelay:0.5f];
    
}


-(void)ask

{
    for ( i=0; i< arrayAsk.count; i++)
    {
        NSString *unitPriceStr= [[arrayAsk objectAtIndex:i]valueForKey:@"unitPriceStr"];
        NSString *askPriceStr= [[arrayAsk objectAtIndex:i]valueForKey:@"negotiable_price"];
        cartIdStr=[[arrayAsk objectAtIndex:i]valueForKey:@"id"];
        if ([unitPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if (unitPriceStr == nil)
        {
            [self askView];
        }
        if ([unitPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if (unitPriceStr == nil)
        {
            [self askView];
        }
        if ([askPriceStr isEqualToString:@"Ask for Price"]) {
            [self askView];
        }
        if (askPriceStr == nil)
        {
            [self askView];
        }
        
        
    }
    
    indicator.hidden=YES;
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    replyView.hidden=NO;
    [self prodListing];
    [productListTable reloadData];
    
}


-(IBAction)OK:(id)sender
{
    replyView.hidden=YES;
    confirmView.hidden=YES;
    
    
    [productListTable reloadData];
}


-(void)askView
{
    NSString *post = [NSString stringWithFormat:@"cartid=%@&userid=%@",cartIdStr,singletonn.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/askforprice.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([addAdress isEqualToString:@"1"])
    {
        return 0;
    }
    else
    {
        return 45;
    }
}


- (IBAction)Edit:(id)sender
{
    if ([editBtn.currentTitle isEqualToString:@"SHOW ORDER"])
    {
        ShowOrder *show1=[[ShowOrder alloc]initWithNibName:@"ShowOrder" bundle:nil];
        [self.navigationController pushViewController:show1 animated:NO];
    }
    else
    {
        if([editBtn.currentTitle isEqualToString:@"DONE"])
        {
            [super setEditing:NO animated:NO];
            [productListTable setEditing:NO animated:NO];
            [editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
            [productListTable reloadData];
            
        }
        else if([editBtn.currentTitle isEqualToString:@"EDIT"])
        {
            [super setEditing:YES animated:YES];
            [productListTable setEditing:YES animated:YES];
            [editBtn setTitle:@"DONE" forState:UIControlStateNormal];
            [productListTable reloadData];
            
        }
    }
}



-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        if (indexPath.row==0)
                                        {
                                            
                                            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                                            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                                            singletonn.userid =[[sectionAnimals1 objectAtIndex:0]valueForKey:@"userid"];
                                            singletonn.venId =[[sectionAnimals1 objectAtIndex:0]valueForKey:@"vendorid"];
                                            statusStrr =[[sectionAnimals1 objectAtIndex:0]valueForKey:@"statusid"];
                                            
                                            if(/*[statusStrr isEqualToString:@"2"]||*/[statusStrr isEqualToString:@"3"])
                                            {
                                                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot delete this product because you already have a pending order with this vendor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alrt show];
                                            }
                                            else
                                            {
                                                indicator.hidden=NO;
                                                [indicator startAnimating];
                                                [ProgressHUD show:@"Loading..." Interaction:NO];
                                                [self performSelector:@selector(deleteView1) withObject:nil afterDelay:0.5f];
                                            }
                                        }
                                        else
                                        {
                                            
                                            NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
                                            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                                            statusStrr =[[sectionAnimals1 objectAtIndex:0]valueForKey:@"statusid"];
                                            cartIdStr =[[sectionAnimals1 objectAtIndex:indexPath.row-1]valueForKey:@"id"];
                                            if (/*[statusStrr isEqualToString:@"2"]||*/[statusStrr isEqualToString:@"3"])
                                            {
                                                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot delete this product because you already have a pending order with this vendor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alrt show];
                                            }
                                            else
                                            {
                                                indicator.hidden=NO;
                                                [indicator startAnimating];
                                                [ProgressHUD show:@"Loading..." Interaction:NO];
                                                
                                                [self performSelector:@selector(deleteView) withObject:nil afterDelay:0.5f];
                                                
                                            }
                                        }
                                    }];
    button.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:1.0];
    return @[button];
}


-(void)deleteView1
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@&userid=%@&statusid=%@",singletonn.venId,singletonn.userid,statusStrr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delvendorcart.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
    [self prodListing];
    [productListTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}

-(void)deleteView
{
    NSString *post = [NSString stringWithFormat:@"cartid=%@",cartIdStr];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delcart.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    [self prodListing];
    [productListTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([editBtn.currentTitle isEqualToString:@"DONE"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [cityTxt resignFirstResponder];
    [addressTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
    [textField resignFirstResponder];
    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [cityTxt resignFirstResponder];
    [addressTxt resignFirstResponder];
    [countryTxt resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == sendMessageTxt || textField == cityTxt || textField == addressTxt )
    {
        [self animateTextField:textField up:YES];
    }
    //if ([editBtn.currentTitle isEqualToString:@"DONE"])
    else
    {
        currentValue=textField.text;
        [keyboardControls setActiveField:textField];
        
    }
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == addressTxt || textField == cityTxt)
    {
        [self animateTextField:textField up:NO];
    }
    else
    {
        
        quantStr=textField.text;
        if (![currentValue isEqualToString:quantStr])
        {
            
            UITableViewCell *cell = (UITableViewCell *)textField.superview.superview;
            NSIndexPath *cellIndexPath = [productListTable indexPathForCell:cell];
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:cellIndexPath.section];
            NSArray *sectionAnimals1 = [[finalArray objectAtIndex:cellIndexPath.section]valueForKey:sectionTitle];
            cartIdStr=[[sectionAnimals1 objectAtIndex:cellIndexPath.row-1]valueForKey:@"id"];
            indicator.hidden=NO;
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];
            
            [self performSelector:@selector(editCartView) withObject:nil afterDelay:0.2f];
        }
    }
}



-(void)editCartView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartquant.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%@",quantStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
        
    }
    [self prodListing];
    [productListTable reloadData];
    
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    
}



-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}



- (IBAction)favrt:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(fav) withObject:nil afterDelay:0.5f];
}


-(void)fav
{
    Favorites *fav = [[Favorites alloc]initWithNibName:@"Favorites" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)home:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
}


-(void)home
{
    ViewController *fav = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)close:(id)sender
{
    countryView.hidden = YES;
}


-(void)catlist
{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/citylistall.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray12 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray12);
    
    if (dataArray12.count > 0)
    {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"city"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray12 sortedArrayUsingDescriptors:sortDescriptors];
        dataArray12=[[NSMutableArray alloc]init];
        dataArray12 = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"countryname" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray12 sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray12=[[NSMutableArray alloc]init];
        array12=[[NSMutableArray alloc]init];
        finalArray12=[[NSMutableArray alloc]init];
        
        vendorNameArray12=[[NSMutableArray alloc]init];
        imageArr12=[[NSMutableArray alloc]init];
        
        for( z =0;z<sortedArray.count;z++)
        {
            stringOne12=[[sortedArray objectAtIndex:z]valueForKey:@"countryid"];
            
            NSString *vendorName1=[[sortedArray objectAtIndex:z]valueForKey:@"countryname"];
            // NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
            if (![stringOne12 isEqualToString:stringTwo12 ])
            {
                [vendorNameArray12 addObject:vendorName1];
                
                // [imageArr addObject:imagestr];
                
                stringTwo12 = stringOne12;
                
                if ([newString12 isEqualToString:@"newString"])
                {
                    NSInteger a=z-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
                    
                    dynamicDict12=[[NSMutableDictionary alloc]init];
                    [dynamicDict12 setValue:newArray12 forKey:vendorName1];
                    [finalArray12 addObject:dynamicDict12];
                    
                    newArray12=[[NSMutableArray alloc]init];
                    newString12=@"";
                }
                if ([newString12 isEqualToString:@"oneString"])
                {
                    NSInteger a=z-1;
                    NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
                    dynamicDict12=[[NSMutableDictionary alloc]init];
                    [dynamicDict12 setValue:newArray12 forKey:vendorName1];
                    [finalArray12 addObject:dynamicDict12];
                    
                    newArray12=[[NSMutableArray alloc]init];
                    newString12=@"";
                }
                newString12=@"oneString";
                array12=[sortedArray objectAtIndex:z];
                [newArray12 addObject:array12];
                
            }
            else
            {
                array12=[sortedArray objectAtIndex:z];
                [newArray12 addObject:array12];
                
                newString12=@"newString";
            }
        }
        
        NSInteger a=z-1;
        NSString *vendorName1=[[sortedArray objectAtIndex:a]valueForKey:@"countryname"];
        dynamicDict12=[[NSMutableDictionary alloc]init];
        [dynamicDict12 setValue:newArray12 forKey:vendorName1];
        [finalArray12 addObject:dynamicDict12];
        
        dynamicDict12=[[NSMutableDictionary alloc]init];
    }
    else
    {
        finalArray12 = [[NSMutableArray alloc]init];
        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
    }
    
    newArray12=[[NSMutableArray alloc]init];
    newString12=@"";
    stringOne12=@"";
    stringTwo12=@"";
    [self.expansionTableView reloadData];
}






@end
