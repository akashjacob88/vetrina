//
//  Item_CellAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface Item_CellAR : UITableViewCell




@property (strong, nonatomic) IBOutlet UIImageView *vendorpic;
@property (strong, nonatomic) IBOutlet UILabel *vendrNme;
@property (strong, nonatomic) IBOutlet UIImageView *itmeImg;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UITextView *productDic;
@property (strong, nonatomic) IBOutlet UIButton *cartBtn;
@property (strong, nonatomic) IBOutlet UIImageView *stafPic;
@property (strong, nonatomic) IBOutlet UILabel *btnLbl;
@property (strong, nonatomic) IBOutlet UILabel *backlabl;

@property (strong, nonatomic) IBOutlet UIButton *VendorBtn;


@end
