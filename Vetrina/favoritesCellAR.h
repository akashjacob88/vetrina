//
//  favoritesCellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favoritesCellAR : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *vendorImg;
@property (strong, nonatomic) IBOutlet UILabel *vendrLbl;


@end
