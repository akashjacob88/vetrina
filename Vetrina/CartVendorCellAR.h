//
//  CartVendorCellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartVendorCellAR : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *vendorImage;
@property (strong, nonatomic) IBOutlet UIImageView *statusImage;
@property (strong, nonatomic) IBOutlet UILabel *vendorName;
@property (strong, nonatomic) IBOutlet UILabel *msgTxt;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UIImageView *chatLbl;



@end
