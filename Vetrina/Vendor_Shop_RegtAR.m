//
//  Vendor_Shop_RegtAR.m
//  Vetrina
//
//  Created by Umesh Kumar on 01/05/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_Shop_RegtAR.h"
#import "HomeAR_Cell1.h"
#import "HomeAR_Cell2.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "Home_VetrinaARViewController.h"


@interface Vendor_Shop_RegtAR ()<UITableViewDataSource,UITableViewDelegate>
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex1;

@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;


@end

@implementation Vendor_Shop_RegtAR

@synthesize isOpen,selectIndex1,expansionTableView;

- (void)dealloc
{
    // [_dataList release];
    //_dataList = nil;
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex1 = nil;
    indicatr = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad

{
    [super viewDidLoad];
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    singlogin = [Singleton instance];
    
    [indicatr stopAnimating];
    
    NSArray *fields = @[instaNameText,emailText,shopNmbrText];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    showTable.hidden = YES;
    thanksView.hidden = YES;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, instaNameText.frame.size.height)];
    instaNameText.leftView = leftView1;
    instaNameText.leftViewMode = UITextFieldViewModeAlways;
    [instaNameText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, shopNmbrText.frame.size.height)];
    shopNmbrText.leftView = leftView2;
    shopNmbrText.leftViewMode = UITextFieldViewModeAlways;
    [shopNmbrText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, shopCatText.frame.size.height)];
    shopCatText.leftView = leftView3;
    shopCatText.leftViewMode = UITextFieldViewModeAlways;
    [shopCatText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView4 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, emailText.frame.size.height)];
    emailText.leftView = leftView4;
    emailText.leftViewMode = UITextFieldViewModeAlways;
    [emailText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
}



//---------------- UITextField Methods ------------------\\


-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [instaNameText resignFirstResponder];
    [shopNmbrText resignFirstResponder];
    [shopCatText resignFirstResponder];
    [emailText resignFirstResponder];
    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [instaNameText resignFirstResponder];
    [shopNmbrText resignFirstResponder];
    [shopCatText resignFirstResponder];
    [emailText resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == instaNameText || textField == shopNmbrText || textField == shopCatText || textField == emailText)
    {
        [self animateTextField:textField up:YES];
    }
    [keyboardControls setActiveField:textField];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    if(textField==instaNameText || textField==shopNmbrText || textField==shopCatText || textField == emailText)
    {
        [self animateTextField:textField up:NO];
    }
}


- (IBAction)showCatTable:(id)sender

{
    showTable.hidden= NO;
    [self catlist];
    
}
#pragma mark - Servise


-(void)catlist
{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/subcatlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"a_subcatname"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [sortedArray1 mutableCopy];
    
    
    NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"a_catname" ascending:YES];
    NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
    sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
    
    NSLog(@"sorted array is %@",sortedArray);
    
    newArray=[[NSMutableArray alloc]init];
    array=[[NSMutableArray alloc]init];
    finalArray=[[NSMutableArray alloc]init];
    
    vendorNameArray=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
    
    for( i =0;i<sortedArray.count;i++)
    {
        stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
        
        NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"a_catname"];
        NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
        if (![stringOne isEqualToString:stringTwo ])
        {
            
            [vendorNameArray addObject:vendorName1];
            
            [imageArr addObject:imagestr];
            
            stringTwo = stringOne;
            
            if ([newString isEqualToString:@"newString"])
            {
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"a_catname"];
                
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            if ([newString isEqualToString:@"oneString"])
            {
                
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"a_catname"];
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
                
            }
            newString=@"oneString";
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
        }
        else
        {
            
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
            newString=@"newString";
        }
    }
    
    NSInteger a=i-1;
    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"a_catname"];
    dynamicDict=[[NSMutableDictionary alloc]init];
    [dynamicDict setValue:newArray forKey:vendorName];
    [finalArray addObject:dynamicDict];
    
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
    
    [expansionTableView reloadData];
    
    
}


#pragma mark - Table view data source and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex1.section == section)
        {
            //   return [[[_dataList objectAtIndex:section] objectForKey:@"list"] count]+1;;
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
            sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
            return [sectionAnimals count]+1;
            
            
        }
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex1.section == indexPath.section&&indexPath.row!=0)
    {
        HomeAR_Cell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell2" bundle:nil] forCellReuseIdentifier:@"Cell_Ar2"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar2"];
        }
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        //     NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"list"];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"a_subcatname"];
        return cell;
    }
    else
    {
        HomeAR_Cell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeAR_Cell1" bundle:nil] forCellReuseIdentifier:@"Cell_Ar1"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Ar1"];
        }
        //    NSString *name = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        
        cell.titleLabel.text = sectionTitle;
        
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
        cell.iconImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
        [cell changeArrowWithUp:([self.selectIndex1 isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex1])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex1 = nil;
            
        }
        else
        {
            if (!self.selectIndex1)
            {
                self.selectIndex1 = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        singlogin.catid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
        singlogin.catName =[[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_catname"];
        singlogin.subName = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"a_subcatname"];
        singlogin.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
        shopCatText.text = singlogin.subName;
        showTable.hidden= YES;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    HomeAR_Cell1 *cell = (HomeAR_Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex1];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex1.section;
    
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex1 = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}


- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)hideBtn:(id)sender

{
    
    showTable.hidden = YES;
    
}

#pragma mark - Submit Button


- (IBAction)submitShop:(id)sender

{
    if (instaNameText.text.length>0 && shopNmbrText.text.length>0 && shopCatText.text.length>0 && emailText.text.length>0)
    {
        [indicatr startAnimating];
        [self performSelector:@selector(subcat) withObject:Nil afterDelay:2.0f];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}




-(void)subcat

{
    singlogin.shopNameVendr = instaNameText.text;
    singlogin.vEnPhone = shopNmbrText.text;
    singlogin.subName = shopCatText.text;
    singlogin.vendorEmail = emailText.text;
    
    NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addvendorsubcat.php"];
    
    NSLog(@"strURL:%@", strULR);
    
    NSURL * url=[NSURL URLWithString:strULR];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrId = [[NSString alloc]initWithFormat:@"%@", singlogin.vUniqueId];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.shopNameVendr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fullname_shopname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *catname = [[NSString alloc]initWithFormat:@"%@", singlogin.catName];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",catname] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *catidstr = [[NSString alloc]initWithFormat:@"%@", singlogin.catid];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",catidstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatstr = [[NSString alloc]initWithFormat:@"%@", singlogin.subName];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",subcatstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subidStr = [[NSString alloc]initWithFormat:@"%@", singlogin.subCatid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",subidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *phoneStr = [[NSString alloc]initWithFormat:@"%@", singlogin.vEnPhone];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",phoneStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *emailStr = [[NSString alloc]initWithFormat:@"%@", singlogin.vendorEmail];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",emailStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *useName = [[NSString alloc]initWithFormat:@"%@", singlogin.vIGUsername];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username_instagramacnt\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",useName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *proiflestr= [[NSString alloc]initWithFormat:@"%@", singlogin.vIGProfilePic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"imagee\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",proiflestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"print mystring==%@",returnString);
    
    NSDictionary *eventarray=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *msg = [[eventarray valueForKey:@"message"]objectAtIndex:0];
    
    NSLog(@"msg -- %@",msg);
    
    
    thanksView.hidden = NO;
    [indicatr stopAnimating];
    
}

- (IBAction)Okay:(id)sender

{
    Home_VetrinaARViewController *home = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:home animated:YES];
    [indicatr stopAnimating];
}



- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}




- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
