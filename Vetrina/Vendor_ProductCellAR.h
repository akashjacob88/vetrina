//
//  Vendor_ProductCellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface Vendor_ProductCellAR : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *statusImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *msgTxt;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UIImageView *profilPic;

- (void)bindData:(PFObject *)recent_;

@end
