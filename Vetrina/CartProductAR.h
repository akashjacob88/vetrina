//
//  CartProductAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"



@interface CartProductAR : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>


{
    IBOutlet UILabel *count_lbl;
    IBOutlet UIButton *chatBtn;
    BSKeyboardControls *keyboardControls;
    Singleton *singletonn;
    IBOutlet UITableView *productListTable;
    IBOutlet UILabel *vendorNameLbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITextField *sendMessageTxt;
    NSString *orderStr;
    IBOutlet UIButton *editBtn;
    NSInteger index;
    IBOutlet UIView *noItmeView;
    IBOutlet UILabel *showOrderLbl;
    NSMutableArray *newArray,*array,*vendorNameArray,*imageArr,*arra_1,*countryArry;
    NSString *btnStr;
    NSString *addAdress,*name;
    NSString *currentValue,*quantStr,*cartIdStr,*statusStr,*addressStr,*untiteld;
    NSMutableArray *arrayAsk;

    IBOutlet UIView *replyView;
    IBOutlet UIView *confirmView;
    IBOutlet UIView *addressView;
    IBOutlet UIView *newAddressView;
    IBOutlet UITableView *addressTable;
    IBOutlet UITextField *addressTxt,*countryTxt,*cityTxt;
    IBOutlet UITableView *countyTbl;
    NSInteger i;
    NSString *stringOne,*newString,*stringTwo,*statusStrr;
    NSString *statusStr0;
    IBOutlet UITableView *cityTbl;
    
    NSMutableDictionary *dynamicDict;

    NSMutableArray *finalArray,*dataArray,*addressArray,*valu,*countyArra,*cityarra;

    UIRefreshControl *refreshControl;

    IBOutlet UIView *country_cityView;
}
- (IBAction)cancel_cntryCityView:(id)sender;

@end
