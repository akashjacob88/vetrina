//
//  PremiumItems.h
//  Vetrina
//
//  Created by Amit Garg on 6/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface PremiumItems : UIViewController<UIAlertViewDelegate,UIActionSheetDelegate>

{
     Singleton *singlogin;
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;
    IBOutlet UITableView *itemTble;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    IBOutlet UILabel *chrtLbl;
    NSMutableArray *arra_1,*arra_2;
    NSArray *sortedarray1,*sortedarray2;
    UIImageView *Image2, *img3;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr;
    NSString *cartIdStr,*quantStr;

    NSString *searchStr,*soldout,*askforPrice,*staffPic,*name,*objectIdStr,*prodid;
    NSMutableArray *cartListArray;

    IBOutlet UIView *sorry;
    IBOutlet UIView *view2;
    
    
    NSString *reson;
    IBOutlet UIButton *reportBtn1,*reportBtn2,*reportBtn3;
}
- (IBAction)signUp:(id)sender;
- (IBAction)noThank:(id)sender;

- (IBAction)search:(id)sender;
- (IBAction)cart:(id)sender;
- (IBAction)shop:(id)sender;

@end
