//
//  Vendor_Profile.m
//  Vetrina
//
//  Created by Amit Garg on 10/3/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import "Vendor_Profile.h"
#import "Walkthrough.h"


#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ProgressHUD.h"
#import "Vendor_setting.h"
#import "Vendor_phone.h"


#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"
#import "Base64.h"
#import "JSON.h"
#import "orderHistory.h"

@interface Vendor_Profile ()

@end

@implementation Vendor_Profile

- (void)viewDidLoad
{
    [super viewDidLoad];
    singling = [Singleton instance];
    
    NSURL *url1=[NSURL URLWithString:singling.venPic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profileVendor.image=emppppic;
    profileVendor.layer.masksToBounds = YES;
    profileVendor.layer.cornerRadius = profileVendor.frame.size.width/2;
    profileVendor.layer.opaque = NO;
    
    
    
    view1Shadow.layer.shadowRadius = 2.0f;
    view1Shadow.layer.shadowOffset = CGSizeMake(0, 2);
    view1Shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    view1Shadow.layer.shadowOpacity = 0.5f;
    
    view2Shadow.layer.shadowRadius = 2.0f;
    view2Shadow.layer.shadowOffset = CGSizeMake(0, 2);
    view2Shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    view2Shadow.layer.shadowOpacity = 0.5f;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)back:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(backBtn) withObject:nil afterDelay:0.5f];
}

-(void)backBtn
{
    [self.navigationController popViewControllerAnimated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)shopDetailsBtn:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(venderSetting)withObject:Nil afterDelay:0.0f];
}


-(void)venderSetting
{
    Vendor_setting *view = [[Vendor_setting alloc]initWithNibName:@"Vendor_setting" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}




- (IBAction)VendorSettingBtn:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(setting) withObject:nil afterDelay:0];
}

-(void)setting
{
    Vendor_phone *view = [[Vendor_phone alloc]initWithNibName:@"Vendor_phone" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)logOutBtn:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Log Out" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 10;

    }

- (IBAction)orderHistory:(id)sender {
    
    
    orderHistory *cart=[[orderHistory alloc]initWithNibName:@"orderHistory" bundle:nil];
    [self.navigationController pushViewController:cart animated:NO];
 
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 10)
    {
        if (buttonIndex==0)
        {
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"VinstagramID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
          
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"arabic"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"english"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            singling.loginStatus = nil;
            singling.userid = nil;
            
            [QBRequest logOutWithSuccessBlock:^(QBResponse *response) {
                
                NSLog(@"QuickBlox Logout Successfully");
                
                [self unsub];
                
                
                
                
                // Successful logout
            } errorBlock:^(QBResponse *response) {
                // Handle error
            }];

            
            
//            [PFUser logOut];
//            ParsePushUserResign();
//            PostNotification(NOTIFICATION_USER_LOGGED_OUT);
//            LoginUser(self);
            Walkthrough *home = [[Walkthrough alloc]initWithNibName:@"Walkthrough" bundle:NULL];
            [self.navigationController pushViewController:home animated:YES];
        }
    }
    
    
}
-(void)unsub{
    NSString *deviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [QBRequest unregisterSubscriptionForUniqueDeviceIdentifier:deviceUdid successBlock:^(QBResponse *response) {
        // Unsubscribed successfully
    } errorBlock:^(QBError *error) {
        // Handle error
    }];
}

- (IBAction)SlectimageBtn:(id)sender

{
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@"Select Image!"
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Gallery",@"Camera",nil];
    actionsheet1.tag=12;
    [actionsheet1 showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
    }
    
    if (buttonIndex==1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypeCamera;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }];
            
        }
        
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error acessing camera"
                                  message:@"Device does not support a camera"
                                  delegate:nil
                                  cancelButtonTitle:@"Dismiss"
                                  otherButtonTitles:nil];
            alert.tag=44;
            [alert show];
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(actionsheet1.tag==12)
    {
        cimage=[info objectForKey:UIImagePickerControllerEditedImage];
        
        CGRect rect = CGRectMake(0,0,cimage.size.width/6,cimage.size.height/6);
        UIGraphicsBeginImageContext( rect.size );
        [cimage drawInRect:rect];
        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageDataForResize = UIImagePNGRepresentation(picture1);
        UIImage *pimage=[UIImage imageWithData:imageDataForResize];
        
        picture = ResizeImage(pimage, 280, 280);
        thumbnail = ResizeImage(pimage, 60, 60);

        
        [profileVendor setImage:pimage];
    }
    [camerapicker dismissViewControllerAnimated:YES completion:nil];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self updatePic];
}


-(void)updatePic

{
    [self PROFILEPIC];
    
    NSData *myimagedata = UIImageJPEGRepresentation(profileVendor.image, 90);
    
    
//    PFFile *filePicture = [PFFile fileWithName:@"picture.jpg" data:UIImageJPEGRepresentation(picture, 0.6)];
//    [filePicture saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    PFFile *fileThumbnail = [PFFile fileWithName:@"thumbnail.jpg" data:UIImageJPEGRepresentation(thumbnail, 0.6)];
//    [fileThumbnail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    PFUser *user = [PFUser currentUser];
//    user[PF_USER_PICTURE] = filePicture;
//    user[PF_USER_THUMBNAIL] = fileThumbnail;
//    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error != nil) [ProgressHUD showError:@"Network error."];
//     }];
//    
    
    
  //  NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/edituserimg.php"];
   
    
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editvendorimg.php"];
 
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singling.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *subid=[[NSString alloc]initWithFormat:@"%@",singling.subId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subid] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[Base64 encode:myimagedata]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSMutableArray *valu = [val valueForKey:@"message"];
    
    NSString *venPic=[[valu objectAtIndex:0] valueForKey:@"imagee"];
    singling.venPic = venPic;
    
    [ProgressHUD showSuccess:@""];
}

-(void)PROFILEPIC{
    
    NSData *myimagedata = UIImageJPEGRepresentation(profileVendor.image, 90);
    
    // NSData * imageData = UIImageJPEGRepresentation([UIImage imageNamed: @"profile-img"], 0.8f);
    
    [QBRequest TUploadFile: myimagedata fileName: @"ProfilePicture"
               contentType: @"image/jpeg"
                  isPublic: YES successBlock: ^ (QBResponse * response, QBCBlob * blob) {
                      
                      // File uploaded, do something
                      // if blob.isPublic == YES
                      NSString * url = [blob publicUrl];
                      
                      NSLog(@"%@",url);
                      
                      
                      QBUpdateUserParameters *params = [QBUpdateUserParameters new];
                      params.blobID = [blob ID];
                      params.customData=url;
                      [QBRequest updateCurrentUser:params successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                          // success block
                      } errorBlock:^(QBResponse * _Nonnull response) {
                          // error block
                          NSLog(@"Failed to update user: %@", [response.error reasons]);
                      }];
                      
                      
                      
                      
                      
                      
                      
                      
                  }
               statusBlock: ^ (QBRequest * request, QBRequestStatus * status) {
                   // handle progress
               }
                errorBlock: ^ (QBResponse * response) {
                    NSLog(@"error: %@", response.error);
                }
     ];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
