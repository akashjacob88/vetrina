//
//  ItemSearch.m
//  Vetrina
//
//  Created by Amit Garg on 5/7/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "ItemSearch.h"
#import "ViewController.h"
#import "Subcatgory.h"
#import "Search.h"
#import "CustomItem_CellTableViewCell.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "All_SignIn.h"
#import "CartVendorList.h"
#import "Vendor_Detaillist.h"
#import "CartProductList.h"
#import "ProgressHUD.h"
#import "ProgressHUD.h"
#import "AppConstant.h"
#import "push.h"
#import <Parse/Parse.h>
#import "Favorites.h"
#import "RecentView.h"
#import "Vendor_Product.h"
#import "Vendor_items.h"
#import "VCFloatingActionButton.h"
#import "ARSpeechActivity.h"
#import "UIImageView+WebCache.h"

@interface ItemSearch ()<floatMenuDelegate,UITextFieldDelegate>
{
    IBOutlet UILabel *msg_countLbl;
    NSMutableArray *users;
    IBOutlet UIView *navView;
    IBOutlet UIView *vendorViewBar;
     IBOutlet UIView *reportView;
    UITapGestureRecognizer *tapRecognizer;
    UIView *new;

}
@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property (nonatomic, strong) UITextField *textField;

@end

@implementation ItemSearch
@synthesize addButton;

-(void)tapped
{
    [self.view removeGestureRecognizer:tapRecognizer];
    new.hidden=YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singlogin = [Singleton instance];
    
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 55, 44, 44);
    
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"plus"] andPressedImage:[UIImage imageNamed:@"cross"] withScrollview:itemsTbl];
    
    //    NSDictionary *optionsDictionary = @{@"fb-icon":@"Facebook",@"twitter-icon":@"Twitter",@"google-icon":@"Google Plus",@"linkedin-icon":@"Linked in"};
    //    addButton.menuItemSet = optionsDictionary;
    
    
    addButton.imageArray = @[@"fb-icon",@"twitter-icon",@"WhatsApp-icon"];
      addButton.labelArray = @[@"Share on Facebook",@"Share on Twitter",@"Share on Whats App"];
    
    
    
    addButton.hideWhileScrolling = YES;
    addButton.delegate = self;
    
    
    itemsTbl.dataSource = self;
    itemsTbl.delegate = self;
    
    
      CGRect screen=[[UIScreen mainScreen] bounds];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    
    NSString *st= [[NSUserDefaults standardUserDefaults] valueForKey:@"coachMarker"];
    
    if ([st isEqualToString:@"one"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"two" forKey:@"coachMarker"];

    
    new = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screen.size.width  , screen.size.height)];
    [self.view addSubview:new];
    
    
    
    UIImageView *arraow=[[UIImageView alloc]initWithFrame:CGRectMake(screen.size.width-80, 65, 55, 55)];
    
    arraow.image=[UIImage imageNamed:@"arrow-down"];
    [new addSubview:arraow];
    
    UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(screen.size.width-270, 90, 200, 40)];
    lblMsg.text=@"List by SHOPS or ITEMS";
        lblMsg.font=[UIFont fontWithName:@"OpenSans-Bold" size:14];
    lblMsg.textColor=[UIColor blackColor];
    lblMsg.backgroundColor=[UIColor whiteColor];
    lblMsg.textAlignment=NSTextAlignmentCenter;
    [new addSubview:lblMsg];
    
    lblMsg.layer.cornerRadius=3.0f;
    lblMsg.layer.masksToBounds=YES;
    [self.view addGestureRecognizer:tapRecognizer];
    
    new.backgroundColor=[UIColor colorWithWhite:0.f alpha:0.6];
    
    }
    
    users = [[NSMutableArray alloc]init];
//    
//    if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
//    {
////        PFUser *user = [PFUser currentUser];
////        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
////        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
////        [query orderByAscending:PF_USER_FULLNAME];
////        [query setLimit:1000];
////        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
////         {
////             if (error == nil)
////             {
////                 [users removeAllObjects];
////                 [users addObjectsFromArray:objects];
////                 [itemsTbl reloadData];
////             }
////             else [ProgressHUD showError:@"Network error."];
////         }];
//    }
//
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, searchText.frame.size.height)];
    searchText.leftView = leftView1;
    searchText.leftViewMode = UITextFieldViewModeAlways;
    [searchText setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    [indicator stopAnimating];
    singlogin = [Singleton instance];
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    reportView.hidden = YES;
    
//    itemsLbl.layer.cornerRadius = 5.0;
//    
//    shoplab.layer.masksToBounds = YES;
//    shoplab.layer.cornerRadius = 5.0;
//    shoplab.layer.borderWidth = 2.0;
//    shoplab.layer.borderColor = [[UIColor whiteColor]CGColor];
//    shoplab.clipsToBounds = YES;
//    shoplab.layer.opaque = NO;
    
   // [self vendorlisting];
    view1.hidden=YES;
    msg_countVen.hidden=YES;
    catLbl.hidden=YES;
    chrtLbl.hidden=YES;
    msg_countLbl.hidden=YES;
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        navView.hidden = YES;
        vendorViewBar.hidden = NO;
        msg_countVen.hidden=YES;
        catLbl.hidden=YES;
        
        if (singlogin.count_msg>0) {
            msg_countVen.hidden=NO;
            msg_countVen.layer.cornerRadius=8.0f;
            msg_countVen.layer.masksToBounds=YES;
            msg_countVen.text=[NSString stringWithFormat:@"%d",singlogin.count_msg];
        }
        if (singlogin.count_cart>0) {
            catLbl.hidden=NO;
            catLbl.layer.cornerRadius=8.0f;
            catLbl.layer.masksToBounds=YES;
            catLbl.text=[NSString stringWithFormat:@"%d",singlogin.count_cart];
        }
        
        
        
        
        
    }
    else if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        msg_countLbl.hidden=YES;
        
        if (singlogin.count_msg>0) {
            msg_countLbl.hidden=NO;
            msg_countLbl.layer.cornerRadius=8.0f;
            msg_countLbl.layer.masksToBounds=YES;
            msg_countLbl.text=[NSString stringWithFormat:@"%d",singlogin.count_msg];
        }
        
        
        
        [self cartTotalView];
        
    }
    else
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        chrtLbl.hidden=YES;
        [self cartTotalView];
    }

    NSArray *fields = @[searchText];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    [keyboardControls setDoneTitle:@"Search"];


}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
            
        }
    }
}


- (IBAction)okay:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)vendorlisting1
{
//    if([singlogin.searchItemArray count]==0)
//    {
////        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"No items available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
////        [alert show];
//        noItemView.hidden = NO;
//        [itemsTbl setHidden:YES];
//    }
//    else
//    {
//        aray_2=[[NSMutableArray alloc]init];
//        aray_2=singlogin.searchItemArray;
//        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
//        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
//        sortedarray1 = [aray_2 sortedArrayUsingDescriptors:sortedarray1];
//        
//        
//            vimagearray1 = [[NSMutableArray alloc]init];
//            for(int i =0;i<sortedarray1.count;i++)
//            {
//                vimage =[[UIImageView alloc]init];
//                UIGraphicsBeginImageContext(vimage.image.size);
//                {
//                    CGContextRef ctx = UIGraphicsGetCurrentContext();
//                    CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//                    trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, vimage.image.size.height));
//                    CGContextConcatCTM(ctx, trnsfrm);
//                    CGContextBeginPath(ctx);
//                    CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, vimage.image.size.width, vimage.image.size.height));
//                    CGContextClip(ctx);
//                    CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, vimage.image.size.width, vimage.image.size.height), vimage.image.CGImage);
//                    vimage.image = UIGraphicsGetImageFromCurrentImageContext();
//                    UIGraphicsEndImageContext();
//                }
//                [[AsyncImageLoader sharedLoader] cancelLoadingURL:vimage.imageURL];
//                vimage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"vendorpic"]]];
//                [vimagearray1 addObject:vimage];
//            }
//            varray1=[[NSMutableArray alloc]init];
//            varray1=vimagearray1;
//
//        
//        
//        
//        pimagearray2 = [[NSMutableArray alloc]init];
//        for(int i =0;i<sortedarray1.count;i++)
//        {
//            pimage =[[UIImageView alloc]init];
//            UIGraphicsBeginImageContext(pimage.image.size);
//            {
//                CGContextRef ctx = UIGraphicsGetCurrentContext();
//                CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//                trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, pimage.image.size.height));
//                CGContextConcatCTM(ctx, trnsfrm);
//                CGContextBeginPath(ctx);
//                CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, pimage.image.size.width, pimage.image.size.height));
//                CGContextClip(ctx);
//                CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, pimage.image.size.width, pimage.image.size.height), pimage.image.CGImage);
//                pimage.image = UIGraphicsGetImageFromCurrentImageContext();
//                UIGraphicsEndImageContext();
//            }
//            [[AsyncImageLoader sharedLoader] cancelLoadingURL:vimage.imageURL];
//            pimage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"prodimg1"]]];
//            [pimagearray2 addObject:pimage];
//        }
//        parray2=[[NSMutableArray alloc]init];
//        parray2=pimagearray2;
//        NSLog(@"--------%@",sortedarray1);
//        [itemsTbl setHidden:NO];
//        [itemsTbl reloadData];
//        noItemView.hidden = YES;
//    }
    
    NSString *post = [NSString stringWithFormat:@"prodname=%@",searchText.text];
    
    NSLog(@"get data=%@",post);
    
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/searchprod.php"];
    
    NSLog(@"PostData--%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSDictionary *eventarray=[data JSONValue];
    
    
    if ([[eventarray objectForKeyedSubscript:@"search  list"]isEqual:@"No vendor found"])
    {
        aray_2 = [[NSMutableArray alloc]init];
        
    }
    else
    {
        aray_2 = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"search  list"]];
        
        
    }
    if (aray_2.count==0)
    {
//        noPrductView.hidden= NO;
        [itemsTbl setHidden:YES];
    }
    else
    {
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [aray_2 sortedArrayUsingDescriptors:sortedarray1];
        [itemsTbl setHidden:NO];
//        noPrductView.hidden= YES;
    }
    [ProgressHUD showSuccess:@""];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sortedarray1 count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 495;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    CustomItem_CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell6"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CustomItem_CellTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell6"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell6"];
        
        UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        doubleTap.numberOfTapsRequired = 2;
        doubleTap.numberOfTouchesRequired = 1;
        [tableView addGestureRecognizer:doubleTap];
    }
    return cell;
}


-(void)doubleTap:(UISwipeGestureRecognizer*)tap
{
    if (UIGestureRecognizerStateEnded == tap.state)
    {
        CGPoint p = [tap locationInView:tap.view];
        NSIndexPath* indexPath = [itemsTbl indexPathForRowAtPoint:p];
        UITableViewCell* cell = [itemsTbl cellForRowAtIndexPath:indexPath];
        NSIndexPath *indexPath1 = [itemsTbl indexPathForCell:cell];
        NSLog(@" string = %@",indexPath1);
              prodid=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"id"];
        vendorImgStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"vendorpic"];
        vendorIdStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"vendorid"];
        vendorNameStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"vendorname"];
        productIdStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"id"];
        prodImgStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"prodimg1"];
        unitPriceStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"unitprice"];
        productNameStr=[[sortedarray1 objectAtIndex:indexPath1.row]valueForKey:@"prodname"];
              actionSheet0 = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add to Wish list",@"Recommend to a friend",@"Report item",nil];
        actionSheet0.tag = 11;
        [actionSheet0 showInView:self.view];
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (actionSheet.tag==11)
    {
        if (buttonIndex==0)
        {
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                 view1.hidden = NO;
            }
            else if ([singlogin.loginStatus isEqualToString:@"User Login"])
            {
                [ProgressHUD show:@"Loading..." Interaction:NO];
                [self performSelector:@selector(wishlist) withObject:Nil afterDelay:0.1f];

                
            }
            else
            {
                view1.hidden = NO;
            }
            
        }
        else if (buttonIndex ==1 )
        {
            NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addwishlist.php"];
            NSLog(@"strURL:%@",strURL);
            NSURL * url=[NSURL URLWithString:strURL];
            NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
            [theLoginRequest setHTTPMethod:@"POST"];
            
            NSMutableData *body = [NSMutableData data];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
            [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
            
            NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sender_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSString *usrnameStr=[[NSString alloc]initWithFormat:@"3936"];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"receiver_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",usrnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSString *podid=[[NSString alloc]initWithFormat:@"%@",prodid];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"receiver_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",podid] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

            
            
            [theLoginRequest  setHTTPBody:body];
            NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"print mystring==%@",returnString);

        }

        else if (buttonIndex==2)
        {
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                 view1.hidden = NO;
            }
            else if ([singlogin.loginStatus isEqualToString:@"User Login"])
            {
                reportView.hidden = NO;
            }
            else
            {

            view1.hidden = NO;
            }
        }
        else
        {
        }
        
    }
    else if (actionSheet.tag==12)
    {
        if (buttonIndex==0)
        {
            NSString *str = @"https://itunes.apple.com/in/app/vetrina/id980820003?mt=8";
            str = [NSString stringWithFormat:@"%@", str];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            
        }
    }

    
}

-(void)wishlist

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addwishlist.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usrnameStr=[[NSString alloc]initWithFormat:@"%@",prodid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
    
    [ProgressHUD showSuccess:@""];
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(CustomItem_CellTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSString *sectional = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    cell.vendrNme.text =[NSString stringWithFormat:@"%@",sectional];
//    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.vendorpic.imageURL];
//    cell.vendorpic.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"vendorpic"]]];
    
    NSString *strImageUrl1 = [[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"vendorpic"];
    
    [cell.vendorpic sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]];
    
    
    cell.vendorpic.layer.masksToBounds = YES;
    cell.vendorpic.layer.cornerRadius = 20.0;
    cell.vendorpic.layer.opaque = NO;
    
    NSString *sectional2 = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"prodname"];
    cell.productName.text =[NSString stringWithFormat:@"%@",sectional2];
    
    askforPrice = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    soldout = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    staffPic = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"staffpick"];

    
    if ([staffPic isEqualToString:@"yes"])
    {
        cell.premiumClub.hidden = NO;
    }
    else
    {
        cell.premiumClub.hidden = YES;
    }
    
    
    if ([askforPrice isEqualToString:@"Ask for Price"])
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    }
    else if ([soldout isEqualToString:@"SOLD OUT"])
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    }
    else
    {
        cell.productPrice.text = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    }
    
    NSString *sectional4 = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"prod_desc"];
    cell.productDic.text =[NSString stringWithFormat:@"%@",sectional4];
    
    
    if ([soldout isEqualToString:@"SOLD OUT"])
    {
        cell.cartBtn.hidden = YES;
        cell.btnLbl.hidden = YES;
    }
    else
    {
        cell.cartBtn.hidden = NO;

        [cell.cartBtn addTarget:self action:@selector(orderBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    [cell.sendBtn addTarget:self action:@selector(VendrBtn:) forControlEvents:UIControlEventTouchUpInside];

    //pimage = [pimagearray2 objectAtIndex:indexPath.row];
    vimage = [vimagearray1 objectAtIndex:indexPath.row];
    
    NSString *strImageUrl = [[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prodimg1"];
    
    
    
    
    __block UIActivityIndicatorView *activityIndicator;
    [cell.itmeImg sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                    placeholderImage:[UIImage imageNamed:@""]
                             options:SDWebImageProgressiveDownload
                            progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
         if (!activityIndicator)
         {
             cell.itmeImg.hidden = YES;
             [cell.contentView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]];
             activityIndicator.center = cell.contentView.center;
             activityIndicator.color = [UIColor colorWithRed:119.0/255.0 green:218.0/255.0 blue:172.0/255.0 alpha:1.0];
             [activityIndicator startAnimating];
         }
     }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                               cell.itmeImg.hidden = NO;
                               [activityIndicator removeFromSuperview];
                               activityIndicator = nil;
                           }];

//    cell.itmeImg.image=pimage.image;
    //cell.vendorpic.image=vimage.image;


//    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.itmeImg.imageURL];
//    cell.itmeImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prodimg1"]]];
}




-(void)VendrBtn:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell * )sender.superview.superview;
    NSIndexPath *indexPath = [itemsTbl indexPathForCell:cell];
    singlogin.SlctVenId=[[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
    singlogin.venName = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    singlogin.objectIdStr = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"objectid"];
    singlogin.vEnPhone = [[sortedarray1 objectAtIndex:indexPath.row]valueForKey:@"phoneno"];
    [indicator setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(detailbtn) withObject:Nil afterDelay:2.0f];
}


-(void)detailbtn
{
    Vendor_Detaillist *vender = [[Vendor_Detaillist alloc]initWithNibName:@"Vendor_Detaillist" bundle:nil];
    [self.navigationController pushViewController:vender animated:NO];
    [ProgressHUD showSuccess:@""];

    [indicator stopAnimating];
}



-(void)orderBtn:(UIButton *)sender
{
  
     if ([singlogin.loginStatus isEqualToString:@"IG Login"])
     {
         UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alrt show];
     }
     else
     {
           UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
           NSIndexPath *cellIndexPath = [itemsTbl indexPathForCell:cell];
           productIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"id"];
           prodImgStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"prodimg1"];
           unitPriceStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"unitprice"];
           productNameStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"prodname"];
           vendorImgStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorpic"];
           vendorIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorid"];
           vendorNameStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"vendorname"];
           objectIdStr=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"objectid"];
    
           askforPrice=[[sortedarray1 objectAtIndex:cellIndexPath.row]valueForKey:@"negotiable_price"];
           if ([askforPrice isEqualToString:@"Ask for Price"])
           {
              unitPriceStr=@"";
           }
    
          [self prodListing];
    
           if (cartListArray.count==0)
           {
            [self addtocart];
           }
          else
           {
                  NSString *exist;
                  for (NSInteger i=0; i<cartListArray.count; i++)
                 {
                   NSString *productIdStr1=[[cartListArray objectAtIndex:i]valueForKey:@"productid"];
                   cartIdStr=[[cartListArray objectAtIndex:i]valueForKey:@"id"];
                    quantStr=[[cartListArray objectAtIndex:i]valueForKey:@"quantityprod"];
            
                     if ([productIdStr1 isEqualToString:productIdStr])
                       {
                            exist=@"yes";
                         i=cartListArray.count;
                            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"Product is already in cart! Do you want to increase quantity?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Add"
                                                        otherButtonTitles:@"Cancel", nil];
                         [myAlert show];
                       }
                  }
        if ([exist isEqualToString:@"yes"])
        {
            
        }
        else
        {
            NSString *match;
            for (NSInteger i=0; i<cartListArray.count; i++)
            {
                NSString *vendoridS=[[cartListArray objectAtIndex:i]valueForKey:@"vendorid"];
                NSString *statusStr=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
                
                if ([vendoridS isEqualToString:vendorIdStr])
                {
                    if ([statusStr isEqualToString:@"0"] || [statusStr isEqualToString:@"1"] )
                    {
                        match=@"yes";
                        i=cartListArray.count;
                        indicator.hidden=NO;
                        [indicator startAnimating];
                        [ProgressHUD show:@"Loading..." Interaction:NO];

                        [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
                    }
                    else
                    {
                        match=@"donot";
                        i=cartListArray.count;
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Your previous order is under being processed.Kindly Wait!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else
                {
                    match=@"no";
                }
            }
            if ([match isEqualToString:@"no"])
            {
                indicator.hidden=NO;
                [indicator startAnimating];
                [ProgressHUD show:@"Loading..." Interaction:NO];

                [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
            }
        }
    }
  }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (buttonIndex==0)
    {
        indicator.hidden=NO;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];

        [self performSelector:@selector(editCartView) withObject:nil afterDelay:0.5f];
    }
    else if (buttonIndex==1)
    {
        
    }
}


-(void)editCartView

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartquant.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSInteger aa=[quantStr integerValue];
    aa=aa+1;
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%li",(long)aa];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
    }
    [ProgressHUD showSuccess:@""];

    [indicator stopAnimating];
    indicator.hidden=YES;
}


-(void)addtocart

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addtocart.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *pic1=[[NSString alloc]initWithFormat:@"%@",singlogin.userproilePic];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userprofilepic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",pic1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        
        NSString *productidStr=[[NSString alloc]initWithFormat:@"%@",productIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",productidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *imagee=[[NSString alloc]initWithFormat:@"%@",prodImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",imagee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *price1=[[NSString alloc]initWithFormat:@"%@",unitPriceStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",price1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *askprice1=[[NSString alloc]initWithFormat:@"%@",askforPrice];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",askprice1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
       
        if ([askforPrice isEqualToString:@"Ask for Price"])
        {
            NSString *statusidStr=@"1";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            //singlogin.objectIdStr = [[singlogin.productArray objectAtIndex:0]valueForKey:@"objectid"];
            for (NSInteger i=0; i< users.count; i++)
            {
//                name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//                if ([name containsString:objectIdStr])
//                {
//                    PFUser *user1 = [PFUser currentUser];
//                    PFUser *user2 = users[i];
//                    NSString *groupId = StartPrivateChat(user1, user2);
//                    NSString *text = @"Asked for price";
//                    // [self actionChat:groupId];
//                    SendPushNotification(groupId, text);
//                    
//                    i = users.count;
//                }
            }

            
        }
        else
        {
            NSString *statusidStr=@"0";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        NSString *final=@"1";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantityprod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",final] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *name11=[[NSString alloc]initWithFormat:@"%@",productNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",name11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",vendorImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",vendorIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",vendorNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSInteger j = [singlogin.totalCart integerValue];
        j = j+1 ;
        singlogin.totalCart = [NSString stringWithFormat:@"%ld",(long)j];
        chrtLbl.text=singlogin.totalCart;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Product added to cart" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [self cartTotalView];
        [indicator stopAnimating];
        [ProgressHUD showSuccess:@""];
        
        indicator.hidden=YES;
        if ([singlogin.rate_status isEqualToString:@"1"])
        {
            
        }
        else
        {
            UIActionSheet *actionsheet1 = [[UIActionSheet alloc]
                                           initWithTitle:@"If you enjoy using Vetrina, would you mind taking moment to rate it? It won't take more than a minute. Thanks for your support!"
                                           delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           destructiveButtonTitle:nil
                                           otherButtonTitles:@"Rate It now",@"No,Thanks",nil];
            actionsheet1.tag=12;
            [actionsheet1 showInView:self.view];
            [self ratestatus];
            
        }
    }
}

-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}

- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singlogin.loginfrom = @"cart";
    All_SignIn *signIn = [[All_SignIn alloc]initWithNibName:@"All_SignIn" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}


- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}


- (IBAction)back:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(backBtn) withObject:nil afterDelay:0.5f];
}

-(void)backBtn
{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)shops:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)cart:(id)sender
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];

        [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
    }
}
-(void)nextpage
{
    CartProductList *cart = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:nil];
    [self.navigationController pushViewController:cart animated:YES];
    [indicator setHidden:YES];
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];

}
-(BOOL)textFieldShouldReturn:(UISearchBar *)textField
{
    [searchText resignFirstResponder];
    return TRUE;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [searchText resignFirstResponder];
    
}
- (IBAction)textDidChange:(id)textField
{
    
//    NSString * match = searchText.text;
//    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"prodname CONTAINS[c] %@", match];
//    NSArray *searchArray1,*listFiles1;
//    searchArray1=[singlogin.searchItemArray mutableCopy];
//    listFiles1 = [NSArray arrayWithArray:[searchArray1 filteredArrayUsingPredicate:predicate]];
//    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"prodname"  ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    NSArray *sortedArray = [listFiles1 sortedArrayUsingDescriptors:sortDescriptors];
//    sortedarray1=[sortedArray mutableCopy];
//    
//    
//    vimagearray1 = [[NSMutableArray alloc]init];
//    for(int i =0;i<sortedarray1.count;i++)
//    {
//        vimage =[[UIImageView alloc]init];
//        UIGraphicsBeginImageContext(vimage.image.size);
//        {
//            CGContextRef ctx = UIGraphicsGetCurrentContext();
//            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, vimage.image.size.height));
//            CGContextConcatCTM(ctx, trnsfrm);
//            CGContextBeginPath(ctx);
//            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, vimage.image.size.width, vimage.image.size.height));
//            CGContextClip(ctx);
//            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, vimage.image.size.width, vimage.image.size.height), vimage.image.CGImage);
//            vimage.image = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//        }
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:vimage.imageURL];
//        vimage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"vendorpic"]]];
//        [vimagearray1 addObject:vimage];
//    }
//    pimagearray2 = [[NSMutableArray alloc]init];
//    for(int i =0;i<sortedarray1.count;i++)
//    {
//        pimage =[[UIImageView alloc]init];
//        UIGraphicsBeginImageContext(pimage.image.size);
//        {
//            CGContextRef ctx = UIGraphicsGetCurrentContext();
//            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity, CGAffineTransformMakeScale(1.0, -1.0));
//            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, pimage.image.size.height));
//            CGContextConcatCTM(ctx, trnsfrm);
//            CGContextBeginPath(ctx);
//            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, pimage.image.size.width, pimage.image.size.height));
//            CGContextClip(ctx);
//            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, pimage.image.size.width, pimage.image.size.height), pimage.image.CGImage);
//            pimage.image = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//        }
//        [[AsyncImageLoader sharedLoader] cancelLoadingURL:vimage.imageURL];
//        pimage.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:i]objectForKey:@"prodimg1"]]];
//        [pimagearray2 addObject:pimage];
//    }
//    
//
//    if ([searchText.text isEqualToString:@""])
//    {
//        aray_2=[[NSMutableArray alloc]init];
//        aray_2=singlogin.searchItemArray;
//        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"vendorname" ascending:YES];
//        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
//        sortedarray1 = [aray_2 sortedArrayUsingDescriptors:sortedarray1];
//        
//        pimagearray2=[[NSMutableArray alloc]init];
//        vimagearray1=[[NSMutableArray alloc]init];
//        pimagearray2=parray2;
//        vimagearray1=varray1;
//    }
//    [itemsTbl reloadData];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
    [ProgressHUD show:@"Loading..." Interaction:NO];
//    [self performSelector:@selector(vendorlisting1) withObject:nil afterDelay:0.2f];
    [self vendorlisting1];
    [itemsTbl reloadData];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [keyboardControls setActiveField:textField];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
}


-(void)viewWillAppear:(BOOL)animated
{
    [self cartTotalView];
}



- (IBAction)favrut:(id)sender
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [ProgressHUD showError:@""];
        
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(favrt) withObject:nil afterDelay:0.5f];
    }
    
}



-(void)favrt
{
    Favorites *view = [[Favorites alloc]initWithNibName:@"Favorites" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
    
}






- (IBAction)chat:(id)sender
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        NSLog(@"Please Login to add cart");
        [ProgressHUD showError:@""];
        
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(chatview) withObject:nil afterDelay:0.5f];
    }
}


-(void)chatview
{
    RecentView *chat = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:chat animated:NO];
    [ProgressHUD showSuccess:@""];
}




- (IBAction)vendorProductbtn:(id)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorProductBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorProductBtn
{
    Vendor_Product *view = [[Vendor_Product alloc]initWithNibName:@"Vendor_Product" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorChat:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorchatBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorchatBtn
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)vendorItems:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(vendorItemsBtn)withObject:Nil afterDelay:0.5f];
    }
    else
    {
        view1.hidden = NO;
        [indicator stopAnimating];
    }
    
}


-(void)vendorItemsBtn
{
    Vendor_items *view = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
    
    switch (row)
    {
        case 0:
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                [mySLComposerSheet setInitialText:@"Love Vetrina App!"];
                
                //[mySLComposerSheet addImage:[UIImage imageNamed:@"myImage.png"]];
                
                [mySLComposerSheet addURL:[NSURL URLWithString:@"https://appsto.re/us/JaID6.i"]];
                
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            break;
        case 1:
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Love Vetrina App!"];
                
                //[mySLComposerSheet addImage:[UIImage imageNamed:@"myImage.png"]];
                
                [mySLComposerSheet addURL:[NSURL URLWithString:@"https://appsto.re/us/JaID6.i"]];
                
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            break;
        case 2:
        {
            NSString* url = [NSString stringWithFormat:@"whatsapp://send?text=https://appsto.re/us/JaID6.i"];
            NSURL *whatsappURL = [NSURL URLWithString:url];
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
            {
                [[UIApplication sharedApplication] openURL: whatsappURL];
            }
        }
            break;
        default:
            break;
    }
    
}



-(void)ratestatus
{
    NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/rate_status.php"];
    
    NSLog(@"strURL:%@", strULR);
    
    
    NSURL * url=[NSURL URLWithString:strULR];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.userid];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *emailstr = [[NSString alloc]initWithFormat:@"1"];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"rate_status\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",emailstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableArray *eventarray=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *msg = [[eventarray valueForKey:@"message"]objectAtIndex:0];
    
    NSLog(@"msg -- %@",msg);
    
    if([[[eventarray valueForKey:@"message"] objectAtIndex:0] isEqual:@" Updated"])
    {
        NSMutableDictionary *details=[eventarray valueForKey:@"detail"];
        NSLog(@"GetArray--%@",details);
        
        singlogin.rate_status = [details valueForKey:@"rate_status"];
    }
    else
    {
        
    }
}



- (IBAction)cancle:(id)sender
{
    reportView.hidden = YES;
}

- (IBAction)flagReprt1:(id)sender

{
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
        reson = reportBtn1.currentTitle;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self report];
        
    }
}


- (IBAction)respnsvReport2:(id)sender

{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        reson = reportBtn2.currentTitle;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self report];
        
    }
}


- (IBAction)reprtSpam3:(id)sender

{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        reson = reportBtn3.currentTitle;
        [indicator startAnimating];
        [self report];
    }
}


-(void)report

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to Report");
        [view1 setHidden:NO];
    }
    else
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/additemreport.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reportdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reporttime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",vendorImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",vendorIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",vendorNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",productIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *userNamee1=[[NSString alloc]initWithFormat:@"%@",productNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *userNamee2=[[NSString alloc]initWithFormat:@"%@",prodImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee2] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *userNamee3=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee3] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *resone=[[NSString alloc]initWithFormat:@"%@",reson];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reason\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",resone] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"THANK YOU" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }
    indicator.hidden=YES;
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    reportView.hidden = YES;
}




- (IBAction)homeBtn:(id)sender
{
    
    
    [indicator setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
    
}


-(void)home
{
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
