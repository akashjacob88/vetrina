//
//  Item_CellAR.m
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Item_CellAR.h"
#import "cell.h"
#import "AsyncImageView.h"
#import "UIImageView+WebCache.h"

@implementation Item_CellAR
{
    cell *arry;
}

- (void)awakeFromNib
{
    
    [self cardsatup];

}


-(void)cardsatup
{
    [self.backlabl setAlpha:1];
    self.backlabl.layer.shadowColor = [UIColor blackColor].CGColor;
    self.backlabl.layer.masksToBounds = NO;
    self.backlabl.layer.cornerRadius = 2; // redius corners
    self.backlabl.layer.shadowOffset = CGSizeMake(0, .2f);
    self.backlabl.layer.shadowRadius = 3;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.backlabl.bounds];
    self.backlabl.layer.shadowPath = path.CGPath;
    self.backlabl.layer.shadowOpacity = 0.8;
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
