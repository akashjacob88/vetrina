//
//  EdititemsAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"



@interface EdititemsAR : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIScrollViewDelegate,UITextFieldDelegate,BSKeyboardControlsDelegate>


{
    Singleton *singlogin;
   
    UIActionSheet *actionsheet1;
    
    /* -------- Array ------- */
    
    NSMutableArray *vendorNameArray,*dataArray,*aray_2,*subCatArry;
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    
    NSMutableArray *imageArr,*img2;
    
    NSArray *sectionAnimals;
    
    IBOutlet UIButton *soldOut;
    IBOutlet UIActivityIndicatorView *indicatr;
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2,*stringTwo3;
    NSString *itme;
    
    NSString *instaNameSave, *shopNmberSave, *shopCatSave, *emailSave ,*addBtnStr;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    
    
    BSKeyboardControls *keyboardControls;
    
    UIImagePickerController *camerapicker;
    UIImage *cimage,*cimage2,*cimage3,*cimage4,*cimage5;
    NSString *btnStr,*btnStr2,*btnStr3;
    UIImageView  *Img;
    
    
    IBOutlet UIView *view1;
    IBOutlet UIButton *addImgs;
    IBOutlet UIScrollView *scrolView;
    IBOutlet UIImageView *image1;
    
    IBOutlet UITextField *productNme;
    IBOutlet UITextView *ProductDec;
    
    IBOutlet UITextField *priceTxt;
    IBOutlet UITextField *priceTxt2;
    IBOutlet UITextField *quntyText;
    IBOutlet UILabel *cat1;
    IBOutlet UILabel *subcat1;
    
    IBOutlet UILabel *item;
    IBOutlet UIButton *deleteBtn;
    IBOutlet UILabel *yesLbl;
    IBOutlet UIButton *ashPriceOut;
  
    NSString *askForPrice,*askForPrice1,*soldStr,*soldStr1,*showPrice;
  
    IBOutlet UITextField *arabicPrductDic;
    IBOutlet UITextView *arabicproduct;
    IBOutlet UIButton *soldOutOut;
    IBOutlet UILabel *showPriceView;
    
    IBOutlet UILabel *kdLabl;
    IBOutlet UILabel *dotLbl;
    IBOutlet UIView *askforPriceLbl;
    IBOutlet UIView *soldOutLbl;
    IBOutlet UIView *PriceView;


    IBOutlet UILabel *askPriceView;
    IBOutlet UIButton *soldoutView;
    IBOutlet UIView *topBar;
    
}

@property (strong, nonatomic) NSString *editItem;
- (IBAction)back:(id)sender;

@end
