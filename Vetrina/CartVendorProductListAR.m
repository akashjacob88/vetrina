//
//  CartVendorProductListAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CartVendorProductListAR.h"
#import "CartVendorProductCellAR.h"
#import "AsyncImageView.h"
#import "DeclarePriceAR.h"
#import "ConfirmProductVenderAR.h"
#import "JSON.h"
#import <Parse/Parse.h>
#import "ChatView.h"
#import "ProgressHUD.h"

#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "push.h"
#import "chat_New.h"

@interface CartVendorProductListAR ()
{
    NSMutableArray *users;
    
    NSArray *dialog_Objects;
    QBUUser *currentUser;
    NSMutableArray *users_array;
    QBChatDialog *chatDialog;
    NSArray *itemArray;

}

@end

@implementation CartVendorProductListAR

- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users21) {
        
        
        [users_array addObjectsFromArray:users21];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
        
        [seen addObject:[NSString stringWithFormat:@"%ld",o]];
        NSUInteger y = 0;
        
        while (y < [users_array count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[users_array objectAtIndex:y];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [users_array removeObjectAtIndex:y];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                y++;
            }
        }
        
        
        
        
        [self chat22];
        
    } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}

-(void)chat

{
    // myei4MTHya
    
    currentUser = [QBUUser user];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
    //    NSInteger i = [ [ NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"] ] integerValue ];
    
    
    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            [self retrieveAllUsersFromPage:1];
            
        }
    }
     
     
     
     ];
    
    
}


-(void)chat22{
    
    
    
    
    
    
    QBChatDialog *test_dialog = nil;
    
    
    NSString *str=[[NSString alloc]init];
    str =@"new";
    
    
    for (int y=0; y<dialog_Objects.count; y++) {
        
        
        test_dialog=[dialog_Objects objectAtIndex:y];
        
        
        NSLog(@"%@",test_dialog.occupantIDs);
        
        NSArray *objs=[[NSArray alloc]initWithArray:test_dialog.occupantIDs];
        
        
        
        
        
        
        for (int x =0; x<1; x++) {
            NSInteger c =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:0]] integerValue ];
            NSInteger d =[ [ NSString stringWithFormat: @"%@",[objs objectAtIndex:1]] integerValue ];
            NSInteger a = [ [ NSString stringWithFormat: @"%@",singletonn.user_id] integerValue ];
            NSInteger b = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
            
            
            if (c==a ) {
                
                if (d ==b) {
                    str=@"got";
                    break;
                }
                
            }else if (c==b) {
                
                if (d==a) {
                    str=@"got";
                    
                    break;
                }
                
                
            }
            
        }
        
        if ([str isEqualToString:@"got"]) {
            break;
        }
        
    }
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    
    // singletonn.chat_user=[[dataArray objectAtIndex:0]valueForKey:@"vendorobjectid"];
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singletonn.chat_user] integerValue ];
    
    chatDialog.occupantIDs = @[@(o) ];
    
    
    
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        
        [self actionChat:createdDialog.ID];
        
        
        
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response.error);
        
        [ProgressHUD showSuccess:@""];
    }];
    
    //    chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
    //
    //    chatDialog.name = @"Chat with Bob, Sam, Garry";
    //
    //    chatDialog.occupantIDs = @[@(9045183), @(9045219)]; // change id with your register user's id
    //
    //    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
    //
    //
    //          NSLog(@"No Error: %@", response);
    //        [self chat1];
    //
    //
    //    } errorBlock:^(QBResponse *response) {
    //
    //          NSLog(@"Error: %@", response);
    //
    //    }];
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    singletonn=[Singleton instance];
    userNameLbl.text=singletonn.usrName;
    NSLog(@"%@",singletonn.productArray);
    
    users = [[NSMutableArray alloc]init];
    

    
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]initWithArray:dialogObjects];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
    singletonn.usrObjctId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"userobjectid"];

    
    for (NSInteger i=0; i< singletonn.productArray.count; i++)
    {
        statusStr= [[singletonn.productArray objectAtIndex:i]valueForKey:@"statusid"];
        if ([statusStr isEqualToString:@"2"])
        {
            statusStr=@"أكد";
        }
        if ([statusStr isEqualToString:@"1"])
        {
            statusStr=@"أعلن التسعير";
            i=singletonn.productArray.count;
        }
        if ([statusStr isEqualToString:@"3"])
        {
            statusStr=@"ابدأ التوصيل";
            i=singletonn.productArray.count;
        }
    }
    
    
    [prodListingTable reloadData];
    indicator.hidden=YES;
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    [sendMessageTxt setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    whatsappView.hidden=YES;
    locationView.hidden=YES;
    
    userAddressLbl.text=[[singletonn.productArray objectAtIndex:0] valueForKey:@"useraddress"];

}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(IBAction)Back:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeBtn)withObject:Nil afterDelay:2.0f];
}


-(void)homeBtn
{
    [self.navigationController popViewControllerAnimated:NO];
    [ProgressHUD showSuccess:@""];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [singletonn.productArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CartVendorProductCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"cartVendorCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CartVendorProductCellAR" bundle:nil] forCellReuseIdentifier:@"cartVendorCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cartVendorCell"];
    }
    
//    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
//    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
//    [cell.contentView addSubview:v11];
//    cell.backgroundColor=[UIColor whiteColor];
//    return cell;

    
    
    

    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(CartVendorProductCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    cell.prodName.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"productname"];
    NSString *price = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    
    
    if ([price isEqualToString:@"Ask for Price"])
    {
        cell.priceLbl.text=@"Ask for Price";
    }
    else if ([price isEqualToString:nil]) {
        cell.priceLbl.text=@"Ask for Price";
    }
    else
    {
        cell.priceLbl.text=[[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    }
    
    cell.qtyLbl.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"quantityprod"];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
    cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[singletonn.productArray objectAtIndex:indexPath.row]objectForKey:@"prodimage"]]];
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    UIButton *statusBtn;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 45)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 45)];
            
        }
    }
    [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    
    
    if ([statusStr isEqualToString:@"أكد"])
    {
        [statusBtn addTarget:self action:@selector(confirmBtn:) forControlEvents:UIControlEventTouchUpInside];
        [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    }
    if ([statusStr isEqualToString:@"أعلن التسعير"])
    {
        [statusBtn addTarget:self action:@selector(declarePriceBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    if ([statusStr isEqualToString:@"ابدأ التوصيل"])
    {
        [statusBtn addTarget:self action:@selector(deliveryBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    if ([statusStr isEqualToString:@"4"]||[statusStr isEqualToString:@"تسليم أرسلت"])
    {
        statusStr=@"تم التوصيل";
        [statusBtn setBackgroundColor:[UIColor clearColor]];
        statusBtn.layer.borderColor=[UIColor whiteColor].CGColor;
        statusBtn.layer.borderWidth=1.5f;
        statusBtn.clipsToBounds=YES;
    }
    [statusBtn setTitle:statusStr forState:UIControlStateNormal];
    
    statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
    [headerView addSubview:statusBtn];
    headerView.backgroundColor=[UIColor clearColor];
    return headerView;

}


-(void)deliveryBtn:(UIButton *)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to deliver this order?" delegate:self cancelButtonTitle:@"Deliver" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 9;
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
            orderIdStr=[[singletonn.productArray objectAtIndex:0] valueForKey:@"orderid"];
            indicator.hidden=NO;
            [indicator startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];
            
            //locationView.hidden=NO;
            [self performSelector:@selector(startDelivery) withObject:nil afterDelay:0.2f];
        }

    }
}



-(void)startDelivery
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/orderdeliver.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",orderIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
        
        itemArray=[[NSArray alloc]initWithArray:[eventArray valueForKey:@"detail"]];

        
        
        
      //  [self prodListing];
        singletonn.usrObjctId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"userobjectid"];
        
        
        
        
        
        
        
         NSString *message =[NSString stringWithFormat:@"%@ Sent your order for delivery",singletonn.userNameStr                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
        
     //   NSString *message = @"Sent your order for delivery";
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:singletonn.usrObjctId successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];

        
        [self addtocartNew];
        
        

    }else{
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    //locationView.hidden = NO;
    whatsappView.hidden=NO;
    }
}


-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singletonn.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        
    }
    else
    {
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"productname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]initWithKey:@"odertime"  ascending:NO];
        NSArray *sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
        NSArray *sortedArray2 = [dataArray sortedArrayUsingDescriptors:sortDescriptors1];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray2 mutableCopy];
        

        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"oderdate" ascending:NO];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        finalArray=[[NSMutableArray alloc]init];
        singletonn.productArray=[[NSMutableArray alloc]init];
        
        for(NSInteger i =0;i<sortedArray.count;i++)
        {
            NSString *stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"userid"];
            if ([stringOne isEqualToString:singletonn.userid]) {
                finalArray=[sortedArray objectAtIndex:i];
                [singletonn.productArray addObject:finalArray];
            }
            
        }
        
        
        for (NSInteger i=0; i< singletonn.productArray.count; i++)
        {
            statusStr= [[singletonn.productArray objectAtIndex:i]valueForKey:@"statusid"];
            if ([statusStr isEqualToString:@"2"]) {
                statusStr=@"أكد";
            }
            if ([statusStr isEqualToString:@"1"]) {
                statusStr=@"أعلن التسعير";
                i=singletonn.productArray.count;
            }
            if ([statusStr isEqualToString:@"3"]) {
                statusStr=@"ابدأ التوصيل";
                i=singletonn.productArray.count;
            }
            
        }
        
        
    }
    [prodListingTable reloadData];
}



-(void)confirmBtn:(UIButton *)sender
{
    ConfirmProductVenderAR *decl=[[ConfirmProductVenderAR alloc]initWithNibName:@"ConfirmProductVenderAR" bundle:nil];
    [self.navigationController pushViewController:decl animated:NO];
    
}
-(void)declarePriceBtn:(UIButton *)sender
{
    DeclarePriceAR *decl=[[DeclarePriceAR alloc]initWithNibName:@"DeclarePriceAR" bundle:nil];
    [self.navigationController pushViewController:decl animated:NO];
}
-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 45;
}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}




-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -253; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == sendMessageTxt)
    {
        [self animateTextField:textField up:YES];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == sendMessageTxt)
    {
        [self animateTextField:textField up:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    userNameLbl.text=singletonn.usrName;
    NSLog(@"%@",singletonn.productArray);
    
    for (NSInteger i=0; i< singletonn.productArray.count; i++)
    {
        statusStr= [[singletonn.productArray objectAtIndex:i]valueForKey:@"statusid"];
        if ([statusStr isEqualToString:@"2"]) {
            statusStr=@"أكد";
        }
        if ([statusStr isEqualToString:@"1"]) {
            statusStr=@"أعلن التسعير";
            i=singletonn.productArray.count;
        }
        if ([statusStr isEqualToString:@"3"]) {
            statusStr=@"ابدأ التوصيل";
            i=singletonn.productArray.count;
        }
        
        
    }
    
    [prodListingTable reloadData];
    [self prodListing];
    indicator.hidden=YES;
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    [sendMessageTxt setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
}


-(IBAction)whatsapp:(id)sender
{ NSString *proNamestr=@"";
    NSInteger  valuer2 = 0;
    for(NSInteger i =0;i<singletonn.productArray.count;i++)
    {
        NSString *odrDate=[[singletonn.productArray objectAtIndex:i]valueForKey:@"orderdate"];
        NSString *ordrtime=[[singletonn.productArray objectAtIndex:i] valueForKey:@"ordertime"];
        NSString *prductname=[[singletonn.productArray objectAtIndex:i] valueForKey:@"productname"];
        NSString *qntityprod=[[singletonn.productArray objectAtIndex:i] valueForKey:@"quantityprod"];
        NSString *amntpay=[[singletonn.productArray objectAtIndex:i] valueForKey:@"amountpay"];
        NSString *usrname=[[singletonn.productArray objectAtIndex:i] valueForKey:@"username"];
        NSString *homaddrs=[[singletonn.productArray objectAtIndex:i] valueForKey:@"useraddress"];
        NSString *country = [[singletonn.productArray objectAtIndex:i] valueForKey:@"country"];
        NSString *city = [[singletonn.productArray objectAtIndex:i] valueForKey:@"city"];
        
        if (i==0)
        {
            proNamestr=prductname;
        }
        else
        {
            proNamestr=[NSString stringWithFormat:@"%@,%@",proNamestr,prductname];
        }
        NSInteger valuer = [qntityprod integerValue];
        valuer2= valuer2+valuer;
        jointText = [NSString stringWithFormat:@"Order Date:%@,Order Time:%@,Item Name: %@,Quantity: %li,Price Total: %@,Name: %@,Address: %@,Country: %@,City: %@",odrDate,ordrtime,proNamestr,(long)valuer2,amntpay,usrname,homaddrs,country,city];
        
        
    }
    
    
    [whatsappView setHidden:YES];
    [locationView setHidden:YES];
    
    NSString *esc_addr =  [jointText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* url = [NSString stringWithFormat:@"whatsapp://send?text=%@", esc_addr];
    NSURL *whatsappURL = [NSURL URLWithString:url];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    
}
-(IBAction)location:(id)sender
{
    locationView.hidden=NO;
}


-(IBAction)chat:(id)sender
{
    
    [self performSelector:@selector(chat) withObject:Nil afterDelay:2.0f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
}

-(void)chat1
{
    for (NSInteger i=0; i< users.count; i++)
    {
        
        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
        if ([name containsString:singletonn.usrObjctId])
        {
            
            
            i = users.count;
            
        }
    }
    
    [[PFUser currentUser] fetch];
    
    if ([PFUser currentUser])
        NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
    singletonn.objectIdStr = [[PFUser currentUser] objectId];
    
    NSString *id1 = name;
    NSString *id2 = singletonn.objectIdStr;
    NSString *groupId = ([id1 compare:id2] < 0) ? [NSString stringWithFormat:@"%@%@", id1, id2] : [NSString stringWithFormat:@"%@%@", id2, id1];
    [self actionChat:groupId];
    
}


- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
    
//    ChatView *chatView = [[ChatView alloc] initWith:groupId];
//    chatView.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatView animated:YES];
//    [ProgressHUD showSuccess:@""];
}



-(void)Objectuser

{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singletonn.vUniqueId];
    NSLog(@"get data=%@",post);
    
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/objectvendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    if(![[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        
        valu = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        
    }
    else
    {
        valu=[[NSMutableArray alloc]init];
        
    }
    
    
    if([valu count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You don’t have any Message." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [ProgressHUD showError:@""];
        
    }
    else
    {
        if (valu.count==1)
        {
            
            NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
            NSLog(@"Unique ID %@",uniqueid);
            NSString *userobid=[[valu objectAtIndex:0] valueForKey:@"user_objectid"];
            NSString *vendorobid=[[valu objectAtIndex:0] valueForKey:@"vendor_objectid"];
            //NSString *userid=[[valu objectAtIndex:0] valueForKey:@"userid"];
            NSString *username=[[valu objectAtIndex:0] valueForKey:@"username"];
            NSString *vendorname=[[valu objectAtIndex:0] valueForKey:@"fullname_shopname"];
            NSString *image=[[valu objectAtIndex:0] valueForKey:@"image"];
            NSString *groupid=[[valu objectAtIndex:0] valueForKey:@"group_orderid"];
            singletonn.userIdStr = userobid;
            singletonn.venId = vendorobid;
            // singletonn.userid = userid;
            singletonn.userNameStr = username;
            singletonn.venName = vendorname;
            singletonn.venPic = image;
            singletonn.venIdList = groupid;
            
        }
        else
        {
            NSInteger a =valu.count-1;
            
            NSString *uniqueid=[[valu objectAtIndex:a]valueForKey:@"id"];
            NSLog(@"Unique ID %@",uniqueid);
            NSString *userobid=[[valu objectAtIndex:a] valueForKey:@"user_objectid"];
            NSString *vendorobid=[[valu objectAtIndex:a] valueForKey:@"vendor_objectid"];
            //NSString *userid=[[valu objectAtIndex:a] valueForKey:@"userid"];
            NSString *username=[[valu objectAtIndex:a] valueForKey:@"username"];
            NSString *vendorname=[[valu objectAtIndex:a] valueForKey:@"fullname_shopname"];
            NSString *image=[[valu objectAtIndex:a] valueForKey:@"image"];
            NSString *groupid=[[valu objectAtIndex:a] valueForKey:@"group_orderid"];
            singletonn.userIdStr = userobid;
            singletonn.venId = vendorobid;
            //singletonn.userid = userid;
            singletonn.userNameStr = username;
            singletonn.venName = vendorname;
            singletonn.venPic = image;
            singletonn.venIdList = groupid;
        }
        [self userObId];
    }
}


-(void)userObId

{
    NSString *id1 =  singletonn.userIdStr;
    NSString *id2 =   singletonn.venId;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSString *groupIdd = ([id1 compare:id2] < 0) ? [NSString stringWithFormat:@"%@%@", id1, id2] : [NSString stringWithFormat:@"%@%@", id2, id1];
    singletonn.venIdList = groupIdd;
    ChatView *chatView = [[ChatView alloc] initWith:singletonn.venIdList];
    chatView.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:chatView animated:YES];
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    [indicator setHidesWhenStopped:YES];
}


-(IBAction)shareDetails:(id)sender
{
    whatsappView.hidden=NO;
}


-(IBAction)nothanx:(id)sender
{
    [whatsappView setHidden:YES];
    [locationView setHidden:YES];
    
}




-(IBAction)sms:(id)sender
{
    NSString *odrDate=[[singletonn.productArray objectAtIndex:0]valueForKey:@"orderdate"];
    NSString *ordrtime=[[singletonn.productArray objectAtIndex:0] valueForKey:@"ordertime"];
    NSString *prductname=[[singletonn.productArray objectAtIndex:0] valueForKey:@"productname"];
    NSString *qntityprod=[[singletonn.productArray objectAtIndex:0] valueForKey:@"quantityprod"];
    NSString *amntpay=[[singletonn.productArray objectAtIndex:0] valueForKey:@"amountpay"];
    NSString *usrname=[[singletonn.productArray objectAtIndex:0] valueForKey:@"username"];
    NSString *homaddrs=[[singletonn.productArray objectAtIndex:0] valueForKey:@"useraddress"];
    jointText = [NSString stringWithFormat:@"Order Date:%@,Order Time:%@,Item Name: %@,Quantity: %@,Price Total: %@,Name: %@,Address: %@",odrDate,ordrtime,prductname,qntityprod,amntpay,usrname,homaddrs];
    
    
    [whatsappView setHidden:YES];
    [locationView setHidden:YES];
    
    
    if([MFMessageComposeViewController canSendText])
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSString *send_str=[[NSString alloc]initWithFormat:@"%@",jointText];
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self; [messageController setRecipients:nil]; [messageController setBody:send_str];
            [self presentViewController:messageController animated:YES completion:nil];
            
        }];
    }
    else
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        
        return;
        
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled: break;
        case MessageComposeResultFailed: {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break; }
        case MessageComposeResultSent: break;
        default: break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)addtocartNew
{
    
    
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/orderhistoryadd.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"userid"] objectAtIndex:0]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // NSString *ordr=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"orderid"] objectAtIndex:0]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    // NSLog(@"print mystring==%@",returnString);
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"%@",eventarray);

    
    
    
    
    
    
//    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addorderhistory.php"];
//    NSLog(@"strURL:%@",strURL);
//    NSURL * url=[NSURL URLWithString:strURL];
//    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
//    [theLoginRequest setHTTPMethod:@"POST"];
//    
//    NSMutableData *body = [NSMutableData data];
//    NSString *boundary = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
//    
//    NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"userid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *productidStr=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"productid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",productidStr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *imagee=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"prodimage"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",imagee] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *price1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"unitprice"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",price1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *pic1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"userprofilepic"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userprofilepic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",pic1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *object=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"userobjectid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",object] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    NSString *object1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"vendorobjectid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",object1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *askprice1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"negotiable_price"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",askprice1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *statusidStr=@"4";
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    NSString *final=[[itemArray valueForKey:@"quantityprod"] objectAtIndex:0];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantityprod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",final] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *name11=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"productname"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",name11] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    
//    NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"orderdate"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"ordertime"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"vendorpic"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"vendorid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    NSString *vendor_obj=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"vendorobjectid"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",vendor_obj] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"vendorname"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"username"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [theLoginRequest  setHTTPBody:body];
//    
//    NSString *delPrice=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"deliveryprice"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"deliveryprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",delPrice] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [theLoginRequest  setHTTPBody:body];
//    
//    NSString *userAddress=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"useraddress"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"useraddress\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",userAddress] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [theLoginRequest  setHTTPBody:body];
//    
//       
//    NSString *amountpay=[[NSString alloc]initWithFormat:@"%@",[[itemArray valueForKey:@"amountpay"] objectAtIndex:0]];
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"amountpay\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",amountpay] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [theLoginRequest  setHTTPBody:body];
//    
//    
//    
//    
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
//    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    // NSLog(@"print mystring==%@",returnString);
//    NSDictionary *eventarray=[returnString JSONValue];
//    NSLog(@"%@",eventarray);
    
    
    [self deleteView1];
    
    
    
}



-(void)deleteView1
{
    NSString *post = [NSString stringWithFormat:@"orderid=%@",[[itemArray valueForKey:@"orderid"] objectAtIndex:0]];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delorder.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
    //locationView.hidden = NO;
    whatsappView.hidden=NO;
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    
}

















@end
