//
//  User_SettingsAR.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface User_SettingsAR : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

{
    Singleton *singlogin;
    
    IBOutlet UITextField *newPass;
    IBOutlet UITextField *confirmPass;
    
    IBOutlet UITextField *oldPassword;
    NSMutableArray *addressArray;
    UITextField *user2;
    
    IBOutlet UIView *view1;
    UIImageView *logOutImg, *image2 , *proImage , *changepasImg;
    UIButton *addressBtn, *logoutBtn, *confrmBtn , *changePassBtn;
    
    IBOutlet UITableView *addresTbl;

}
@property(strong,nonatomic) UITextField *txtField_Selected;

@property(strong,nonatomic) NSString *test;
@property (strong, nonatomic) NSString *editaddres;



@end
