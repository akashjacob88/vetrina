//
//  User_SettingsAR.m
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "User_SettingsAR.h"
#import "Address_CellAR.h"
#import "Home_VetrinaARViewController.h"
#import "Edit_AddressAR.h"
#import "JSON.h"
#import "User_Settings.h"
#import "Walkthrough.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ProgressHUD.h"
#import "ViewController.h"

#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"



@interface User_SettingsAR ()
{
    
    IBOutlet UIView *topBar;
}

@end

@implementation User_SettingsAR
@synthesize test,editaddres;


- (void)viewDidLoad
{
    [super viewDidLoad];
    view1.hidden = YES;
    singlogin = [Singleton instance];
    user2 = [[UITextField alloc]init];
    user2.text = singlogin.usrName ;
    user2.delegate = self;
    user2.userInteractionEnabled=NO;
    
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    

    addresTbl.sectionHeaderHeight = 91;
    addresTbl.sectionFooterHeight = 170;
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, newPass.frame.size.height)];
    newPass.leftView = leftView1;
    newPass.leftViewMode = UITextFieldViewModeAlways;
    [newPass setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, confirmPass.frame.size.height)];
    confirmPass.leftView = leftView2;
    confirmPass.leftViewMode = UITextFieldViewModeAlways;
    [confirmPass setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    UIView *leftView3= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, oldPassword.frame.size.height)];
    oldPassword.leftView = leftView3;
    oldPassword.leftViewMode = UITextFieldViewModeAlways;
    [oldPassword setValue:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];

    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    [self addressListing];

}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




- (IBAction)back2:(id)sender

{
    view1.hidden = YES;
}

- (IBAction)back:(id)sender

{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Changes have not been saved, are you sure you want to leave?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
//    [alert show];
//    alert.tag = 9;
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (alertView.tag == 10)
    {
        if (buttonIndex==0)
        {
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"passWord"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"instagramID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            singlogin.loginStatus = nil;
            singlogin.userid = nil;
            [PFUser logOut];
            ParsePushUserResign();
            PostNotification(NOTIFICATION_USER_LOGGED_OUT);
            LoginUser(self);
            Walkthrough *home = [[Walkthrough alloc]initWithNibName:@"Walkthrough" bundle:NULL];
            [self.navigationController pushViewController:home animated:YES];
        }
    }

}



-(void)addressListing

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/addresslistuser.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        addressArray=[[NSMutableArray alloc]init];
    }
    else
    {
        addressArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return addressArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    Address_CellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"addreAR_Cell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Address_CellAR" bundle:nil] forCellReuseIdentifier:@"addreAR_Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"addreAR_Cell"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(Address_CellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    cell.addressLbl.text = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    singlogin.countryAddres = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"country"];
    singlogin.cityAddr = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"city"];
    singlogin.addresaddr=[[addressArray objectAtIndex:indexPath.row]valueForKey:@"address"];
    singlogin.adresId = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    singlogin.userFonNo = [[addressArray objectAtIndex:indexPath.row]valueForKey:@"phoneno"];
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(nextPage) withObject:nil afterDelay:0.5f];
    
}

-(void)nextPage
{
    Edit_AddressAR *edit = [[Edit_AddressAR alloc]initWithNibName:@"Edit_AddressAR" bundle:nil];
    edit.editaddres = @"edit";
    [self.navigationController pushViewController:edit animated:YES];
    [ProgressHUD showSuccess:@""];
    
}





- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *headerView = [[UIView alloc] init];
    UILabel *userSettings;
    UIView *anitame;
    UILabel *addressLbl;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            user2.frame = CGRectMake(0, 0, 255, 45);
            [user2 setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 46, 255, 45)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(255, 0, 50, 45);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(255, 46, 50, 45);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            user2.frame = CGRectMake(0, 0, 255, 45);
            [user2 setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 46, 255, 45)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(255, 0, 50, 45);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(255, 46, 50, 45);

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            user2.frame = CGRectMake(0, 40, 308, 50);
            [user2 setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 91, 308, 50)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(308, 40, 51, 50);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(308, 91, 51, 50);
            
            
            userSettings = [[UILabel alloc]initWithFrame:CGRectMake(54, 5, 304, 30)];
            anitame = [[UIView alloc]initWithFrame:CGRectMake(0, 91, 359, 50)];
            addressLbl = [[UILabel alloc]initWithFrame:CGRectMake(54, 147, 304, 30)];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            user2.frame = CGRectMake(0, 0, 348, 50);
            [user2 setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            user2.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
            changePassBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 46, 348, 50)];
            [changePassBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
            proImage= [[UIImageView alloc]init];
            proImage.frame=CGRectMake(348, 0, 50, 50);
            changepasImg = [[UIImageView alloc]init];
            changepasImg.frame=CGRectMake(348, 46, 50, 50);
        }
    }
    
    anitame.backgroundColor = [UIColor whiteColor];
    anitame.layer.shadowRadius = 2.0f;
    anitame.layer.shadowOffset = CGSizeMake(0, 2);
    anitame.layer.shadowColor = [UIColor blackColor].CGColor;
    anitame.layer.shadowOpacity = 0.5f;
    [headerView addSubview:anitame];
    
    
    [changePassBtn setTitle:@"تغيير كلمة المرور" forState:UIControlStateNormal];
    [changePassBtn setTitleColor:[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1] forState:UIControlStateNormal];
    changePassBtn.backgroundColor=[UIColor whiteColor];
    changePassBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [changePassBtn addTarget:self action:@selector(changePassBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:changePassBtn];
    
    user2.backgroundColor = [UIColor whiteColor];
    user2.textAlignment = NSTextAlignmentRight;
    [headerView addSubview:user2];
    
    proImage.image=[UIImage imageNamed:@"profile_icon.png"];
    proImage.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:proImage];
    
    changepasImg.image=[UIImage imageNamed:@"lock.png"];
    changepasImg.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:changepasImg];
    
    
    userSettings.text = @"إعدادات المستخدم";
    [userSettings setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
    userSettings.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    userSettings.backgroundColor=[UIColor clearColor];
    userSettings.textAlignment = NSTextAlignmentRight;
    [headerView addSubview:userSettings];
    
    
    addressLbl.text = @"عنوان";
    [addressLbl setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
    addressLbl.textColor = [UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    addressLbl.backgroundColor=[UIColor clearColor];
    addressLbl.textAlignment = NSTextAlignmentRight;
    [headerView addSubview:addressLbl];
    
    return headerView;
    
}







- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    UIView *footerView = [[UIView alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 304, 45)];
            [addressBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 65, 255, 45)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(255, 65, 50, 45);
//            
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 304, 45)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 304, 45)];
            [addressBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 65, 255, 45)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(255, 65, 50, 45);
//            
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 304, 45)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 359, 50)];
            [addressBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 65, 308, 50)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(308, 65, 50, 50);
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 120, 359, 50)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 1, 398, 50)];
            [addressBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
//            logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 70, 348, 50)];
//            [logoutBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:17]];
//            logOutImg = [[UIImageView alloc]init];
//            logOutImg.frame=CGRectMake(348, 70, 50, 50);
//            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 130, 398, 50)];
//            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
        }
        
    }
    
      
    [addressBtn setTitle:@"إضافة عنوان جديد" forState:UIControlStateNormal];
    addressBtn.backgroundColor=[UIColor whiteColor];
    [addressBtn.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:20]];
    [addressBtn setTitleColor:[UIColor colorWithRed:237.0f/255 green:92.0f/255 blue:118.0f/255 alpha:1] forState:UIControlStateNormal];
    addressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    addressBtn.layer.shadowRadius = 2.0f;
    addressBtn.layer.shadowOffset = CGSizeMake(0, 2);
    addressBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    addressBtn.layer.shadowOpacity = 0.5f;
    [addressBtn addTarget:self action:@selector(addAdrs:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:addressBtn];

    
//    [logoutBtn setTitle:@"تسجيل الخروج" forState:UIControlStateNormal];
//    [logoutBtn setTitleColor:[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1] forState:UIControlStateNormal];
//    logoutBtn.backgroundColor=[UIColor whiteColor];
//    logoutBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//    [logoutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:logoutBtn];
//    
//    logOutImg.image=[UIImage imageNamed:@"logout.png"];
//    logOutImg.backgroundColor = [UIColor whiteColor];
//    [footerView addSubview:logOutImg];
//    
//    [confrmBtn setTitle:@"أكد" forState:UIControlStateNormal];
//    confrmBtn.backgroundColor = [UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1];
//    [confrmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:confrmBtn];
    
    return footerView;
}


-(void)confirm:(UIButton *)sender
{
    if (user2.text.length > 0 )
    {
       
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(Submitted)withObject:Nil afterDelay:3.0f];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty username" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


-(void)Submitted
{
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editvendor.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *userstr=[[NSString alloc]initWithFormat:@"%@",user2.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",userstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *passStr=[[NSString alloc]initWithFormat:@"%@",newPass.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",passStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [ProgressHUD showSuccess:@"Success."];
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat ff;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            ff= 91.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            ff=  181.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            ff=  181.0f;
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            ff=  91.0f;
        }
    }
    return  ff;
}


-(void)logout:(UIButton *)sender

{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Log Out" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 10;
}


-(void)addAdrs:(UIButton *)sender

{
    singlogin.addresaddr = @"";
    singlogin.countryAddres = @"";
    singlogin.cityAddr = @"";
    Edit_AddressAR *edit = [[Edit_AddressAR alloc]initWithNibName:@"Edit_AddressAR" bundle:nil];
    [self.navigationController pushViewController:edit animated:YES];
    
}


- (void)changePassBtn:(UIButton *)sender

{
    view1.hidden = NO;
    
    newPass.text=@"";
    oldPassword.text=@"";
    confirmPass.text=@"";
    
    [oldPassword becomeFirstResponder];
    
}



- (IBAction)userRegin:(id)sender

{
    [user2 resignFirstResponder];
}


- (IBAction)okay:(id)sender

{
    
    if (newPass.text.length < 1  && confirmPass.text.length < 1  && oldPassword.text.length < 1)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![newPass.text isEqualToString:confirmPass.text])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password does not match" message:@"Ensure the passwords entered are identical. Note that passwords case-sensitive." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if (newPass.text.length<8){
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter Password At least 8 Characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
    }else
    {
        
        NSString *post;
        if ([singlogin.loginStatus isEqual:@"User Login"])
        {
            post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
        }
        else if ([singlogin.loginStatus isEqual:@"IG Login"])
        {
            post = [NSString stringWithFormat:@"userid=%@",singlogin.igUniqueId];
        }    NSLog(@"get data=%@",post);
        
        
        
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/updateuserpass.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *idstr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",idstr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *namestr=[[NSString alloc]initWithFormat:@"%@",newPass.text];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
      
        
        
        
        QBUUser *user=[QBUUser user];
        user=[QBSession currentSession].currentUser;
        
        QBUpdateUserParameters *updateParameters = [QBUpdateUserParameters new];
        
        updateParameters.oldPassword=oldPassword.text;
        
        updateParameters.password=newPass.text;
        
        
        [QBRequest updateCurrentUser:updateParameters successBlock:^(QBResponse *response, QBUUser *user) {
            
            [[NSUserDefaults standardUserDefaults]  setObject:newPass.text forKey:@"password"];
            
            NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:savedPassword forKey:@"pass_word"];
            
            NSLog(@"%@",user);
            
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@",response.error);
        }];
        
        
        
        
        
        
        
       [view1 setHidden:YES];
        
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    self.txtField_Selected = textField;
}



#pragma mark - Keyboard Delegates
- (void)keyboardWillShow: (NSNotification *)notification {
    //Get Keyboard Info From Notification
    NSDictionary* info = [notification userInfo];
    
    //Get Minimum Y co-ordinates of Keyboard
    float keyboardMinYValue = CGRectGetMinY([[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue]);
    
    //Get Maximum Y co-ordinates of Selected Textfield
    float txtFldMaxYValue = CGRectGetMaxY([self.txtField_Selected.superview convertRect:self.txtField_Selected.frame toView:nil]);
    
    //Check If Overlapping, Then Scroll View Up
    if (keyboardMinYValue<txtFldMaxYValue) {
        [UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:1.0f initialSpringVelocity:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, (keyboardMinYValue-txtFldMaxYValue));
        } completion:nil];
        
        
        
        
        
    }
}

- (void)keyboardWillHide: (NSNotification *)notification {
    self.view.transform = CGAffineTransformIdentity;
}

#pragma mark - Dismiss Keyboard
-(IBAction)dismissKeyboard:(id)sender {
    [self resignKeyboard:self.view];
}

- (void)resignKeyboard:(UIView *)view {
    for (UIView *subv in view.subviews) {
        if ([subv isKindOfClass:[UITextField class]]) {
            UITextField *txtf = (UITextField *)subv;
            [txtf resignFirstResponder];
        }
        [self resignKeyboard:subv];
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    NSInteger nextTag = textField.tag + 1;
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [textField resignFirstResponder];
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;

    
    
    
    
//    [user2 resignFirstResponder];
//    [newPass resignFirstResponder];
//    [confirmPass resignFirstResponder];
//    [oldPassword resignFirstResponder];
//
//    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [user2 resignFirstResponder];
    [newPass resignFirstResponder];
    [confirmPass resignFirstResponder];
    [oldPassword resignFirstResponder];

}


- (IBAction)english:(id)sender

{
    ViewController *user = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:user animated:YES];
    NSString *str = @"1";
    [[NSUserDefaults standardUserDefaults] setObject:str forKey: @"english"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"arabic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker"];
    [[NSUserDefaults standardUserDefaults] setObject:@"one" forKey:@"coachMarker1"];
  

}


-(void)viewWillAppear:(BOOL)animated

{
    [self addressListing];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
