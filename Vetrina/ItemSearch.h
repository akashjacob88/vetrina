//
//  ItemSearch.h
//  Vetrina
//
//  Created by Amit Garg on 5/7/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"
#include <Social/Social.h>

@interface ItemSearch : UIViewController<BSKeyboardControlsDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    Singleton *singlogin;
    SLComposeViewController*compose;
    IBOutlet UILabel *catLbl;

    IBOutlet UILabel *msg_countVen;
    IBOutlet UITableView *itemsTbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    UIImageView *Image2, *img3;
    NSMutableArray *aray_2;
    NSArray *sortedarray1,*sortedarray2;    
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;

    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr,*askforPrice ,*soldout,*name,*prodid;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    NSMutableArray *cartListArray;
    NSString *cartIdStr,*quantStr,*staffPic,*objectIdStr;
    IBOutlet UILabel *chrtLbl;
    IBOutlet UITextField *searchText;
    NSMutableArray *vimagearray1,*pimagearray2,*varray1,*parray2;
    UIImageView *vimage,*pimage;
    BSKeyboardControls *keyboardControls;
    IBOutlet UIView *noItemView;
    
    IBOutlet UIView *topBar;
    
    NSString *reson;
    IBOutlet UIButton *reportBtn1,*reportBtn2,*reportBtn3;

}
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;
- (IBAction)textDidChange:(id)textField;

@end
