//
//  Vendor_ProductCell.h
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Singleton.h"


@interface Vendor_ProductCell : UITableViewCell
{
    Singleton *signloging;
}
@property (strong, nonatomic) IBOutlet UIImageView *statusImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *msgTxt;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UIImageView *profileUser;

- (void)bindData:(PFObject *)recent_;


@end
