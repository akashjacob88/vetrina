//
//  SignUp_Shop.h
//  Vetrina
//
//  Created by Amit Garg on 4/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface SignUp_Shop : UIViewController<UITextFieldDelegate,UIWebViewDelegate>


{
    Singleton *singlogin;
    
    
}
@property (strong, nonatomic) IBOutlet UITextField *fieldName;
@property (strong, nonatomic) IBOutlet UITextField *fieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *fieldEmail;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (IBAction)termsAction:(id)sender;

@end
