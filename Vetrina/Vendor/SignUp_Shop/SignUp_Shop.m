//
//  SignUp_Shop.m
//  Vetrina
//
//  Created by Amit Garg on 4/10/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "SignUp_Shop.h"
#import "Base64.h"
#import "JSON.h"

#import "Vendor_Instagramlogin.h"
#import <Parse/Parse.h>
#import "ProgressHUD.h"
#import "AppConstant.h"
#import "push.h"
#import "Vendor_Login.h"

#import <Quickblox/Quickblox.h>
@interface SignUp_Shop ()<UITextFieldDelegate>

@end

@implementation SignUp_Shop
@synthesize indicator;
@synthesize fieldName, fieldPassword, fieldEmail;


- (void)viewDidLoad

{
    [super viewDidLoad];
   
    singlogin = [Singleton instance];
    
    [indicator stopAnimating];
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, fieldName.frame.size.height)];
    fieldName.leftView = leftView1;
    fieldName.leftViewMode = UITextFieldViewModeAlways;
    [fieldName setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, fieldEmail.frame.size.height)];
    fieldEmail.leftView = leftView2;
    fieldEmail.leftViewMode = UITextFieldViewModeAlways;
    [fieldEmail setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, fieldPassword.frame.size.height)];
    fieldPassword.leftView = leftView3;
    fieldPassword.leftViewMode = UITextFieldViewModeAlways;
    [fieldPassword setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
}


- (void) viewDidAppear:(BOOL)animated

{
    [super viewDidAppear:animated];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == fieldEmail || textField == fieldName || textField == fieldPassword)
    {
        [self animateTextField:textField up:YES];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField==fieldEmail || textField==fieldName || textField==fieldPassword )
    {
        [self animateTextField:textField up:NO];
    }
}




//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    // Prevent crashing undo bug – see note below.
//    if(range.length + range.location > fieldPassword.text.length)
//    {
//        return NO;
//    }
//    
//    NSUInteger newLength = [fieldPassword.text length] + [string length] - range.length;
//    return newLength <= 6;
//}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- signUp Button


- (IBAction)signUP:(id)sender

{
    
    
    if (fieldEmail.text && fieldName.text && fieldPassword.text.length > 0)
    {
        
        
        if (fieldName.text.length < 3) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter username minimum 3 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }else if (fieldPassword.text.length < 8){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter Password minimum 8 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else{
            
            
            [ProgressHUD show:@"Loading" Interaction:NO];
            
//            [indicatr setHidden:NO];
//            [indicatr startAnimating];
            
            
            [self actionRegister];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

    
    
    
   
}

- (void)actionRegister
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
    NSString *name		= fieldName.text;
        NSString *password	= fieldPassword.text;
        NSString *email		= [fieldEmail.text lowercaseString];
    
    
    if ([name length] == 0)		{ [ProgressHUD showError:@"Name must be set."]; return; }
        if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
        if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }

    
    QBUUser *user1 = [QBUUser user];
    user1.login = email;
    user1.password = password;
    user1.fullName = name;
    
     [ProgressHUD show:@"Please wait..." Interaction:NO];
    
    [QBRequest signUp:user1 successBlock:^(QBResponse *response, QBUUser *user) {

        
        
        
        
                     singlogin.objectIdStr =[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
        
        
        [self actionLogin];
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        
        [ProgressHUD showError:@""];
        
        NSLog(@"error: %@", response.error);
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"SignUp" message:@"Email has already taken" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        
        [alert show];
        
        
    }];
 
    
    
    
    
    
    
    
    
    
//    NSString *name		= fieldName.text;
//    NSString *password	= fieldPassword.text;
//    NSString *email		= [fieldEmail.text lowercaseString];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    if ([name length] == 0)		{ [ProgressHUD showError:@"Name must be set."]; return; }
//    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
//    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    [ProgressHUD show:@"Please wait..." Interaction:NO];
//    
//    PFUser *user = [PFUser user];
//    user.username = email;
//    user.password = password;
//    user.email = email;
//    user[PF_USER_EMAILCOPY] = email;
//    user[PF_USER_FULLNAME] = name;
//    user[PF_USER_FULLNAME_LOWER] = [name lowercaseString];
//    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error == nil)
//         {
//             ParsePushUserAssign();
//             [ProgressHUD showSuccess:@"Succeed."];
//             [[PFUser currentUser] fetch];
//             if ([PFUser currentUser])
//             NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
//             singlogin.objectIdStr = [[PFUser currentUser] objectId];
//             [self dismissViewControllerAnimated:YES completion:nil];
//             Vendor_Instagramlogin *login = [[Vendor_Instagramlogin alloc]initWithNibName:@"Vendor_Instagramlogin" bundle:nil];
//             [self.navigationController pushViewController:login animated:YES];
//         }
//         else [ProgressHUD showError:error.userInfo[@"error"]];
//         {
//             
//         }
//     }];
}
- (void)actionLogin
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *email13 = [fieldEmail.text lowercaseString];
    NSString *password = fieldPassword.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email13 length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];
    
    
    
    [QBRequest logInWithUserLogin:email13 password:password successBlock:^(QBResponse *response, QBUUser *user) {
        
        [ProgressHUD showSuccess:@"Succeed."];
        singlogin.uId=[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];

        
        [self subscriptionAction];
//        singlogin.user_active=user;
//        
//        singlogin.uId=[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
//        NSLog(@"%@",singlogin.uId);
//        singlogin.userNameStr=user.fullName;
        
        
        
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
        [ProgressHUD showError:@""];
        
    }];
    
}

-(void)subscriptionAction{
    
    
    
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey: @"device_token"];
    
    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
        
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
        Vendor_Instagramlogin *login = [[Vendor_Instagramlogin alloc]initWithNibName:@"Vendor_Instagramlogin" bundle:nil];
        [self.navigationController pushViewController:login animated:YES];
        
        
        
        NSLog(@"%ld",(long)response.status);
        
    } errorBlock:^(QBResponse *response) {
        
        NSLog(@"%ld",(long)response.error);
        
        
    }];

    
    
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [fieldEmail resignFirstResponder];
    [fieldName resignFirstResponder];
    [fieldPassword resignFirstResponder];
    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [fieldEmail resignFirstResponder];
    [fieldName resignFirstResponder];
    [fieldPassword resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated

{
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)login:(id)sender
{
    Vendor_Login *login = [[Vendor_Login alloc]initWithNibName:@"Vendor_Login" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}


- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)termsAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://VetrinaApp.com/PrivacyPolicy.html"]];

    
}
@end
