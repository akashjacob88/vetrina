//
//  Edit_item.m
//  Vetrina
//
//  Created by Amit Garg on 4/24/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#define MAX_LENGTH 120

#import "Edit_item.h"
#import "Vendor_InstapicsViewController.h"
#import "BSKeyboardControls.h"
#import "Cell1.h"
#import "Cell2.h"
#import "JSON.h"
#import "Base64.h"
#import "AsyncImageView.h"
#import "Vendor_items.h"
#import "ProgressHUD.h"


@interface Edit_item ()<UITableViewDataSource,UITableViewDelegate>


{
    
    IBOutlet UIButton *editImageOut;
    IBOutlet UIView *addImageView;
    IBOutlet UIView *topbar;
}
@property (strong, nonatomic) IBOutlet UITableView *expansionTableView;
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;

@end

@implementation Edit_item

@synthesize isOpen,selectIndex,expansionTableView;


- (void)dealloc

{
    self.expansionTableView = nil;
    self.isOpen = NO;
    self.selectIndex = nil;
    
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self)
    {
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    singlogin = [Singleton instance];
  
     subcat1.text = singlogin.mainSubCat;
     cat1.text = singlogin.mainCat;
    
    // TopBar View///
    
    image1=[[UIImageView alloc]init];
    
    
    topbar.layer.shadowRadius = 2.0f;
    topbar.layer.shadowOffset = CGSizeMake(0, 2);
    topbar.layer.shadowColor = [UIColor blackColor].CGColor;
    topbar.layer.shadowOpacity = 0.5f;
    
    addImageView.hidden = YES;
    
      if ([self.editItem isEqualToString:@"edit"])
      {
          
          btnStr = singlogin.askforprice;
          if ([btnStr isEqualToString:@"Ask for Price"])
          {
              soldStr = @"Ask for Price";
              askforPriceLbl.hidden = NO ;
              askPriceView.hidden = NO;
              soldOutLbl.hidden = YES;
              priceTxt.hidden = YES;
              PriceView.hidden = YES;
              askForPrice1 =@"1";
          }
          btnStr2 = singlogin.soldout;
          if ([btnStr2 isEqualToString:@"SOLD OUT"])
          {
              askforPriceLbl.hidden = YES ;
              askPriceView.hidden = YES;
              soldOutLbl.hidden = NO;
              soldoutView.hidden = NO;
              priceTxt.hidden = YES;
              PriceView.hidden = YES;
              askForPrice1 =@"2";
              soldStr = @"SOLD OUT";
          }
          btnStr= singlogin.ePrice;
          if (![btnStr isEqualToString:@""])
         {
              askforPriceLbl.hidden = YES ;
              soldOutLbl.hidden = YES;
             soldoutView.hidden = YES;
             askPriceView.hidden = YES;
              priceTxt.text=singlogin.ePrice;
              priceTxt.hidden = NO;
              PriceView.hidden = NO;
              askForPrice1 =@"3";
             soldStr = @"";
          }

          
        deleteBtn.hidden=NO;
       
        
        item.text=@"Edit Item";
     

        productNme.text=singlogin.eProdName;
        ProductDec.text=singlogin.eDesc ;
        quntyText.text=singlogin.eQuant ;
        
        cat1.text=singlogin.eCat;
        subcat1.text=singlogin.eSubCat ;
        singlogin.catName=cat2.text;
        singlogin.subName=subcat2.text;
        singlogin.catid=singlogin.eCatId;
        singlogin.subCatid=singlogin.eSubCatId;

          addPic2.hidden=NO;
          addPic3.hidden=NO;
          addPic4.hidden=NO;
          addPic5.hidden=NO;
          
          image2.hidden=NO;
          image3.hidden=NO;
          image4.hidden=NO;
          image5.hidden=NO;
          
          NSURL *url=[NSURL URLWithString:singlogin.epImg1];
          NSData *datapic=[NSData dataWithContentsOfURL:url];
          UIImage *emppppic=[UIImage imageWithData:datapic];
          [image_product setImage:emppppic];
          
          if (![singlogin.epImg2 isEqualToString:@""])
          {
              NSURL *url1=[NSURL URLWithString:singlogin.epImg2];
              NSData *datapic1=[NSData dataWithContentsOfURL:url1];
              UIImage *emppppic1=[UIImage imageWithData:datapic1];
              [image2 setImage:emppppic1];
          }
          if (![singlogin.epImg3 isEqualToString:@""])
          {
              NSURL *url11=[NSURL URLWithString:singlogin.epImg3];
              NSData *datapic11=[NSData dataWithContentsOfURL:url11];
              UIImage *emppppic11=[UIImage imageWithData:datapic11];
              [image3 setImage:emppppic11];
          }
          if (![singlogin.epImg4 isEqualToString:@""])
          {
              NSURL *url12=[NSURL URLWithString:singlogin.epImg4];
              NSData *datapic12=[NSData dataWithContentsOfURL:url12];
              UIImage *emppppic12=[UIImage imageWithData:datapic12];
              [image4 setImage:emppppic12];
          }
          if (![singlogin.epImg5 isEqualToString:@""])
          {
              NSURL *url13=[NSURL URLWithString:singlogin.epImg5];
              NSData *datapic13=[NSData dataWithContentsOfURL:url13];
              UIImage *emppppic13=[UIImage imageWithData:datapic13];
              [image5 setImage:emppppic13];
          }

        

        
    }
     else
    {
        deleteBtn.hidden=YES;
        askforPriceLbl.hidden = YES ;
        soldOutLbl.hidden = YES;
        PriceView.hidden = YES;
        priceTxt.hidden = YES;
        askPriceView.hidden = YES;
        soldoutView.hidden = YES;
       
        addPic2.hidden=YES;
        addPic3.hidden=YES;
        addPic4.hidden=YES;
        addPic5.hidden=YES;
        
        image2.hidden=YES;
        image3.hidden=YES;
        image4.hidden=YES;
        image5.hidden=YES;

       item.text=@"Add Item";
      
        if ([singlogin.selectInstaPic1 isEqualToString:@"2"])
        {
            singlogin.selectInstaPic1 = @"";
            NSURL *url2 = [NSURL URLWithString:singlogin.selectedPic];
            NSData *data2 = [NSData dataWithContentsOfURL:url2];
            UIImage *insta= [[UIImage alloc] initWithData:data2];
            
            [ image1 setHidden:NO];
            [image1 setImage:insta];
            [image_product setImage:insta];
        }
        else
        {
            image1.image = singlogin.imageCamera.image;
            image_product.image=singlogin.imageCamera.image;
        }
    }
    
    
    
    NSArray *fields = @[productNme,ProductDec,priceTxt];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, productNme.frame.size.height)];
    productNme.leftView = leftView1;
    productNme.leftViewMode = UITextFieldViewModeAlways;
    [productNme setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    UIView *leftView3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, priceTxt.frame.size.height)];
    priceTxt.leftView = leftView3;
    priceTxt.leftViewMode = UITextFieldViewModeAlways;
    [priceTxt setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView4 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, quntyText.frame.size.height)];
    quntyText.leftView = leftView4;
    quntyText.leftViewMode = UITextFieldViewModeAlways;
    [quntyText setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];

    view1.hidden = YES;

    [indicatr stopAnimating];
    [scrolView  setScrollEnabled:YES];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            scrolView.frame=CGRectMake(0,64, 320,416);
            
            [scrolView setContentSize:CGSizeMake(320, 578)];
            
        }
        
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            scrolView.frame=CGRectMake(0, 64, 320, 504);
            
            [scrolView setContentSize:CGSizeMake(320, 660)];
            
        }
        
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            scrolView.frame=CGRectMake(0, 64,self.view.frame.size.width,603);
            
            [scrolView setContentSize:CGSizeMake(320, 850)];
            
        }
        
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            scrolView.frame=CGRectMake(0, 64, self.view.frame.size.width, 672);
            
            [scrolView setContentSize:CGSizeMake(320, 872)];
            
        }
        
    }
    else
    {
        
    }
    
    [scrolView setAutoresizesSubviews:YES];
    [scrolView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleWidth];
    

}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)replacementText
{
    NSString *newText = [ ProductDec.text stringByReplacingCharactersInRange: range withString: replacementText ];
    if( [newText length]<= MAX_LENGTH ){
        return YES;
    }
    // case where text length > MAX_LENGTH
    ProductDec.text = [ newText substringToIndex: MAX_LENGTH ];
   
    NSString *newText1 = [ arabicproduct.text stringByReplacingCharactersInRange: range withString: replacementText ];
    if( [newText length]<= MAX_LENGTH ){
        return YES;
    }
    // case where text length > MAX_LENGTH
    arabicproduct.text = [ newText1 substringToIndex: MAX_LENGTH ];
    
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [productNme resignFirstResponder];
    [ProductDec resignFirstResponder];
    [priceTxt resignFirstResponder];
    [priceTxt2 resignFirstResponder];
    [quntyText resignFirstResponder];
    [arabicPrductDic resignFirstResponder];
    [arabicproduct resignFirstResponder];

    return true;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    [productNme resignFirstResponder];
    [ProductDec resignFirstResponder];
    [priceTxt resignFirstResponder];
    [priceTxt2 resignFirstResponder];
    [quntyText resignFirstResponder];
    [arabicPrductDic resignFirstResponder];
    [arabicproduct resignFirstResponder];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)animateTextView:(UITextView *)textView up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == productNme || textField == priceTxt || textField == priceTxt2 || textField == quntyText || textField == arabicPrductDic)
    {
        [self animateTextField:textField up:YES];
    }
    [keyboardControls setActiveField:textField];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    if(textField==productNme || textField==priceTxt || textField == priceTxt2 || textField==quntyText || textField == arabicPrductDic )
    {
        [self animateTextField:textField up:NO];
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
   // textView.text = @"";

    
    
    if(textView==ProductDec)
    {
        [self animateTextView:textView up:YES];
    }
    if ([textView.text isEqualToString:@"Description of the item, letting customers know what the product is about. 1 to 150 characters"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView==ProductDec)
    {
        [self animateTextView:textView up:NO];
    }
    
    if ([textView.text isEqualToString:@""])
    {
//        textView.text = @"Description of the item, letting customers know what the product is about. 1 to 150 characters";
        textView.textColor = [UIColor blackColor]; //optional
    }
   
    [textView resignFirstResponder];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}



- (IBAction)back:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"Changes have not saved, are you sure?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
    [alert show];
    
    alert.tag=501;
}

-(void)backBtn
{
    if ([self.editItem isEqualToString:@"edit"])
    {
        singlogin.nextPage=@"yes";
    }
    Vendor_items *itmes = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:itmes animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)close:(id)sender
{
    view1.hidden = YES;
}




-(IBAction)DeleteProduct:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to delete this product?" delegate:self cancelButtonTitle:@"Delete" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 9;
}







-(void)deleteProduct
{
    NSString *post = [NSString stringWithFormat:@"productid=%@",singlogin.prodIdStr];
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delprod.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    
    
   
    indicatr.hidden=YES;
    [indicatr stopAnimating];
    [ProgressHUD showSuccess:@"Product has been deleted!"];
    Vendor_items *edit = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:edit animated:YES];
    singlogin.nextPage=@"yes";
}







-(void)viewWillAppear:(BOOL)animatedFaskp
{
       if ([singlogin.instagrmStr isEqualToString:@"1"])
    {
        singlogin.instagrmStr = @"";
        NSURL *url2 = [NSURL URLWithString:singlogin.selectedPic];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        UIImage *insta= [[UIImage alloc] initWithData:data2];
        
        if ([singlogin.imgeStr isEqualToString:@"1"])
        {
            [ image1 setHidden:NO];
            [image_product setImage:insta];
        }
//        else if ([singlogin.imgeStr isEqualToString:@"2"])
//        {
//            [ image2 setHidden:NO];
//            [image2 setImage:insta];
//        }
//        else if ([singlogin.imgeStr isEqualToString:@"3"])
//        {
//            [ image3 setHidden:NO];
//            [image3 setImage:insta];
//        }
//        else if ([singlogin.imgeStr isEqualToString:@"4"])
//        {
//            [ image4 setHidden:NO];
//            [image4 setImage:insta];
//        }
//        else if ([singlogin.imgeStr isEqualToString:@"5"])
//        {
//            [ image5 setHidden:NO];
//            [image5 setImage:insta];
//        }
        if ([self.editItem isEqualToString:@"edit"])
        {
            [indicatr setHidden:NO];
            [indicatr startAnimating];
            [self performSelector:@selector(editImage)withObject:Nil afterDelay:0.5f];
        }
    }

}


- (IBAction)addproduct2:(id)sender

{
    view1.hidden = NO;
    [self catlist];
    addBtnStr = @"2";
    
}



-(void)catlist

{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/subcatlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"subcatname"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [sortedArray1 mutableCopy];
    
    
    NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"catname" ascending:YES];
    NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
    sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
    
    NSLog(@"sorted array is %@",sortedArray);
    
    newArray=[[NSMutableArray alloc]init];
    array=[[NSMutableArray alloc]init];
    finalArray=[[NSMutableArray alloc]init];
    
    vendorNameArray=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
    
    for( i =0;i<sortedArray.count;i++)
    {
        stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"catid"];
        
        NSString *vendorName1=[[sortedArray objectAtIndex:i]valueForKey:@"catname"];
        NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"catimage"];
        if (![stringOne isEqualToString:stringTwo ])
        {
            
            [vendorNameArray addObject:vendorName1];
            
            [imageArr addObject:imagestr];
            
            stringTwo = stringOne;
            
            if ([newString isEqualToString:@"newString"])
            {
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];
                
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            if ([newString isEqualToString:@"oneString"])
            {
                NSInteger a=i-1;
                NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];
                dynamicDict=[[NSMutableDictionary alloc]init];
                [dynamicDict setValue:newArray forKey:vendorName];
                [finalArray addObject:dynamicDict];
                
                newArray=[[NSMutableArray alloc]init];
                newString=@"";
            }
            newString=@"oneString";
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
        }
        else
        {
            
            array=[sortedArray objectAtIndex:i];
            [newArray addObject:array];
            
            newString=@"newString";
        }
    }
    
    NSInteger a=i-1;
    NSString *vendorName=[[sortedArray objectAtIndex:a]valueForKey:@"catname"];
    dynamicDict=[[NSMutableDictionary alloc]init];
    [dynamicDict setValue:newArray forKey:vendorName];
    [finalArray addObject:dynamicDict];
    
    newArray=[[NSMutableArray alloc]init];
    newString=@"";
    stringOne=@"";
    stringTwo=@"";
    
    [expansionTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [finalArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen)
    {
        if (self.selectIndex.section == section)
        {
            //   return [[[_dataList objectAtIndex:section] objectForKey:@"list"] count]+1;;
            
            NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
            sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
            return [sectionAnimals count]+1;
        }
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0)
    {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        //     NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"list"];
        cell.titleLabel.text = [[sectionAnimals objectAtIndex:indexPath.row-1]valueForKey:@"subcatname"];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        //    NSString *name = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        
        cell.titleLabel.text = sectionTitle;
        
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:Img.imageURL];
        cell.iconImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[imageArr objectAtIndex:indexPath.section]]];
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.row == 0)
    {
        if ([indexPath isEqual:self.selectIndex])
        {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            
        }
        else
        {
            if (!self.selectIndex)
            {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }
            else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    else
    {
        NSString *sectionTitle = [vendorNameArray objectAtIndex:indexPath.section];
        sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
        
        singlogin.catid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catid"];
        singlogin.catName =[[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"catname"];
        singlogin.subName = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"subcatname"];
        singlogin.subCatid = [[sectionAnimals objectAtIndex:indexPath.row-1]objectForKey:@"id"];
        
        if ([addBtnStr isEqualToString:@"2"])
        {
            cat1.text = singlogin.catName;
            subcat1.text = singlogin.subName;
        }
        
        
        view1.hidden = YES;

    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert

{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    NSInteger  section = self.selectIndex.section;
    
    NSString *sectionTitle = [vendorNameArray objectAtIndex:section];
    sectionAnimals = [[finalArray objectAtIndex:section]valueForKey:sectionTitle];
    NSInteger contentCount= [sectionAnimals count];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    
    for ( i = 1; i < contentCount + 1; i++)
    {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
    [rowToInsert release];
    
    [self.expansionTableView endUpdates];
    if (nextDoInsert)
    {
        self.isOpen = YES;
        self.selectIndex = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}




- (IBAction)confrmBtn:(id)sender
{
    if ([self.editItem isEqualToString:@"edit"])
    {
        if ([askForPrice1 isEqualToString:@"2"])
        {
            if (productNme.text.length > 0 || ProductDec.text > 0  )
            {
                if (![productNme.text isEqualToString:@"Your Product Name"])
                {
                    if (![ProductDec.text isEqualToString:@"Description of the item, letting customers know what the product is about. 1 to 150 characters"])
                    {
                    [indicatr setHidden:NO];
                    [indicatr startAnimating];
                    [ProgressHUD show:@"Loading..." Interaction:NO];
                    
                    [self performSelector:@selector(editProduct)withObject:Nil afterDelay:0.5f];
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please entrt product description." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                    }

                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Please entrt product name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }

               
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }

            
        }
        else if ([askForPrice1 isEqualToString:@"1"])
        {
            [ProgressHUD show:@"Loading..." Interaction:NO];
            
            [self performSelector:@selector(editProduct)withObject:Nil afterDelay:0.5f];
        }
        else if ([askForPrice1 isEqualToString:@"3"])
        {
        [indicatr setHidden:NO];
        [indicatr startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];

        [self performSelector:@selector(editProduct)withObject:Nil afterDelay:0.5f];
        }
    }
    else
    {
        if ([askForPrice1 isEqualToString:@"1"])
        {
            if (productNme.text.length > 0 && ProductDec.text > 0  )
            {
            [indicatr setHidden:NO];
            [indicatr startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];
            [self performSelector:@selector(vendorsubcatAdd)withObject:Nil afterDelay:0.5f];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }

        }
        else if ([askForPrice1 isEqualToString:@"2"])
        {
            if (productNme.text.length > 0&& ProductDec.text > 0  )
            {
            [indicatr setHidden:NO];
            [indicatr startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];
            [self performSelector:@selector(vendorsubcatAdd)withObject:Nil afterDelay:0.5f];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else if ([askForPrice1 isEqualToString:@"3"])
        {
            
            if (productNme.text.length > 0  && priceTxt.text.length > 0 && ProductDec.text > 0)
        {
            [indicatr setHidden:NO];
            [indicatr startAnimating];
            [ProgressHUD show:@"Loading..." Interaction:NO];

            [self performSelector:@selector(vendorsubcatAdd)withObject:Nil afterDelay:0.5f];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        }
        
    }
    
}


- (IBAction)askPrice:(id)sender

{
    askforPriceLbl.hidden = NO;
    soldOutLbl.hidden = YES;
    priceTxt.hidden = YES;
    PriceView.hidden = YES;
    askPriceView.hidden = NO;
    soldoutView.hidden = YES;
    soldStr = @"";
    priceTxt.text = @"";
    askForPrice1 = @"1";
    askForPrice = @"Ask for Price";
}


- (IBAction)soldout:(id)sender

{
    soldOutLbl.hidden = NO;
    askforPriceLbl.hidden = YES;
    priceTxt.hidden = YES;
    PriceView.hidden = YES;
    askPriceView.hidden = YES;
    soldoutView.hidden = NO;
    askForPrice = @"";
    priceTxt.text = @"";
    soldStr = @"SOLD OUT";
    askForPrice1= @"2";
}

- (IBAction)addpic:(id)sender {
    
    
    
    singlogin.imgeStr = @"1";
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@"Add Images"
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Instagram",@"Camera Roll",@"Camera",nil];
    actionsheet1.tag=12;
    
    [actionsheet1 showInView:self.view];
}


- (IBAction)showPrice:(id)sender
{
    soldOutLbl.hidden = YES;
    askforPriceLbl.hidden = YES;
    PriceView.hidden = NO;
    priceTxt.hidden = NO;
    showPrice = @"3";
    soldStr = @"";
    askForPrice = @"";
    askForPrice1=@"3";
    askPriceView.hidden = YES;
    soldoutView.hidden = YES;
}



-(void)editProduct
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editproduct2.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrId = [[NSString alloc]initWithFormat:@"%@", singlogin.vUniqueId];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *objectIdstr= [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.shopNameVendr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *prodidStr = [[NSString alloc]initWithFormat:@"%@", singlogin.prodIdStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",prodidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *sub1Str=[[NSString alloc]initWithFormat:@"%@",singlogin.shopDec];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"descr\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",sub1Str] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    NSString *staffPic=[[NSString alloc]initWithFormat:@"%@",singlogin.pid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"staffpick\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",staffPic] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSDate * now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm:ss a"];
    NSString *currentTime = [formatter stringFromDate:now];
    
    NSDate * now2 = [NSDate date];
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"dd-MM-yyyy"];
    NSString *currentDate = [formatter2 stringFromDate:now2];
    
    
    NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@ %@",currentDate,currentTime];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"datetime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    
    NSString *name1=[[NSString alloc]initWithFormat:@"%@",productNme.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",name1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *descrStr=[[NSString alloc]initWithFormat:@"%@",ProductDec.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prod_desc\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",descrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([askForPrice1 isEqualToString:@"2"])
    {
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%@",soldStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"soldout\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *priceStr2=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr2] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *priceStr1=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    }
    else if ([askForPrice1 isEqualToString:@"1"])
    {
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%@",askForPrice];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *priceStr2=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr2] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *priceStr1=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"soldout\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([askForPrice1 isEqualToString:@"3"])
    {
        float price = [priceTxt.text floatValue];
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%0.3f KD",price];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *priceStr1=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"soldout\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *priceStr2=@"";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr2] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }

    NSString *quntyStr=@"";
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",quntyStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([addBtnStr isEqualToString:@"2"])
    {
        NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.catid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",cat1.text];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.subCatid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",subcat1.text];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
    
    NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.catid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.eCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.subCatid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.eSubCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *venderStr=[[NSString alloc]initWithFormat:@"%@",singlogin.venPic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",venderStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSLog(@"print mystring==%@",returnString);
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Product has been Updated!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
    singlogin.nextPage=@"yes";
    Vendor_items *edit = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:edit animated:YES];
    [indicatr stopAnimating];
    [ProgressHUD showSuccess:@"Product has been Updated!"];
}



-(void)vendorsubcatAdd
{
    NSData *imageData1 = UIImageJPEGRepresentation(image1.image, 200);
    
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addprod.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrId = [[NSString alloc]initWithFormat:@"%@", singlogin.vUniqueId];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.shopNameVendr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSDate * now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm:ss a"];
    NSString *currentTime = [formatter stringFromDate:now];
    
    NSDate * now2 = [NSDate date];
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"dd-MM-yyyy"];
    NSString *currentDate = [formatter2 stringFromDate:now2];
    
    
    NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@ %@",currentDate,currentTime];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"datetime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    
    NSString *name1=[[NSString alloc]initWithFormat:@"%@",productNme.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",name1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *descrShopStr=[[NSString alloc]initWithFormat:@"%@",singlogin.shopDec];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"desc\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",descrShopStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *descrStr=[[NSString alloc]initWithFormat:@"%@",ProductDec.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prod_desc\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",descrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
   
    if ([askForPrice1 isEqualToString:@"1"])
    {
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%@",askForPrice];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([askForPrice1 isEqualToString:@"2"])
    {
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%@",soldStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"soldout\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    }
    else if ([askForPrice1 isEqualToString:@"3"])
    {
        float price = [priceTxt.text floatValue];
        NSString *priceStr=[[NSString alloc]initWithFormat:@"%0.3f KD",price];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",priceStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
       
    NSString *quntyStr=[[NSString alloc]initWithFormat:@"%@",quntyText.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",quntyStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([addBtnStr isEqualToString:@"2"])
    {
        NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.catid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.catName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.subCatid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.subName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    }
    else
    {
    NSString *catidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.catid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *catnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.mainCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"catname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",catnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.subCatid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *subcatnmStr=[[NSString alloc]initWithFormat:@"%@",singlogin.mainSubCat];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subcatname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subcatnmStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *venderStr=[[NSString alloc]initWithFormat:@"%@",singlogin.venPic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",venderStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    NSString *objectIdstr= [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",objectIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    NSString *sub1Str=[[NSString alloc]initWithFormat:@"%@",singlogin.shopDec];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"descr\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",sub1Str] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    
    NSString *staffPic=[[NSString alloc]initWithFormat:@"%@",singlogin.pid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"staffpick\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",staffPic] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename1\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[Base64 encode:imageData1]dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSLog(@"print mystring==%@",returnString);
    [self stafpicYes];
     [self stafpicYes2];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Product is added" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    singlogin.nextPage=@"yes";
    Vendor_items *edit = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:edit animated:NO];
    [indicatr stopAnimating];
    [ProgressHUD showSuccess:@""];
}


-(void)stafpicYes{
    
    
    NSString *post = [NSString stringWithFormat:@"id=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/staffpick_yes.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    
}
-(void)stafpicYes2{
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/staffpick_yesvendor.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    
}


- (IBAction)editImageBtn:(id)sender
{
    addImageView.hidden = NO;
    view1.hidden = YES;
}

- (IBAction)Done:(id)sender
{
    addImageView.hidden = YES;
    view1.hidden = YES;
}


- (IBAction)addPic1:(id)sender
{
    singlogin.imgeStr = @"1";
    actionsheet1 = [[UIActionSheet alloc]
                    initWithTitle:@"Add Images"
                    delegate:self
                    cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                    otherButtonTitles:@"Instagram",@"Camera Roll",@"Camera",nil];
    actionsheet1.tag=12;
    
    [actionsheet1 showInView:self.view];
}


- (IBAction)addPic2:(id)sender
{
    singlogin.imgeStr = @"2";
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                      message:@"Do you want to?"
                                                     delegate:self
                                            cancelButtonTitle:@"Change"
                                            otherButtonTitles:@"Delete", nil];
    [myAlert show];
}


- (IBAction)addPic3:(id)sender
{
    singlogin.imgeStr = @"3";
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                      message:@"Do you want to?"
                                                     delegate:self
                                            cancelButtonTitle:@"Change"
                                            otherButtonTitles:@"Delete", nil];
    [myAlert show];
}


- (IBAction)addPic4:(id)sender
{
    singlogin.imgeStr = @"4";
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                      message:@"Do you want to?"
                                                     delegate:self
                                            cancelButtonTitle:@"Change"
                                            otherButtonTitles:@"Delete", nil];
    [myAlert show];
}


- (IBAction)addPic5:(id)sender
{
    singlogin.imgeStr = @"5";
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                      message:@"Do you want to?"
                                                     delegate:self
                                            cancelButtonTitle:@"Change"
                                            otherButtonTitles:@"Delete", nil];
    [myAlert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
    
    if (alertView.tag==9) {
        if (buttonIndex==0) {
            [indicatr setHidden:NO];
            [indicatr startAnimating];
            [self performSelector:@selector(deleteProduct)withObject:Nil afterDelay:0.5f];
        }else{
            NSLog(@"cancel tapped");
        }
    }
    else{
    
    
    if (buttonIndex==0 && alertView.tag!=501)
    {
        actionsheet1 = [[UIActionSheet alloc]
                        initWithTitle:@"Add Images"
                        delegate:self
                        cancelButtonTitle:@"Cancel"
                        destructiveButtonTitle:nil
                        otherButtonTitles:@"Instagram",@"Camera Roll",@"Camera",nil];
        actionsheet1.tag=12;
        
        [actionsheet1 showInView:self.view];
        
    }
    }
    
    if (alertView.tag==501) {
        
        
        if (buttonIndex==0){
            [self performSelector:@selector(backBtn) withObject:nil afterDelay:0.5f];
            
            
        }else
            [indicatr setHidden:YES];
        [indicatr stopAnimating];
        [ProgressHUD showSuccess:@""];
        
        
    }

    
    
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (actionSheet.tag==12)
    {
        
        if (buttonIndex==0)
        {
            Vendor_InstapicsViewController *vInstPics = [[Vendor_InstapicsViewController alloc]initWithNibName:@"Vendor_InstapicsViewController" bundle:nil];
            [self.navigationController pushViewController:vInstPics animated:NO];
        }
        
        
        if (buttonIndex==1)
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }
            
            
        }
        
        if (buttonIndex==2)
            
        {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                camerapicker =[[UIImagePickerController alloc]init];
                camerapicker.delegate=self;
                camerapicker.sourceType=UIImagePickerControllerSourceTypeCamera;
                camerapicker.allowsEditing=YES;
                [self presentViewController:camerapicker animated:YES
                                 completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"Error acessing camera"
                                      message:@"Device does not support a camera"
                                      delegate:nil
                                      cancelButtonTitle:@"Dismiss"
                                      otherButtonTitles:nil];
                alert.tag=44;
                [alert show];
            }
        }
    }
}



-(void)deleteImage
{
    NSString *post = [NSString stringWithFormat:@"productid=%@",singlogin.prodIdStr];
    NSLog(@"get data=%@",post);
    NSURL* url;
    if ([singlogin.imgeStr isEqualToString:@"2"])
    {
        image2.image=[UIImage imageNamed:@"add.png"];
        url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delprodimg1.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"3"])
    {
        image3.image=[UIImage imageNamed:@"add.png"];
        url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delprodimg2.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"4"])
    {
        image4.image=[UIImage imageNamed:@"add.png"];
        url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delprodimg3.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"5"])
    {
        image5.image=[UIImage imageNamed:@"add.png"];
        url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delprodimg4.php"];
    }
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    NSLog(@"%@",eventarray);
    indicatr.hidden=YES;
    [indicatr stopAnimating];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    cimage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    CGRect rect = CGRectMake(0,0,cimage.size.width/4,cimage.size.height/4);
    UIGraphicsBeginImageContext( rect.size );
    [cimage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageDataForResize = UIImagePNGRepresentation(picture1);
    UIImage *pimage=[UIImage imageWithData:imageDataForResize];
    
    
    if ([singlogin.imgeStr isEqualToString:@"1"])
    {
     //   [ image1 setHidden:NO];
        [image_product setImage:pimage];
        
    }
    else if ([singlogin.imgeStr isEqualToString:@"2"])
    {
        [ image2 setHidden:NO];
        [image2 setImage:pimage];
    }
    else if ([singlogin.imgeStr isEqualToString:@"3"])
    {
        [ image3 setHidden:NO];
        [image3 setImage:pimage];
    }
    else if ([singlogin.imgeStr isEqualToString:@"4"])
    {
        [ image4 setHidden:NO];
        [image4 setImage:pimage];
    }
    else if ([singlogin.imgeStr isEqualToString:@"5"])
    {
        [image5 setHidden:NO];
        [image5 setImage:pimage];
    }
    if ([self.editItem isEqualToString:@"edit"])
    {
        [indicatr setHidden:NO];
        [indicatr startAnimating];
         [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(editImage)withObject:Nil afterDelay:0.5f];
    }
    [camerapicker dismissViewControllerAnimated:YES completion:nil];
}


-(void)editImage
{
    NSData *imageData1 = UIImageJPEGRepresentation(image_product.image, 40);
    NSData *imageData2 = UIImageJPEGRepresentation(image2.image, 40);
    NSData *imageData3 = UIImageJPEGRepresentation(image3.image, 40);
    NSData *imageData4 = UIImageJPEGRepresentation(image4.image, 40);
    NSData *imageData5 = UIImageJPEGRepresentation(image5.image, 40);
    
    NSString* strURL;
    if ([singlogin.imgeStr isEqualToString:@"1"])
    {
        strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editprodimg1.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"2"])
    {
        strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editprodimg2.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"3"])
    {
        strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editprodimg3.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"4"])
    {
        strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editprodimg4.php"];
    }
    else if ([singlogin.imgeStr isEqualToString:@"5"])
    {
        strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editprodimg5.php"];
    }
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *prodIdStr = [[NSString alloc]initWithFormat:@"%@", singlogin.prodIdStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",prodIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    if ([singlogin.imgeStr isEqualToString:@"1"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename1\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[Base64 encode:imageData1]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([singlogin.imgeStr isEqualToString:@"2"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename2\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[Base64 encode:imageData2]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([singlogin.imgeStr isEqualToString:@"3"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename3\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[Base64 encode:imageData3]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([singlogin.imgeStr isEqualToString:@"4"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename4\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[Base64 encode:imageData4]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else if ([singlogin.imgeStr isEqualToString:@"5"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[ NSString stringWithFormat:@"Content-Disposition: attachment; name=\"filename5\";\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[Base64 encode:imageData5]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    [indicatr stopAnimating];
    indicatr.hidden=YES;
    [ProgressHUD showSuccess:@""];
}



@end
