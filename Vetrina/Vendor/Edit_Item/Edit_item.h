//
//  Edit_item.h
//  Vetrina
//
//  Created by Amit Garg on 4/24/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"


@interface Edit_item : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIScrollViewDelegate,UITextFieldDelegate,BSKeyboardControlsDelegate>


{
    Singleton *singlogin;
    UIActionSheet *actionsheet1;
    
    /* -------- Array ------- */
    
    NSMutableArray *vendorNameArray,*dataArray,*aray_2;
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    
    NSMutableArray *imageArr,*img2;
    
    NSArray *sectionAnimals;

    IBOutlet UIButton *soldOut;
    IBOutlet UIActivityIndicatorView *indicatr;
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2,*stringTwo3;
    NSString *itme;
    
    NSString *instaNameSave, *shopNmberSave, *shopCatSave, *emailSave ,*addBtnStr;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;

    
    BSKeyboardControls *keyboardControls;
    
    UIImagePickerController *camerapicker;
    UIImage *cimage,*cimage2,*cimage3,*cimage4,*cimage5;
    NSString *btnStr,*btnStr2,*btnStr3;
    UIImageView  *Img;
    
    
    IBOutlet UIImageView *image1;
    IBOutlet UIImageView *image2;
    IBOutlet UIImageView *image3;
    IBOutlet UIImageView *image4;
    IBOutlet UIImageView *image5;
    
    
    IBOutlet UIButton *addPic1,*addPic2,*addPic3,*addPic4,*addPic5;

    
    
    
    
    
    IBOutlet UIView *view1;
   
    IBOutlet UIScrollView *scrolView;
    
    IBOutlet UITextField *productNme;
    IBOutlet UITextView *ProductDec;
    
    IBOutlet UITextField *priceTxt;
    IBOutlet UITextField *priceTxt2;
    IBOutlet UITextField *quntyText;
    IBOutlet UILabel *cat1;
    IBOutlet UILabel *subcat1;
    IBOutlet UILabel *cat2;
    IBOutlet UILabel *subcat2;
    IBOutlet UILabel *cat3;
    IBOutlet UILabel *subcat3,*item;
    IBOutlet UIButton *deleteBtn;
    IBOutlet UILabel *yesLbl;
    IBOutlet UIButton *ashPriceOut;
    NSString *askForPrice,*askForPrice1,*soldStr,*soldStr1,*showPrice;
    IBOutlet UITextField *arabicPrductDic;
    IBOutlet UITextView *arabicproduct;
    IBOutlet UIButton *soldOutOut;
    IBOutlet UILabel *showPriceView;

    IBOutlet UILabel *kdLabl;
    IBOutlet UILabel *dotLbl;
    IBOutlet UIView *askforPriceLbl;
    IBOutlet UIView *soldOutLbl;
    IBOutlet UIView *PriceView;
    IBOutlet UILabel *askPriceView;
    IBOutlet UIButton *soldoutView;
    IBOutlet UIImageView *image_product;
}

- (IBAction)close:(id)sender;
- (IBAction)askPrice:(id)sender;



- (IBAction)addproduct2:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Confrm;
- (IBAction)confrmBtn:(id)sender;
@property (strong, nonatomic) NSString *editItem;
- (IBAction)DeleteProduct:(id)sender;
- (IBAction)soldout:(id)sender;

- (IBAction)addpic:(id)sender;






@end
