//
//  Vendor_setting.h
//  Vetrina
//
//  Created by Amit Garg on 4/24/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface Vendor_setting : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,BSKeyboardControlsDelegate>

{
    Singleton *singling;
   
    BSKeyboardControls *keyboardControls;
    
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
     UIImageView  *Image2;
    UIImage *cimage,*cimage2;

    IBOutlet UITextField *phoneText;
    
    IBOutlet UIScrollView *scrolVIew;
    
    IBOutlet UITextField *vendorName;
    
    IBOutlet UIImageView *vendorImg;
    IBOutlet UITextView *ShowDec;
    IBOutlet UILabel *instaLbl;
    IBOutlet UILabel *cat1;
    IBOutlet UILabel *subcat1;

    IBOutlet UIView *catView;
   
    NSMutableArray *vendorNameArray,*dataArray,*aray_2;
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    
    NSMutableArray *imageArr,*img2;
    
    NSArray *sectionAnimals;

    
    /* -------- Strings ------- */
    
    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2,*stringTwo3;
    NSString *itme;
    
    NSString *instaNameSave, *shopNmberSave, *shopCatSave, *emailSave;
    
    /* -------- Integer ------- */
    
    NSInteger i;
    
    /* -------- Dictionary ------- */
    
    NSMutableDictionary *dynamicDict;
    IBOutlet UIView *topBar;
    IBOutlet UILabel *nameShedow;
    IBOutlet UIButton *catShedow;
    IBOutlet UIButton *confrmBtnOut;
    IBOutlet UILabel *Outlet;


}

- (IBAction)ChangePswrd:(id)sender;
- (IBAction)logoutBtn:(id)sender;

- (IBAction)ComfrmBtn:(id)sender;

- (IBAction)SlectimageBtn:(id)sender;



@end
