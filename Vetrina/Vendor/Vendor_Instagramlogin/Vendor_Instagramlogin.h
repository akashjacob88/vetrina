//
//  Vendor_Instagramlogin.h
//  Vetrina
//
//  Created by Amit Garg on 4/15/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface Vendor_Instagramlogin : UIViewController

{
    Singleton *singlogin;
    
    NSMutableArray *usrID,*newdatalist;
    UIImage *pro;
    NSString *img4;

    
    IBOutlet UIWebView *webView2;
    IBOutlet UILabel *decLbl;
    
}


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)loginInsta:(id)sender;
- (IBAction)shopSignUp:(id)sender;
- (IBAction)back:(id)sender;

@end
