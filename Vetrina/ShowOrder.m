//
//  ShowOrder.m
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "ShowOrder.h"
#import "AsyncImageView.h"
#import "CartProductCell.h"

@interface ShowOrder ()

@end

@implementation ShowOrder

- (void)viewDidLoad {
    [super viewDidLoad];
    singletonn=[Singleton instance];
    NSLog(@"%@",singletonn.productArray);
    [productlistingTable reloadData];
     productlistingTable.transform = CGAffineTransformMakeRotation(-M_PI);
    
    deliPriceStr =[[singletonn.productArray objectAtIndex:0]valueForKey:@"deliveryprice"];
    totalStr=[[singletonn.productArray objectAtIndex:0]valueForKey:@"amountpay"];
    addressStr=[[singletonn.productArray objectAtIndex:0]valueForKey:@"useraddress"];

       
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [singletonn.productArray count];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CartProductCell" bundle:nil] forCellReuseIdentifier:@"ProductCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
    }
    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    [cell.contentView addSubview:v11];
    cell.transform = CGAffineTransformMakeRotation(M_PI);
    cell.backgroundColor=[UIColor whiteColor];
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(CartProductCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.prodName.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"productname"];
    cell.priceLbl.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    
    cell.qtyLbl.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"quantityprod"];
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
    cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[singletonn.productArray objectAtIndex:indexPath.row]objectForKey:@"prodimage"]]];
    
}
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    UIButton *statusBtn;
    UIView *v1,*v2,*v3,*v4;
    UILabel *deliveryLbl,*TotalLbl,*deliverAddressLbl,*deliveryLbl1,*TotalLbl1;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 304, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 304, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 304, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(100, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 100, 150, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 50, 150, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(15, 151, 274, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            deliveryLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 100, 100, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 50, 100, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 304, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 304, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 304, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(100, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 100, 150, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 50, 150, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(15, 151, 274, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
            deliveryLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 100, 100, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 50, 100, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];


    }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 361, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 361, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 361, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 361, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(120, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(165, 100, 175, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(165, 50, 175, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 151, 315, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:18];
            deliveryLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];

            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(15, 100, 120, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(15, 50, 120, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];

        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 398, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 398, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 398, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(130, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(180, 100, 200, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(180, 50, 200, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 151, 354, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:19];
            deliveryLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(17, 100, 130, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(17, 50, 130, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            

        }
    }
    [statusBtn setTitle:@"OKAY" forState:UIControlStateNormal];
    [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    [statusBtn addTarget:self action:@selector(okayBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    v1.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    v2.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    v3.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    v4.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];

    deliveryLbl.text=@"Delivery Charge";
    deliveryLbl.textAlignment=NSTextAlignmentLeft;
    deliveryLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];

    TotalLbl.text=@"Total";
    TotalLbl.textAlignment=NSTextAlignmentLeft;
    TotalLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];

    deliveryLbl1.text=[NSString stringWithFormat:@"%@",deliPriceStr];
    deliveryLbl1.textAlignment=NSTextAlignmentRight;
    deliveryLbl1.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    TotalLbl1.text=[NSString stringWithFormat:@"%@",totalStr];
    TotalLbl1.textAlignment=NSTextAlignmentRight;
    TotalLbl1.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];

    
    deliverAddressLbl.text=[NSString stringWithFormat:@"%@",addressStr];
    deliverAddressLbl.textAlignment=NSTextAlignmentLeft;
    deliverAddressLbl.numberOfLines=0;
    deliverAddressLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    [headerView addSubview:deliverAddressLbl];
    [headerView addSubview:statusBtn];
    [headerView addSubview:v1];
    [headerView addSubview:v2];
    [headerView addSubview:v3];
    [headerView addSubview:deliveryLbl];
    [headerView addSubview:TotalLbl];
    [headerView addSubview:v4];
    [headerView addSubview:deliveryLbl1];
    [headerView addSubview:TotalLbl1];

    headerView.backgroundColor=[UIColor whiteColor];
    statusBtn.transform = CGAffineTransformMakeRotation(M_PI);
    v1.transform = CGAffineTransformMakeRotation(M_PI);
    v2.transform = CGAffineTransformMakeRotation(M_PI);
    v3.transform = CGAffineTransformMakeRotation(M_PI);
    v4.transform = CGAffineTransformMakeRotation(M_PI);
    deliveryLbl.transform = CGAffineTransformMakeRotation(M_PI);
    TotalLbl.transform = CGAffineTransformMakeRotation(M_PI);
    deliverAddressLbl.transform = CGAffineTransformMakeRotation(M_PI);
    deliveryLbl1.transform = CGAffineTransformMakeRotation(M_PI);
    TotalLbl1.transform = CGAffineTransformMakeRotation(M_PI);

    return headerView;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 210;
}
-(void)okayBtn:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
