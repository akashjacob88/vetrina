//
//  SubcatgoryAR.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"


@interface SubcatgoryAR : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate,UIGestureRecognizerDelegate>

{
    
    BSKeyboardControls *keyboardControls;
    Singleton *singloging, *singemplist;
    
    IBOutlet UIButton *shopesBtn;
    
    IBOutlet UIButton *itemsBtm;
    UIImageView *image3;
    NSMutableArray *array1, *array5, *array2;
    NSArray *sortedarray1,*sortedarray2;
    
    NSString *accesToken,*staffPic;
    
    NSMutableArray *imagearrayLIst, *newdatalist;
    
    UIImageView *image2, *arrowIcon;
    UILabel *costText1, *lineLbl;
    
    IBOutlet UIActivityIndicatorView *indicator;
    
    IBOutlet UILabel *subName;
   
    IBOutlet UITableView *vendorTbl;
    
    IBOutlet UITextField *searchText;
    NSMutableArray *searchArray,*imageSearchArray;
    NSString *searchStr;
    IBOutlet UIView *view1;
    IBOutlet UILabel *chrtLbl;

    IBOutlet UIButton *searchBtn;
    IBOutlet UIView *noItmView;
    



}
- (IBAction)items:(id)sender;

- (IBAction)back:(id)sender;


@end
