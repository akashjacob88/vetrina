//
// Copyright (c) 2015 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

#import "AppConstant.h"
#import "converter.h"

#import "RecentCell.h"
#import <Quickblox/Quickblox.h>
#import "UIImageView+WebCache.h"
//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface RecentCell()
{
	PFObject *recent;
    QBChatDialog *recent1;
    NSString *ab;
}

@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelLastMessage;
@property (strong, nonatomic) IBOutlet UILabel *labelElapsed;
@property (strong, nonatomic) IBOutlet UILabel *labelCounter;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation RecentCell

@synthesize imageUser;
@synthesize labelDescription, labelLastMessage;
@synthesize labelElapsed, labelCounter;

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)bindData:(PFObject *)recent_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	recent = recent_;
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
//	imageUser.layer.masksToBounds = YES;
//	//---------------------------------------------------------------------------------------------------------------------------------------------
////	PFUser *lastUser = recent[PF_RECENT_LASTUSER];
////	[imageUser setFile:lastUser[PF_USER_PICTURE]];
////	[imageUser loadInBackground];
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	labelDescription.text = recent[PF_RECENT_DESCRIPTION];
//	labelLastMessage.text = recent[PF_RECENT_LASTMESSAGE];
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:recent[PF_RECENT_UPDATEDACTION]];
//	labelElapsed.text = TimeElapsed(seconds);
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	int counter = [recent[PF_RECENT_COUNTER] intValue];
//	labelCounter.text = (counter == 0) ? @"" : [NSString stringWithFormat:@"%d new", counter];
}



- (void)bindData1:(QBChatDialog *)recent_
{
    recent1 = recent_;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
    imageUser.layer.masksToBounds = YES;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //	PFUser *lastUser = recent[PF_RECENT_LASTUSER];
//    	[imageUser setFile:];
//    	[imageUser loadInBackground];
    //---------------------------------------------------------------------------------------------------------------------------------------------
  //  NSInteger ii=(int)[[NSString stringWithFormat:@"%lu",(unsigned long)recent1.userID] integerValue];
    
    int a =(int)[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"indexpath"]] integerValue];
   
    NSArray *arrra=[[NSArray alloc]init];
    arrra=[[NSUserDefaults standardUserDefaults] objectForKey:@"picArray"];
    NSString *aaa=[arrra objectAtIndex:a];
    
    if ([aaa isEqualToString:@"no image"]) {
        
        
        imageUser.image=[UIImage imageNamed:@"UserMaleIcon"];
        
    }else{
    
    
     [imageUser sd_setImageWithURL:[NSURL URLWithString:aaa] placeholderImage:[UIImage imageNamed:@"UserMaleIcon"]];
    }
    
    labelDescription.text = recent1.name;
    labelLastMessage.text = recent1.lastMessageText;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:recent1.lastMessageDate];
    labelElapsed.text = TimeElapsed(seconds);
    //---------------------------------------------------------------------------------------------------------------------------------------------
    int counter = (int)recent1.unreadMessagesCount;
    labelCounter.text = (counter == 0) ? @"" : [NSString stringWithFormat:@"%d new", counter];
 
    
    
    
}

-(NSString *)imagestr : (NSInteger )ret{
    
   
    ab=[[NSString alloc]init];
    
    [QBRequest userWithID:ret successBlock:^(QBResponse *response, QBUUser *user) {

        ab=user.customData;
        
        if (ab.length<1) {
        ab=@"ab";
        }
    
    } errorBlock:^(QBResponse *response) {

    
    ab=@"abc";
    
    }];
    
    
    while (ab.length<1) {
        
        
        NSLog(@"%@",ab);
    }
    
    
    
        return ab;
    
    
}





@end
