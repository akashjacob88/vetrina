//
//  Friends_view.h
//  Vetrina
//
//  Created by Zappy Dhiiman on 29/08/1937 Saka.
//  Copyright © 1937 Saka Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Friends_view : UIViewController

{
    
    Singleton *singlogin;
    IBOutlet UITableView *friends_table;
    IBOutlet UIView *topBar;
    IBOutlet UITextField *searchText;
    IBOutlet UIButton *serach;
    IBOutlet UITextField *arabicSrch;
    NSMutableArray *array,*array2,*array3;
    NSString *name;
    IBOutlet UIButton *backout;
    IBOutlet UILabel *titleLblout;
    IBOutlet UIView *arebicTopBar;
    IBOutlet UIButton *arebicSrch;
}

@end
