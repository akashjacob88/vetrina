//
//  Vendor_InstapicsAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Vendor_InstapicsAR : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    Singleton *singlogin;
    
    IBOutlet UILabel *vendrNme;
    IBOutlet UITableView *instaImgTble;
    UIButton *confrmBtn;
    NSString *produttname,*delivery11,*minimum11,*unitPrice,*deliverPrice,*rquirement,*quantity,*vendorID,*vendorPic,*prodimag,*img4,*img5,*img6,*amountPay,*orderDate,*orderTime,*dmndQty,*cellLbl,*accesToken;
    
    UIImageView *imageView2,*image,*image2;
    UIImage *pro,*pro1,*pro2,*cimage,*emppppic;
    
    
    NSMutableArray *scrollImage,*aray_2,*venImageArrayList,*followsImageArrayList,*totalArray,*newdatalist;
    NSDictionary *dynamicDict;

}

@end
