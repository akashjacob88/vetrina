//
//  ConfirmProductVenderAR.m
//  Vetrina
//
//  Created by Amit Garg on 5/6/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "ConfirmProductVenderAR.h"
#import "AsyncImageView.h"
#import "ConfirmProductCellAR.h"
#import "JSON.h"
#import "CartVendorProductListAR.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "push.h"




@interface ConfirmProductVenderAR ()
{
    NSMutableArray *users;
}

@end

@implementation ConfirmProductVenderAR


- (void)viewDidLoad

{
    [super viewDidLoad];
    
    singletonn=[Singleton instance];
    NSLog(@"%@",singletonn.productArray);
    
    users = [[NSMutableArray alloc]init];
    
//    PFUser *user = [PFUser currentUser];
//    PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
//    [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
//    [query orderByAscending:PF_USER_FULLNAME];
//    [query setLimit:1000];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (error == nil)
//         {
//             [users removeAllObjects];
//             [users addObjectsFromArray:objects];
//             // [productListTable reloadData];
//         }
//         else [ProgressHUD showError:@"Network error."];
//     }];

    [confirmTable reloadData];
    confirmTable.transform = CGAffineTransformMakeRotation(-M_PI);
    float t1=0;
    for(NSInteger i =0;i<singletonn.productArray.count;i++)
    {
        NSString *unitprice=[[singletonn.productArray objectAtIndex:i]valueForKey:@"unitprice"];
        NSString *quantity=[[singletonn.productArray objectAtIndex:i]valueForKey:@"quantityprod"];
        orderStr=[[singletonn.productArray objectAtIndex:i]valueForKey:@"orderid"];
        addressStr = [[singletonn.productArray objectAtIndex:i]valueForKey:@"useraddress"];
        float u1=[unitprice floatValue];
        float q1=[quantity floatValue];
        float total=u1*q1;
        t1=t1+total;
    }
    
    totalPriceStr=[NSString stringWithFormat:@"%.3f",t1];
    indicator.hidden=YES;
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [singletonn.productArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConfirmProductCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"confirmCell1"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ConfirmProductCellAR" bundle:nil] forCellReuseIdentifier:@"confirmCell1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"confirmCell1"];
    }
//    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 49, 414, 1)];
//    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
//    [cell.contentView addSubview:v11];
    cell.transform = CGAffineTransformMakeRotation(M_PI);
    cell.backgroundColor=[UIColor whiteColor];
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(ConfirmProductCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.prodName.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"productname"];
    NSString *price = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    cell.priceLbl.text=[NSString stringWithFormat:@"%@",price];
    cell.qtyLbl.text = [[singletonn.productArray objectAtIndex:indexPath.row]valueForKey:@"quantityprod"];
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.prodImg.imageURL];
    cell.prodImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[singletonn.productArray objectAtIndex:indexPath.row]objectForKey:@"prodimage"]]];
    
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    UIButton *statusBtn;
    UIView *v1,*v2,*v3,*v4;
    UILabel *deliveryLbl,*TotalLbl,*deliverAddressLbl,*deliveryLbl1,*TotalLbl1;
    UITextField *deliveryPriceTxt;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 304, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 304, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 304, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(100, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 100, 150, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 50, 150, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(15, 151, 274, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            deliveryLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 100, 30, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 50, 100, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            deliveryPriceTxt=[[UITextField alloc]initWithFrame:CGRectMake(35, 100, 70, 50)];
            deliveryPriceTxt.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 304, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 304, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 304, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(100, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 100, 150, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(140, 50, 150, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(15, 151, 274, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:17];
            deliveryLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 100, 30, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, 50, 100, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            deliveryPriceTxt=[[UITextField alloc]initWithFrame:CGRectMake(35, 100, 70, 50)];
            deliveryPriceTxt.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 361, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 361, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 361, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 361, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(120, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(165, 100, 175, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(165, 50, 175, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 151, 315, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:18];
            deliveryLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(15, 100, 30, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(15, 50, 120, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            deliveryPriceTxt=[[UITextField alloc]initWithFrame:CGRectMake(40, 100, 70, 50)];
            deliveryPriceTxt.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            statusBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 50)];
            v1=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 398, 1)];
            v2=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 398, 1)];
            v3=[[UIView alloc]initWithFrame:CGRectMake(0, 150, 398, 1)];
            v4=[[UIView alloc]initWithFrame:CGRectMake(130, 50, 1, 100)];
            deliveryLbl=[[UILabel alloc]initWithFrame:CGRectMake(180, 100, 200, 50)];
            TotalLbl=[[UILabel alloc]initWithFrame:CGRectMake(180, 50, 200, 50)];
            deliverAddressLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 151, 354, 60)];
            deliverAddressLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            statusBtn.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:19];
            deliveryLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            
            deliveryLbl1=[[UILabel alloc]initWithFrame:CGRectMake(17, 100, 30, 50)];
            TotalLbl1=[[UILabel alloc]initWithFrame:CGRectMake(17, 50, 130, 50)];
            deliveryLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            TotalLbl1.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:15];
            deliveryPriceTxt=[[UITextField alloc]initWithFrame:CGRectMake(42, 100, 70, 50)];
            deliveryPriceTxt.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
            
            
        }
    }
    [statusBtn setTitle:@"أكد" forState:UIControlStateNormal];
    [statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [statusBtn setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    [statusBtn addTarget:self action:@selector(confirmBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    v1.backgroundColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    v2.backgroundColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    v3.backgroundColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    v4.backgroundColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    deliveryLbl.text=@"رسوم التوصيل";
    deliveryLbl.textAlignment=NSTextAlignmentRight;
    deliveryLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    TotalLbl.text=@"مجموع";
    TotalLbl.textAlignment=NSTextAlignmentRight;
    TotalLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    deliveryLbl1.text=@"KD";
    deliveryLbl1.textAlignment=NSTextAlignmentRight;
    deliveryLbl1.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    
    if ([deliPriceStr isEqualToString:@""])
    {
        deliveryPriceTxt.text=@"0.000";
    }
    else if([deliPriceStr isEqualToString:nil])
    {
        deliveryPriceTxt.text=@"0.000";
    }
    else
    {
        deliveryPriceTxt.text=deliPriceStr;
    }
    deliveryPriceTxt.textAlignment=NSTextAlignmentRight;
    deliveryPriceTxt.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    [deliveryPriceTxt setDelegate:self];
    deliveryPriceTxt.keyboardType=UIKeyboardTypeDecimalPad;
    deliveryPriceTxt.placeholder=@"0.000";
    NSArray *fields = @[deliveryPriceTxt];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    
    TotalLbl1.text=[NSString stringWithFormat:@"%@ KD",totalPriceStr];
    TotalLbl1.textAlignment=NSTextAlignmentRight;
    TotalLbl1.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    
    deliverAddressLbl.text= addressStr;
    deliverAddressLbl.textAlignment=NSTextAlignmentLeft;
    deliverAddressLbl.numberOfLines=0;
    deliverAddressLbl.textColor=[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    
    [headerView addSubview:deliverAddressLbl];
    [headerView addSubview:statusBtn];
    [headerView addSubview:v1];
    [headerView addSubview:v2];
    [headerView addSubview:v3];
    [headerView addSubview:deliveryLbl];
    [headerView addSubview:TotalLbl];
    [headerView addSubview:v4];
    [headerView addSubview:deliveryLbl1];
    [headerView addSubview:TotalLbl1];
    [headerView addSubview:deliveryPriceTxt];
    
    headerView.backgroundColor=[UIColor whiteColor];
    statusBtn.transform = CGAffineTransformMakeRotation(M_PI);
    v1.transform = CGAffineTransformMakeRotation(M_PI);
    v2.transform = CGAffineTransformMakeRotation(M_PI);
    v3.transform = CGAffineTransformMakeRotation(M_PI);
    v4.transform = CGAffineTransformMakeRotation(M_PI);
    deliveryLbl.transform = CGAffineTransformMakeRotation(M_PI);
    TotalLbl.transform = CGAffineTransformMakeRotation(M_PI);
    deliverAddressLbl.transform = CGAffineTransformMakeRotation(M_PI);
    deliveryLbl1.transform = CGAffineTransformMakeRotation(M_PI);
    TotalLbl1.transform = CGAffineTransformMakeRotation(M_PI);
    deliveryPriceTxt.transform = CGAffineTransformMakeRotation(M_PI);
    
    return headerView;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 210;
}


-(void)confirmBtn:(UIButton *)sender
{
   
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to confirm this order?" delegate:self cancelButtonTitle:@"Confirm" otherButtonTitles:@"Cancel",nil];
    [alert show];
    alert.tag = 9;
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 9)
    {
        if (buttonIndex==0)
        {
            [ProgressHUD show:@"Loading..." Interaction:NO];
            
            [self performSelector:@selector(confirmOrderView) withObject:nil afterDelay:0.5f];
        }
    }
}





-(void)confirmOrderView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/order_confirm.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singletonn.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *venIdStr=[[NSString alloc]initWithFormat:@"%@",singletonn.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",venIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *orderidStr=[[NSString alloc]initWithFormat:@"%@",orderStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",orderidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    float price = [deliPriceStr floatValue];
    NSString *deliStr=[[NSString alloc]initWithFormat:@"%0.3f KD",price];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"deliveryprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",deliStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    float price1 = [totalPriceStr floatValue];
    NSString *totalstr=[[NSString alloc]initWithFormat:@"%0.3f KD",price1];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"amountpay\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",totalstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
        [self prodListing];
        singletonn.usrObjctId = [[singletonn.productArray objectAtIndex:0]valueForKey:@"userobjectid"];
        
        
        
        NSString *message =[NSString stringWithFormat:@"%@ Confirmed your order",singletonn.userNameStr                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
     //   NSString *message = @"Confirmed your order";
        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        [aps setObject:message forKey:QBMPushMessageAlertKey];
        [payload setObject:aps forKey:QBMPushMessageApsKey];
        
        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        
        // Send push to users with ids 292,300,1395
        [QBRequest sendPush:pushMessage toUsers:singletonn.usrObjctId successBlock:^(QBResponse *response, QBMEvent *event) {
            // Successful response with event
            
            
            
        } errorBlock:^(QBError *error) {
            
            
            
            // Handle error
        }];

        
        
        
        
  //      for (NSInteger i=0; i< users.count; i++)
//        {
//            name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//            if ([name containsString:singletonn.usrObjctId])
//            {
//                PFUser *user1 = users[i];
//                PFUser *user2 = [PFUser currentUser];
//                NSString *groupId = StartPrivateChat(user1, user2);
//                NSString *text = @"Confirmed your order";
//                // [self actionChat:groupId];
//                SendPushNotification(groupId, text);
//                
//                i = users.count;
//            }
//        }
//
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    indicator.hidden=YES;
}



-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singletonn.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        
        
    }
    else
    {
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"productname"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        finalArray=[[NSMutableArray alloc]init];
        singletonn.productArray=[[NSMutableArray alloc]init];
        
        for(NSInteger i =0;i<sortedArray.count;i++)
        {
            NSString *stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"userid"];
            if ([stringOne isEqualToString:singletonn.userid]) {
                finalArray=[sortedArray objectAtIndex:i];
                [singletonn.productArray addObject:finalArray];
            }
            
        }
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -160; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    NSInteger movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
    [keyboardControls setActiveField:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
    deliPriceStr=textField.text;
    float d1=[deliPriceStr floatValue];
    float t1=0;
    for(NSInteger i =0;i<singletonn.productArray.count;i++)
    {
        NSString *unitprice=[[singletonn.productArray objectAtIndex:i]valueForKey:@"unitprice"];
        NSString *quantity=[[singletonn.productArray objectAtIndex:i]valueForKey:@"quantityprod"];
        float u1=[unitprice floatValue];
        float q1=[quantity floatValue];
        float total=u1*q1;
        t1=t1+total;
    }
    t1=t1+d1;
    totalPriceStr=[NSString stringWithFormat:@"%.3f",t1];
    
    [confirmTable reloadData];
}


- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}


- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender 
 {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
