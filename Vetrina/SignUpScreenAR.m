//
//  SignUpScreenAR.m
//  Vetrina
//
//  Created by Amit Garg on 7/24/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "SignUpScreenAR.h"

#import "All_SignInAR.h"
#import "Vendor_signAR.h"
#import "User_LoginAR.h"
#import "vendor_loginAR.h"


@interface SignUpScreenAR ()

@end

@implementation SignUpScreenAR

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)userLogIn:(id)sender
{
    User_LoginAR *login   = [[User_LoginAR alloc]initWithNibName:@"User_LoginAR" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}


- (IBAction)userSignUp:(id)sender
{
    All_SignInAR *view = [[All_SignInAR alloc]initWithNibName:@"All_SignInAR" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
}


- (IBAction)vendorLogin:(id)sender
{
    vendor_loginAR *vLogin = [[vendor_loginAR alloc]initWithNibName:@"vendor_loginAR" bundle:nil];
    [self.navigationController pushViewController:vLogin animated:YES];
}


- (IBAction)vendorSignUp:(id)sender
{
    Vendor_signAR *view1 = [[Vendor_signAR alloc]initWithNibName:@"Vendor_signAR" bundle:nil];
    [self.navigationController pushViewController:view1 animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
