//
//  Vendor_Login.m
//  Vetrina
//
//  Created by Amit Garg on 5/18/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_Login.h"
#import "AppConstant.h"
#import "push.h"
#import "ProgressHUD.h"
#import "Vendor_Instagramlogin.h"

#import <Quickblox/Quickblox.h>
@interface Vendor_Login ()

@end

@implementation Vendor_Login

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    signlogin = [Singleton instance];
    
    UIView *leftView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, email.frame.size.height)];
    email.leftView = leftView1;
    email.leftViewMode = UITextFieldViewModeAlways;
    [email setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *leftView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, passwor.frame.size.height)];
    passwor.leftView = leftView2;
    passwor.leftViewMode = UITextFieldViewModeAlways;
    [passwor setValue:[UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];

}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up

{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == email || textField == passwor )
    {
        [self animateTextField:textField up:YES];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if(textField== email || textField== passwor)
    {
        [self animateTextField:textField up:NO];
    }
}



- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)login:(id)sender

{
    NSString *email1 = [email.text lowercaseString];
    NSString *password = passwor.text;
     [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"pass_word"];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email1 length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];

    [QBRequest logInWithUserLogin:email1 password:password successBlock:^(QBResponse *response, QBUUser *user) {
        
                         [ProgressHUD showSuccess:@"Succeed."];
        
        
       
                         signlogin.objectIdStr = [NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
        
        
    //    signlogin.user_active=user;
        
       signlogin.user_id=[NSString stringWithFormat:@"%lu",(unsigned long)user.ID];
        
                         [[NSUserDefaults standardUserDefaults] setObject:signlogin.objectIdStr forKey:@"object_idVender"];
        
        
                          [self performSelector:@selector(subscriptionAction)withObject:Nil afterDelay:1.0f];
                         [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user.fullName]];
                         [self dismissViewControllerAnimated:YES completion:nil];
      
        
        
        // Success, do something
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"error: %@", response.error);
        [ProgressHUD showError:@""];
    }];

    
    
    
    
    
    
    
   
//        NSString *email1 = [email.text lowercaseString];
//        NSString *password = passwor.text;
//        //---------------------------------------------------------------------------------------------------------------------------------------------
//        if ([email1 length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
//        if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
//        //---------------------------------------------------------------------------------------------------------------------------------------------
//        [ProgressHUD show:@"Signing in..." Interaction:NO];
//        [PFUser logInWithUsernameInBackground:email1 password:password block:^(PFUser *user, NSError *error)
//         {
//             if (user != nil)
//             {
//                 ParsePushUserAssign();
//                 [ProgressHUD showSuccess:@"Succeed."];[[PFUser currentUser] fetch];
//                 if ([PFUser currentUser])
//                     NSLog(@"current user: %@", [[PFUser currentUser] objectId]);
//                 signlogin.objectIdStr = [[PFUser currentUser] objectId];
//                 
//                 [[NSUserDefaults standardUserDefaults] setObject:signlogin.objectIdStr forKey:@"object_idVender"];
//                 
//                 
//                  [self performSelector:@selector(loginView)withObject:Nil afterDelay:1.0f];
//                 [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back %@!", user[PF_USER_FULLNAME]]];
//                 [self dismissViewControllerAnimated:YES completion:nil];
//             }
//             else [ProgressHUD showError:error.userInfo[@"error"]];
//         }];
    
    
}


-(void)subscriptionAction{
    
    
    
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey: @"device_token"];
    
    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
        
        
        
        [self loginView];
        
        
        NSLog(@"%ld",(long)response.status);
        
    } errorBlock:^(QBResponse *response) {
        
        NSLog(@"%ld",(long)response.error);
        
        
    }];
    
    
    
    
}


-(void)loginView

{
    Vendor_Instagramlogin *login = [[Vendor_Instagramlogin alloc]initWithNibName:@"Vendor_Instagramlogin" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField

{
    
    [email resignFirstResponder];
    [passwor resignFirstResponder];
    return TRUE;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    // Prevent crashing undo bug – see note below.
//    if(range.length + range.location > passwor.text.length)
//    {
//        return NO;
//    }
//    
//    NSUInteger newLength = [passwor.text length] + [string length] - range.length;
//    return newLength <= 6;
//}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}






- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)termsAction:(id)sender {
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://VetrinaApp.com/PrivacyPolicy.html"]];

    
}
@end
