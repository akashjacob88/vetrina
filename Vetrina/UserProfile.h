//
//  UserProfile.h
//  Vetrina
//
//  Created by Amit Garg on 9/25/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface UserProfile : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

{
    Singleton *singlogin;
    IBOutlet UIImageView *profilePic;
    IBOutlet UIView *view1,*view2;
    IBOutlet UIView *topBar;
    
    
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    UIImageView  *Image2;
    UIImage *cimage,*cimage2;
    UIImage *picture ;
    UIImage *thumbnail;

    
}

@property(strong,nonatomic) NSString *test;
- (IBAction)oderHistory_Action:(id)sender;

@end
