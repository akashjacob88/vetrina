//
//  Vendor_phoneAR.m
//  Vetrina
//
//  Created by Zappy Dhiiman on 30/08/1937 Saka.
//  Copyright © 1937 Saka Amit Garg. All rights reserved.
//

#import "Vendor_phoneAR.h"
#import "ProgressHUD.h"
#import "JSON.h"
#import "New_address_cell.h"
#import "ViewController.h"
@interface Vendor_phoneAR ()

@end

@implementation Vendor_phoneAR
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    singling =  [Singleton instance];
    [self.navigationController setNavigationBarHidden:YES];
    
    countryCodetbl.hidden = YES;
    countryCodetbl.sectionHeaderHeight=0;
    countryCodetbl.sectionFooterHeight=0;
    
    confirmOtl.layer.shadowRadius = 2.0f;
    confirmOtl.layer.shadowOffset = CGSizeMake(0, 2);
    confirmOtl.layer.shadowColor = [UIColor blackColor].CGColor;
    confirmOtl.layer.shadowOpacity = 0.5f;
    
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    view2Shadow.layer.shadowRadius = 2.0f;
    view2Shadow.layer.shadowOffset = CGSizeMake(0, 2);
    view2Shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    view2Shadow.layer.shadowOpacity = 0.5f;
    
    view1Shadow.layer.shadowRadius = 2.0f;
    view1Shadow.layer.shadowOffset = CGSizeMake(0, 2);
    view1Shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    view1Shadow.layer.shadowOpacity = 0.5f;
    
    NSString *str = [NSString stringWithFormat:@"@%@",singling.vendorName];
    instaNameLbl.text = str;
    phoneText.text = singling.vPhone;
    countrycode.text=  singling.vCountry;

    NSArray *fields = @[countrycode,phoneText];
    keyboardControls=[[BSKeyboardControls alloc] initWithFields:fields];
    [keyboardControls setDelegate:self];
    
    [self country];
    
}
-(void)country

{
    NSURL *url = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/countrylist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dic =[data JSONValue];
    
    dataArray = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    NSLog(@"GetDatadictt--%@",dataArray);
    
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"country"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [sortedArray1 mutableCopy];
    [countryCodetbl reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [dataArray count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    New_address_cell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"New_address_cell" bundle:nil] forCellReuseIdentifier:@"cell_1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell_1"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(New_address_cell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *contryNme = [[dataArray objectAtIndex:indexPath.row]valueForKey:@"country"];
    NSString *contryCode = [[dataArray objectAtIndex:indexPath.row]valueForKey:@"countrycode"];
    cell.lable1.text = [NSString stringWithFormat:@"%@ %@",contryNme,contryCode];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *contryNme = [[dataArray objectAtIndex:indexPath.row]valueForKey:@"country"];
    NSString *contryCode = [[dataArray objectAtIndex:indexPath.row]valueForKey:@"countrycode"];
    countrycode.text = [NSString stringWithFormat:@"%@ %@",contryNme,contryCode];
    countryCodetbl.hidden = YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [countrycode resignFirstResponder];
    [phoneText resignFirstResponder];
    return TRUE;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [countrycode resignFirstResponder];
    [phoneText resignFirstResponder];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (textField == countrycode || textField == phoneText )
    {
        [self animateTextField:textField up:YES];
    }
    [keyboardControls setActiveField:textField];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    if(textField==countrycode || textField==phoneText)
    {
        [self animateTextField:textField up:NO];
    }
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    NSInteger movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    view = field.superview.superview.superview;
}


- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}


- (IBAction)back:(id)sender
{
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Changes have not saved, are you sure?" delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Cancel",nil];
    [alert show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==0) {
        [self.navigationController popViewControllerAnimated:YES];

    }else{
        
    }
    
}

- (IBAction)srabic:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(Submitted)withObject:Nil afterDelay:3.0f];
    
}


-(void)Submitted
{
    NSString *strAR = @"1";
    [[NSUserDefaults standardUserDefaults] setObject: nil forKey: @"arabic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject: strAR forKey: @"english"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    
    //  ViewController *view = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    
    [self.navigationController pushViewController:view animated:YES];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)countryBtn:(id)sender
{
    countryCodetbl.hidden = NO;
}


- (IBAction)confire:(id)sender
{
    if (phoneText.text.length > 0 && countrycode.text.length > 0)// && phoneText.text.length >0)
    {
        
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(Submitted1)withObject:Nil afterDelay:3.0f];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Text Fields" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


-(void)Submitted1

{
    
    
    //    NSData *myimagedata = UIImageJPEGRepresentation(vendorImg.image, 90);
    
    
    
    NSURL *strURL = [NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/editphone.php"];
    
    NSLog(@"strURL:%@", strURL);
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:strURL];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *vendrIdstr=[[NSString alloc]initWithFormat:@"%@",singling.vUniqueId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vendrIdstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *shopNamestr=[[NSString alloc]initWithFormat:@"%@",phoneText.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"phoneno\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",shopNamestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *country_code=[[NSString alloc]initWithFormat:@"%@",countrycode.text];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"countrycode\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",country_code] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"countrycode\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@",country_code] dataUsingEncoding:NSUTF8StringEncoding]];

    
    
    NSString *subid=[[NSString alloc]initWithFormat:@"%@",singling.subId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"subid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",subid] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [theLoginRequest  setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *val=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",val);
    
    
    NSMutableArray *valu = [val valueForKey:@"detail"];
    
    
    
    NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    singling.vPhone =[[valu objectAtIndex:0] valueForKey:@"phoneno"];
    singling.vCountry=[[valu objectAtIndex:0] valueForKey:@"countrycode"];

    
    [ProgressHUD showSuccess:@""];
    [self.navigationController popViewControllerAnimated:NO];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
