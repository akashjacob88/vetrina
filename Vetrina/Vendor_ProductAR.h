//
//  Vendor_ProductAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 01/05/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"



@interface Vendor_ProductAR : UIViewController

{
    Singleton *singloging;
    IBOutlet UIButton *orderbtn;
    IBOutlet UIButton *itembtn;
    IBOutlet UILabel *vendorName;
    
    NSMutableArray *finalArray,*dataArray,*newArray,*array,*userNameArray,*imageArr;
    NSInteger i;
    NSString *stringOne,*newString,*stringTwo,*orderId;
    NSMutableDictionary *dynamicDict;
    IBOutlet UITableView *userListingTable;
    IBOutlet UIActivityIndicatorView *indicator;
        NSString *appear;
    IBOutlet UIView *novendrView;
    UIRefreshControl *refreshControl;

}
@property (strong, nonatomic) IBOutlet UILabel *count_lbl;

@end
