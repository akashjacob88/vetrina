//
// Copyright (c) 2015 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Parse/Parse.h>
#import "ProgressHUD.h"

#import "AppConstant.h"
#import "common.h"
#import "recent.h"

#import "RecentView.h"
#import "RecentCell.h"
#import "ChatView.h"

#import "UserProfile.h"
#import "ViewController.h"
#import "Favorites.h"
#import "CartProductList.h"
#import "Home_VetrinaARViewController.h"
#import "Vendor_itemsAR.h"
#import "Vendor_ProductAR.h"
#import "JSON.h"

#import "chat_New.h"
#import "Vendor_Product.h"
#import "Vendor_Profile.h"
#import "Vendor_items.h"
#import "FavoritesAR.h"
#import "CartProductAR.h"
#import "User_ProfileAR.h"
#import "Vendor_profileAR.h"
//#import "SelectSingleView.h"
//#import "SelectMultipleView.h"
//#import "AddressBookView.h"
//#import "FacebookFriendsView.h"
//#import "NavigationController.h"


#import "UIImageView+WebCache.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface RecentView()<UITableViewDataSource, UITableViewDelegate>
{
	NSMutableArray *recents;
    IBOutlet UILabel *cartLbl;
    IBOutlet UILabel *countLbl;
    IBOutlet UITableView *tableView;
    IBOutlet UIImageView *profilepic;
    IBOutlet UIView *topBar;
    IBOutlet UIView *vendortView;
    IBOutlet UIView *userView;
    IBOutlet UILabel *countLbl1;
    NSInteger n;
    QBUUser *currentUser;
    QBChatDialog *recent_partner;
    NSMutableArray *dataArray;
    NSMutableArray *imageArray;
    IBOutlet UILabel *message_lbl_tittle;
    int num;
    
}

//@property (strong, nonatomic) IBOutlet UIImageView *imageUser;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation RecentView

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tab_recent"]];
        self.tabBarItem.title = @"Messages";
        //-----------------------------------------------------------------------------------------------------------------------------------------
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionCleanup) name:NOTIFICATION_USER_LOGGED_OUT object:nil];
    }
    return self;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidLoad];
    singlogin = [Singleton instance];
    num=0;
    [self prodListing123];
    
    imageArray=[[NSMutableArray alloc]init];
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    NSString *abc=    [[NSUserDefaults standardUserDefaults] valueForKey:@"language"];

    if ([abc isEqualToString:@"arebic"]) {
        message_lbl_tittle.text=@"رسائل";
        [message_lbl_tittle setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:18]];
        
        [profilepic setFrame:CGRectMake(self.view.frame.size.width-48, topBar.frame.size.height-43, 40, 40)];
        
        [_setting_btn_outlet setFrame:CGRectMake(self.view.frame.size.width-50, topBar.frame.size.height-45, 50, 45)];
        profilepic.backgroundColor=[UIColor clearColor];

        [topBar addSubview:profilepic];
        [topBar addSubview:_setting_btn_outlet];

        
    }
  //  [[self titleLabel] setFont:[UIFont fontWithName:@"System" size:36]];

//	self.title = @"Messages";
//    
//    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
//   
	//---------------------------------------------------------------------------------------------------------------------------------------------
    self.navigationController.navigationBarHidden = YES;
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:266.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:1];
//    
//    
//    
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
	[tableView registerNib:[UINib nibWithNibName:@"RecentCell" bundle:nil] forCellReuseIdentifier:@"RecentCell"];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
    countLbl.hidden = YES;
    countLbl1.hidden = YES;
    cartLbl.hidden = YES;
    
    recents = [[NSMutableArray alloc] init];

    [self signIn];
//	self.refreshControl = [[UIRefreshControl alloc] init];
//	[self.refreshControl addTarget:self action:@selector(loadRecents) forControlEvents:UIControlEventValueChanged];
    [self loadRecents];
    [self cartTotalView];
    
    
    
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)signIn

{
    if ([singlogin.loginStatus isEqual:@"User Login"])
    {
        NSURL *url1=[NSURL URLWithString:singlogin.userproilePic];
        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        UIImage *emppppic=[UIImage imageWithData:datapic];
        profilepic.image=emppppic;
        
        if (emppppic==nil) {
            
            profilepic.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
            
            
        }
        
        profilepic.layer.masksToBounds = YES;
        profilepic.layer.cornerRadius = profilepic.frame.size.width/2;
        profilepic.layer.opaque = NO;
        userView.hidden = NO;
        vendortView.hidden = YES;
        
    }
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        NSURL *url1=[NSURL URLWithString:singlogin.venPic];
        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        UIImage *emppppic=[UIImage imageWithData:datapic];
        profilepic.image=emppppic;
        profilepic.layer.masksToBounds = YES;
        profilepic.layer.borderWidth = 1.0f;
        profilepic.layer.cornerRadius = profilepic.frame.size.width/2;;
        profilepic.layer.borderColor = [[UIColor clearColor]CGColor];
        profilepic.clipsToBounds = YES;
        profilepic.layer.opaque = NO;
        userView.hidden = YES;
        vendortView.hidden = NO;
        
        
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidAppear:animated];
	//---------------------------------------------------------------------------------------------------------------------------------------------
//	if ([PFUser currentUser] != nil)
//	{
//		[self loadRecents];
//	}
//	else LoginUser(self);
}

#pragma mark - Backend methods
-(void)prodListing123
{
    //     NSString *post1;
    //    if ([singloging.loginStatus isEqual:@"User Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
    //    }
    //    else if ([singloging.loginStatus isEqual:@"IG Login"])
    //    {
    //        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    //    }
    
    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singlogin.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    
    
    if (dataArray.count==0) {
        vendorChatLblk.hidden=YES;
        
        
    }else{
        int count1=0;
        
        for (int a=0; a<dataArray.count; a++) {
            
            NSString *aarra =[[dataArray objectAtIndex:a] valueForKey:@"statusid"];
            
            if ([aarra isEqualToString:@"2"]) {
                count1++;
            }
            
        }
        
        if (count1 == 0) {
            vendorChatLblk.hidden=YES;
        }else{
            
            
            vendorChatLblk.hidden=NO;
            vendorChatLblk.text=[NSString stringWithFormat:@"%d",count1];
            vendorChatLblk.layer.cornerRadius=8.f;
            vendorChatLblk.layer.masksToBounds=YES;
            
        }
    }
    
    
    
    
}



-(void)photo :(int )abc{
    
    n=0;
    if (abc>=recents.count) {
        
        [[NSUserDefaults standardUserDefaults]setObject:imageArray forKey:@"picArray"];
        [tableView reloadData];
    
        
        [ProgressHUD showSuccess:@"" Interaction:YES];
    
    }else{
        
        QBChatDialog *user111=recents[abc];
        NSArray *list123= user111.occupantIDs;
      
        
        for (int q=0; q<list123.count; q++) {
            
            
            NSString *str=[NSString stringWithFormat:@"%@",[list123 objectAtIndex:q]];
            NSString *str1=singlogin.user_id;
            
            
            
            
            if ([str isEqualToString:str1]) {
                
                
            }else{
                
                n=[[list123 objectAtIndex:q] integerValue];

                
            }
            
            
        }
        
        
    
    
    [QBRequest userWithID:n successBlock:^(QBResponse *response, QBUUser *user) {
        
        NSString *ii=user.customData;
        
        
        if (ii.length <1) {
            [imageArray addObject:@"no image"];
            
            
            
        }else{
            [imageArray addObject:user.customData];
        }
        
        num++;
        
        
        
        [self photo:num];
        
        
    } errorBlock:^(QBResponse *response) {
        
        
        [ProgressHUD showSuccess:@"" Interaction:YES];
    }];
    
    
    }
    
    NSLog(@"----------------------------------------%@",imageArray);
//     [tableView reloadData];
}




//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"Occupants ID": singlogin.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
       [ recents  addObjectsFromArray:dialogObjects];
        [self photo:num];
        
       
       [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        [ProgressHUD showSuccess:@""];
       // [ProgressHUD showError:@"Network error."];
     //   [self.refreshControl endRefreshing];
        
    }];
    
    
    
    
//	PFQuery *query = [PFQuery queryWithClassName:PF_RECENT_CLASS_NAME];
//	[query whereKey:PF_RECENT_USER equalTo:[PFUser currentUser]];
//	[query includeKey:PF_RECENT_LASTUSER];
//	[query orderByDescending:PF_RECENT_UPDATEDACTION];
//	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//	{
//		if (error == nil)
//		{
//			[recents removeAllObjects];
//			[recents addObjectsFromArray:objects];
//			[tableView reloadData];
//			[self updateTabCounter];
//		}
//		else [ProgressHUD showError:@"Network error."];
//		//[self.refreshControl endRefreshing];
//	}];
}






#pragma mark - Helper methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	int total = 0;
//	for (PFObject *recent in recents)
//	{
//		total += [recent[PF_RECENT_COUNTER] intValue];
//	}
//    countLbl.text=[NSString stringWithFormat:@"%d", total];
//    countLbl1.text=[NSString stringWithFormat:@"%d", total];
//	   
//    if (singlogin.userid == nil)
//    {
//        countLbl.hidden=YES;
//        countLbl1.hidden = YES;
//    }
//    else
//    {
//        if ([countLbl.text isEqualToString:@"0"])
//        {
//            countLbl.hidden=YES;
//            countLbl1.hidden = YES;
//        }
//        else if ([countLbl.text isEqualToString:@""])
//        {
//            countLbl.hidden=YES;
//            countLbl1.hidden = YES;
//        }
//        else
//        {
//            countLbl.hidden=NO;
//            countLbl1.hidden = NO;
//            countLbl.layer.cornerRadius=countLbl.frame.size.height/2;
//            countLbl.clipsToBounds=YES;
//            countLbl1.layer.cornerRadius=countLbl1.frame.size.height/2;
//            countLbl1.clipsToBounds=YES;
//            //countLbl.text=[NSString stringWithFormat:@"%d", total];
//        }
//    }

}

-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        cartLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            cartLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            cartLbl.hidden=YES;
        }
        else
        {
            cartLbl.hidden=NO;
            cartLbl.layer.cornerRadius=cartLbl.frame.size.height/2;
            cartLbl.clipsToBounds=YES;
            cartLbl.text=singlogin.totalCart;
        }
    }
}

#pragma mark - User actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
    
//	ChatView *chatView = [[ChatView alloc] initWith:groupId];
//	chatView.hidesBottomBarWhenPushed = YES;
//	[self.navigationController pushViewController:chatView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[recents removeAllObjects];
	[tableView reloadData];
	[self updateTabCounter];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCompose
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
//			   otherButtonTitles:@"Single recipient", @"Multiple recipients", @"Address Book", @"Facebook Friends", nil];
//	[action showFromTabBar:[[self tabBarController] tabBar]];
}

#pragma mark - UIActionSheetDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
////-------------------------------------------------------------------------------------------------------------------------------------------------
//{
//	if (buttonIndex != actionSheet.cancelButtonIndex)
//	{
//		if (buttonIndex == 0)
//		{
//			SelectSingleView *selectSingleView = [[SelectSingleView alloc] init];
//			selectSingleView.delegate = self;
//			NavigationController *navController = [[NavigationController alloc] initWithRootViewController:selectSingleView];
//			[self presentViewController:navController animated:YES completion:nil];
//		}
//		if (buttonIndex == 1)
//		{
//			SelectMultipleView *selectMultipleView = [[SelectMultipleView alloc] init];
//			selectMultipleView.delegate = self;
//			NavigationController *navController = [[NavigationController alloc] initWithRootViewController:selectMultipleView];
//			[self presentViewController:navController animated:YES completion:nil];
//		}
//		if (buttonIndex == 2)
//		{
//			AddressBookView *addressBookView = [[AddressBookView alloc] init];
//			addressBookView.delegate = self;
//			NavigationController *navController = [[NavigationController alloc] initWithRootViewController:addressBookView];
//			[self presentViewController:navController animated:YES completion:nil];
//		}
//		if (buttonIndex == 3)
//		{
//			FacebookFriendsView *facebookFriendsView = [[FacebookFriendsView alloc] init];
//			facebookFriendsView.delegate = self;
//			NavigationController *navController = [[NavigationController alloc] initWithRootViewController:facebookFriendsView];
//			[self presentViewController:navController animated:YES completion:nil];
//		}
//	}
//}

#pragma mark - SelectSingleDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectSingleUser:(PFUser *)user2
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PFUser *user1 = [PFUser currentUser];
	NSString *groupId = StartPrivateChat(user1, user2);
	[self actionChat:groupId];
}

#pragma mark - SelectMultipleDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectMultipleUsers:(NSMutableArray *)users
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSString *groupId = StartMultipleChat(users);
	[self actionChat:groupId];
}

#pragma mark - AddressBookDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectAddressBookUser:(PFUser *)user2
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PFUser *user1 = [PFUser currentUser];
	NSString *groupId = StartPrivateChat(user1, user2);
	[self actionChat:groupId];
}

#pragma mark - FacebookFriendsDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectFacebookUser:(PFUser *)user2
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PFUser *user1 = [PFUser currentUser];
	NSString *groupId = StartPrivateChat(user1, user2);
	[self actionChat:groupId];
}

#pragma mark - Table view data source

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return 1;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [recents count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int a =(int)indexPath.row;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", a] forKey:@"indexpath"];
    
	RecentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecentCell" forIndexPath:indexPath];
	[cell bindData1:recents[indexPath.row]];

//    NSString *aaa=[imageArray objectAtIndex:indexPath.row];
//    [_imageUser sd_setImageWithURL:[NSURL URLWithString:aaa] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
//    
//    _imageUser.layer.cornerRadius = _imageUser.frame.size.width/2;
//    _imageUser.layer.masksToBounds = YES;
    
	return cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return YES;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    QBChatDialog *dialog= recents[indexPath.row];
    NSString *groupid=[NSString stringWithFormat:@"%@",dialog.ID];
  //  [recents removeObject:dialog];

    
    
    
    
    
    
    
    
    
    
    
    
//    
//	PFObject *recent = recents[indexPath.row];
//	
//    NSString *groupid=[NSString stringWithFormat:@"%@",[recent valueForKey:@"groupId"]];
//    [recents removeObject:recent];
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	[recent deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//	{
//		if (error != nil) [ProgressHUD showError:@"Network error."];
//	}];
//    
//    PFQuery *query = [PFQuery queryWithClassName:PF_MESSAGE_CLASS_NAME];
//    [query whereKey:PF_MESSAGE_GROUPID equalTo:groupid];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            // The find succeeded.
//            NSLog(@"Successfully retrieved %d scores.", (int)objects.count);
//            // Do something with the found objects
//            for (PFObject *object in objects) {
//                [object deleteInBackground];
//            }
//        } else {
//            // Log details of the failure
//            NSLog(@"Error: %@ %@", error, [error userInfo]);
//        }
//    }];
//    
//    
    
    [self updateTabCounter];

    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Table view delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
    
    recent_partner=recents[indexPath.row];
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    singlogin.namechat=recent_partner.name;
    NSMutableArray *users_array=[[NSMutableArray alloc]initWithArray:recent_partner.occupantIDs];
    
    NSMutableSet *seen = [NSMutableSet set];
    [seen addObject:[NSString stringWithFormat:@"%@",singlogin.user_id]];
    NSUInteger i = 0;
    
    while (i < [users_array count]) {
        
//        QBUUser *user1   =[QBUUser user];
//        
//        user1 =[users_array objectAtIndex:i];
        
        
        id obj = [NSString stringWithFormat:@"%@",[users_array objectAtIndex:i]];
        
        if ([seen containsObject:obj]) {
            [users_array removeObjectAtIndex:i];
            // NB: we *don't* increment i here; since
            // we've removed the object previously at
            // index i, [originalArray objectAtIndex:i]
            // now points to the next object in the array.
        } else {
            //  [seen addObject:obj];
            i++;
        }
    }

    
    
    
    singlogin.chat_user=[users_array objectAtIndex:0];

    //singlogin.chat_user=[NSString stringWithFormat:@"%lu",recent_partner.userID];
    
    [self chat];
    
    
//	PFObject *recent = recents[indexPath.row];
//	[self actionChat:recent[PF_RECENT_GROUPID]];
}


-(void)chat

{
    // myei4MTHya
    
    currentUser = [QBUUser user];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.user_id] integerValue ];
    //    NSInteger i = [ [ NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"] ] integerValue ];
    
    
    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            
            
            [self actionChat:recent_partner.ID];
            
            
            
        }else{
            
            
             [ProgressHUD showSuccess:@"Check internet connection"];
            
        }
    }
     
     
     
     ];
    
    
    
    
    
    
    
 }


- (IBAction)settings:(id)sender
{
    [self performSelector:@selector(pofie) withObject:nil afterDelay:0.5f];
}

-(void)pofie
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(profile) withObject:nil afterDelay:0.5f];
}


-(void)profile
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        if ([singlogin.loginStatus isEqual:@"User Login"])
        {
            User_ProfileAR *view = [[User_ProfileAR alloc]initWithNibName:@"User_ProfileAR" bundle:nil];
            [self.navigationController pushViewController:view animated:NO];
            [ProgressHUD showSuccess:@""];
            
        }
        else if ([singlogin.loginStatus isEqual:@"IG Login"])
        {
            Vendor_profileAR *view = [[Vendor_profileAR alloc]initWithNibName:@"Vendor_profileAR" bundle:nil];
            [self.navigationController pushViewController:view animated:NO];
            [ProgressHUD showSuccess:@""];
        }

    }
    else
    {
    
    if ([singlogin.loginStatus isEqual:@"User Login"])
    {
        UserProfile *view = [[UserProfile alloc]initWithNibName:@"UserProfile" bundle:nil];
        [self.navigationController pushViewController:view animated:NO];
        [ProgressHUD showSuccess:@""];
        
    }
    else if ([singlogin.loginStatus isEqual:@"IG Login"])
    {
        Vendor_Profile *view = [[Vendor_Profile alloc]initWithNibName:@"Vendor_Profile" bundle:nil];
        [self.navigationController pushViewController:view animated:NO];
        [ProgressHUD showSuccess:@""];
    }
    }

}



-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;

}


- (IBAction)home:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
}


-(void)home
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        Home_VetrinaARViewController *fav = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    else
    {
    ViewController *fav = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    
   
    [ProgressHUD showSuccess:@""];
}


- (IBAction)userFav:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(fav) withObject:nil afterDelay:0.5f];
}


-(void)fav
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        FavoritesAR *fav = [[FavoritesAR alloc]initWithNibName:@"FavoritesAR" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    else
    {
    Favorites *fav = [[Favorites alloc]initWithNibName:@"Favorites" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    }
    [ProgressHUD showSuccess:@""];
}


- (IBAction)userCart:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [self performSelector:@selector(cart) withObject:nil afterDelay:0.5f];
}


-(void)cart
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        CartProductAR *fav = [[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    else
    {
    CartProductList *fav = [[CartProductList alloc]initWithNibName:@"CartProductList" bundle:nil];
    [self.navigationController pushViewController:fav animated:NO];
    }
    [ProgressHUD showSuccess:@""];
}


- (IBAction)vendorItemsBtn:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(itemsBtm) withObject:nil afterDelay:0.5f];
}


-(void)itemsBtm
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        Vendor_itemsAR *fav = [[Vendor_itemsAR alloc]initWithNibName:@"Vendor_itemsAR" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    else
    {
    Vendor_items *fav = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:fav
                                         animated:NO];
    }
    [ProgressHUD showSuccess:@""];
}


- (IBAction)vendorOrderBtn:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self performSelector:@selector(orderbtn) withObject:nil afterDelay:0.5f];
}


-(void)orderbtn
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"arabic"] != nil)
    {
        Vendor_ProductAR *fav = [[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
        [self.navigationController pushViewController:fav
                                             animated:NO];
    }
    else
    {
    Vendor_Product *fav = [[Vendor_Product alloc]initWithNibName:@"Vendor_Product" bundle:nil];
    [self.navigationController pushViewController:fav animated:NO];
    }
    [ProgressHUD showSuccess:@""];
}



@end
