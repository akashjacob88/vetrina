//
//  wishlist_ViewController.m
//  Vetrina
//
//  Created by Amit Garg on 2/6/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import "wishlist_ViewController.h"
#import "mentionDetails_Cell.h"
#import <Parse/Parse.h>
#import "recent.h"
#import "ProgressHUD.h"
#import "JSON.h"
#import "AsyncImageView.h"
#import "UIImageView+WebCache.h"

@interface wishlist_ViewController ()
{
    
    NSString *wishlist_id;
    NSArray *item_array ,*sortedarray1;
}
@end

@implementation wishlist_ViewController



-(void)wishlist

{
    
    
    NSString *post= [NSString stringWithFormat:@"userid=%@",wishlist_id];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/wishproductlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    [ProgressHUD showSuccess:@"" Interaction:YES];
    
    if(![[dic objectForKey:@"list"] isEqual:@"No list"])
    {
        
        item_array = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        item_array=[[NSMutableArray alloc]init];
        
    }
    if([item_array count]==0)
    {
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message:@"No List" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSSortDescriptor *itemXml =  [[NSSortDescriptor alloc] initWithKey:@"staffpick" ascending:NO];
        sortedarray1 = [[NSArray alloc] initWithObjects:itemXml, nil];
        sortedarray1 = [item_array sortedArrayUsingDescriptors:sortedarray1];
        
        NSLog(@"--------%@",sortedarray1);
     
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    singlogin = [Singleton instance];

  wishlist_id=  [[NSUserDefaults standardUserDefaults] valueForKey:@"wish_id"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
    {
        _arebicTittle_lbl.hidden=YES;
        _tittle_lbl.text=@"WishList";
        _tittle_lbl.font=[UIFont fontWithName:@"OpenSans-Bold" size:18];
        _arebicBack_btn.hidden=YES;
        
    }else{
        _back_btn.hidden=YES;
        _arebicTittle_lbl.text=@"توصيات من الأصدقاء";
        _tittle_lbl.hidden=YES;
    }

    [self wishlist];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 450;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
    
    
    return sortedarray1.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    mentionDetails_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"details_cell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"mentionDetails_Cell" bundle:nil] forCellReuseIdentifier:@"details_cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"details_cell"];
    }
    
    
    UILabel *name1=(UILabel *)[cell viewWithTag:1];
    name1.text=[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"vendorname"]];
    
    
    UIImageView *img=(UIImageView *)[cell viewWithTag:2];
    NSString *strImageUrl1 = [[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prodimg1"];
    [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@""]];
    
    
    UILabel *product_name=(UILabel *)[cell viewWithTag:3];
    product_name.text=[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prodname"]];
    
    
    
    if ([[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"soldout"] isEqualToString:@"SOLD OUT"]) {
        UILabel *product_price=(UILabel *)[cell viewWithTag:4];
        product_price.text=@"Sold Out";
        
        UIButton *cart_btn=(UIButton *)[cell viewWithTag:7];
        cart_btn.hidden=YES;
        UIImageView *img=(UIImageView *)[cell viewWithTag:6];
        img.hidden=YES;
    }else{
        
        UILabel *product_price=(UILabel *)[cell viewWithTag:4];
        product_price.text=[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"unitprice"]];
        
        UIButton *cart_btn=(UIButton *)[cell viewWithTag:7];
        cart_btn.hidden=NO;
        
        [cart_btn addTarget:self action:@selector(addtocart:) forControlEvents:UIControlEventTouchUpInside];
        
        cart_btn.tag=indexPath.row;
        
        UIImageView *img=(UIImageView *)[cell viewWithTag:6];
        img.hidden=NO;
        
    }
    
    UITextView *product_desc=(UITextView *)[cell viewWithTag:5];
    product_desc.text=[NSString stringWithFormat:@"%@",[[sortedarray1 objectAtIndex:indexPath.row]objectForKey:@"prod_desc"]];
    
    
    return cell;
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)arebicBack_action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}
@end
