//
//  Singleton.h
//  SingletonTest
//
//  Created by Luc Wollants on 02/02/12.
//  Copyright (c) 2012 www.apptite.be All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>

// A class that represents a Singleton
@interface Singleton : NSObject
{

}
@property(nonatomic,strong)NSString *admin_idstr,*logo_id,*vid,*catid,*catname,*scatid,*scatname,*pid,*userid,*catdesc,*testUser,*statusId,*cartValue,*loginStatus,*searchResult,*cartLbl,*uId,*usrName,*usrAdd,*homAddress,*usrPic,*subCatid,*shopNameVendr,*shopDec,*adresId,*check,*usrObjctId,*orderId,*userproilePic,*vendorProfilepic,*dect,*wishListDot,*userFonNo;

@property(nonatomic,strong)NSString *user_id;

@property(nonatomic,strong)NSString *vendor_objectid;

@property(assign,nonatomic)QBUUser *user_active;
@property(nonatomic,strong)NSString *chat_user;

@property(nonatomic)int count_msg;
@property(nonatomic)int count_cart;




@property(nonatomic,strong)NSMutableArray *filteraray,*sectionAnimals;
@property(nonatomic,strong)NSString *userNameStr,*userEmailStr,*userPhoneStr,*userImageStr,*userIdStr,*userTypeStr,*mNamestr,*citystr,*stateStr,*countryStr,*addressStr,*vendorEmail,*objectIdStr,*pedagori,*namechat;
@property(nonatomic,strong)NSString *igUsername, *igUserId, *igFullname, *igProfilePic,*igUniqueId;
@property(nonatomic,strong)NSString *vIGUsername, *vIGUserId, *vIGFullname, *vIGProfilePic, *vSelectPic,*vUniqueId,*fUserId,*followsUniqueId,*vPhone,*vCountry,*vDeliveryCharges,*askforprice,*soldout,*primium;
@property(nonatomic,strong)NSMutableArray *vImgUrl,*vImgUrl2,*vFolowUserName,*vFolowFullName,*vFolowUserId,*vFolowProfilePic,*newarayList1;
@property(nonatomic,strong)UIImageView *imageCamera;

@property(nonatomic,strong)NSString *venId,*vendorName,*about,*deliverPrice,*venPic,*venProdImg,*venProdName,*unitPrice,*venProdQty,*venIdList,*vendorNameList,*likePics,*labelDescription,*vEnPhone,*venInstaId,*venInstaName,*venName,*SlctVenId,*selectInstaPic1,*countryAddres,*cityAddr,*addresaddr;
@property(nonatomic,strong)NSString *prodName,*aboutName,*catName,*unitName,*inventName,*subName,*subId,*delvrName,*miniName,*prodid,*qntyName,*prodImg,*selectedPic,*c1,*c2,*c3,*sc1,*sc2,*sc3,*cI1,*cI2,*cI3,*scI1,*scI2,*scI3;
@property(nonatomic,strong)NSMutableArray *categoryArray,*subCategoryArray,*cart_productIds,*productArray;
@property(nonatomic,strong)NSString *accesstokenNew,*oldAccesstoken,*addressBtnDetail,*area,*block,*street,*jada,*house,*floor,*apartment,*notes,*toPic,*fromPic,*loginfrom,*addVendorSub,*vFolowinstaname,*vFolowphone,*vFolowProfile,*imgeStr,*instagrmStr,*mainCat,*mainSubCat,*prodIdStr;
@property(nonatomic,strong)NSString *eProdName,*eDesc,*eQuant,*ePrice,*eCat,*eSubCat,*epImg1,*epImg2,*epImg3,*epImg4,*epImg5,*nextPage,*eCatId,*eSubCatId,*totalCart,*groupIdStr,*rate_status,*prodNameStr;


@property(nonatomic,strong)NSMutableArray *searchItemArray,*searchShopArray;
// A static method, in Objective-C jargon class method

+ (Singleton*) instance;

@end
