//
//  Singleton.m
//  SingletonTest
//
//  Created by Luc Wollants on 02/02/12.
//  Copyright (c) 2012 www.apptite.be All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

@synthesize admin_idstr,logo_id,filteraray,vid,catid,catname,scatid,scatname,pid,userid,catdesc,prodName,aboutName,catName,unitName,inventName,subName,subId,delvrName,miniName,prodid,qntyName,prodImg,loginStatus,labelDescription,searchResult,vFolowUserName,vFolowFullName,vFolowUserId,vFolowProfilePic,cartLbl,vEnPhone,venInstaId,venInstaName,uId,usrName,usrAdd,homAddress,usrPic,venName,SlctVenId,newarayList1,selectedPic,vPhone,vCountry,vDeliveryCharges,c1,c2,c3,sc1,sc2,sc3,cI1,cI2,cI3,scI1,scI2,scI3,categoryArray,subCategoryArray,accesstokenNew,oldAccesstoken,venPic,addressBtnDetail,area,block,street,jada,house,floor,apartment,notes,toPic,fromPic,loginfrom,addVendorSub,vFolowinstaname,vFolowphone,vFolowProfile,cart_productIds,subCatid,shopNameVendr,vendorEmail,shopDec,objectIdStr,selectInstaPic1,imageCamera,countryAddres,cityAddr,addresaddr,adresId,pedagori,check,usrObjctId,groupIdStr,orderId,sectionAnimals,askforprice,soldout,primium,rate_status,userproilePic,vendorProfilepic,userFonNo;
// Implement the instance class method
+ (Singleton*) instance
{
    // This replaces the static and private data member
	static Singleton* _instance = nil;
    // Construct only when not initialized 
	if (!_instance)
	{
        // Create the Singleton instance
		_instance = [[Singleton alloc]init];
	}
    // Return the Singleton instance
	return _instance;
}

@end