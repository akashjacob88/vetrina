//
//  CartVendorCell.m
//  Vetrina
//
//  Created by Amit Garg on 5/2/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "CartVendorCell.h"

@implementation CartVendorCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
