//
//  Vendor_profileAR.h
//  Vetrina
//
//  Created by Amit Garg on 11/18/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Vendor_profileAR : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    Singleton *singling;
    IBOutlet UIView *topBar;
    IBOutlet UIImageView *profileVendor;
    IBOutlet UIView *view2Shadow;
    IBOutlet UIView *view1Shadow;
    
    UIActionSheet *actionsheet1;
    UIImagePickerController *camerapicker;
    UIImageView  *Image2;
    UIImage *cimage,*cimage2;
    UIImage *picture ;
    UIImage *thumbnail;
 
}
- (IBAction)orderHistory:(id)sender;

@end
