
//
//  Vendor_DetaillistAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface Vendor_DetaillistAR : UIViewController<UIAlertViewDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

{
    Singleton *singlogin;
    NSString *name;
    IBOutlet UIView *twilloView;

    IBOutlet UIButton *reportBtn1;
    IBOutlet UIButton *reportBtn2;
    IBOutlet UIButton *reportBtn3;
    IBOutlet UIView *reportView;
    IBOutlet UILabel *vendrNme;
    IBOutlet UILabel *cartLbl;
    UILabel *sorry,*you,*blackLbl,*whiteBack,*costText3,*costText2;
    IBOutlet UIButton *likeBtn;
    IBOutlet UITableView *productTbl;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn,*confrmBtn;
    IBOutlet UIView *thnxxView;
    IBOutlet UIActivityIndicatorView *indicator;
    UIButton *btn,*btn1,*ordernow,*askPrice,*openInstagram,*btn2;
    NSString *reson;
    UIImageView *venderImge;

    UIImage *pro1,*pro2,*emppppic;
    UIImageView *Image3,*image3;
    UIImageView *Image1,*prod,*Image2,*Image4,*prodImage,*arrowIcon,*cart_img;
    NSMutableArray *imagearrayLIst,*data1,*prodImageArrayList,*likeArray,*totalArray,*followsImageArrayList,*array2Value,*newdatalist;

    NSString *name2,*desc2,*catid,*navLogin,*productName,*venName,*unit,*quantity,*subcatid,*chckLbl,*staffPic;
    NSString *delivery11,*minimum11,*img3,*unitPrice,*deliverPrice,*rquirement,*vendorID,*vendorPic,*prodimag,*img4,*img5,*img6,*amountPay,*orderDate,*orderTime,*dmndQty,*cellLbl,*productname,*prodId,*selectedImageMediaId,*likeStatus,*vendorpic_browserLink,*statusIDe,*productAlreadyStatus,*vendorFeedStatus;
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr,*askForPrice,*soldout,*v_obj;
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;

    NSMutableArray *tapArray;
   
   
    NSMutableArray *cartListArray;
    NSString *cartIdStr,*quantStr;
    
    NSMutableArray *arra_1,*aray_2;
}
- (IBAction)back:(id)sender;
@property (nonatomic, strong) UITextField *textField;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@end
