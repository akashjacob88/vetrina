//
//  orderHistory.h
//  Vetrina
//
//  Created by Amit Garg on 3/30/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface orderHistory : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *english_Btn;
@property (strong, nonatomic) IBOutlet UIButton *arebic_btn;
- (IBAction)english_btnAction:(id)sender;
- (IBAction)arebic_btnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *englishOrderLbl;
@property (strong, nonatomic) IBOutlet UILabel *arebicOrderLbl;

@end
