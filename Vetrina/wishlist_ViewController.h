//
//  wishlist_ViewController.h
//  Vetrina
//
//  Created by Amit Garg on 2/6/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface wishlist_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    
    Singleton *singlogin;

}
@property (strong, nonatomic) IBOutlet UIView *topbar;
@property (strong, nonatomic) IBOutlet UIButton *back_btn;
@property (strong, nonatomic) IBOutlet UILabel *tittle_lbl;
@property (strong, nonatomic) IBOutlet UIButton *arebicBack_btn;
@property (strong, nonatomic) IBOutlet UILabel *arebicTittle_lbl;
- (IBAction)back_action:(id)sender;
- (IBAction)arebicBack_action:(id)sender;

@end
