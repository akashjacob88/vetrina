//
//  WalkthroughAR.m
//  Vetrina
//
//  Created by Amit Garg on 4/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "WalkthroughAR.h"
#import "Home_VetrinaARViewController.h"
#import "JSON.h"
#import "ABCIntroView.h"
#import "ProgressHUD.h"
#import "Vendor_ProductAR.h"


@interface WalkthroughAR ()
@property ABCIntroView *introView;

@end

@implementation WalkthroughAR


- (void)viewDidLoad

{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:@"intro_screen_viewed"])
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                if ([[UIScreen mainScreen] bounds].size.height == 480)
                {
                    self.introView = [[ABCIntroView alloc] initWithFrame:CGRectMake(0, 120, 320, 250)];
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    self.introView = [[ABCIntroView alloc] initWithFrame:CGRectMake(0, 120, 320, 320)];
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 667)
                {
                    self.introView = [[ABCIntroView alloc] initWithFrame:CGRectMake(0, 145, 375, 375)];
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 736)
                {
                    self.introView = [[ABCIntroView alloc] initWithFrame:CGRectMake(0, 153, 414, 430)];
                }
            }
            self.introView.backgroundColor = [UIColor clearColor];
            [self.view addSubview:self.introView];
            [self.view addSubview:Indicator];
        }

    [super viewDidLoad];
    signLogin = [Singleton instance];
    
    strtShopngoult.layer.cornerRadius = 5;
    
    swipImagesArray = [[NSMutableArray alloc]initWithObjects:@"sti01.png", @"sti02.png", @"sti03.png", nil];
    
    [Indicator stopAnimating];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)swaip:(UISwipeGestureRecognizer *)sender
{
    NSLog(@"Swipe Image");
    UISwipeGestureRecognizerDirection direction=[(UISwipeGestureRecognizer *)sender direction];
    
    
    switch (direction)
    
    {
        case UISwipeGestureRecognizerDirectionLeft:
            
            imageIndex++;
            
            if( pageControl.currentPage == 2 )
                
            {
                pageControl.currentPage=0 ;
            }
            
            else
                
            {
                
                pageControl.currentPage+=1;
                
            }
            
            break;
            
        case UISwipeGestureRecognizerDirectionRight:
            imageIndex--;
            
            if( pageControl.currentPage == 0 )
                
            {
                pageControl.currentPage=2 ;
            }
            
            else
                
            {
                
                pageControl.currentPage -=1;
                
            }
            
            break;
        default:
            break;
            
    }
    
    imageIndex=(imageIndex <0) ? ([swipImagesArray count] -1):
    imageIndex % [swipImagesArray count];
    slideImgView.image=[UIImage imageNamed:[swipImagesArray objectAtIndex:imageIndex]];

}

- (IBAction)englishBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)startshopingBtn:(id)sender

{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:2.0f];
}


-(void)home
{
    [self searchItems];
    [self searchShops];
  
    if ([[NSUserDefaults standardUserDefaults] objectForKey: @"email"] != nil)
    {
        [self autoLogingEmail];
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey: @"instagramID"] != nil)
    {
        //[self autoLoginInstagram];
    }
    else if([[NSUserDefaults standardUserDefaults] objectForKey: @"VinstagramID"] != nil)
    {
       [self LoginIG];
    }
    else
    {
        Home_VetrinaARViewController *home = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
    }

}

-(void)LoginIG

{
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"VinstagramID"];
    
    NSString *post = [NSString stringWithFormat:@"instagramid=%@&logintype=%@",savedUsername,@"instagram"];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlogin2.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
    // NSArray *val = [eventarray valueForKey:@"0"];
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique ID %@",uniqueid);
    NSString *phoneno=[eventarray  valueForKey:@"phoneno"];
    NSString *uName=[eventarray  valueForKey:@"username_instagramacnt"];
    NSString *shopName=[eventarray  valueForKey:@"fullname_shopname"];
    NSString *image=[eventarray  valueForKey:@"imagee"];
    signLogin.mainCat=[eventarray  valueForKey:@"main_catname"];
    signLogin.mainSubCat=[eventarray  valueForKey:@"main_subcatname"];
    signLogin.shopDec=[eventarray  valueForKey:@"descr"];
    signLogin.subId=[eventarray  valueForKey:@"subid"];
    signLogin.pid = [eventarray  valueForKey:@"staffpick"];
    signLogin.catid = [eventarray  valueForKey:@"main_catid"];
    signLogin.subCatid = [eventarray valueForKey:@"main_subcatid"];
    signLogin.objectIdStr = [eventarray valueForKey:@"objectid"];
    signLogin.vIGUserId =[eventarray valueForKey:@"instagramid"];
    signLogin.venPic = image;
    signLogin.vendorName=uName;
    signLogin.vUniqueId=uniqueid;
    signLogin.userid=uniqueid;
    signLogin.vPhone = phoneno;
    signLogin.shopNameVendr = shopName;
    //signLogin.loginStatus = @"IG Login";
    //signLogin.loginStatus = @"IG Login";
    if([neww isEqualToString:@"False"])
    {
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
        [ProgressHUD showSuccess:@""];
    }
    else
    {
        if ([signLogin.vUniqueId isEqualToString:@"1653"])
        {
            //singlogin.loginStatus = @"Admin";
            //            Admin *admin=[[Admin alloc]initWithNibName:@"Admin" bundle:NULL];
            //            [self.navigationController pushViewController:admin animated:YES];
        }
        else
        {
            signLogin.loginStatus = @"IG Login";
            Vendor_ProductAR *product=[[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:NULL];
            [self.navigationController pushViewController:product animated:YES];
            [ProgressHUD showSuccess:@""];
        }
    }
}


-(void)autoLogingEmail
{
    
    NSString *savedUsername = [[NSUserDefaults standardUserDefaults]  stringForKey:@"email"];
    NSString *savedPassword = [[NSUserDefaults standardUserDefaults]  stringForKey:@"password"];
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",savedUsername,savedPassword];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/userlogin.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *neww = [eventarray valueForKey:@"Login"];
   
    
    NSString *uniqueid=[eventarray  valueForKey:@"id"];
    NSLog(@"Unique User ID %@",uniqueid);
    NSString *uName=[eventarray  valueForKey:@"username"];
    NSString *uaddress=[eventarray  valueForKey:@"address"];
    signLogin.check = [eventarray valueForKey:@"check"];
    signLogin.usrObjctId = [eventarray valueForKey:@"userobjectid"];
    
    signLogin.homAddress=uaddress;
    signLogin.usrName=uName;
    signLogin.userid=uniqueid;
    
    [self prodListing];
    
    if([neww isEqualToString:@"False"])
    {
        
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"User Login" message:@"This email id is not registered! please create new account!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alert show];
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        [self.navigationController pushViewController:home animated:YES];
    }
    else
    {
        //Vetrina_Home *home=[[Vetrina_Home alloc]initWithNibName:@"Vetrina_Home" bundle:NULL];
        Home_VetrinaARViewController *home=[[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:NULL];
        signLogin.loginStatus = @"User Login";
        [ProgressHUD showSuccess:@""];
        [self.navigationController pushViewController:home animated:YES];
    }
}


-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",signLogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if (cartListArray.count==0) {
        
    }
    else
    {
        NSInteger value=0;
        for(NSInteger i =0;i<cartListArray.count;i++)
        {
            NSString *status=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
            if ([status isEqualToString:@"0"] || [status isEqualToString:@"1"] ) {
                value=value+1;
            }
            
        }
        signLogin.totalCart=[NSString stringWithFormat:@"%ld",(long)value];
    }
}



-(void)searchItems
{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/prodlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        signLogin.searchItemArray=[[NSMutableArray alloc]init];
        signLogin.searchItemArray=searchArray;
    }
}

-(void)searchShops
{
    NSString *post = nil;
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
    }
    else
    {
        NSMutableArray *searchArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
        signLogin.searchShopArray=[[NSMutableArray alloc]init];
        signLogin.searchShopArray=searchArray;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
