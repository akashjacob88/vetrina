//
//  Vendor_DetaillistAR.m
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_DetaillistAR.h"
#import "Sorry_cartAR.h"
#import "AsyncImageView.h"
#import "JSON.h"
#import "Base64.h"
#import "All_SignInAR.h"
#import "ChatView.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "common.h"
#import "recent.h"
#import "ProgressHUD.h"
#import "AppConstant.h"
#import "CartProductAR.h"
#import "push.h"
#import "Home_VetrinaARViewController.h"
#import "FavoritesAR.h"
#import "RecentView.h"
#import "Vendor_ProductAR.h"
#import "Vendor_itemsAR.h"
#import "ARSpeechActivity.h"
#import "UIImageView+WebCache.h"
#import "VCFloatingActionButton.h"
#import "PCRapidSelectionView.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Quickblox/Quickblox.h>
#import "chat_New.h"



@interface Vendor_DetaillistAR ()<floatMenuDelegate>
{
    NSMutableArray *users;
    IBOutlet UIButton *menuBtn;
    IBOutlet UIView *navView;
    IBOutlet UIView *vendorViewBar;
    IBOutlet UIView *itemReport;
    IBOutlet UIView *topBar;
    IBOutlet UIButton *chatVendorOut;
    
    UITableView *list_tableView;
    NSMutableArray *friend_array;
    NSMutableArray *array5;
    UIView *list_view;
    UIButton *cancel_btn;
    NSString *frind_id, *id_str;
    UILabel *cancel_lbl;
    NSArray *dialog_Objects;
    QBUUser *currentUser;
    NSMutableArray *users_array;
    QBChatDialog *chatDialog;
    NSArray *instaArray;
}


@property (strong, nonatomic) VCFloatingActionButton *addButton;


@end

@implementation Vendor_DetaillistAR

@synthesize indicator;

@synthesize addButton;

-(void)friendList{
    
    
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/friend_req_list.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSArray *eventarray =[data JSONValue];
    
    if (eventarray.count<1)
    {
        friend_array = [[NSMutableArray alloc]init];
        
    }
    else
    {
        array5 = [[NSMutableArray alloc]initWithArray:eventarray];
        //        [[NSUserDefaults standardUserDefaults] setObject:array3 forKey:@"request_list"];
        //  array4=[[NSMutableArray alloc]init];
        friend_array=[[NSMutableArray alloc]init];
        
        for (int i=0; i<array5.count; i++) {
            if ([[[array5 objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"sent"]) {
                //  [array4 addObject:[array3 objectAtIndex:i]];
            }else{
                [friend_array addObject:[array5 objectAtIndex:i]];
                
            }
        }
        
        
        
        
    }
    
    //    if (friend_array.count==0)
    //    {
    //    }
    //    else
    //    {
    //        friend_status=@"friends available";
    //        [friends_table reloadData];
    //
    //
    //    }
    
    
    
    
    
}


- (void)viewDidLoad

{
    [super viewDidLoad];
    singlogin = [Singleton instance];
    // vendrNme.text = singlogin.vendorNameList;
    vendrNme.text = singlogin.venName;
    singlogin.namechat=singlogin.venName;
    users = [[NSMutableArray alloc]init];
    [self friendList];
    // TopBar View///
    
    users_array =[[NSMutableArray alloc]init];
    [ProgressHUD showSuccess:@""];
    
    instaArray =[[NSArray alloc]init];
    
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    
    //    if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
    //    {
    //        PFUser *user = [PFUser currentUser];
    //        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
    //        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
    //        [query orderByAscending:PF_USER_FULLNAME];
    //        [query setLimit:1000];
    //        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    //         {
    //             if (error == nil)
    //             {
    //                 [users removeAllObjects];
    //                 [users addObjectsFromArray:objects];
    //                 [productTbl reloadData];
    //             }
    //             else [ProgressHUD showError:@"Network error."];
    //         }];
    //    }
    
    
    if ([singlogin.loginStatus isEqualToString:@"User Login"] || [singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
        
        [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
            
            NSLog(@"%@",dialogObjects);
            dialog_Objects=[[NSArray alloc]init];
            dialog_Objects=dialogObjects;
            
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@",response);
            
            
        }];
        
        
        
        
        //        NSUInteger limit = 1000;
        //        __block NSUInteger skip = 0;
        //
        //        [ProgressHUD show:@"Loading..." Interaction:NO];
        //
        //        [indicator startAnimating];
        //
        //
        //        PFUser *user = [PFUser currentUser];
        //        PFQuery *query = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
        //        [query whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
        //        [query orderByAscending:PF_USER_FULLNAME];
        //        //  [query setLimit:1000];
        //        [query setLimit: limit];
        //        [query setSkip: skip];
        //
        //
        //        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
        //         {
        //
        //             [indicator setHidden:NO];
        //             [indicator startAnimating];
        //             [ProgressHUD show:@"Loading..." Interaction:NO];
        //
        //             if (error == nil)
        //             {
        //                 [users addObjectsFromArray:objects];
        //                 NSLog(@"%lu  111  ",(unsigned long)objects.count);
        //                 skip += limit;
        //                 [query setSkip: skip];
        //
        //                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                     if (error == nil){
        //                         [users addObjectsFromArray:objects];
        //                         skip += limit;
        //                         [query setSkip: skip];
        //
        //                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                             if (error == nil){
        //                                 [users addObjectsFromArray:objects];
        //                                 skip += limit;
        //                                 [query setSkip: skip];
        //
        //                                 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                                     if (error == nil){
        //                                         [users addObjectsFromArray:objects];
        //                                         skip += limit;
        //                                         [query setSkip: skip];
        //
        //                                         [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //
        //                                             if (error == nil){
        //
        //                                                 [users addObjectsFromArray:objects];
        //                                                 skip += limit;
        //                                                 [query setSkip: skip];
        //                                                 [ProgressHUD showSuccess:@""];
        //
        //
        //                                             }
        //
        //
        //                                         }];
        //
        //
        //                                     }
        //
        //
        //                                 }];
        //
        //
        //                             }
        //
        //
        //                         }];
        //
        //
        //                     }
        //
        //
        //                 }];
        //
        //                 NSLog(@"%lu  444",(unsigned long)objects.count);
        //
        //             }
        //
        //             //                 [users removeAllObjects];
        //             //                 [users addObjectsFromArray:objects];
        //             // [productListTable reloadData];
        //
        //             else [ProgressHUD showError:@"Network error."];
        //         }];
    }
    
    
    twilloView.hidden = YES;
    
    cartLbl.hidden = YES;
    
    //    [indicator stopAnimating];
    //    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
    [self vendorlisting];
    likeStatus = @"unlike";
    
    tapArray = [[NSMutableArray alloc] init];
    [thnxxView setHidden:YES];
    
    
    
    
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 40, [UIScreen mainScreen].bounds.size.height - 44 - 80, 80, 80);
    
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"More1.png"] andPressedImage:[UIImage imageNamed:@"MoreClose.png"] withScrollview:productTbl];
    
    addButton.imageArray = @[@"ReportFlot.png",@"ContactsFlot.png",@"CallFlot.png",@"OpenInInstagramFlot.png",@"FavoriteFlot.png"];
    
    // addButton.labelArray = @[@"Report",@"Add to contacts",@"Call",@"Open in Instagram",@"Add to favourites"];
    addButton.labelArray = @[@"تحذير", @"أضف إلى الجوال",@"اتصال هاتفي",@"فتح في انستقرام",@"أضف إلى المحلات المفضلة"];
    
    addButton.hideWhileScrolling = YES;
    addButton.delegate = self;
    
    [self.view addSubview:addButton];
    
    
    
    [self performSelector:@selector(vendorfavlisting) withObject:nil afterDelay:3.0f];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:singlogin.vendorProfilepic]];
        
        //set your image on main thread.
        dispatch_async(dispatch_get_main_queue(), ^{
            emppppic =[UIImage imageWithData:data];
            
        });
    });
    
    reportView.hidden = YES;
    
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        navView.hidden = YES;
        vendorViewBar.hidden = NO;
    }
    else if ([singlogin.loginStatus isEqualToString:@"User Login"])
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        cartLbl.hidden=YES;
        [self cartTotalView];
        
    }
    else
    {
        navView.hidden = NO;
        vendorViewBar.hidden = YES;
        cartLbl.hidden=YES;
        [self cartTotalView];
    }
    
}


-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        cartLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            cartLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            cartLbl.hidden=YES;
        }
        else
        {
            cartLbl.hidden=NO;
            cartLbl.layer.cornerRadius=cartLbl.frame.size.height/2;
            cartLbl.clipsToBounds=YES;
            cartLbl.text=singlogin.totalCart;
        }
    }
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)setAllElementofArrayToZero
{
    for(int i = 0;i < totalArray.count ;i++)
    {
        [tapArray addObject:[NSNumber numberWithInteger:0]];
        
    }
}




-(void) CountImg

{
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        totalArray = array2Value;
        instaArray = singlogin.newarayList1;
        
        chatVendorOut.hidden = NO;
    }
    else
    {
        totalArray = singlogin.newarayList1;
        chatVendorOut.hidden = YES;
    }
    
    if (totalArray.count == 0)
    {
        chatVendorOut.hidden = YES;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Account is not on Vetrina yet. Would you like to invite them?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alertView.tag=2;
        [alertView show];
        [indicator stopAnimating];
        
    }
    prodImageArrayList = [[NSMutableArray alloc]init];
    followsImageArrayList = [[NSMutableArray alloc]init];
    
    
    for(int i =0;i<totalArray.count;i++)
    {
        Image3 =[[UIImageView alloc]init];
        UIGraphicsBeginImageContext(Image3.image.size);
        {
            CGContextRef ctx = UIGraphicsGetCurrentContext();
            CGAffineTransform trnsfrm = CGAffineTransformConcat(CGAffineTransformIdentity,CGAffineTransformMakeScale(1.0, -1.0));
            trnsfrm = CGAffineTransformConcat(trnsfrm, CGAffineTransformMakeTranslation(0.0, Image3.image.size.height));
            CGContextConcatCTM(ctx, trnsfrm);
            CGContextBeginPath(ctx);
            CGContextAddEllipseInRect(ctx, CGRectMake(0.0, 0.0, Image3.image.size.width, Image3.image.size.height));
            CGContextClip(ctx);
            CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, Image3.image.size.width, Image3.image.size.height), Image3.image.CGImage);
            Image3.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image3.imageURL];
        [indicator stopAnimating];
        if ([vendorFeedStatus isEqualToString:@"yes"])
        {
            Image3.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[totalArray objectAtIndex:i]objectForKey:@"prodimg1"]]];
        }
        else
        {
            Image3.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[totalArray objectAtIndex:i]objectForKey:@"prodimage"]]];
        }
        
        [prodImageArrayList addObject:Image3];
        [productTbl reloadData];
        
    }
}



-(void)vendorlisting

{
    NSString *post= [NSString stringWithFormat:@"vendorid=%@",singlogin.SlctVenId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/productlistingvendor.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[data JSONValue];
    
    if(![[eventarray objectForKey:@"productlist"] isEqual:@"No product available"])
    {
        aray_2=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"product list"]];
        
    }
    else
    {
        aray_2=[[NSMutableArray alloc]init];
        
        
    }
    if([aray_2 count]==0)
    {
        
        [productTbl reloadData];
        [indicator startAnimating];
        vendorFeedStatus = @"no";
        [self CountImg];
        
    }
    else
    {
        singlogin.vendor_objectid=[[aray_2 objectAtIndex:0]valueForKey:@"objectid"];
        
        singlogin.objectIdStr=[[aray_2 objectAtIndex:0]valueForKey:@"objectid"];
        singlogin.vendorProfilepic=[[aray_2 objectAtIndex:0]valueForKey:@"vendorpic"];
        singlogin.shopDec=[[aray_2 objectAtIndex:0]valueForKey:@"descr"];
        singlogin.pid = [[aray_2 objectAtIndex:0]valueForKey:@"staffpick"];
        vendorFeedStatus = @"yes";
        array2Value = aray_2;
        NSLog(@"array2 value -- %@",array2Value);
        [self CountImg];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView==list_tableView) {
        return friend_array.count;
    }
    return totalArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==list_tableView) {
        
        static NSString *CellIdentifier = @"Cell_identifier";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSString *id_str1  =   singlogin.userid;
        
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey: @"english"] != nil)
            
            
        {
            
            
            
            if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str1]) {
                
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_img"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(60 , cell.frame.size.height/2- 20 , 200, 40)];
                name1.tag=1;
                name1.textAlignment=NSTextAlignmentLeft;
                //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_username"]];
                
                if (([cell.contentView viewWithTag:1]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                }
                
                [cell.contentView addSubview:name1];
                
            }else{
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_photo"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(60 , cell.frame.size.height/2- 20 , 200, 40)];
                name1.tag=1;
                name1.textAlignment=NSTextAlignmentLeft;
                
                //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_username"]];
                
                if (([cell.contentView viewWithTag:1]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                }
                
                [cell.contentView addSubview:name1];
                
            }
            
            return cell;
            
            
            
            
        }else{
            
            
            if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str1]) {
                
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(list_view.frame.size.width - 55, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_img"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width - 260 , 15 , 200, 40)];
                name1.tag=1;
                
                name1.textAlignment=NSTextAlignmentRight;
                
                //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received_username"]];
                
                UILabel *mention_btn=[[UILabel alloc]initWithFrame:CGRectMake(10 , 20, 100, 30)];
                mention_btn.layer.cornerRadius=5.0;
                mention_btn.layer.masksToBounds=YES;
                // [mention_btn setTitle:@"Mention" forState:UIControlStateNormal];
                //    mention_btn.titleLabel.textColor=[UIColor whiteColor];
                
                mention_btn.text=@"Mention";
                mention_btn.textAlignment=NSTextAlignmentCenter;
                mention_btn.textColor=[UIColor whiteColor];
                mention_btn.backgroundColor=[UIColor colorWithRed:105/255.f green:218/255.f blue:172/255.f alpha:1.0f];
                
                
                mention_btn.tag=2;
                
                
                
                
                if (([cell.contentView viewWithTag:1]) &&  ([cell.contentView viewWithTag:2]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                    [[cell.contentView viewWithTag:2]removeFromSuperview];
                    
                }
                
                [cell.contentView addSubview:mention_btn];
                [cell.contentView addSubview:name1];
                
            }else{
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(list_view.frame.size.width - 55, 5, 50, 50)];
                NSString *strImageUrl1 = [[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_photo"];
                [img sd_setImageWithURL:[NSURL URLWithString:strImageUrl1] placeholderImage:[UIImage imageNamed:@"CircledUserMaleFilled.png"]];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 25.0;;
                [cell addSubview:img];
                img.backgroundColor=[UIColor grayColor];
                
                UILabel *name1=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width - 260 , 15 , 200, 40)];
                name1.tag=1;
                name1.textAlignment=NSTextAlignmentRight;
                
                //   name1.backgroundColor=[UIColor lightGrayColor];
                name1.text=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent_username"]];
                
                
                
                UILabel *mention_btn=[[UILabel alloc]initWithFrame:CGRectMake(10, 20 , 100, 30)];
                mention_btn.layer.cornerRadius=5.0;
                mention_btn.layer.masksToBounds=YES;
                //                [mention_btn setTitle:@"Mention" forState:UIControlStateNormal];
                //                mention_btn.titleLabel.textColor=[UIColor whiteColor];
                mention_btn.text=@"Mention";
                mention_btn.textAlignment=NSTextAlignmentCenter;
                mention_btn.textColor=[UIColor whiteColor];
                
                
                mention_btn.backgroundColor=[UIColor colorWithRed:105/255.f green:218/255.f blue:172/255.f alpha:1.0f];
                
                mention_btn.tag=2;
                
                
                
                
                if (([cell.contentView viewWithTag:1]) &&  ([cell.contentView viewWithTag:2]))
                {
                    [[cell.contentView viewWithTag:1]removeFromSuperview];
                    [[cell.contentView viewWithTag:2]removeFromSuperview];
                    
                }
                
                [cell.contentView addSubview:mention_btn];
                [cell.contentView addSubview:name1];
                
            }
            
            
            
            [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
            [cell.layer setBorderWidth:1.0f];
            // [cell.layer setCornerRadius:7.5f];
            [cell.layer setShadowOffset:CGSizeMake(0, 1)];
            [cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
            [cell.layer setShadowRadius:8.0];
            [cell.layer setShadowOpacity:0.8];
            
            [cell.layer setMasksToBounds:NO];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            
            return cell;
            
            
            
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
    }
    for(UIView *v in cell.contentView.subviews)
    {
        [v removeFromSuperview];
    }
    
    productName =[[totalArray objectAtIndex:indexPath.row]valueForKey:@"prodname"];
    unit=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
    vendrNme=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    askForPrice=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
    soldout=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"soldout"];
    quantity=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"quantity"];
    
    NSString *descrStr=[[totalArray objectAtIndex:indexPath.row]valueForKey:@"prod_desc"];
    
    
    UILabel *descripLbl;
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            arrowIcon=[[UIImageView alloc] initWithFrame:CGRectMake(89, 140, 144, 43)];
            Image3.frame = CGRectMake(0, 0, 304, 304);
            btn=[[UIButton alloc]initWithFrame:CGRectMake(0, 304, 55, 40)];
            btn2=[[UIButton alloc]initWithFrame:CGRectMake(0, 304, 60, 60)];
            askPrice=[[UIButton alloc]initWithFrame:CGRectMake(0, 304, 151, 45)];
            [askPrice.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            openInstagram=[[UIButton alloc]initWithFrame:CGRectMake(10, 310, 60, 60)];
            //[openInstagram.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            costText3 =[[UILabel alloc]initWithFrame:CGRectMake(60, 306, 235, 20)];
            costText3.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:18];
            costText2 =[[UILabel alloc]initWithFrame:CGRectMake(60, 326, 235, 18)];
            costText2.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            whiteBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 304, 316, 115)];
            cart_img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 310, 60, 60)];
            descripLbl=[[UILabel alloc]initWithFrame:CGRectMake(8,360,292,65)];
            descripLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            arrowIcon=[[UIImageView alloc] initWithFrame:CGRectMake(89, 140, 144, 43)];
            Image3.frame = CGRectMake(0, 0, 304, 304);
            btn=[[UIButton alloc]initWithFrame:CGRectMake(0, 304, 55, 40)];
            btn2=[[UIButton alloc]initWithFrame:CGRectMake(10, 280, 60, 60)];
            askPrice=[[UIButton alloc]initWithFrame:CGRectMake(0, 304, 151, 45)];
            [askPrice.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            openInstagram=[[UIButton alloc]initWithFrame:CGRectMake(10, 280, 60, 60)];
            //[openInstagram.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            costText3 =[[UILabel alloc]initWithFrame:CGRectMake(60, 306, 235, 20)];
            costText3.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:18];
            costText2 =[[UILabel alloc]initWithFrame:CGRectMake(60, 326, 235, 18)];
            costText2.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            whiteBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 304, 316, 115)];
            cart_img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 280, 60, 60)];
            descripLbl=[[UILabel alloc]initWithFrame:CGRectMake(8,345,292,80)];
            descripLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            
            
            arrowIcon=[[UIImageView alloc] initWithFrame:CGRectMake(110, 140, 144, 43)];
            Image3.frame = CGRectMake(0, 0, 359, 356);
            btn=[[UIButton alloc]initWithFrame:CGRectMake(10, 356, 59, 40)];
            btn2=[[UIButton alloc]initWithFrame:CGRectMake(0, 310, 80, 80)];
            //  btn2.backgroundColor=[UIColor grayColor];
            askPrice=[[UIButton alloc]initWithFrame:CGRectMake(0, 356, 178, 45)];
            [askPrice.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            openInstagram=[[UIButton alloc]initWithFrame:CGRectMake(10, 315, 80, 80)];
            //[openInstagram.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            costText3 =[[UILabel alloc]initWithFrame:CGRectMake(62, 358, 290, 20)];
            costText3.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:18];
            costText2 =[[UILabel alloc]initWithFrame:CGRectMake(62, 380, 290, 20)];
            costText2.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            whiteBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 356, 361, 100)];
            cart_img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 315, 80, 80)];
            //   cart_img.backgroundColor=[UIColor yellowColor];
            descripLbl=[[UILabel alloc]initWithFrame:CGRectMake(6,391,345,50)];
            descripLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:13];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            arrowIcon=[[UIImageView alloc] initWithFrame:CGRectMake(130, 150, 144, 43)];
            Image3.frame = CGRectMake(0, 0, 398, 393);
            btn=[[UIButton alloc]initWithFrame:CGRectMake(0, 393, 55, 40)];
            btn2=[[UIButton alloc]initWithFrame:CGRectMake(0, 340, 80, 80)];
            askPrice=[[UIButton alloc]initWithFrame:CGRectMake(0, 393, 198, 45)];
            [askPrice.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            openInstagram=[[UIButton alloc]initWithFrame:CGRectMake(8, 345, 80, 80)];
            // [openInstagram.titleLabel setFont:[UIFont fontWithName:@"DroidArabicKufi-Bold" size:15]];
            costText3 =[[UILabel alloc]initWithFrame:CGRectMake(8, 395, 372, 20)];
            costText3.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:18];
            costText2 =[[UILabel alloc]initWithFrame:CGRectMake(8, 415, 372, 20)];
            costText2.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            whiteBack=[[UILabel alloc]initWithFrame:CGRectMake(0, 393, 398, 100)];
            cart_img = [[UIImageView alloc] initWithFrame:CGRectMake(8, 345, 80, 80)];
            descripLbl=[[UILabel alloc]initWithFrame:CGRectMake(0,424,395,65)];
            descripLbl.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:13];
        }
    }
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        
        UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        doubleTap.numberOfTapsRequired = 2;
        doubleTap.numberOfTouchesRequired = 1;
        [tableView addGestureRecognizer:doubleTap];
        
        whiteBack.backgroundColor =[UIColor whiteColor];
        [cell.contentView addSubview:whiteBack];
        
        
        
        arrowIcon.image=[UIImage imageNamed:@"logo.png"];
        [cell.contentView addSubview:arrowIcon];
        
        
        Image3 = [prodImageArrayList objectAtIndex:indexPath.row];
        Image3.tag=105;
        [[Image3 layer] setBorderColor:[UIColor whiteColor].CGColor];
        [cell.contentView addSubview:Image3];
        
        
        if ([soldout isEqualToString:@"SOLD OUT"]){
            
        }else{
            
            cart_img.image=[UIImage imageNamed:@"AddToCart@3x.png"];
            [cell.contentView addSubview:cart_img];
            
            btn2.backgroundColor=[UIColor clearColor];
            [cell.contentView addSubview:btn2];
            
        }
        
        
        
        
        
        costText3.text =[NSString stringWithFormat:@"%@",productName];
        costText3.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
        costText3.backgroundColor =[UIColor clearColor];
        costText3.numberOfLines = 1;
        costText3.textAlignment = NSTextAlignmentRight;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:costText3];
        
        
        descripLbl.text =[NSString stringWithFormat:@"%@",descrStr];
        descripLbl.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
        descripLbl.backgroundColor =[UIColor clearColor];
        descripLbl.numberOfLines = 3;
        descripLbl.textAlignment = NSTextAlignmentRight;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:descripLbl];
        
        
        if ([askForPrice isEqualToString:@"Ask for Price"])
        {
            costText2.text = [[totalArray objectAtIndex:indexPath.row]valueForKey:@"negotiable_price"];
        }
        else if ([soldout isEqualToString:@"SOLD OUT"])
        {
            costText2.text = [[totalArray objectAtIndex:indexPath.row]valueForKey:@"soldout"];
        }
        else
        {
            costText2.text = [[totalArray objectAtIndex:indexPath.row]valueForKey:@"unitprice"];
            
        }
        costText2.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
        costText2.backgroundColor =[UIColor clearColor];
        costText2.numberOfLines = 1;
        costText2.textAlignment = NSTextAlignmentRight;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:costText2];
        
        btn.tag = 101;
        if ([soldout isEqualToString:@"SOLD OUT"])
        {
            btn.hidden = YES;
            btn2.hidden = YES;
        }
        else
        {
            btn.exclusiveTouch = YES;
            [btn2 addTarget:self action:@selector(orderBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else
    {
        arrowIcon.image=[UIImage imageNamed:@"logo.png"];
        [cell.contentView addSubview:arrowIcon];
        
        Image3 = [prodImageArrayList objectAtIndex:indexPath.row];
        Image3.tag=105;
        [[Image3 layer] setBorderColor:[UIColor whiteColor].CGColor];
        [cell.contentView addSubview:Image3];
        
        //            [askPrice setTitle:@"اسأل عن سعر" forState:UIControlStateNormal];
        //            [askPrice setTitleColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1] forState:UIControlStateNormal];
        //            askPrice.backgroundColor=[UIColor whiteColor];
        //            [askPrice addTarget:self action:@selector(askPrice:) forControlEvents:UIControlEventTouchUpInside];
        //            [cell.contentView addSubview:askPrice];
        
        [openInstagram setImage:[UIImage imageNamed:@"Instagram1.png"] forState:UIControlStateNormal];
        //            [openInstagram setTitle:@"فتح في إينستاجرام" forState:UIControlStateNormal];
        //            [openInstagram setTitleColor:[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1] forState:UIControlStateNormal];
        openInstagram.backgroundColor=[UIColor clearColor];
        [openInstagram addTarget:self action:@selector(openInstagram:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:openInstagram];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==list_tableView) {
        frind_id=[[NSString alloc]init];
        id_str  =   singlogin.userid;
        
        
        if ([[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"] isEqualToString:id_str]) {
            
            
            //   name1.backgroundColor=[UIColor lightGrayColor];
            frind_id=[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_received"]];
            
            
        }else{
            
            
            
            frind_id =[NSString stringWithFormat:@"%@",[[friend_array objectAtIndex:indexPath.row]objectForKey:@"user_sent"]];
            
            
        }
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"Do You want to recommend this product to your Friend" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"cancel", nil];
        
        
        alert.tag= 100;
        [alert show];
        
        
        
        
    }
}


////////////////////////


-(void)doubleTap:(UISwipeGestureRecognizer*)tap
{
    if (UIGestureRecognizerStateEnded == tap.state)
    {
        CGPoint p = [tap locationInView:tap.view];
        NSIndexPath* indexPath = [productTbl indexPathForRowAtPoint:p];
        UITableViewCell* cell = [productTbl cellForRowAtIndexPath:indexPath];
        NSIndexPath *indexPath1 = [productTbl indexPathForCell:cell];
        NSLog(@" string = %@",indexPath1);
        productIdStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"id"];
        vendorImgStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
        [[totalArray objectAtIndex:indexPath1.row]valueForKey:@"vendorpic"];
        vendorIdStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"vendorid"];
        vendorNameStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"vendorname"];
        productIdStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"id"];
        prodImgStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"prodimg1"];
        unitPriceStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"unitprice"];
        productNameStr=[[totalArray objectAtIndex:indexPath1.row]valueForKey:@"prodname"];
        
        actionSheet0 = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Wish list",@"Recommend to a friend",@"Report item",nil];
        actionSheet0.tag = 11;
        [actionSheet0 showInView:self.view];
    }
}

-(void)cancel_list{
    list_view.hidden=YES;
    cancel_btn.hidden=YES;
    cancel_lbl.hidden=YES;
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (actionSheet.tag==11)
    {
        if (buttonIndex==0)
        {
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                view1.hidden = NO;
            }
            else if ([singlogin.loginStatus isEqualToString:@"User Login"])
            {
                [ProgressHUD show:@"Loading..." Interaction:NO];
                [self performSelector:@selector(wishlist) withObject:Nil afterDelay:0.1f];
            }
            else
            {
                view1.hidden = NO;
            }
            
        }
        else if (buttonIndex ==1 )
        {
            if ([singlogin.loginStatus isEqualToString:@"User Login"]){
                list_view=[[UIView alloc]initWithFrame:CGRectMake(0, topBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height- (topBar.frame.size.height+navView.frame.size.height))];
                [self.view addSubview:list_view];
                
                list_view.backgroundColor=[UIColor greenColor];
                list_tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, list_view.frame.size.width, list_view.frame.size.height)];
                [list_view addSubview:list_tableView];
                list_tableView.delegate=self;
                list_tableView.dataSource=self;
                list_tableView.separatorColor=[UIColor clearColor];
                
                
                cancel_lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, list_view.frame.origin.y + list_view.frame.size.height, list_view.frame.size.width, self.view.frame.size.height-list_view.frame.size.height-topBar.frame.size.height)];
                [self.view addSubview:cancel_lbl];
                
                cancel_lbl.text=@"Cancel";
                cancel_lbl.textColor=[UIColor whiteColor];
                cancel_lbl.textAlignment=NSTextAlignmentCenter;
                cancel_lbl.backgroundColor=[UIColor colorWithRed:105/255.f green:218/255.f blue:172/255.f alpha:1.0];
                cancel_btn=[[UIButton alloc]initWithFrame:CGRectMake(0, list_view.frame.origin.y + list_view.frame.size.height, list_view.frame.size.width, list_view.frame.size.height - navView.frame.size.height)];
                [self.view addSubview:cancel_btn];
                cancel_btn.backgroundColor=[UIColor clearColor];
                
                
                
                [cancel_btn addTarget:self action:@selector(cancel_list) forControlEvents:UIControlEventTouchUpInside];
                
                
            }else{
                
                view1.hidden = NO;
                
                
                
            }
            
            
            //            CGSize screenSize = [UIScreen mainScreen].bounds.size;
            //
            //            self.textField = [[UITextField alloc] init];
            //            [self.textField setDelegate:self];
            //            [self.textField setText:@"Vetrina:https://itunes.apple.com/in/app/vetrina/id980820003?mt=8"];
            //            [self.textField setFrame:CGRectMake((screenSize.width/2)-135, (screenSize.height/2)-90, 270, 60)];
            //            self.textField.textColor = [UIColor clearColor];
            //            self.textField.enabled = NO;
            //            [self.textField setReturnKeyType:UIReturnKeyDone];
            //            [self.view addSubview:self.textField];
            //
            //
            //            ARSpeechActivity *speechActivity = [[ARSpeechActivity alloc] init];
            //
            //            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[self.textField.text] applicationActivities:@[speechActivity]];
            //
            //
            //            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            //            {
            //                [self presentViewController:activityVC animated:YES completion:nil];
            //            }
            //
            //            else
            //            {
            //                UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
            //                NSLog(@"%f",self.view.frame.size.width/2);
            //                [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            //            }
            
        }
        
        
        else if (buttonIndex==2)
        {
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                view1.hidden = NO;
            }
            else if ([singlogin.loginStatus isEqualToString:@"User Login"])
            {
                itemReport.hidden = NO;
                
            }
            else
            {
                
                view1.hidden = NO;
            }
        }
        else
        {
        }
    }
    else if (actionSheet.tag==12)
    {
        if (buttonIndex==0)
        {
            NSString *str = @"https://itunes.apple.com/in/app/vetrina/id980820003?mt=8";
            str = [NSString stringWithFormat:@"%@", str];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            
        }
    }
    
    
}

-(void)wishlist

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addwishlist.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usrnameStr=[[NSString alloc]initWithFormat:@"%@",productIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
    
    [ProgressHUD showSuccess:@""];
    
}



/////////////////////////

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView==list_tableView) {
        UIView *headerView = [[UIView alloc] init];
        
        
        return headerView;
    }
    
    UIView *headerView = [[UIView alloc] init];
    
    UILabel *desc;
    UIButton *report,*chat;
    UIImageView *profileImg;
    image3 = [[UIImageView alloc]init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 62, 62)];
            image3.frame = CGRectMake(20, -13, 44, 44);
            
            desc=[[UILabel alloc]initWithFrame:CGRectMake(8,77,292,79)];
            report=[[UIButton alloc]initWithFrame:CGRectMake(239,25,60,28)];
            chat=[[UIButton alloc]initWithFrame:CGRectMake(79,25,153,28)];
            desc.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            report.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:11];
            chat.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            
            
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 62, 62)];
            image3.frame = CGRectMake(20, -13, 44, 44);
            
            desc=[[UILabel alloc]initWithFrame:CGRectMake(8,77,292,79)];
            report=[[UIButton alloc]initWithFrame:CGRectMake(239,25,60,28)];
            chat=[[UIButton alloc]initWithFrame:CGRectMake(79,25,153,28)];
            
            
            desc.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
            report.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:11];
            chat.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:12];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 76, 76)];
            image3.frame = CGRectMake(20, -13, 44, 44);
            
            desc=[[UILabel alloc]initWithFrame:CGRectMake(7,77,349,80)];
            desc.backgroundColor=[UIColor lightGrayColor];
            report=[[UIButton alloc]initWithFrame:CGRectMake(279,32,69,28)];
            chat=[[UIButton alloc]initWithFrame:CGRectMake(103,32,168,28)];
            
            desc.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:13];
            report.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi" size:13];
            chat.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:14];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 89, 89)];
            image3.frame = CGRectMake(20, -13, 44, 44);
            
            desc=[[UILabel alloc]initWithFrame:CGRectMake(11,103,376,79)];
            report=[[UIButton alloc]initWithFrame:CGRectMake(304,39,69,28)];
            chat=[[UIButton alloc]initWithFrame:CGRectMake(124,39,168,28)];
            
            desc.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:13];
            report.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:13];
            chat.titleLabel.font = [UIFont fontWithName:@"DroidArabicKufi-Bold" size:14];
        }
    }
    
    staffPic = singlogin.pid;
    if ([staffPic isEqualToString:@"yes"])
    {
        image3.hidden = NO;
    }
    else
    {
        image3.hidden = YES;
    }
    image3.image=[UIImage imageNamed:@"PremiumClub.png"];
    [headerView addSubview:image3];
    
    
    NSLog(@"%@",singlogin.shopDec);
    desc.text =[NSString stringWithFormat:@"%@",singlogin.shopDec];
    desc.textColor =[UIColor colorWithRed:51.0f/255 green:51.0f/255 blue:51.0f/255 alpha:1];
    desc.backgroundColor =[UIColor clearColor];
    desc.numberOfLines = 4;
    desc.textAlignment = NSTextAlignmentRight;
    
    NSURL *url1=[NSURL URLWithString:singlogin.vendorProfilepic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic1=[UIImage imageWithData:datapic];
    profileImg.image=emppppic1;
    profileImg.layer.cornerRadius =5;
    profileImg.layer.cornerRadius = profileImg.frame.size.height /2;
    profileImg.layer.masksToBounds = YES;
    profileImg.clipsToBounds=YES;
    
    [report setTitle:@"تقرير" forState:UIControlStateNormal];
    [report setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    report.layer.cornerRadius=2.5f;
    report.clipsToBounds=YES;
    [report addTarget:self action:@selector(report:) forControlEvents:UIControlEventTouchUpInside];
    //  [report addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
    
    [chat setTitle:@"التحدث مع متجر" forState:UIControlStateNormal];
    [chat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    chat.layer.cornerRadius=2.5f;
    chat.clipsToBounds=YES;
    
    [chat addTarget:self action:@selector(actionChat1:) forControlEvents:UIControlEventTouchUpInside];
    //  [chat addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
    [chat setBackgroundColor:[UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1]];
    [report setBackgroundColor:[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1]];
    
    [headerView addSubview:profileImg];
    [headerView addSubview:desc];
    //      [headerView addSubview:report];
    //      [headerView addSubview:chat];
    
    
    headerView.backgroundColor=[UIColor whiteColor];
    return headerView;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    
    if (tableView==list_tableView) {
        UIView *headerView = [[UIView alloc] init];
        
        
        return headerView;
    }
    
    
    UIView *footerView = [[UIView alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 304, 45)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 667)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 359, 50)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736)
        {
            confrmBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 398, 50)];
            [confrmBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:20]];
        }
        
    }
    
    
    [confrmBtn setTitle:@"LOAD MORE" forState:UIControlStateNormal];
    confrmBtn.backgroundColor = [UIColor colorWithRed:119.0f/255 green:218.0f/255 blue:172.0f/255 alpha:1];
    [confrmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        
    }
    else
    {
        
        if (singlogin.pedagori .length > 0)
        {
            [footerView addSubview:confrmBtn];
        }
        //        else  if ([singlogin.pedagori  isEqualToString:nil])
        //        {
        //
        //        }
        else
        {
            
        }
        
    }
    return footerView;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView==list_tableView) {
        return 0;
    }
    
    
    
    CGFloat ff;
    
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        ff=0;
    }
    else
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                ff= 48.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                ff=  48.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                ff=  53.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                ff=  53.0f;
            }
        }
        
    }
    return  ff;
}

-(void)confirm:(UIButton *)sender

{
    [self performSelector:@selector(load) withObject:Nil afterDelay:0.3f];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [indicator startAnimating];
    [indicator setHidden:NO];
}


-(void)load

{
    NSURL * url=[NSURL URLWithString:singlogin.pedagori];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    
    NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    //  NSLog(@"GetData--%@",data);
    
    NSDictionary *value1=[data JSONValue];
    NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
    NSLog(@"dadadad %@",pedagori);
    NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
    NSLog(@"dadadad %@",pedagori1);
    singlogin.pedagori = pedagori1;
    
    NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
    NSLog(@"dadadad %@",userDict1);
    NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
    
    NSMutableArray *namef = [usef valueForKeyPath:@"username"];
    
    NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
    NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
    NSLog(@"image links %@",imgId);
    NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
    NSLog(@"image links %@",img);
    NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
    NSLog(@"hmmm %@",urlvalue);
    NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
    NSLog(@"yooooo %@",righturl);
    //newdatalist = [[NSMutableArray alloc]init];
    for (int i=0; i< righturl.count; i++)
    {
        NSDictionary *dynamicDict=[[NSDictionary alloc]init];
        dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
        NSLog(@"Dynamic ---- %@",dynamicDict);
        
        [newdatalist addObject:dynamicDict];
    }
    
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singlogin.newarayList1= newdatalist;
    [self CountImg];
    
    [productTbl reloadData];
    [productTbl setContentOffset:CGPointZero animated:YES];
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    [indicator setHidden:YES];
    
}


-(void)report:(UIButton *)sender

{
    reportView.hidden = NO;
}



- (IBAction)flagReprt1:(id)sender

{
    
    vendorImgStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
    //[[totalArray objectAtIndex:0]valueForKey:@"vendorpic"];
    vendorIdStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorid"];
    vendorNameStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorname"];
    reson = reportBtn1.currentTitle;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self report];
    
    
}


- (IBAction)respnsvReport2:(id)sender

{
    vendorImgStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
    [[totalArray objectAtIndex:0]valueForKey:@"vendorpic"];
    vendorIdStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorid"];
    vendorNameStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorname"];
    reson = reportBtn2.currentTitle;
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self report];
}


- (IBAction)reprtSpam3:(id)sender

{
    vendorImgStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
    [[totalArray objectAtIndex:0]valueForKey:@"vendorpic"];
    vendorIdStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorid"];
    vendorNameStr=[[totalArray objectAtIndex:0]valueForKey:@"vendorname"];
    reson = reportBtn3.currentTitle;
    [indicator startAnimating];
    [self report];
}



-(void)report

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to Report");
        [view1 setHidden:NO];
    }
    else
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addreport.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reportdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reporttime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",vendorImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",vendorIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",vendorNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSString *resone=[[NSString alloc]initWithFormat:@"%@",reson];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reason\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",resone] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"THANK YOU" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }
    indicator.hidden=YES;
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    reportView.hidden = YES;
}


- (void)actionChat1:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to Report");
        [view1 setHidden:NO];
    }
    else
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(chatnew) withObject:Nil afterDelay:2.0f];
        
    }
}


-(void)chat

{
    //    NSLog(@"%@",singlogin.objectIdStr);
    //    NSString *str=[[NSString alloc]init];
    //    str =@"no user";
    //
    //    for (int i=0; i< users.count; i++)
    //    {
    //
    //        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
    //        if ([name containsString:singlogin.objectIdStr])
    //        {
    //            str=@"user found";
    //
    //            PFUser *user1 = [PFUser currentUser];
    //            PFUser *user2 = users[i];
    //            NSString *groupId = StartPrivateChat(user1, user2);
    //            [self actionChat:groupId];
    //            i =(int) users.count;
    //        }
    //    }
    //
    //
    //    if ([str isEqualToString:@"no user"]) {
    //
    //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"No User Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //        [alert show];
    //        [ProgressHUD showSuccess:@""];
    //
    //    }
}
-(void)chatnew

{
    // myei4MTHya
    
    currentUser = [QBUUser user];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.usrObjctId] integerValue ];
    //    NSInteger i = [ [ NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"] ] integerValue ];
    
    
    currentUser.ID =o;
    currentUser.password=[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pass_word"]);
    NSLog(@"%ld",(long)o);
    
    
    
    [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
        
        
        
        if (error==nil) {
            [self retrieveAllUsersFromPage:1];
            
        }
    }
     
     
     
     ];
    
    
    
    
    
    
    
    
    
    //    NSLog(@"%@",singlogin.objectIdStr);
    //
    //    NSString *str=[[NSString alloc]init];
    //    str =@"no user";
    //
    //
    //    for (NSInteger i=0; i< users.count; i++)
    //    {
    //        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
    //        if ([name containsString:singlogin.objectIdStr])
    //        {
    //
    //            str=@"user found";
    //
    //            PFUser *user1 = [PFUser currentUser];
    //            PFUser *user2 = users[i];
    //            NSString *groupId = StartPrivateChat(user1, user2);
    //            [self actionChat:groupId];
    //            i = users.count;
    //        }
    //    }
    //
    //    if ([str isEqualToString:@"no user"]) {
    //
    //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"No User Found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //        [alert show];
    //        [ProgressHUD showSuccess:@""];
    //
    //    }
}


- (void)actionChat:(NSString *)groupId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    //    ChatView *chatView = [[ChatView alloc] initWith:groupId];
    //    chatView.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:chatView animated:YES];
    
    
    chat_New *chatView = [[chat_New alloc] initWith:groupId];
    //  chat_New.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    
    [ProgressHUD showSuccess:@""];
}

- (void)retrieveAllUsersFromPage:(int)page{
    
    // int userNumber;
    
    [QBRequest usersForPage:[QBGeneralResponsePage responsePageWithCurrentPage:page perPage:100] successBlock:^(QBResponse *response, QBGeneralResponsePage *pageInformation, NSArray *users21) {
        
        
        [users_array addObjectsFromArray:users21];
        
        
        NSMutableSet *seen = [NSMutableSet set];
        NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.usrObjctId] integerValue ];
        
        [seen addObject:[NSString stringWithFormat:@"%ld",o] ];
        NSUInteger i = 0;
        
        while (i < [users_array count]) {
            
            QBUUser *user1   =[QBUUser user];
            
            user1 =[users_array objectAtIndex:i];
            
            
            id obj = [NSString stringWithFormat:@"%lu",(unsigned long)user1.ID];
            
            if ([seen containsObject:obj]) {
                [users_array removeObjectAtIndex:i];
                // NB: we *don't* increment i here; since
                // we've removed the object previously at
                // index i, [originalArray objectAtIndex:i]
                // now points to the next object in the array.
            } else {
                //  [seen addObject:obj];
                i++;
            }
        }
        
        
        
        [self chat22];
        
        
        
        //    userNumber += users.count;
        
        
        //        if (pageInformation.totalEntries > userNumber) {
        //
        //
        //            [self retrieveAllUsersFromPage:pageInformation + 1];
        //        }
    } errorBlock:^(QBResponse *response) {
        // Handle error
    }];
}


-(void)newChat{
    
    QBChatDialog *test_dialog;
    
    for (int i=0; i<dialog_Objects.count; i++) {
        
        
        test_dialog=[dialog_Objects objectAtIndex:i];
        
        
        
        
    }
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.vendor_objectid] integerValue ];
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    
    
    chatDialog.occupantIDs = @[@(o) ];
    
    
    
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        
        [self actionChat:createdDialog.ID];
        
        
        
        
    } errorBlock:^(QBResponse *response) {
        [ProgressHUD showSuccess:@"Cannot Connect To Vendor, try again"];
        
        
    }];
    
    
}

-(void)chat22{
    
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.vendor_objectid] integerValue ];
    
    for (int x=0; x<users_array.count; x++) {
        
        
        QBUUser *user1   =[QBUUser user];
        
        user1 =[users_array objectAtIndex:x];
        
        
        if (user1.ID==o) {
            [self newChat];
            break;
            
        }
        
        
        
    }
    
    
    [ProgressHUD showSuccess:@"Cannot Connect To Vendor, try again"];
    
    
    //    chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
    //
    //    chatDialog.name = @"Chat with Bob, Sam, Garry";
    //
    //    chatDialog.occupantIDs = @[@(9045183), @(9045219)]; // change id with your register user's id
    //
    //    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
    //
    //
    //          NSLog(@"No Error: %@", response);
    //        [self chat1];
    //
    //
    //    } errorBlock:^(QBResponse *response) {
    //
    //          NSLog(@"Error: %@", response);
    //
    //    }];
    
    
    
}


//- (void)actionChat:(NSString *)groupId
////-------------------------------------------------------------------------------------------------------------------------------------------------
//{
//    ChatView *chatView = [[ChatView alloc] initWith:groupId];
//    chatView.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatView animated:YES];
//    [ProgressHUD showSuccess:@""];
//}


-(void)addobjectid

{
    NSString *strULR = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addobjectid.php"];
    
    NSLog(@"strURL:%@", strULR);
    
    NSURL * url=[NSURL URLWithString:strULR];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundry = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundry];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *namestr = [[NSString alloc]initWithFormat:@"%@", singlogin.userNameStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",namestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *userIdStr = [[NSString alloc]initWithFormat:@"%@", singlogin.userid];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",userIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *userObIdStr = [[NSString alloc]initWithFormat:@"%@", singlogin.usrObjctId];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",userObIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *groupidIdStr = [[NSString alloc]initWithFormat:@"%@", singlogin.groupIdStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"group_orderid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",groupidIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *vendrIdStr = [[NSString alloc]initWithFormat:@"%@",vendorIdStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *vendrObIdStr = [[NSString alloc]initWithFormat:@"%@", singlogin.objectIdStr];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendor_objectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrObIdStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vendrNamestr = [[NSString alloc]initWithFormat:@"%@",singlogin.venName];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fullname_shopname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrNamestr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *vendrPicstr = [[NSString alloc]initWithFormat:@"%@",singlogin.vendorProfilepic];
    [body appendData : [[NSString stringWithFormat:@"--%@\r\n", boundry] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [[NSString stringWithFormat:@"%@",vendrPicstr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData : [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"print mystring==%@",returnString);
    
    NSDictionary *eventarray=[returnString JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    NSString *msg = [[eventarray valueForKey:@"message"]objectAtIndex:0];
    
    NSLog(@"msg -- %@",msg);
    
    ChatView *chatView = [[ChatView alloc] initWith:singlogin.groupIdStr];
    chatView.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:chatView animated:YES];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)cancelRport:(id)sender

{
    reportView.hidden = YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==list_tableView) {
        
        return 70;
        
    }
    
    
    
    
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                return 430.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                return 435.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                return 478.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                return 510.0f;
            }
            
            else
            {
                return 160.0f;
            }
        }
        else
        {
            return 525.0f;
        }
        
    }
    else
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                return 358.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                return 368.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 667)
            {
                return 418.0f;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 736)
            {
                return 458.0f;
            }
            
            else
            {
                return 160.0f;
            }
        }
        else
        {
            return 525.0f;
        }
        
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (tableView==list_tableView) {
        
        return 0;
        
    }
    
    
    
    
    
    
    CGFloat ff;
    
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        NSString *str = singlogin.pid;
        if ([str isEqualToString:@"yes"] )
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                if ([[UIScreen mainScreen] bounds].size.height == 480)
                {
                    //  ff= 161.0f;
                    ff= 0;
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //  ff=  161.0f;
                    ff= 0;
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 667)
                {
                    //  ff=  174.0f;
                    ff= 0;
                }
                else if ([[UIScreen mainScreen] bounds].size.height == 736)
                {
                    //  ff=  187;
                    ff= 0;
                }
                
            }
        }
        else
        {
            ff=0;
        }
        
    }
    else
    {
        ff=0;
    }
    return  ff;
}


-(void)askPrice:(UIButton *)sender
{
    if (singlogin.userid == nil)
    {
        NSLog(@"Please Login to add cart");
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        [indicator setHidesWhenStopped:YES];
        [view1 setHidden:NO];
    }
    
    else
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
        NSIndexPath *path = [productTbl indexPathForCell:cell];
        NSString *selectedImage = [[totalArray objectAtIndex:path.row]valueForKey:@"prodimage"];
        vendorNameStr = singlogin.venName;
        statusIDe = @"1";
        deliverPrice=[[totalArray objectAtIndex:path.row]valueForKey:@"deliveryprice"];
        askForPrice = @"Ask for Price";
        productNameStr =@"Untitled";
        NSLog(@"IMGURL %@",selectedImage);
        
        prodImgStr = selectedImage;
        
        NSString *s_img4 = [NSString stringWithFormat:@"%@",selectedImage];
        NSURL *url2 = [NSURL URLWithString:s_img4];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        pro1 = [[UIImage alloc] initWithData:data2];
        venderImge = [[UIImageView alloc]init];
        venderImge.image = pro1;
        vendorImgStr =[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
        ///singlogin.vendorProfilepic;
        vendorIdStr = singlogin.SlctVenId;
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"YYYY-MM-dd"]; // Date formater
        orderDate = [dateformate stringFromDate:[NSDate date]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm"]; // Time formater
        orderTime = [dateFormatter stringFromDate:[NSDate date]];
        
        NSURL *url=[NSURL URLWithString:singlogin.usrPic];
        NSData *datapic=[NSData dataWithContentsOfURL:url];
        emppppic=[UIImage imageWithData:datapic];
        
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(Submitted)withObject:Nil afterDelay:3.0f];
        
    }
}


-(void)Submitted
{
    [self addtocart];
    [self twillow];
    
}


-(void)twillow
{
    
    NSString *post=[NSString stringWithFormat:@"vendorid=%@",singlogin.SlctVenId];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/twilio.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"PostData--%@",eventarray);
    //        thnxxView.hidden = NO;
    [ProgressHUD showSuccess:@""];
    twilloView.hidden = NO;
    [indicator stopAnimating];
    
}

- (IBAction)tawillobtn:(id)sender
{
    twilloView.hidden = YES;
}


-(void)openInstagram:(UIButton *)sender

{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    
    
    
    
    
    
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *path = [productTbl indexPathForCell:cell];
    selectedImageMediaId = [[totalArray objectAtIndex:path.row]valueForKey:@"media_id"];
    NSLog(@"IMGURL %@",selectedImageMediaId);
    vendorpic_browserLink = [[totalArray objectAtIndex:path.row]valueForKey:@"linkpicbrw"];
    NSLog(@"vendorInstaName %@",vendorpic_browserLink);
    
    NSString* url = [NSString stringWithFormat:@"instagram://media?id=%@", selectedImageMediaId];
    NSURL *instagramURL = [NSURL URLWithString:url];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        [[UIApplication sharedApplication] openURL:instagramURL];
        [ProgressHUD showSuccess:@""];
        
        [indicator stopAnimating];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:vendorpic_browserLink];
        [[UIApplication sharedApplication] openURL:url];
        [ProgressHUD showSuccess:@""];
        
        [indicator stopAnimating];
    }
    
    
    
    
    
    
    
    
    
}


-(void)increaseItemCount:(UIButton *)sender

{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *path = [productTbl indexPathForCell:cell];
    
    [tapArray replaceObjectAtIndex:path.row withObject:[NSNumber numberWithInteger:[[tapArray objectAtIndex:path.row] intValue]+1 ]];
    [productTbl reloadData];
}


-(void)decreaseItemCount:(UIButton *)sender

{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *path = [productTbl indexPathForCell:cell];
    
    [tapArray replaceObjectAtIndex:path.row withObject:[NSNumber numberWithInteger:[[tapArray objectAtIndex:path.row] intValue]-1 ]];
    [productTbl reloadData];
}


-(void)orderBtn:(UIButton *)sender
{
    
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        UIAlertView *alrt1 = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt1 show];
    }
    else if (singlogin.userid == nil)
    {
        [view1 setHidden:NO];
    }
    else
    {
        
        
        
        UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
        NSIndexPath *cellIndexPath = [productTbl indexPathForCell:cell];
        productIdStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"id"];
        prodImgStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"prodimg1"];
        unitPriceStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"unitprice"];
        productNameStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"prodname"];
        vendorImgStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"ven_pic"];
        //[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"vendorpic"];
        vendorIdStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"vendorid"];
        vendorNameStr=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"vendorname"];
        askForPrice=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"negotiable_price"];
        v_obj=[[totalArray objectAtIndex:cellIndexPath.row]valueForKey:@"objectid"];
        
        if ([askForPrice isEqualToString:@"Ask for Price"]) {
            unitPriceStr=@"";
        }
        
        
        [self prodListing];
        
        if (cartListArray.count==0) {
            [self addtocart];
            
        }
        else
        {
            NSString *exist;
            for (NSInteger i=0; i<cartListArray.count; i++)
            {
                NSString *productIdStr1=[[cartListArray objectAtIndex:i]valueForKey:@"productid"];
                cartIdStr=[[cartListArray objectAtIndex:i]valueForKey:@"id"];
                quantStr=[[cartListArray objectAtIndex:i]valueForKey:@"quantityprod"];
                
                if ([productIdStr1 isEqualToString:productIdStr])
                {
                    exist=@"yes";
                    i=cartListArray.count;
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@""
                                                                      message:@"Product is already in cart! Do you want to increase quantity?"
                                                                     delegate:self
                                                            cancelButtonTitle:@"Add"
                                                            otherButtonTitles:@"Cancel", nil];
                    [myAlert show];
                    
                    
                    
                }
            }
            if ([exist isEqualToString:@"yes"])
            {
                
            }
            else
            {
                
                NSString *match;
                for (NSInteger i=0; i<cartListArray.count; i++)
                {
                    NSString *vendoridS=[[cartListArray objectAtIndex:i]valueForKey:@"vendorid"];
                    NSString *statusStr=[[cartListArray objectAtIndex:i]valueForKey:@"statusid"];
                    
                    if ([vendoridS isEqualToString:vendorIdStr])
                    {
                        if ([statusStr isEqualToString:@"0"] || [statusStr isEqualToString:@"1"] )
                        {
                            match=@"yes";
                            i=cartListArray.count;
                            indicator.hidden=NO;
                            [indicator startAnimating];
                            [ProgressHUD show:@"Loading..." Interaction:NO];
                            
                            [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
                            
                        }
                        else
                        {
                            match=@"donot";
                            i=cartListArray.count;
                            
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Your previous order is under being processed.Kindly Wait!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                            [alert show];
                        }
                    }
                    else
                    {
                        match=@"no";
                    }
                }
                if ([match isEqualToString:@"no"])
                {
                    indicator.hidden=NO;
                    [indicator startAnimating];
                    [ProgressHUD show:@"Loading..." Interaction:NO];
                    
                    [self performSelector:@selector(addtocart) withObject:nil afterDelay:0.5f];
                    
                }
                
            }
        }
        
        
    }
}


-(void)prodListing
{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/cartlistuser_ven.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list list"] isEqual:@"No list available"])
    {
        cartListArray=[[NSMutableArray alloc]init];
    }
    else
    {
        cartListArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.tag==101 && buttonIndex==0) {
        
    }
    
    if (alertView.tag==100 && buttonIndex==0) {
        
        NSString *post = [NSString stringWithFormat:@"uid=%@&fid=%@&vid=%@&pid=%@&vimg=%@",id_str,frind_id,vendorIdStr,productIdStr,vendorImgStr];
        NSLog(@"get data=%@",post);
        NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/recommend.php"];
        NSLog(@"PostData--%@",post);
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSArray *eventarray =[data JSONValue];
        NSLog(@"%@",eventarray);
        list_view.hidden=YES;
        cancel_btn.hidden=YES;
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Vetrina" message:@"Successfully Recomended to friend" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        
        
        alert.tag= 101;
        [alert show];
        
        list_view.hidden=YES;
        cancel_btn.hidden=YES;
        cancel_lbl.hidden=YES;
        [ProgressHUD showSuccess:@"Succeed."];
        
    }else if (alertView.tag==100 && buttonIndex==1){
        [ProgressHUD showSuccess:@" "];
        
    }
    
    
    
    
    if (buttonIndex==0  && alertView.tag!=100)
    {
        indicator.hidden=NO;
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(editCartView) withObject:nil afterDelay:0.5f];
    }
    else if (buttonIndex==1  && alertView.tag!=100)
    {
        
    }
}


-(void)editCartView
{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/editcartquant.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSInteger aa=[quantStr integerValue];
    aa=aa+1;
    NSString *prodQuant=[[NSString alloc]initWithFormat:@"%li",(long)aa];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantity\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",prodQuant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *guestId1=[[NSString alloc]initWithFormat:@"%@",cartIdStr];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cartid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",guestId1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSDictionary *eventarray=[returnString JSONValue];
    NSLog(@"GetDatadictt--%@",eventarray);
    NSLog(@"print mystring==%@",returnString);
    
    NSMutableDictionary *eventArray=[returnString JSONValue];
    NSString *neww = [[eventArray valueForKey:@"message"]objectAtIndex:0];
    NSLog(@"%@",neww);
    
    if([neww isEqualToString:@" Updated"])
    {
        
    }
    [indicator stopAnimating];
    [ProgressHUD showSuccess:@""];
    
    indicator.hidden=YES;
    
}

-(void)addtocart

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addtocart.php"];
        NSLog(@"strURL:%@",strURL);
        NSURL * url=[NSURL URLWithString:strURL];
        NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
        [theLoginRequest setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSString *useridStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",useridStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *productidStr=[[NSString alloc]initWithFormat:@"%@",productIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",productidStr] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *pic1=[[NSString alloc]initWithFormat:@"%@",singlogin.userproilePic];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userprofilepic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",pic1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        NSString *imagee=[[NSString alloc]initWithFormat:@"%@",prodImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",imagee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        NSString *price1=[[NSString alloc]initWithFormat:@"%@",unitPriceStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"unitprice\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",price1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *askprice1=[[NSString alloc]initWithFormat:@"%@",askForPrice];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"negotiable_price\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",askprice1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        if ([statusIDe isEqualToString:@"1"])
        {
            NSString *statusidStr=@"1";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else if ([askForPrice isEqualToString:@"Ask for Price"])
        {
            NSString *statusidStr=@"1";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            NSString *message =[NSString stringWithFormat:@"%@ Asked Price of %@",singlogin.userNameStr,productNameStr                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
            NSMutableDictionary *payload = [NSMutableDictionary dictionary];
            NSMutableDictionary *aps = [NSMutableDictionary dictionary];
            [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
            [aps setObject:message forKey:QBMPushMessageAlertKey];
            [payload setObject:aps forKey:QBMPushMessageApsKey];
            
            QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
            
            // Send push to users with ids 292,300,1395
            [QBRequest sendPush:pushMessage toUsers:singlogin.vendor_objectid successBlock:^(QBResponse *response, QBMEvent *event) {
                // Successful response with event
                
                
                
            } errorBlock:^(QBError *error) {
                
                
                
                // Handle error
            }];
            
        }
        else
        {
            NSString *statusidStr=@"0";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"statusid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",statusidStr] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        
        
        
        NSString *final=@"1";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"quantityprod\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",final] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *name11=[[NSString alloc]initWithFormat:@"%@",productNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"productname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",name11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSDate * now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm:ss"];
        NSString *currentTime = [formatter stringFromDate:now];
        
        NSDate * now2 = [NSDate date];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd-MM-yyyy"];
        NSString *currentDate = [formatter2 stringFromDate:now2];
        
        
        NSString *orderdate1=[[NSString alloc]initWithFormat:@"%@",currentDate];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderdate\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",orderdate1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSString *ordertime1=[[NSString alloc]initWithFormat:@"%@",currentTime];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"ordertime\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ordertime1] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *venPic11=[[NSString alloc]initWithFormat:@"%@",vendorImgStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorpic\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venPic11] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *vendoridd=[[NSString alloc]initWithFormat:@"%@",vendorIdStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendoridd] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        
        NSString *vendor_obj=[[NSString alloc]initWithFormat:@"%@",v_obj];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorobjectid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",vendor_obj] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        
        
        NSString *venNamee=[[NSString alloc]initWithFormat:@"%@",vendorNameStr];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",venNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *userNamee=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",userNamee] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [theLoginRequest  setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"print mystring==%@",returnString);
        
        
        NSInteger j = [singlogin.totalCart integerValue];
        j = j+1 ;
        singlogin.totalCart = [NSString stringWithFormat:@"%ld",(long)j];
        cartLbl.text=singlogin.totalCart;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Product added to cart" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [ProgressHUD showSuccess:@""];
        [self cartTotalView];
        
        [indicator setHidden:YES];
        [indicator stopAnimating];
    }
    
}

- (IBAction)back:(id)sender

{
    totalArray = nil;
    singlogin.newarayList1 = Nil;
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)cart:(id)sender

{
    
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        [indicator setHidden:NO];
        [indicator startAnimating];
        [ProgressHUD show:@"Loading..." Interaction:NO];
        
        [self performSelector:@selector(cartbtn) withObject:nil afterDelay:0.5f];
    }
    
}


-(void)cartbtn

{
    CartProductAR *cart=[[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:nil];
    [self.navigationController pushViewController:cart animated:YES];
    [indicator setHidden:YES];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
}

- (IBAction)okay:(id)sender

{
    [thnxxView setHidden:YES];
}


- (IBAction)like:(id)sender

{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        if ([likeStatus isEqualToString:@"like"])
        {
            [likeBtn setImage:[UIImage imageNamed:@"NotLiked.png"] forState:UIControlStateNormal];
            likeStatus = @"unlike";
            [self delFavorite];
        }
        else
        {
            [likeBtn setImage:[UIImage imageNamed:@"Liked.png"] forState:UIControlStateNormal];
            likeStatus = @"like";
            [self addFavorite];
        }
    }
    
}


-(void)addFavorite

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/addfavvendor.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *usrnameStr=[[NSString alloc]initWithFormat:@"%@",singlogin.usrName];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.SlctVenId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vnameStr=[[NSString alloc]initWithFormat:@"%@",singlogin.venName];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vnameStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *instaidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.fUserId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"instagramid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",instaidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *picStr=[[NSString alloc]initWithFormat:@"%@",singlogin.vendorProfilepic];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorimage\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",picStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    if ([vendorFeedStatus isEqualToString:@"yes"])
    {
        NSString *status=@"Yes";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodstatus\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",status] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        NSString *status=@"No";
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"prodstatus\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",status] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
}


-(void)delFavorite

{
    NSString* strURL = [NSString stringWithFormat:@"http://vetrinaapp.com/vetrina2/delfavvendor.php"];
    NSLog(@"strURL:%@",strURL);
    NSURL * url=[NSURL URLWithString:strURL];
    NSMutableURLRequest *theLoginRequest = [NSMutableURLRequest requestWithURL:url];
    [theLoginRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theLoginRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSString *usrStr=[[NSString alloc]initWithFormat:@"%@",singlogin.userid];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",usrStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *vidStr=[[NSString alloc]initWithFormat:@"%@",singlogin.SlctVenId];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"vendorid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",vidStr] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theLoginRequest  setHTTPBody:body];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:theLoginRequest returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"print mystring==%@",returnString);
    
}


-(void)vendorfavlisting

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/favvendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No vendor available"])
    {
        
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
        
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
        
    }
    
    if([arra_1 count]==0)
    {
        
    }
    else
    {
        for (int j = 0;j<arra_1.count;j++)
        {
            NSString *str =[[arra_1 objectAtIndex:j]valueForKey:@"vendorid"];
            if ([str isEqualToString:singlogin.SlctVenId ])
            {
                [likeBtn setImage:[UIImage imageNamed:@"Liked.png"] forState:UIControlStateNormal];
                likeStatus = @"like";
            }
        }
    }
}


- (IBAction)signup:(id)sender
{
    [view1 setHidden:YES];
    singlogin.loginfrom = @"cart";
    All_SignInAR *signIn = [[All_SignInAR alloc]initWithNibName:@"All_SignInAR" bundle:NULL];
    [self.navigationController pushViewController:signIn animated:YES];
}


- (IBAction)noThanks:(id)sender
{
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
    [view1 setHidden:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self cartTotalView];
    [self.navigationController setNavigationBarHidden:YES];
}


- (IBAction)chatWithVendor:(id)sender
{
    if ([singlogin.loginStatus isEqualToString:@"IG Login"])
    {
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot chat as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        
        if (singlogin.userid == nil)
        {
            [indicator stopAnimating];
            [ProgressHUD showError:@""];
            NSLog(@"Please Login to Report");
            [view1 setHidden:NO];
        }
        else
        {
            [ProgressHUD show:@"Loading..." Interaction:NO];
            [self performSelector:@selector(chatnew) withObject:Nil afterDelay:2.0f];
        }
    }
}


//- (void)actionChat1:(NSString *)groupId
////-------------------------------------------------------------------------------------------------------------------------------------------------
//{
//    if (singlogin.userid == nil)
//    {
//        [indicator stopAnimating];
//        [ProgressHUD showError:@""];
//        NSLog(@"Please Login to Report");
//        [view1 setHidden:NO];
//    }
//    else
//    {
//        [ProgressHUD show:@"Loading..." Interaction:NO];
//        [self performSelector:@selector(chat) withObject:Nil afterDelay:2.0f];
//
//    }
//}


//-(void)chat
//
//{
//    for (NSInteger i=0; i< users.count; i++)
//    {
//        name= [[users objectAtIndex:i]valueForKey:@"objectId"];
//        if ([name containsString:singlogin.objectIdStr])
//        {
//            PFUser *user1 = [PFUser currentUser];
//            PFUser *user2 = users[i];
//            NSString *groupId = StartPrivateChat(user1, user2);
//            [self actionChat:groupId];
//            i = users.count;
//        }
//    }
//}
//
//
//- (void)actionChat:(NSString *)groupId
////-------------------------------------------------------------------------------------------------------------------------------------------------
//{
//    ChatView *chatView = [[ChatView alloc] initWith:groupId];
//    chatView.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatView animated:YES];
//    [ProgressHUD showSuccess:@""];
//}



- (IBAction)homeBtn:(id)sender
{
    [indicator setHidden:NO];
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:0.5f];
}


-(void)home
{
    Home_VetrinaARViewController *view = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)favrtBtn:(id)sender
{
    if (singlogin.userid == nil)
    {
        [view1 setHidden:NO];
    }
    else
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(fav) withObject:nil afterDelay:0.5f];
    }
}


-(void)fav
{
    FavoritesAR *view = [[FavoritesAR alloc]initWithNibName:@"FavoritesAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)chatBtnUser:(id)sender
{
    
    if (singlogin.userid == nil)
    {
        [view1 setHidden:NO];
    }
    else
    {
        [ProgressHUD show:@"Loading..." Interaction:NO];
        [self performSelector:@selector(chatbtnRe) withObject:nil afterDelay:0.5f];
    }
}


-(void)chatbtnRe
{
    RecentView *view = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)vendorProdctBtn:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendorProduct) withObject:nil afterDelay:0.5f];
}


-(void)vendorProduct
{
    Vendor_ProductAR *view = [[Vendor_ProductAR alloc]initWithNibName:@"Vendor_ProductAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (IBAction)vendrItme:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(vendoritems) withObject:nil afterDelay:0.5f];
}


-(void)vendoritems
{
    Vendor_itemsAR *view = [[Vendor_itemsAR alloc]initWithNibName:@"Vendor_itemsAR" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}





- (void)secretMenu:(UIGestureRecognizer *)gesture
{
    
    [PCRapidSelectionView viewForParentView:self.navigationController.view currentGuestureRecognizer:gesture interactive:YES options:@[@"Add to favorites",@"Open in Instagram",@"Report"] title:@"Advanced Options" completionHandler:^(NSInteger selectedIndex)
     {
         NSLog(@"Secret menu selected: %i",(int)selectedIndex);
         
         switch (selectedIndex)
         {
             case 0:
                 // [self addFavorite];
                 break;
             case 1:
                 // [self openInstagram];
                 break;
             case 2:
                 //  reportView.hidden = NO;
                 break;
             default:
                 break;
         }
         
     }];
}


-(void)openInstagram

{
    [indicator startAnimating];
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    
    
    if (instaArray.count==0) {
        selectedImageMediaId = [[totalArray objectAtIndex:0]valueForKey:@"vendorname"];
        NSLog(@"IMGURL %@",selectedImageMediaId);
        vendorpic_browserLink = [[totalArray objectAtIndex:0]valueForKey:@"linkpicbrw"];
        NSLog(@"vendorInstaName %@",vendorpic_browserLink);
        
        NSString* url = [NSString stringWithFormat:@"instagram://user?username=%@",selectedImageMediaId];
        NSURL *instagramURL = [NSURL URLWithString:url];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
        {
            [[UIApplication sharedApplication] openURL:instagramURL];
            [ProgressHUD showSuccess:@""];
            [indicator stopAnimating];
        }
        else
        {
            NSURL *url = [NSURL URLWithString:vendorpic_browserLink];
            [[UIApplication sharedApplication] openURL:url];
            [ProgressHUD showSuccess:@""];
            [indicator stopAnimating];
        }
        
    }else{
        
        
        
        selectedImageMediaId = [[totalArray objectAtIndex:0]valueForKey:@"vendorname"];
        NSLog(@"IMGURL %@",selectedImageMediaId);
        vendorpic_browserLink = [[instaArray objectAtIndex:0]valueForKey:@"linkpicbrw"];
        NSLog(@"vendorInstaName %@",vendorpic_browserLink);
        
        NSString* url = [NSString stringWithFormat:@"instagram://user?username=%@",selectedImageMediaId];
        NSURL *instagramURL = [NSURL URLWithString:url];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
        {
            [[UIApplication sharedApplication] openURL:instagramURL];
            [ProgressHUD showSuccess:@""];
            [indicator stopAnimating];
        }
        else
        {
            NSURL *url = [NSURL URLWithString:vendorpic_browserLink];
            [[UIApplication sharedApplication] openURL:url];
            [ProgressHUD showSuccess:@""];
            [indicator stopAnimating];
        }
        
        
        
        
    }
    
    
    
}



-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
    
    switch (row)
    {
        case 0:
            reportView.hidden = NO;
            
            break;
        case 1:
            [self addtocontacts];
            break;
        case 2:
        {
            NSString *mobileNo= singlogin.vEnPhone;
            NSString *contactNo = [NSString stringWithFormat:@"+%@",mobileNo];
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:contactNo];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
            break;
        case 3:
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                
                [self openInstagram];

                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
               // [alrt show];
            }
            else if (singlogin.userid == nil)
            {
                [self openInstagram];
                //[view1 setHidden:NO];
            }
            else
            {
                [self openInstagram];
                
            }
            break;
        case 4:
            if ([singlogin.loginStatus isEqualToString:@"IG Login"])
            {
                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You cannot do that as a vendor, please log out and sign in as a shopper." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alrt show];
            }
            else if (singlogin.userid == nil)
            {
                [view1 setHidden:NO];
            }
            else
            {
                
                [self like];
            }
            break;
        default:
            break;
    }
}



-(void)addtocontacts
{
    //    NSString * addressString1 = @"123 addree";
    //
    //    NSString * addressString2 = @"chandigarh";
    //
    //    NSString * cityName = @"Chandigarh";
    //
    //    NSString * stateName = @"Chandigarh";
    //
    //    NSString * postal = @"160055";
    //
    //    NSString * emailString = @"dhiman93umesh@gmail.com";
    
    NSString * phoneNumber =   singlogin.vEnPhone;
    
    NSString * prefName =[NSString stringWithFormat:@"%@ (Added using Vetrina)", singlogin.venName];
    
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        NSLog(@"Denied");
        
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
        ABRecordRef pet = ABPersonCreate();
        
        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)prefName, nil);
        //  ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
        
        
        
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)phoneNumber, kABPersonPhoneMainLabel, NULL);
        
        
        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
        // ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
        
        NSLog(@"%@", singlogin.vendorProfilepic);
        // [[NSUserDefaults standardUserDefaults] valueForKey:@"image_url"]
        NSURL *url1=[NSURL URLWithString:singlogin.vendorProfilepic];
        NSData *datapic=[NSData dataWithContentsOfURL:url1];
        
        // NSData *dataRef = UIImagePNGRepresentation(personImageView.image);
        ABPersonSetImageData(pet, (__bridge CFDataRef)datapic, nil);
        
        //       CFErrorRef  anError = NULL;
        
        
        ABAddressBookAddRecord(addressBookRef, pet, nil);
        ABAddressBookSave(addressBookRef, nil);
        //
        
        UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:@"Contact Added" message:@"The shop has been saved to your phone contacts" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        NSLog(@"Authorized");
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                NSLog(@"Just denied");
                
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                
                
                return;
            }
            //5
            NSLog(@"Just authorized");
            ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
            ABRecordRef pet = ABPersonCreate();
            
            ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)prefName, nil);
            // ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)petLastName, nil);
            
            
            
            ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            
            ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)phoneNumber, kABPersonPhoneMainLabel, NULL);
            
            
            ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
            //     ABPersonSetImageData(pet, (__bridge CFDataRef)petImageData, nil);
            
            NSURL *url1=[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] valueForKey:@"image_url"]];
            NSData *datapic=[NSData dataWithContentsOfURL:url1];
            
            // NSData *dataRef = UIImagePNGRepresentation(personImageView.image);
            ABPersonSetImageData(pet, (__bridge CFDataRef)datapic, nil);
            
            
            ABAddressBookAddRecord(addressBookRef, pet, nil);
            ABAddressBookSave(addressBookRef, nil);
            
        });
        
        
        
        
        
    }
    
    
    
    
    //    ABAddressBookRef libroDirec = ABAddressBookCreate();
    //
    //    ABRecordRef persona = ABPersonCreate();
    //
    //    ABRecordSetValue(persona, kABPersonFirstNameProperty, (__bridge CFTypeRef)(prefName), nil);
    //
    //    ABMutableMultiValueRef multiHome = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    //
    //    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    //
    //    NSString *homeStreetAddress=[addressString1 stringByAppendingString:addressString2];
    //
    //    [addressDictionary setObject:homeStreetAddress forKey:(NSString *) kABPersonAddressStreetKey];
    //
    //    [addressDictionary setObject:cityName forKey:(NSString *)kABPersonAddressCityKey];
    //
    //    [addressDictionary setObject:stateName forKey:(NSString *)kABPersonAddressStateKey];
    //
    //    [addressDictionary setObject:postal forKey:(NSString *)kABPersonAddressZIPKey];
    //
    //    bool didAddHome = ABMultiValueAddValueAndLabel(multiHome, (__bridge CFTypeRef)(addressDictionary), kABHomeLabel, NULL);
    //
    //    if(didAddHome)
    //    {
    //        ABRecordSetValue(persona, kABPersonAddressProperty, multiHome, NULL);
    //
    //        NSLog(@"Address saved.....");
    //    }
    //
    //  //  [addressDictionary release];
    //
    //    //##############################################################################
    //
    //    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    //
    //    bool didAddPhone = ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(phoneNumber), kABPersonPhoneMobileLabel, NULL);
    //
    //    if(didAddPhone){
    //
    //        ABRecordSetValue(persona, kABPersonPhoneProperty, multiPhone,nil);
    //
    //        NSLog(@"Phone Number saved......");
    //
    //
    //
    //    }
    //
    //    CFRelease(multiPhone);
    //
    //    //##############################################################################
    //
    //    ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABPersonEmailProperty);
    //
    //    bool didAddEmail = ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFTypeRef)(emailString), kABOtherLabel, NULL);
    //
    //    if(didAddEmail){
    //
    //        ABRecordSetValue(persona, kABPersonEmailProperty, emailMultiValue, nil);
    //
    //        NSLog(@"Email saved......");
    //    }
    //
    //    CFRelease(emailMultiValue);
    //
    //    //##############################################################################
    //
    //    ABAddressBookAddRecord(libroDirec, persona, nil);
    //
    //    CFRelease(persona);
    //
    //    ABAddressBookSave(libroDirec, nil);
    //
    //    CFRelease(libroDirec);
    //
    //    NSString * errorString = [NSString stringWithFormat:@"Information are saved into Contact"];
    //
    //    UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:@"New Contact Info" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //    [errorAlert show];
    
    // [errorAlert release];
}

- (void)like
{
    if (singlogin.userid == nil)
    {
        [indicator stopAnimating];
        [ProgressHUD showError:@""];
        NSLog(@"Please Login to add cart");
        [view1 setHidden:NO];
    }
    else
    {
        if ([likeStatus isEqualToString:@"like"])
        {
            [likeBtn setImage:[UIImage imageNamed:@"NotLiked.png"] forState:UIControlStateNormal];
            likeStatus = @"unlike";
            addButton.imageArray = @[@"ReportFlot.png",@"ContactsFlot.png",@"CallFlot.png",@"OpenInInstagramFlot.png",@"UnfavoriteFlot.png"];
            addButton.labelArray = @[@"تحذير", @"أضف إلى الجوال",@"اتصال هاتفي",@"فتح في انستقرام",@"حذف من المحلات المفضلة"];
            
            [self delFavorite];
        }
        else
        {
            [likeBtn setImage:[UIImage imageNamed:@"Liked.png"] forState:UIControlStateNormal];
            addButton.imageArray = @[@"ReportFlot.png",@"ContactsFlot.png",@"CallFlot.png",@"OpenInInstagramFlot.png",@"FavoriteFlot.png"];
            addButton.labelArray = @[@"تحذير", @"أضف إلى الجوال",@"اتصال هاتفي",@"فتح في انستقرام",@"أضف إلى المحلات المفضلة"];
            
            
            likeStatus = @"like";
            [self addFavorite];
        }
    }
}


@end




