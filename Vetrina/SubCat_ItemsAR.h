//
//  SubCat_ItemsAR.h
//  Vetrina
//
//  Created by Umesh Kumar on 30/04/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"



@interface SubCat_ItemsAR : UIViewController<UITableViewDataSource,UITextFieldDelegate,BSKeyboardControlsDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UITextFieldDelegate>

{
    Singleton *singlogin;
    BSKeyboardControls *keyboardControls;
    IBOutlet UITableView *itemsTbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIButton *itemsLbl;
    IBOutlet UIButton *shoplab;
    IBOutlet UILabel *subcatName;
    
    UIImageView *Image2, *img3;
    
    NSString *productIdStr,*prodImgStr,*unitPriceStr,*productNameStr,*vendorImgStr,*vendorIdStr,*vendorNameStr,*objectIdStr;
    
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIView *view1;
    UIActionSheet *actionSheet0,*actionSheet1,*actionSheet2,*actionSheet3,*actionSheet4,*actionSheet5;
    
    IBOutlet UIButton *noThanks;
    NSMutableArray *aray_2;
    NSArray *sortedarray1,*sortedarray2;
    NSMutableArray *cartListArray;
    NSString *cartIdStr,*quantStr,*prodid;
    NSString *searchStr,*soldout,*askforPrice,*staffPic,*name;
    
    IBOutlet UIButton *searchBtn;
    IBOutlet UITextField *searchText;
    NSMutableArray *searchArray;
    IBOutlet UIView *noitemview;
    IBOutlet UILabel *chrtLbl;


}
@end
