//
//  AddressCellAR.h
//  Vetrina
//
//  Created by Amit Garg on 5/8/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCellAR : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *addressBtn;


@end
