//
//  HomeAR_Cell1.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeAR_Cell1 : UITableViewCell

@property (nonatomic,retain)IBOutlet UIImageView *arrowImageView;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *lineLbl;

- (void)changeArrowWithUp:(BOOL)up;


@end
