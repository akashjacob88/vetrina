//
//  HomeAR_Cell2.h
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeAR_Cell2 : UITableViewCell

@property (nonatomic,retain)IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *lineLbl;

@end
