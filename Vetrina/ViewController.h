//
//  ViewController.h
//  ExpansionTableView
//
//  Created by JianYe on 13-2-18.
//  Copyright (c) 2013年 JianYe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface ViewController : UIViewController

{
    Singleton *singlogin;
    
    /* -------- Arrays ------- */
    
    IBOutlet UILabel *vendorChatLblk;
    IBOutlet UILabel *chatLbl;
    NSMutableArray *newArray;
    NSMutableArray *array,*finalArray;
    NSMutableArray *vendorNameArray,*dataArray;
    NSArray *sectionAnimals;
    
    NSMutableArray *imageArr,*img2;
    
    
    /* -------- Strings ------- */

    NSString *newString;
    NSString *stringOne,*stringTwo,*stringTwo1,*stringTwo2;
    
    NSString *itme;
    
    /* -------- Integer ------- */

    NSInteger i;
    
    /* -------- Dictionary ------- */

    NSMutableDictionary *dynamicDict;
    
    /* -------- OutLets ------- */
    
    IBOutlet UIButton *signIn;
    
    IBOutlet UIImageView *settingIcon;

    IBOutlet UILabel *chrtLbl;

    IBOutlet UIImageView *img33;
    
    IBOutlet UIScrollView *scrollView;
    
    UIImageView  *Image2;
    IBOutlet UIImageView *leftSearch,*rightSearch,*cart,*profilePic;
    IBOutlet UIView *view1;
    IBOutlet UIButton *signUpbtn;
    IBOutlet UIButton *noThanksbtn;
    IBOutlet UIView *vendorViewBar;

    
}
@property (strong, nonatomic) IBOutlet UILabel *count_lbl_user;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

- (IBAction)signBtn:(id)sender;

- (IBAction)search:(id)sender;

- (IBAction)chartBtn:(id)sender;

- (IBAction)myFavrtBtn:(id)sender;

- (IBAction)permotionalBtn:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)noThanks:(id)sender;


@end
