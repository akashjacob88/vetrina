//
//  ShowOrder.h
//  Vetrina
//
//  Created by Amit Garg on 5/4/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface ShowOrder : UIViewController
{
    Singleton *singletonn;
    IBOutlet UITableView *productlistingTable;
    NSString *deliPriceStr,*totalStr,*addressStr;
}
@end
