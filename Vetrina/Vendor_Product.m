//
//  Vendor_Product.m
//  Vetrina
//
//  Created by Amit Garg on 4/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "Vendor_Product.h"
#import "Vendor_items.h"
#import "Vendor_setting.h"
#import "JSON.h"
#import "Vendor_ProductCell.h"
#import "CartVendorProductListViewController.h"
#import <Parse/Parse.h>
#import "ChatView.h"
#import "ProgressHUD.h"
#import "RecentView.h"
#import "Vendor_Profile.h"
#import "ViewController.h"

#import "common.h"
#import "recent.h"
#import "AppConstant.h"
#import "UIImageView+WebCache.h"



@interface Vendor_Product ()
{
    NSMutableArray *recents;
    IBOutlet UIImageView *userPice;
     NSArray *dialog_Objects;
    NSString *userobj;
    NSString *prodname;
    NSString *uname;


}

@end

@implementation Vendor_Product

- (void)viewDidLoad
{
    
//    PFQuery *query = [PFQuery queryWithClassName:PF_RECENT_CLASS_NAME];
//    [query whereKey:PF_RECENT_USER equalTo:[PFUser currentUser]];
//    [query includeKey:PF_RECENT_LASTUSER];
//    [query orderByDescending:PF_RECENT_UPDATEDACTION];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (error == nil)
//         {
//             [recents removeAllObjects];
//             [recents addObjectsFromArray:objects];
//             [userListingTable reloadData];
//             [self updateTabCounter];
//         }
//         else [ProgressHUD showError:@"Network error."];
//        }];
    
    [super viewDidLoad];
    singloging = [Singleton instance];
    
    NSURL *url1=[NSURL URLWithString:singloging.venPic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    userPice.image=emppppic;
    userPice.layer.masksToBounds = YES;
    userPice.layer.borderWidth = 1.0f;
    userPice.layer.cornerRadius = userPice.frame.size.width/2;
    userPice.layer.borderColor = [[UIColor clearColor]CGColor];
    userPice.clipsToBounds = YES;
    userPice.layer.opaque = NO;

    
    _count_lbl.hidden=YES;
    
    
    
    // TopBar View///
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;

    
    NSLog(@"test--- %@",singloging.loginStatus);
    if ([singloging.loginStatus isEqual:@"User Login"])
    {
        NSLog(@"vendorid=%@",singloging.vid) ;
    }
    else if([singloging.loginStatus isEqual:@"IG Login"])
    {
        NSLog(@"vendorid=%@",singloging.vUniqueId) ;
    }

//    vendorName.text= singloging.shopNameVendr;
//    orderbtn.layer.cornerRadius = 5.0;
//    itembtn.layer.masksToBounds = YES;
//    itembtn.layer.cornerRadius = 5.0;
//    itembtn.layer.borderWidth = 2.0;
//    itembtn.layer.borderColor = [[UIColor whiteColor]CGColor];
//    itembtn.clipsToBounds = YES;
//    itembtn.layer.opaque = NO;

    [self prodListing];
    indicator.hidden=YES;
    [indicator stopAnimating];
    appear=@"yes";
   
    recents = [[NSMutableArray alloc] init];
    [self loadRecents];

}


- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidAppear:animated];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([PFUser currentUser] != nil)
    {
        [self loadRecents];
    }
    else LoginUser(self);
}


- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singloging.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }

    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    _count_lbl.text=[NSString stringWithFormat:@"%d", total];
    _count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singloging.userid == nil)
    {
        _count_lbl.hidden=YES;
        _count_lbl.hidden = YES;
    }
    else
    {
        if ([_count_lbl.text isEqualToString:@"0"])
        {
            _count_lbl.hidden=YES;
            _count_lbl.hidden = YES;
        }
        else if ([_count_lbl.text isEqualToString:@""])
        {
            _count_lbl.hidden=YES;
            _count_lbl.hidden = YES;
        }
        else
        {
            _count_lbl.hidden=NO;
            _count_lbl.hidden = NO;
            _count_lbl.layer.cornerRadius=_count_lbl.frame.size.height/2;
            _count_lbl.clipsToBounds=YES;
            _count_lbl.layer.cornerRadius=_count_lbl.frame.size.height/2;
            _count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }

    
    
    
}


- (IBAction)chat:(id)sender
{
    
    
      [ProgressHUD show:@"Loading..." Interaction:NO];
   [self performSelector:@selector(chatBtn) withObject:Nil afterDelay:2.0f];
}

-(void)chatBtn
{
    RecentView *view = [[ RecentView alloc] initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:view animated:NO];
    [ProgressHUD showSuccess:@""];
}



-(void)Objectuser

{
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/objectvendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    
    NSLog(@"GetDatadictt--%@",eventarray);
    
    
    
    NSMutableArray *valu = [eventarray valueForKey:@"list"];
    
  
    
    if (valu.count==1)
    {
        
        NSString *uniqueid=[[valu objectAtIndex:0]valueForKey:@"id"];
        NSLog(@"Unique ID %@",uniqueid);
        NSString *userobid=[[valu objectAtIndex:0] valueForKey:@"user_objectid"];
        NSString *vendorobid=[[valu objectAtIndex:0] valueForKey:@"vendor_objectid"];
        NSString *userid=[[valu objectAtIndex:0] valueForKey:@"userid"];
        NSString *username=[[valu objectAtIndex:0] valueForKey:@"username"];
        NSString *vendorname=[[valu objectAtIndex:0] valueForKey:@"fullname_shopname"];
        NSString *image=[[valu objectAtIndex:0] valueForKey:@"image"];
        NSString *groupid=[[valu objectAtIndex:0] valueForKey:@"group_orderid"];
        singloging.userIdStr = userobid;
        singloging.venId = vendorobid;
        singloging.userid = userid;
        singloging.userNameStr = username;
        singloging.venName = vendorname;
        singloging.venPic = image;
        singloging.venIdList = groupid;

    }
    else
    {
        
        NSInteger a =valu.count-1;
        
        NSString *uniqueid=[[valu objectAtIndex:a]valueForKey:@"id"];
        NSLog(@"Unique ID %@",uniqueid);
        NSString *userobid=[[valu objectAtIndex:a] valueForKey:@"user_objectid"];
        NSString *vendorobid=[[valu objectAtIndex:a] valueForKey:@"vendor_objectid"];
        NSString *userid=[[valu objectAtIndex:a] valueForKey:@"userid"];
        NSString *username=[[valu objectAtIndex:a] valueForKey:@"username"];
        NSString *vendorname=[[valu objectAtIndex:a] valueForKey:@"fullname_shopname"];
        NSString *image=[[valu objectAtIndex:a] valueForKey:@"image"];
        NSString *groupid=[[valu objectAtIndex:a] valueForKey:@"group_orderid"];
        singloging.userIdStr = userobid;
        singloging.venId = vendorobid;
        singloging.userid = userid;
        singloging.userNameStr = username;
        singloging.venName = vendorname;
        singloging.venPic = image;
        singloging.venIdList = groupid;

    
    }
    
       [self userObId];


}


-(void)userObId
{
    NSString *id1 =  singloging.userIdStr;
    NSString *id2 =   singloging.venId;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSString *groupIdd = ([id1 compare:id2] < 0) ? [NSString stringWithFormat:@"%@%@", id1, id2] : [NSString stringWithFormat:@"%@%@", id2, id1];
    singloging.venIdList = groupIdd;
   
    ChatView *chatView = [[ChatView alloc] initWith:singloging.venIdList];
    chatView.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:chatView animated:NO];

}



- (IBAction)setting:(id)sender
{
    [self performSelector:@selector(settingBtn)withObject:Nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [indicator startAnimating];
    indicator.hidden=NO;
}

-(void)settingBtn

{
    Vendor_Profile *setting = [[Vendor_Profile alloc]initWithNibName:@"Vendor_Profile" bundle:nil];
    [self.navigationController pushViewController:setting animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)items:(id)sender
{
    [self performSelector:@selector(itemBtn)withObject:Nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];

    [indicator startAnimating];
    indicator.hidden=NO;
    
}


-(void)itemBtn
{
    Vendor_items *items = [[Vendor_items alloc]initWithNibName:@"Vendor_items" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
}



-(void)prodListing
{
//     NSString *post1;
//    if ([singloging.loginStatus isEqual:@"User Login"])
//    {
//        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vid];
//    }
//    else if ([singloging.loginStatus isEqual:@"IG Login"])
//    {
//        post1 = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
//    }

    
    NSString *post = [NSString stringWithFormat:@"vendorid=%@",singloging.vUniqueId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/vendororderlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *eventarray =[data JSONValue];
    if([[eventarray objectForKey:@"list"] isEqual:@"No list available"])
    {
        dataArray=[[NSMutableArray alloc]init];
    }
    else
    {
        dataArray = [[NSMutableArray alloc]initWithArray:[eventarray valueForKey:@"list"]];
    }
    if([dataArray count]==0)
    {
        noOrderView.hidden = NO;
        userListingTable.hidden = YES;
        editBtn.hidden = YES;
        //[userListingTable reloadData];
    }
    else
    {
        NSLog(@"GetDatadictt--%@",dataArray);
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"statusid"  ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray1 = [dataArray sortedArrayUsingDescriptors:sortDescriptors];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray1 mutableCopy];
        
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc]initWithKey:@"orderdate"  ascending:NO];
        NSArray *sortDescriptors1 = [NSArray arrayWithObject:sortDescriptor1];
        NSArray *sortedArray2 = [dataArray sortedArrayUsingDescriptors:sortDescriptors1];
        dataArray=[[NSMutableArray alloc]init];
        dataArray = [sortedArray2 mutableCopy];

        
        
        NSSortDescriptor *itemXml = [[NSSortDescriptor alloc] initWithKey:@"ordertime" ascending:NO];
        NSArray *sortedArray = [[NSArray alloc] initWithObjects:itemXml,nil];
        sortedArray = [dataArray sortedArrayUsingDescriptors:sortedArray];
        
        NSLog(@"sorted array is %@",sortedArray);
        
        newArray=[[NSMutableArray alloc]init];
        array=[[NSMutableArray alloc]init];
        finalArray=[[NSMutableArray alloc]init];
        
        userNameArray=[[NSMutableArray alloc]init];
        imageArr=[[NSMutableArray alloc]init];
        
        for( i =0;i<sortedArray.count;i++)
        {
            stringOne=[[sortedArray objectAtIndex:i]valueForKey:@"username"];
            
            NSString *userName1=[[sortedArray objectAtIndex:i]valueForKey:@"username"];
            NSString *imagestr=[[sortedArray objectAtIndex:i]valueForKey:@"vendorpic"];
            if (![stringOne isEqualToString:stringTwo ])
            {
                [userNameArray addObject:userName1];
                [imageArr addObject:imagestr];
                stringTwo = stringOne;
                
                if ([newString isEqualToString:@"newString"])
                {
                    NSInteger a=i-1;
                    NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                }
                if ([newString isEqualToString:@"oneString"])
                {
                    
                    NSInteger a=i-1;
                    NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
                    dynamicDict=[[NSMutableDictionary alloc]init];
                    [dynamicDict setValue:newArray forKey:userName];
                    [finalArray addObject:dynamicDict];
                    newArray=[[NSMutableArray alloc]init];
                    newString=@"";
                    
                }
                newString=@"oneString";
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
            }
            else
            {
                
                array=[sortedArray objectAtIndex:i];
                [newArray addObject:array];
                
                newString=@"newString";
            }
        }
        
        NSInteger a=i-1;
        NSString *userName=[[sortedArray objectAtIndex:a]valueForKey:@"username"];
        dynamicDict=[[NSMutableDictionary alloc]init];
        [dynamicDict setValue:newArray forKey:userName];
        [finalArray addObject:dynamicDict];
        newArray=[[NSMutableArray alloc]init];
        newString=@"";
        stringOne=@"";
        stringTwo=@"";
        [userListingTable reloadData];
        noOrderView.hidden = YES;
         editBtn.hidden = NO;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [finalArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Vendor_ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VendorCartCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Vendor_ProductCell" bundle:nil] forCellReuseIdentifier:@"VendorCartCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"VendorCartCell"];
//        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
//        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
//        [userListingTable addSubview:refreshControl];
        
        UITableViewController *tableViewController = [[UITableViewController alloc] init];
        tableViewController.tableView = userListingTable;
        
        refreshControl  = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(getConnections) forControlEvents:UIControlEventValueChanged];
        tableViewController.refreshControl = refreshControl;
    }
    cell.backgroundColor=[UIColor whiteColor];
    UIView *v11=[[UIView alloc]initWithFrame:CGRectMake(0, 54, 414, 1)];
    v11.backgroundColor=[UIColor colorWithRed:227.0f/255 green:227.0f/255 blue:227.0f/255 alpha:1];
    [cell.contentView addSubview:v11];
    return cell;
}


-(void)getConnections
{
    [self prodListing];
    [refreshControl endRefreshing];
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(Vendor_ProductCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [userNameArray objectAtIndex:indexPath.row];
    NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
    cell.userName.text = [[sectionAnimals objectAtIndex:0]valueForKey:@"username"];
    
    NSString *date = [[sectionAnimals objectAtIndex:0]valueForKey:@"orderdate"];
    NSString *time = [[sectionAnimals objectAtIndex:0]valueForKey:@"ordertime"];
    
    cell.msgTxt.text =  [NSString stringWithFormat:@"%@, %@",date,time];
    NSString *strImageUrl = [[sectionAnimals objectAtIndex:0]valueForKey:@"userprofilepic"];

   
    __block UIActivityIndicatorView *activityIndicator;
    [cell.profileUser sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                    placeholderImage:[UIImage imageNamed:@"UserMaleIcon.png"]
                             options:SDWebImageProgressiveDownload
                            progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
         if (!activityIndicator)
         {
             cell.profileUser.hidden = YES;
             [cell.contentView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
             activityIndicator.center = cell.profileUser.center;
             activityIndicator.color = [UIColor colorWithRed:119.0/255.0 green:218.0/255.0 blue:172.0/255.0 alpha:1.0];
             [activityIndicator startAnimating];
         }
     }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                               cell.profileUser.hidden = NO;
                               [activityIndicator removeFromSuperview];
                               activityIndicator = nil;
                           }];

    cell.profileUser.layer.masksToBounds = YES;
    cell.profileUser.layer.borderWidth = 1.0f;
    cell.profileUser.layer.cornerRadius = cell.profileUser.frame.size.width/2;
    cell.profileUser.layer.borderColor = [[UIColor clearColor]CGColor];
    cell.profileUser.clipsToBounds = YES;
    cell.profileUser.layer.opaque = NO;

    
//    for (i=0; i< recents.count; i++)
//    {
//
//        NSString *name= [[recents objectAtIndex:i]valueForKey:@"description"];
//       if ([name containsString:cell.userName.text])
//       {
//           cell.msgTxt.text = [[recents objectAtIndex:i]valueForKey:@"lastMessage"];
//           [cell bindData:recents[i]];
//
//           i = recents.count;
//
//       }
//    }
    
    for (i=0; i< sectionAnimals.count; i++)
    {
        NSString *statusStr= [[sectionAnimals objectAtIndex:i]valueForKey:@"statusid"];
      
        if ([statusStr isEqualToString:@"2"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Ordered.png"];
        }
        if ([statusStr isEqualToString:@"1"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Query1.png"];
            i=sectionAnimals.count;
        }
        if ([statusStr isEqualToString:@"3"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Confirmed.png"];
            i=sectionAnimals.count;
        }
        if ([statusStr isEqualToString:@"4"])
        {
            cell.statusImage.image=[UIImage imageNamed:@"Delivered.png"];
            i=sectionAnimals.count;
        }
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     singloging.productArray=[[NSMutableArray alloc]init];
     NSString *sectionTitle = [userNameArray objectAtIndex:indexPath.row];
     singloging.productArray = [[finalArray objectAtIndex:indexPath.row]valueForKey:sectionTitle];
     singloging.usrName = [[singloging.productArray objectAtIndex:0]valueForKey:@"username"];
     singloging.userid = [[singloging.productArray objectAtIndex:0]valueForKey:@"userid"];
    
    singloging.chat_user= [[singloging.productArray objectAtIndex:0]valueForKey:@"userobjectid"];
    
    NSLog(@"%@",singloging.chat_user);
    NSLog(@"%@",[[finalArray objectAtIndex:indexPath.row]valueForKey:@"userobjectid"]);

    
    
     [self performSelector:@selector(nextPage)withObject:Nil afterDelay:0.5f];
    [ProgressHUD show:@"Loading..." Interaction:NO];

     [indicator startAnimating];
      indicator.hidden=NO;
}
-(void)nextPage
{
    CartVendorProductListViewController *cart=[[CartVendorProductListViewController alloc]initWithNibName:@"CartVendorProductListViewController" bundle:nil];
    [self.navigationController pushViewController:cart animated:NO];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    [indicator setHidden:YES];
}

//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] init];
//    // NSString *vendorName11 = [userNameArray objectAtIndex:section];
////    NSMutableArray *statusStr1 = [[NSMutableArray alloc]init];
////    statusStr1 = [[dataArray objectAtIndex:section]valueForKey:@"anurag"];
//    if (dataArray.count == 0)
//    {
//        
//    }
//    else
//    {
//    NSString *statusStr = [[dataArray objectAtIndex:section]valueForKey:@"statusid"];
//    
//    UILabel *vendor =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 310, 40)];
//    vendor.font = [UIFont fontWithName:@"OpenSans-Bold" size:16];
//    
//    if ([statusStr isEqualToString:@"2"])
//    {
//        vendor.text =[NSString stringWithFormat:@"ORDERS"];
//    }
//    if ([statusStr isEqualToString:@"1"])
//    {
//        vendor.text =[NSString stringWithFormat:@"ASK FOR PRICE"];
//    }
//    if ([statusStr isEqualToString:@"3"])
//    {
//        vendor.text =[NSString stringWithFormat:@"WAITING FOR DELIVERY"];
//    }
//    if ([statusStr isEqualToString:@"4"])
//    {
//        vendor.text =[NSString stringWithFormat:@"DELIVERED"];
//    }
//
//   // vendor.text =[NSString stringWithFormat:@"%@",vendorName11];
//    vendor.textColor =[UIColor darkGrayColor];
//    vendor.backgroundColor =[UIColor clearColor];
//    vendor.numberOfLines = 1;
//    vendor.textAlignment = NSTextAlignmentLeft;
//    [headerView addSubview:vendor];
//    headerView.backgroundColor=[UIColor clearColor];
//    
//    
//    
//    }
//    
//    
//    return headerView;
//}
//-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40;
//}





-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
-(void)viewWillAppear:(BOOL)animated
{
    if (![appear isEqualToString:@"yes"])
    {
        [self prodListing];
    }
    appear=@"";
    [self.navigationController setNavigationBarHidden:YES];
}



- (IBAction)homeVendor:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeBtn)withObject:Nil afterDelay:0.5f];
}


-(void)homeBtn
{
    ViewController *home = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:home animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)edit:(id)sender
{
    if([editBtn.currentTitle isEqualToString:@"DONE"])
    {
        [super setEditing:NO animated:NO];
        [userListingTable setEditing:NO animated:NO];
        [editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
        [userListingTable reloadData];
        
    }
    else if([editBtn.currentTitle isEqualToString:@"EDIT"])
    {
        [super setEditing:YES animated:YES];
        [userListingTable setEditing:YES animated:YES];
        [editBtn setTitle:@"DONE" forState:UIControlStateNormal];
        [userListingTable reloadData];
        
    }

    
}



-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
//                                        if (indexPath.row == 0)
//                                        {
                                        
                                            NSArray *sectionAnimals= [dataArray objectAtIndex:indexPath.section];
                                           // NSArray *sectionAnimals = [[finalArray objectAtIndex:indexPath.section]valueForKey:sectionTitle];
                                            orderId =[sectionAnimals valueForKey:@"orderid"];
                                        userobj=[sectionAnimals valueForKey:@"userobjectid"];
                                        prodname=[sectionAnimals valueForKey:@"productname"];
                                        uname=[sectionAnimals valueForKey:@"vendorname"];

                                                [ProgressHUD show:@"Loading..." Interaction:NO];
                                                [self performSelector:@selector(deleteView1) withObject:nil afterDelay:0.5f];
                                    //}
                                                                            }];
    button.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:68.0/255.0 blue:102.0/255.0 alpha:1.0];
    
    return @[button];
}

-(void)deleteView1
{
    NSString *post = [NSString stringWithFormat:@"orderid=%@",orderId];
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/delorder.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSDictionary *eventarray=[data JSONValue];
    NSLog(@"%@",eventarray);
    
    NSString *message =[NSString stringWithFormat:@"%@ cancelled your order of %@",uname,prodname                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ];
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    NSMutableDictionary *aps = [NSMutableDictionary dictionary];
    [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
    [aps setObject:message forKey:QBMPushMessageAlertKey];
    [payload setObject:aps forKey:QBMPushMessageApsKey];
    
    QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
    
    // Send push to users with ids 292,300,1395
    [QBRequest sendPush:pushMessage toUsers:userobj successBlock:^(QBResponse *response, QBMEvent *event) {
        // Successful response with event
        
        
        
    } errorBlock:^(QBError *error) {
        
        
        
        // Handle error
    }];

    
    
    
    
    [self prodListing];
    [userListingTable reloadData];
    [ProgressHUD showSuccess:@""];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([editBtn.currentTitle isEqualToString:@"DONE"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


@end
