//
//  CartVendorProductListViewController.h
//  Vetrina
//
//  Created by Amit Garg on 5/5/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface CartVendorProductListViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    Singleton *singletonn;
    IBOutlet UITableView *prodListingTable;
    IBOutlet UILabel *userNameLbl,*userAddressLbl;
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UITextField *sendMessageTxt;
    NSString *statusStr;
    IBOutlet UIView *whatsappView,*locationView;
    NSString *jointText,*orderIdStr,*name;
    NSMutableArray *dataArray,*finalArray,*valu;

}
-(IBAction)Back:(id)sender;
-(IBAction)sms:(id)sender;
-(IBAction)whatsapp:(id)sender;
-(IBAction)nothanx:(id)sender;
-(IBAction)shareDetails:(id)sender;
-(IBAction)location:(id)sender;

@end
