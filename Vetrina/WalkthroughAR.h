//
//  WalkthroughAR.h
//  Vetrina
//
//  Created by Amit Garg on 4/20/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


@interface WalkthroughAR : UIViewController

{
    Singleton * signLogin;
    
    /* ----------- All Arrays ---------- */
    
    NSMutableArray *imagesArray;
    
    NSMutableArray *imgArray,*cartListArray;
    
    NSMutableArray *animationNrmlViewImg;
    
    NSMutableArray *swipImagesArray;
    
    /* ----------- IBoutlets ---------- */
    
    IBOutlet UIActivityIndicatorView *Indicator;
    
    IBOutlet UIImageView *slideImgView;
    
    IBOutlet UIButton *strtShopngoult;
    
    IBOutlet UIPageControl *pageControl;
    NSInteger imageIndex;

}

- (IBAction)swaip:(UISwipeGestureRecognizer *)sender;
- (IBAction)englishBtn:(id)sender;
- (IBAction)startshopingBtn:(id)sender;



@end
