//
//  FavoritesAR.m
//  Vetrina
//
//  Created by Amit Garg on 4/30/15.
//  Copyright (c) 2015 Amit Garg. All rights reserved.
//

#import "FavoritesAR.h"

#import "JSON.h"
#import "AsyncImageView.h"
#import "Vendor_DetaillistAR.h"
#import "Sorry_cartAR.h"
#import "favoritesCellAR.h"
#define kInstagramAPIBaseURL @"https://api.instagram.com"
#import "User_Login_InstaAR.h"
#import "CartProductAR.h"
#import "ProgressHUD.h"
#import "favrotItemsAR.h"
#import "User_ProfileAR.h"
#import "Home_VetrinaARViewController.h"
#import "RecentView.h"

#import <Parse/Parse.h>
#import "AppConstant.h"

@interface FavoritesAR ()
{
    IBOutlet UIView *topBar;
    
    IBOutlet UIImageView *profilePic;
    NSMutableArray *recents;
NSArray *dialog_Objects;
}
@end

@implementation FavoritesAR

- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    
    //    [@"type"] = @(2);
    
    [QBRequest dialogsForPage:page extendedRequest:@{@"id": singlogin.user_id} successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        NSLog(@"%@",dialogObjects);
        dialog_Objects=[[NSArray alloc]init];
        dialog_Objects=dialogObjects;
        [self updateTabCounter];
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"%@",response);
        
        
    }];
    
    
}


- (void)updateTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    int total = 0;
    for (QBChatDialog *recent in dialog_Objects)
    {
        total += recent.unreadMessagesCount;
    }
    
    UITabBarItem *item = self.tabBarController.tabBar.items[0];
    item.badgeValue = (total == 0) ? nil : [NSString stringWithFormat:@"%d", total];
    
    
    
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
    count_lbl.text=[NSString stringWithFormat:@"%d", total];
	   
    if (singlogin.userid == nil)
    {
        count_lbl.hidden=YES;
        count_lbl.hidden = YES;
    }
    else
    {
        if ([count_lbl.text isEqualToString:@"0"])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else if ([count_lbl.text isEqualToString:@""])
        {
            count_lbl.hidden=YES;
            count_lbl.hidden = YES;
        }
        else
        {
            count_lbl.hidden=NO;
            count_lbl.hidden = NO;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            count_lbl.layer.cornerRadius=count_lbl.frame.size.height/2;
            count_lbl.clipsToBounds=YES;
            //countLbl.text=[NSString stringWithFormat:@"%d", total];
        }
    }
    
}






- (void)viewDidLoad

{
    [super viewDidLoad];
    vendorTabl.sectionFooterHeight = 0;
    vendorTabl.sectionHeaderHeight = 0;
    [indicator stopAnimating];
    
    singlogin=[Singleton instance];
    // TopBar View///
    
    
    recents=[[NSMutableArray alloc]init];
    [self loadRecents];
    
    topBar.layer.shadowRadius = 2.0f;
    topBar.layer.shadowOffset = CGSizeMake(0, 2);
    topBar.layer.shadowColor = [UIColor blackColor].CGColor;
    topBar.layer.shadowOpacity = 0.5f;
    
    
    // Tableview///
    
    vendorTabl.layer.shadowRadius = 3.0f;
    vendorTabl.layer.shadowOffset = CGSizeMake(0, 2);
    vendorTabl.layer.shadowColor = [UIColor blackColor].CGColor;
    vendorTabl.layer.shadowOpacity = 0.5f;
    
    count_lbl.hidden=YES;
    
    
    NSURL *url1=[NSURL URLWithString:singlogin.userproilePic];
    NSData *datapic=[NSData dataWithContentsOfURL:url1];
    UIImage *emppppic=[UIImage imageWithData:datapic];
    profilePic.image=emppppic;
    
    if (emppppic==nil) {
        
        profilePic.image=[UIImage imageNamed:@"CircledUserMaleFilled.png"];
        
        
    }
    
    
    profilePic.layer.masksToBounds = YES;
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.layer.opaque = NO;

    if ([singlogin.check isEqualToString:@"1"])
    {
        viewImprt.hidden = YES;
    }
    else
    {
        viewImprt.hidden = NO;
    }

    
    [self vendornamelisting];
    [self accessTokenList];
    vendorTabl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    chrtLbl.hidden=YES;
    [self cartTotalView];
    appear=@"yes";
}


- (IBAction)items:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(home) withObject:nil afterDelay:2.0f];
}


-(void)home
{
    favrotItemsAR *items =[[favrotItemsAR alloc]initWithNibName:@"favrotItemsAR" bundle:nil];
    [self.navigationController pushViewController:items animated:NO];
    [ProgressHUD showSuccess:@""];
}


-(void)cartTotalView
{
    if (singlogin.userid == nil)
    {
        chrtLbl.hidden=YES;
    }
    else
    {
        if ([singlogin.totalCart isEqualToString:@"0"])
        {
            chrtLbl.hidden=YES;
        }
        else if (singlogin.totalCart ==nil)
        {
            chrtLbl.hidden=YES;
        }
        else
        {
            chrtLbl.hidden=NO;
            chrtLbl.layer.cornerRadius=chrtLbl.frame.size.height/2;
            chrtLbl.clipsToBounds=YES;
            chrtLbl.text=singlogin.totalCart;
        }
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)vendornamelisting

{
    NSString *post = [NSString stringWithFormat:@"userid=%@",singlogin.userid];
    
    NSLog(@"get data=%@",post);
    NSURL *url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/favvendorlist.php"];
    NSLog(@"PostData--%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    // NSLog(@"GetData--%@",data);
    NSMutableDictionary *dic = [data JSONValue];
    
    NSLog(@"GetDatadictt--%@",dic);
    
    if(![[dic objectForKey:@"Vendor list"] isEqual:@"No vendor available"])
    {
        arra_1 = [[NSMutableArray alloc]initWithArray:[dic valueForKey:@"list"]];
    }
    else
    {
        arra_1=[[NSMutableArray alloc]init];
    }
    
    
    if([arra_1 count]==0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You don’t have any favorite shops" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        [vendorTabl setHidden:YES];
    }
    else
    {
        [vendorTabl setHidden:NO];
        [vendorTabl reloadData];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arra_1 count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    favoritesCellAR *cell = [tableView dequeueReusableCellWithIdentifier:@"favCell1"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"favoritesCellAR" bundle:nil] forCellReuseIdentifier:@"favCell1"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"favCell1"];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(favoritesCellAR *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSString *sectional = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    cell.vendrLbl.text =[NSString stringWithFormat:@"%@",sectional];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:Image2.imageURL];
    cell.vendorImg.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arra_1 objectAtIndex:indexPath.row]objectForKey:@"vendorimage"]]];
    cell.vendorImg.layer.masksToBounds = YES;
    cell.vendorImg.layer.cornerRadius = 20.0;
    cell.vendorImg.layer.opaque = NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    singlogin.fUserId = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramid"];
    singlogin.venInstaName = [[arra_1 objectAtIndex:indexPath.row]valueForKey:@"instagramaccount"];
    singlogin.SlctVenId=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorid"];
    singlogin.venName=[[arra_1 objectAtIndex:indexPath.row]valueForKey:@"vendorname"];
    
    [self insta];
    
    [self performSelector:@selector(vendorlist)withObject:Nil afterDelay:0.5f];
    [indicator startAnimating];
}


-(void)vendorlist

{
    Vendor_DetaillistAR *vInstPics = [[Vendor_DetaillistAR alloc]initWithNibName:@"Vendor_DetaillistAR" bundle:NULL];
    [self.navigationController pushViewController:vInstPics animated:NO];
    [indicator stopAnimating];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 50;
}


-(void)insta
{
    if ([accesToken length] > 0 )
        
    {
        NSLog(@"followinstaid-- %@",singlogin.fUserId);
        
        NSString* userInfoUrl = [NSString stringWithFormat:@"%@/v1/users/%@/media/recent/?access_token=%@", kInstagramAPIBaseURL,singlogin.fUserId,accesToken];
        
        NSURL * url=[NSURL URLWithString:userInfoUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        NSString *data=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        //  NSLog(@"GetData--%@",data);
        
        NSDictionary *value1=[data JSONValue];
        NSMutableArray *pedagori = [value1 objectForKey:@"pagination"];
        NSLog(@"dadadad %@",pedagori);
        NSString *pedagori1 = [pedagori valueForKey:@"next_url"];
        NSLog(@"dadadad %@",pedagori1);
        singlogin.pedagori = pedagori1;
        NSMutableArray *userDict1 = [value1 objectForKey:@"data"];
        NSLog(@"dadadad %@",userDict1);
        NSMutableArray *usef = [userDict1 valueForKeyPath:@"user"];
        
        NSMutableArray *namef = [usef valueForKeyPath:@"username"];
        
        NSMutableArray *imgId = [userDict1 valueForKeyPath:@"id"];
        NSMutableArray *imglinksbrowsr = [userDict1 valueForKeyPath:@"link"];
        NSLog(@"image links %@",imgId);
        NSMutableArray *img = [userDict1 valueForKeyPath:@"images"];
        NSLog(@"image links %@",img);
        NSMutableArray *urlvalue = [img valueForKeyPath:@"standard_resolution"];
        NSLog(@"hmmm %@",urlvalue);
        NSMutableArray *righturl = [urlvalue valueForKey:@"url"];
        NSLog(@"yooooo %@",righturl);
        
        newdatalist = [[NSMutableArray alloc]init];
        for (int i=0; i< righturl.count; i++)
        {
            dynamicDict=[[NSMutableDictionary alloc]init];
            
            dynamicDict = @{ @"prodimage":[righturl objectAtIndex:i], @"id":@"", @"media_id":[imgId objectAtIndex:i], @"prodname":@"", @"about":@"", @"requirement":@"", @"quantity":@"", @"unitprice":@"", @"deliveryprice":@"", @"inventory":@"", @"subcatname":@"", @"catname":@"", @"vendorname":[namef objectAtIndex:i], @"likepic":@"", @"linkpicbrw":[imglinksbrowsr objectAtIndex:i], @"subcatid":@"", @"catid":@"", @"vendorid":@"", @"adminid":@"", @"instagramid":@""};
            
            NSLog(@"Dynamic ---- %@",dynamicDict);
            
            [newdatalist addObject:dynamicDict];
        }
    }
    //NSLog(@"Dynamic ---- %@",dynamicDict);
    NSLog(@"product list -------- %@",newdatalist);
    NSLog(@"Image Links Counts----- %lu",(unsigned long)newdatalist.count);
    singlogin.newarayList1 = newdatalist;
}


-(void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"View Disapear");
    [indicator stopAnimating];
    [indicator setHidesWhenStopped:YES];
}


-(void)accessTokenList
{
    
    NSURL * url=[NSURL URLWithString:@"http://vetrinaapp.com/vetrina2/tokenlist.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        //If data were received
        if (data) {
            //Convert to string
            NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSLog(@"GetData--%@",result);
            NSDictionary *eventarray=[result JSONValue];
            NSLog(@"GetDatadictt--%@",eventarray);
            
            if(![[eventarray objectForKey:@"list"] isEqual:@"No subcategory available"])
            {
                array_5=[[NSMutableArray alloc]initWithArray:[eventarray  valueForKey:@"list"]];
            }
            else
            {
                array_5=[[NSMutableArray alloc]init];
                
            }
            if([array_5 count]==0)
            {
                
            }
            else
            {
                accesToken = [[array_5 objectAtIndex:0]valueForKey:@"accesstoken"];
            }
            
        }
        //No data received
        else {
            NSString *errorText;
            //Specific error
            if (error)
                errorText = [error localizedDescription];
            //Generic error
            else
                errorText = @"An error occurred when downloading the list of issues. Please check that you are connected to the Internet.";
            
            //Show error
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            //Hide activity indicator
            //[self clearIssuesAccessoryView];
        }
    }];
    
    
    
    NSLog(@"Done");
}


- (IBAction)back:(id)sender

{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)chart:(id)sender

{
    [indicator setHidden:NO];
    [indicator startAnimating];
    [self performSelector:@selector(nextpage) withObject:nil afterDelay:0.5f];
}


-(void)nextpage
{
    CartProductAR *cart=[[CartProductAR alloc]initWithNibName:@"CartProductAR" bundle:nil];
    [self.navigationController pushViewController:cart animated:NO];

    [indicator setHidden:YES];
    [indicator stopAnimating];

}

- (IBAction)search:(id)sender

{
  
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(profile) withObject:nil afterDelay:0.0f];
}


-(void)profile
{
    User_ProfileAR *srch = [[User_ProfileAR alloc]initWithNibName:@"User_ProfileAR" bundle:nil];
    [self.navigationController pushViewController:srch animated:NO];
    [ProgressHUD showSuccess:@""];
}



- (IBAction)importFav:(id)sender

{
    User_Login_InstaAR *import = [[User_Login_InstaAR alloc]initWithNibName:@"User_Login_InstaAR" bundle:nil];
    [self.navigationController pushViewController:import animated:NO];
}


-(void)viewWillAppear:(BOOL)animated

{
    if (![appear isEqualToString:@"yes"])
    {
        [self vendornamelisting];
       // [viewImprt setHidden:YES];
        [self cartTotalView];
    }
    appear=@"";
    [self.navigationController setNavigationBarHidden:YES];
}


- (IBAction)home:(id)sender
{
    
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(homeBtn) withObject:nil afterDelay:0.0f];
}


-(void)homeBtn
{
    Home_VetrinaARViewController *srch = [[Home_VetrinaARViewController alloc]initWithNibName:@"Home_VetrinaARViewController" bundle:nil];
    [self.navigationController pushViewController:srch animated:NO];
    [ProgressHUD showSuccess:@""];
}

- (IBAction)chat:(id)sender
{
    [ProgressHUD show:@"Loading..." Interaction:NO];
    [self performSelector:@selector(chatBtn) withObject:nil afterDelay:0.0f];
}


-(void)chatBtn
{
    RecentView *srch = [[RecentView alloc]initWithNibName:@"RecentView" bundle:nil];
    [self.navigationController pushViewController:srch animated:NO];
    [ProgressHUD showSuccess:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
