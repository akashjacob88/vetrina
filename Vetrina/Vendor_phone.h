//
//  Vendor_phone.h
//  Vetrina
//
//  Created by Amit Garg on 10/27/15.
//  Copyright © 2015 Amit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "BSKeyboardControls.h"

@interface Vendor_phone : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>

{
    Singleton *singling;
    IBOutlet UIView *topBar;
    IBOutlet UIView *view2Shadow;
    IBOutlet UIView *view1Shadow;
    IBOutlet UILabel *instaNameLbl;
    IBOutlet UITextField *countrycode;
    IBOutlet UITextField *phoneText;
    BSKeyboardControls *keyboardControls;
    
    NSMutableArray *dataArray;

}

@end
