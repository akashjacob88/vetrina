//
//  chat_New.m
//  Vetrina
//
//  Created by Amit Garg on 2/19/16.
//  Copyright © 2016 Amit Garg. All rights reserved.
//

#import "chat_New.h"
#import <Quickblox/Quickblox.h>
#import "ProgressHUD.h"

@interface chat_New ()
{
    
    NSString *groupId;
    QBChatDialog *chatDialog;
    
}
@end

@implementation chat_New

@synthesize All_messages;

//    CGSize avatarSize;
//    CGSize containerSize;
//    UIEdgeInsets containerInsets;
//    CGFloat topLabelHeight;
//    CGFloat bottomLabelHeight;
//    CGSize staticContainerSize;
//
//typedef struct QMChatLayoutModel QMChatCellLayoutModel;


- (id)initWith:(NSString *)groupId_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super init];
    groupId = groupId_;
    return self;
}

-(void)chat22{

    QBChatDialog *test_dialog;
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID: test_dialog.ID	 type:QBChatDialogTypePrivate];
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.vendor_objectid] integerValue ];
    
    chatDialog.occupantIDs = @[@(o) ];
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        groupId=createdDialog.ID;
        
        
    } errorBlock:^(QBResponse *response) {
        
    }];
    
}
-(void)chat21{
    
    chatDialog = [[QBChatDialog alloc]initWithDialogID:groupId type:QBChatDialogTypePrivate];
    
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.chat_user] integerValue ];
    
    chatDialog.occupantIDs = @[@(o) ];
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        NSLog(@"No Error: %@", response);
        // groupId=createdDialog.ID;
        
       
    } errorBlock:^(QBResponse *response) {
        
    }];
    
}

- (void)loadMessages
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    QBResponsePage *resPage = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    [QBRequest messagesWithDialogID:groupId extendedRequest:nil forPage:resPage successBlock:^(QBResponse *response, NSArray *message, QBResponsePage *responcePage) {
        
        
        if (message.count>0) {
            
            
            for (int i =0; i<message.count; i++) {
                
                QBChatMessage *mes=message[i];
                
                [self insertMessageToTheBottomAnimated:mes];
                
            }
            
            
        }
        

    } errorBlock:^(QBResponse *response) {
        NSLog(@"error: %@", response.error);
    }];
    
}

- (void)loadMessagesNew:(QBChatMessage *)message
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    //	JSQMessage *message_last = [messages lastObject];
    
    
    //    QBResponsePage *resPage = [QBResponsePage responsePageWithLimit:100 skip:0];
    //
    //    [QBRequest messagesWithDialogID:groupId extendedRequest:nil forPage:resPage successBlock:^(QBResponse *response, NSArray *message, QBResponsePage *responcePage) {
    //
    //
    //        if (message.count>0) {
    
    [self insertMessageToTheBottomAnimated:message];
    
    //        }
    //
    //
    //
    //
    //
    //
    //    } errorBlock:^(QBResponse *response) {
    //        NSLog(@"error: %@", response.error);
    //    }];
    
 
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
        
    }];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    self.view.backgroundColor=[UIColor colorWithRed:227/255.f green:227/255.f blue:227/255.f alpha:1.0f];

    [self.navigationController setNavigationBarHidden:YES];
    
    self.navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:237/255.f green:92/255.f blue:118/255.f alpha:1.0];
    self.navigationController.title=@"Chat";
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    singlogin = [Singleton instance];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    UIView *nav_view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    UIButton *back_btn=[[UIButton alloc]initWithFrame:CGRectMake(5, 10, 50, 50)];
    [nav_view1 addSubview:back_btn];
    [back_btn setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [back_btn addTarget:self action:@selector(back_action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nav_view1];
    nav_view1.backgroundColor=[UIColor colorWithRed:238/255.f green:63/255.f blue:99/255.f alpha:1.0];
    
    UILabel *tittle=[[UILabel alloc]initWithFrame:CGRectMake(70, 10, 200, 46)];
  //  tittle.text=@"Chat";
    tittle.textColor=[UIColor whiteColor];
    tittle.textAlignment=NSTextAlignmentLeft;
    tittle.font=[UIFont fontWithName:@"OpenSans-Bold" size:20];
    tittle.text= singlogin.namechat;
    [nav_view1 addSubview:tittle];
    
    
    
    [[QBChat instance] addDelegate:self];
    
    
    All_messages=[[NSMutableArray alloc]init];
    
    NSInteger o = [ [ NSString stringWithFormat: @"%@",singlogin.user_id] integerValue ];
    
    self.senderID =o;
    
    
    NSLog(@"%lu",(unsigned long)self.senderID);
    
    //(NSUInteger)[NSString stringWithFormat:@"%@",singlogin.usrObjctId];
    self.senderDisplayName = singlogin.userNameStr;
    [self loadMessages];
    
    
    if (singlogin.vendor_objectid !=nil) {
       
        [self chat22];
        
    }else{
        
        [self chat21];
        
    }
    
    
    
}

-(void )back_action{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark QBChatDelegate

-(void)chatDidConnect {
    
}
-(void)chatDidAccidentallyDisconnect{
    
}
-(void)chatDidReconnect{
    
}
-(void)chatDidReceiveMessage:(QBChatMessage *)message{
    
    //NSLog(@"Received Message %@",message);
    
  //  [self performSelector:@selector(loadMessagesNew:) withObject:message afterDelay:5];
    
    [self loadMessagesNew:message];
    
}
-(void)chatDidNotSendMessage:(QBChatMessage *)message error:(NSError *)error{
    
    
}
-(void)chatDidDeliverMessageWithID:(NSString *)messageID {
    
    
}


//-(NSString *)getUTCFormateDate:(NSDate *)localDate
//{
//   
//    return dateString;
//}

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSUInteger)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    // Add sending message - for example:
    QBChatMessage *message = [QBChatMessage message];
    message.text = text;
    message.senderID = senderId;
    
     NSDate *currentDateInLocal = [NSDate date];
   
//    NSDate* sourceDate = [NSDate date];
//    
//    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
//    
//    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
//    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
//    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
//    
//    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
//    
//    
//   
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    NSString *currentLocalDateAsStr = [dateFormatter stringFromDate:currentDateInLocal];
//    
//        NSDate *currentDate = [[NSDate alloc] init];
//        double secsUtc1970 = [[NSDate date]timeIntervalSince1970];
//        NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
//        // or specifc Timezone: with name
//    
//        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//        [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *localDateString = [dateFormatter1 stringFromDate:currentDate];
    
    
    
    message.dateSent=currentDateInLocal;
    message.senderNick=@"Ac";
    
    QBChatAttachment *attacment = [[QBChatAttachment alloc] init];
    message.attachments = @[attacment];
    
    
    [All_messages addObject:message];
    
    // Save message to your cache/memory storage.
    // Send message using Quickblox SDK
    
    
    [self insertMessageToTheBottomAnimated:message];
    //
    //    [self finishSendingMessageAnimated:YES];
    //
    
    [[QBChat instance] addDelegate:self];
    
    
    
    
    //
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"save_to_history"] = @YES;
    [message setCustomParameters:params];
    //56c572dba28f9a0058000822
    
    
    [chatDialog sendMessage:message completionBlock:^(NSError * _Nullable error) {
        
        
        NSLog(@"Error: %@", error);
        //   [self loadMessages];
        
        
        //   [self insertMessageToTheBottomAnimated:message];
        
        [self finishSendingMessage];
        
        
        //        NSString *message = text;
        //        NSMutableDictionary *payload = [NSMutableDictionary dictionary];
        //        NSMutableDictionary *aps = [NSMutableDictionary dictionary];
        //        [aps setObject:@"default" forKey:QBMPushMessageSoundKey];
        //        [aps setObject:message forKey:QBMPushMessageAlertKey];
        //        [payload setObject:aps forKey:QBMPushMessageApsKey];
        //
        //
        //        QBMPushMessage *pushMessage = [[QBMPushMessage alloc] initWithPayload:payload];
        //
        //        // Send push to users with ids 292,300,1395
        //        [QBRequest sendPush:pushMessage toUsers:singlogin.vendor_objectid successBlock:^(QBResponse *response, QBMEvent *event) {
        //            // Successful response with event
        //
        //
        //
        //        } errorBlock:^(QBError *error) {
        //
        //
        //            [ProgressHUD showError:@"Network error."];
        //            // Handle error
        //        }];
        
        
        
        
        
    }];
    
    
    
    
    
    
    
    
    
}




- (Class)viewClassForItem:(QBChatMessage *)item {
    // Cell class for message
    
    if (item==nil) {
        return nil;
    }
    
    
    NSLog(@"%@",item.ID);
    NSLog(@"%lu", self.senderID);
    
    
    if (item.senderID != self.senderID) {
        
        return [QMChatIncomingCell class];
    } else {
        
        return [QMChatOutgoingCell class];
    }
    
    return nil;
}




- (CGSize)collectionView:(QMChatCollectionView *)collectionView dynamicSizeAtIndexPath:(NSIndexPath *)indexPath maxWidth:(CGFloat)maxWidth {
    
    QBChatMessage *item = [self messageForIndexPath:indexPath];
    
    
    
    NSAttributedString *attributedString = [self attributedStringForItem:item];
    
    CGSize size = [TTTAttributedLabel sizeThatFitsAttributedString:attributedString
                                                   withConstraints:CGSizeMake(maxWidth, MAXFLOAT)
                                            limitedToNumberOfLines:0];
    return size;
}

- (CGFloat)collectionView:(QMChatCollectionView *)collectionView minWidthAtIndexPath:(NSIndexPath *)indexPath {
    QBChatMessage *item = [self messageForIndexPath:indexPath];
    
    NSAttributedString *attributedString =
    [item senderID] == self.senderID ?  [self bottomLabelAttributedStringForItem:item] : [self topLabelAttributedStringForItem:item];
    
    CGSize size = [TTTAttributedLabel sizeThatFitsAttributedString:attributedString
                                                   withConstraints:CGSizeMake(1000, 10000)
                                            limitedToNumberOfLines:1];
    
    return size.width;
}



//--------Top, bottom and text labels.------------------------

- (NSAttributedString *)attributedStringForItem:(QBChatMessage *)messageItem {
    UIColor *textColor = [messageItem senderID] == self.senderID ? [UIColor whiteColor] : [UIColor colorWithWhite:0.290 alpha:1.000];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
    NSDictionary *attributes = @{ NSForegroundColorAttributeName:textColor, NSFontAttributeName:font};
    
    if (messageItem==nil) {
        return nil;
    }
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:messageItem.text attributes:attributes];
    
    return attrStr;
}

- (NSAttributedString *)topLabelAttributedStringForItem:(QBChatMessage *)messageItem {
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:14];
    
    if ([messageItem senderID] == self.senderID) {
        return nil;
    }
    if (messageItem==nil) {
        return nil;
    }
    NSDictionary *attributes = @{ NSForegroundColorAttributeName:[UIColor colorWithRed:0.184 green:0.467 blue:0.733 alpha:1.000], NSFontAttributeName:font};
    
    
    if (messageItem.senderNick==nil) {
        return nil;
    }
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:messageItem.senderNick attributes:attributes];
    
    return attrStr;
}

- (NSAttributedString *)bottomLabelAttributedStringForItem:(QBChatMessage *)messageItem {
    UIColor *textColor = [messageItem senderID] == self.senderID ? [UIColor colorWithWhite:1.000 alpha:0.510] : [UIColor colorWithWhite:0.000 alpha:0.490];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:12];
    
    if (messageItem==nil) {
        return nil;
    }
    
    
    if (messageItem.dateSent==nil) {
        return nil;
    }
    
    NSDictionary *attributes = @{ NSForegroundColorAttributeName:textColor, NSFontAttributeName:font};
  
    
    
//    NSString *dateStr = [messageItem.dateSent description];
  
     NSString *dateStr = @"";
    
//    NSDate *date = messageItem.dateSent;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle]; // Set date and time styles
//  //  [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//       NSString *dateString = [dateFormatter stringFromDate:date];
  
    
//    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
//    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
//    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
//    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
//    [dateFormatters setDoesRelativeDateFormatting:YES];
//    [dateFormatters setTimeZone:[NSTimeZone timeZoneWithName:<#(nonnull NSString *)#>]];
//    
//    
//    dateStr = [dateFormatters stringFromDate:messageItem.dateSent];
    NSLog(@"DateString : %@", dateStr);
    
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:dateStr attributes:attributes];
    
    
    [[attrStr mutableString] replaceOccurrencesOfString:@"+0000" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, attrStr.string.length)];
    
       NSLog(@"%@-----------Date is here ",attrStr);
    return attrStr;
}




//-----------------Modifying collection chat cell attributes without changing constraints:---------------------

//struct QMChatLayoutModel {
//
//    CGSize avatarSize;
//    CGSize containerSize;
//    UIEdgeInsets containerInsets;
//    CGFloat topLabelHeight;
//    CGFloat bottomLabelHeight;
//    CGSize staticContainerSize;
//};
//
//typedef struct QMChatLayoutModel QMChatCellLayoutModel;



//------------------------You can modify this attributes in this method:----------------------//

- (QMChatCellLayoutModel)collectionView:(QMChatCollectionView *)collectionView layoutModelAtIndexPath:(NSIndexPath *)indexPath {
    QMChatCellLayoutModel layoutModel = [super collectionView:collectionView layoutModelAtIndexPath:indexPath];
    // update attributes here
    
    return layoutModel;
}
- (NSTimeInterval)timeIntervalBetweenSections {
    return 300.0f;
}

- (CGFloat)heightForSectionHeader {
    return 40.0f;
}


@end






